<?php

namespace NextFormTests\Form\Element;

use Abivia\NextForm\Form\Element\StaticElement;

class StaticElementTest extends \PHPUnit\Framework\TestCase {

	public function testFormStaticElementInstantiation() {
        $obj = new StaticElement();
		$this->assertInstanceOf('\Abivia\NextForm\Form\Element\StaticElement', $obj);
	}

    /**
     * Check that a skeleton element gets set up correctly
     */
	public function testConfiguration() {
        $config = json_decode('
            {
                "type": "static"
            }'
        );
        $this->assertTrue(false !== $config, 'JSON error!');
        $obj = new StaticElement();
        $this->assertTrue($obj->configure($config));
		$this->assertEquals('static', $obj->getType());
		$this->assertEquals('', $obj->getName());
		$this->assertEquals(true, $obj->getDisplay());
    }

	public function testConfigurationBad() {
        $config = json_decode('"somestring"');
        $this->assertTrue(false !== $config, 'JSON error!');
        $obj = new StaticElement();
        $this->assertFalse($obj->configure($config));
    }

    /**
     * Check a static section with roles
     */
	public function testConfigurationRoles() {
        $config = json_decode('
            {
                "type": "static",
                "roles": {"*": "hide", "admin": "write"}
            }'
        );
        $this->assertTrue(false !== $config, 'JSON error!');
        $obj = new StaticElement();
        $loaded = $obj->configure($config);
        $this->assertTrue($loaded);
        $this->assertEquals('hide', $obj->getAccess('foo'));
        $this->assertEquals('write', $obj->getAccess('admin'));
    }

    /**
     * Check a static section with roles
     */
	public function testConfigurationCompactRoles() {
        $config = json_decode('
            {
                "type": "static",
                "roles": "*:hide|admin:write"
            }'
        );
        $this->assertTrue(false !== $config, 'JSON error!');
        $obj = new StaticElement();
        $loaded = $obj->configure($config);
        $this->assertTrue($loaded);
        $this->assertEquals('hide', $obj->getAccess('foo'));
        $this->assertEquals('write', $obj->getAccess('admin'));
    }

    /**
     * Check a static section with invalid roles
     */
	public function testConfigurationCompactRolesBad() {
        $config = json_decode('
            {
                "type": "static",
                "roles": "*:hide|admin:invalid"
            }'
        );
        $this->assertTrue(false !== $config, 'JSON error!');
        $obj = new StaticElement();
        $loaded = $obj->configure($config);
        $this->assertFalse($loaded);
    }

    /**
     * Check a static section with invalid roles
     */
	public function testHtml() {
        $obj = new StaticElement();
        $this->assertFalse($obj->getHtml());
        $obj->html(true);
        $this->assertTrue($obj->getHtml());
    }

}
