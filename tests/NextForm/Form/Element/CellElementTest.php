<?php

namespace NextFormTests\Form\Element;

use Abivia\NextForm\Form\DefinitionException;
use Abivia\NextForm\Form\Element\CellElement;
use Abivia\NextForm;

/**
 * @covers \Abivia\NextForm\Form\Element\CellElement
 */
class CellElementTest extends \PHPUnit\Framework\TestCase
{
    public $commonJson;

    public function setUp(): void
    {
        $this->commonJson = '{
                "type": "cell",
                "elements": [
                    {
                        "type": "field",
                        "name": "p0",
                        "object": "some-identifier"
                    },
                    {
                        "type": "html",
                        "name": "p1",
                        "value": "<em>stuff</em>"
                    },
                    {
                        "type": "static",
                        "name": "p2",
                        "value": "your text here"
                    }
                ]
            }';
    }

	public function testInstantiation() {
        $obj = new CellElement();
		$this->assertInstanceOf('\Abivia\NextForm\Form\Element\CellElement', $obj);
	}

    /**
     * Check that a skeleton element gets set up correctly
     */
	public function testConfiguration() {
        $config = json_decode('
            {
                "type": "cell",
                "elements": []
            }'
        );
        $this->assertTrue(false !== $config, 'JSON error!');
        $obj = new CellElement();
        $this->assertTrue($obj->configure($config));
		$this->assertEquals('cell', $obj->getType());
		$this->assertEquals('', $obj->getName());
		$this->assertEquals(true, $obj->getDisplay());
    }

    /**
     * Check that a cell can contain all the valid things
     */
	public function testNestedValid() {
        $config = json_decode($this->commonJson);
        $this->assertTrue(false !== $config, 'JSON error!');
        $obj = new CellElement();
        $this->assertTrue($obj->configure($config));
		$this->assertEquals('cell', $obj->getType());
        $elements = $obj->getElements();
		$this->assertEquals(3, count($elements));
		$this->assertInstanceOf('\Abivia\NextForm\Form\Element\FieldElement', $elements[0]);
		$this->assertInstanceOf('\Abivia\NextForm\Form\Element\HtmlElement', $elements[1]);
		$this->assertInstanceOf('\Abivia\NextForm\Form\Element\StaticElement', $elements[2]);
    }

    /**
     * Check that a cell can't contain a cell
     */
	public function testNestedCell() {
        $config = json_decode('
            {
                "type": "cell",
                "elements": [
                    {
                        "type": "cell",
                        "elements": []
                    }
                ]
            }'
        );
        $this->assertTrue(false !== $config, 'JSON error!');
        $obj = new CellElement();
        $this->expectException(\RuntimeException::class);
        $this->assertFalse($obj->configure($config));
    }

    /**
     * Check that a cell can't contain a section
     */
	public function testNestedSection() {
        $config = json_decode('
            {
                "type": "cell",
                "elements": [
                    {
                        "type": "section",
                        "elements": []
                    }
                ]
            }'
        );
        $this->assertTrue(false !== $config, 'JSON error!');
        $obj = new CellElement();
        $this->expectException(\RuntimeException::class);
        $this->assertFalse($obj->configure($config));
    }

    /**
     * Test duplicate name on form load
     */
    public function testDuplicateName() {
        $config = json_decode($this->commonJson);
        // Create a duplicate element
        $config->elements[] = $config->elements[1];
        $obj = new CellElement();
        $this->expectException(DefinitionException::class);
        $this->assertFalse($obj->configure($config));
    }

    /**
     * Test element access by name
     */
    public function testNamedElementExists() {
        $config = json_decode($this->commonJson);
        $obj = new CellElement();
        $obj->configure($config);

        $this->assertTrue($obj->elementExists('p1'));
        $this->assertFalse($obj->elementExists('not a legal name'));
        $this->assertTrue(null === $obj->elementExists(''));
    }

    /**
     * Test element access by name/Element
     */
    public function testGetElementIndex() {
        $config = json_decode($this->commonJson);
        $obj = new CellElement();
        $obj->configure($config);

        $this->assertTrue(null === $obj->findElementIndex('not ok'));
        $p1Index = $obj->findElementIndex('p1');
        $this->assertEquals(1, $p1Index);
        $p1Element = $obj[1];
        $p1Index = $obj->findElementIndex($p1Element);
        $this->assertEquals(1, $p1Index);

        $newElement = new \Abivia\NextForm\Form\Element\StaticElement();
        $this->assertTrue(null === $obj->findElementIndex($newElement));
    }

    /**
     * Test array iteration
     */
    public function testIteration() {
        $config = json_decode($this->commonJson);
        $obj = new CellElement();
        $obj->configure($config);

        foreach ($obj as $key => $element) {
            $this->assertTrue($element->getName() === 'p' . $key);
        }
    }

    /**
     * Test the Countable interface
     */
    public function testCountable() {
        $config = json_decode($this->commonJson);
        $obj = new CellElement();
        $obj->configure($config);

        $this->assertEquals(3, count($obj));
    }

    /**
     * Test the ArrayAccess interface
     */
    public function testArrayAccess() {
        $config = json_decode($this->commonJson);
        $obj = new CellElement();
        $obj->configure($config);

        $this->assertTrue(isset($obj[2]));
        $this->assertFalse(isset($obj[9993]));

        $count = $obj->count();
        $element = $obj[2];
        $this->assertEquals('p2', $element->getName());
        $element = $element->copy();
        $element->name('p3');
        $obj[] = $element;
        $this->assertEquals('p3', $obj[$count]->getName());

        unset($obj[2]);
        $this->assertTrue(null === $obj->findElementIndex('p2'));

        $this->assertTrue(null === $obj[$count + 1]);
    }

    /**
     * Test the append element function - trap duplicate names
     */
    public function testAppendNoDup()
    {
        $config = json_decode($this->commonJson);
        $obj = new CellElement();
        $obj->configure($config);

        $element = $obj[1]->copy();

        $this->expectException('\RuntimeException');
        $obj->append($element);
    }

    /**
     * Test the append element function
     */
    public function testAppend()
    {
        $config = json_decode($this->commonJson);
        $obj = new CellElement();
        $obj->configure($config);

        $count = $obj->count();
        $element = $obj[2]->copy();
        $element->name('etPhoneHome');

        $obj->append($element);
        $this->assertEquals($count, $obj->findElementIndex($element));
    }

    /**
     * Test the append element in place function
     */
    public function testAppendInPlace()
    {
        $config = json_decode($this->commonJson);
        $obj = new CellElement();
        $obj->configure($config);

        $element = $obj[2]->copy();
        $element->name('etPhoneHome');

        $obj->append($element, 'p1');
        $this->assertEquals(2, $obj->findElementIndex($element));
    }

    /**
     * Test the append element in place function with an array
     */
    public function testAppendInPlaceArray()
    {
        $config = json_decode($this->commonJson);
        $obj = new CellElement();
        $obj->configure($config);

        $elements = [$obj[2]->copy(), $obj[2]->copy()];
        $elements[0]->name('etPhoneHome');
        $elements[1]->name('etPhoneWork');

        $obj->append($elements, 'p1');
        $this->assertEquals(2, $obj->findElementIndex($elements[0]));
        $this->assertEquals(3, $obj->findElementIndex($elements[1]));
    }

    /**
     * Test the prepend element function
     */
    public function testPrepend()
    {
        $config = json_decode($this->commonJson);
        $obj = new CellElement();
        $obj->configure($config);

        $element = $obj[2]->copy();
        $element->name('etPhoneHome');

        $obj->prepend($element);
        $this->assertEquals(0, $obj->findElementIndex($element));
    }

    /**
     * Test the prepend element in place function
     */
    public function testPrependInPlace()
    {
        $config = json_decode($this->commonJson);
        $obj = new CellElement();
        $obj->configure($config);

        $element = $obj[0]->copy();
        $element->name('etPhoneHome');

        $obj->prepend($element, 'p2');
        $this->assertEquals(2, $obj->findElementIndex($element));
    }

    /**
     * Test the prepend element in place function with an array
     */
    public function testPrependInPlaceArray()
    {
        $config = json_decode($this->commonJson);
        $obj = new CellElement();
        $obj->configure($config);

        $elements = [$obj[2]->copy(), $obj[2]->copy()];
        $elements[0]->name('etPhoneHome');
        $elements[1]->name('etPhoneWork');

        $obj->prepend($elements, 'p1');
        $this->assertEquals(1, $obj->findElementIndex($elements[0]));
        $this->assertEquals(2, $obj->findElementIndex($elements[1]));
    }

}
