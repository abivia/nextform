<?php

namespace NextFormTests\Form\Element;

use Abivia\NextForm\Form\DefinitionException;
use Abivia\NextForm\Form\Element\CellElement;
use Abivia\NextForm\Form\Element\HtmlElement;
use Abivia\NextForm\Form\Element\FieldElement;
use Abivia\NextForm\Form\Element\SectionElement;
use Abivia\NextForm\Form\Element\StaticElement;

class SectionElementTest extends \PHPUnit\Framework\TestCase
{
    public $goodConfig;

    public function setUp(): void
    {
        $this->goodConfig = json_decode('
            {
                "type": "section",
                "elements": [
                    {
                        "type": "field",
                        "object": "some-identifier"
                    },
                    {
                        "type": "html",
                        "value": "<em>stuff</em>"
                    },
                    {
                        "type": "static",
                        "value": "your text here"
                    },
                    {
                        "name": "subcell",
                        "type": "cell",
                        "elements": ["child1", "child2", "child3"]
                    }
                ]
            }'
        );

    }

	public function testInstantiation()
    {
        $obj = new SectionElement();
		$this->assertInstanceOf(SectionElement::class, $obj);
	}

    /**
     * Check that a skeleton element gets set up correctly
     */
	public function testConfiguration()
    {
        $config = json_decode('
            {
                "type": "section",
                "elements": []
            }'
        );
        $obj = new SectionElement();
        $this->assertTrue($obj->configure($config));
		$this->assertEquals('section', $obj->getType());
		$this->assertEquals('', $obj->getName());
		$this->assertEquals(true, $obj->getDisplay());
    }

    /**
     * Check that a section can contain all the valid things
     */
	public function testNestedValid()
    {
        $obj = new SectionElement();
        $this->assertTrue($obj->configure($this->goodConfig));
		$this->assertEquals('section', $obj->getType());

        $this->assertEquals(4, count($obj));
		$this->assertInstanceOf(FieldElement::class, $obj[0]);
		$this->assertInstanceOf(HtmlElement::class, $obj[1]);
		$this->assertInstanceOf(StaticElement::class, $obj[2]);
		$this->assertInstanceOf(CellElement::class, $obj[3]);
    }

    /**
     * Check that a section can't contain a section
     */
	public function testNestedSectionFails()
    {
        $config = json_decode('
            {
                "type": "section",
                "elements": [
                    {
                        "type": "section",
                        "elements": []
                    }
                ]
            }'
        );
        $this->assertTrue(false !== $config, 'JSON error!');
        $obj = new SectionElement();
        $this->expectException(DefinitionException::class);
        $this->assertFalse($obj->configure($config));
    }

    public function testGetSegment()
    {
        $obj = new SectionElement();
        $segment = $obj->getSegment();
        $this->assertTrue($segment === null);
    }

    /**
     * Test display propagation.
     */
	public function testDisplay()
    {
        $obj = new SectionElement();
        $this->assertTrue($obj->configure($this->goodConfig));

        // Validate initial state
        $this->assertTrue($obj[3]->getDisplay());
        $this->assertTrue($obj[3][0]->getDisplay());

        // Turn off for the cell, with propagation
        $obj[3]->display(false);
        $this->assertFalse($obj[3]->getDisplay());
        $this->assertFalse($obj[3][0]->getDisplay());

        // Turn the cell on, no propagation
        $obj[3]->display(true, false);
        $this->assertTrue($obj[3]->getDisplay());
        $this->assertFalse($obj[3][0]->getDisplay());

        // Turn everything on, with propagation
        $obj->display();
        $this->assertTrue($obj[3]->getDisplay());
        $this->assertTrue($obj[3][0]->getDisplay());
    }

    /**
     * Test enable propagation.
     */
	public function testEnable()
    {
        $obj = new SectionElement();
        $this->assertTrue($obj->configure($this->goodConfig));

        // Validate initial state
        $this->assertTrue($obj[3]->getEnabled());
        $this->assertTrue($obj[3][0]->getEnabled());

        // Turn off for the cell, with propagation
        $obj[3]->enable(false);
        $this->assertFalse($obj[3]->getEnabled());
        $this->assertFalse($obj[3][0]->getEnabled());

        // Turn the cell on, no propagation
        $obj[3]->enable(true, false);
        $this->assertTrue($obj[3]->getEnabled());
        $this->assertFalse($obj[3][0]->getEnabled());

        // Turn everything on, with propagation
        $obj->enable();
        $this->assertTrue($obj[3]->getEnabled());
        $this->assertTrue($obj[3][0]->getEnabled());
    }

    /**
     * Test read-only propagation.
     */
	public function testReadonly()
    {
        $obj = new SectionElement();
        $this->assertTrue($obj->configure($this->goodConfig));

        // Validate initial state
        $this->AssertFalse($obj[3]->getReadonly());
        $this->AssertFalse($obj[3][0]->getReadonly());

        // Turn off for the cell, with propagation
        $obj[3]->readonly(false);
        $this->AssertFalse($obj[3]->getReadonly());
        $this->AssertFalse($obj[3][0]->getReadonly());

        // Turn the cell on, no propagation
        $obj[3]->readonly(true, false);
        $this->AssertTrue($obj[3]->getReadonly());
        $this->AssertFalse($obj[3][0]->getReadonly());

        // Turn everything on, with propagation
        $obj->readonly();
        $this->AssertTrue($obj[3]->getReadonly());
        $this->AssertTrue($obj[3][0]->getReadonly());
    }

    public function testElementArray()
    {
        $obj = new SectionElement();
        $elements = [new HtmlElement(), new StaticElement()];
        $obj->append($elements[0]);
        $this->assertEquals(1, count($obj));

        $obj->append($elements[1]);
        $this->assertEquals(2, count($obj));

        $this->assertEquals($elements, $obj->getElements());

        foreach ($obj as $key => $element) {
            $this->assertEquals($elements[$key], $element);
            $this->assertEquals($key, $obj->findElementIndex($element));
        }

        $fred = HtmlElement::build()->name('fred');
        $elements[] = $fred;
        $obj->append($fred);
        $this->assertEquals($elements, $obj->getElements());
        $this->assertEquals(2, $obj->findElementIndex($fred));

        // No appending an object of the same name
        try {
            $obj->append($fred);
            $result = false;
        } catch (DefinitionException $defnErr) {
            $result = true;
        }
        $this->assertTrue($result);

        // No replacing an object of the same name unless it is an overwrite
        try {
            $obj[1] = $fred;
            $result = false;
        } catch (DefinitionException $defnErr) {
            $result = true;
        }
        $this->assertTrue($result);

        $obj[2] = $fred;

        // No nested sections
        $badness = new SectionElement();
        try {
            $obj[1] = $badness;
            $result = false;
        } catch (DefinitionException $defnErr) {
            $result = true;
        }
        $this->assertTrue($result);

        unset($obj[0]);
        unset($elements[0]);
        $this->assertEquals($elements, $obj->getElements());

        $bob = HtmlElement::build()->name('bob');
        array_unshift($elements, $bob);
        $obj->prepend($bob);
        $this->assertEquals($elements, $obj->getElements());

    }

}
