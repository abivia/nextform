<?php

namespace NextFormTests\Form;

require_once __DIR__ . '/../../test-tools/JsonComparison.php';

use Abivia\NextForm\Form\Form;
use Abivia\NextForm\NextForm;
use NextFormTests\Test_Tools\JsonComparison;

/**
 * @covers Abivia\NextForm\Form\Form
 */
class FormTest extends \PHPUnit\Framework\TestCase {
    use JsonComparison;

    public function testInstantiation() {
        $obj = new Form();
		$this->assertInstanceOf('\Abivia\NextForm\Form\Form', $obj);
    }

	public function testConfiguration() {
        $config = json_decode(file_get_contents(dirname(__FILE__) . '/form.json'));
        $this->assertTrue(false !== $config, 'JSON error!');
        $obj = new Form();
        $populate = $obj->configure($config, ['strict' => true]);
        if ($populate) {
            $errors = '';
        } else {
            $errors = $obj->configureGetErrors();
            $errors = 'Form load:' . "\n" . implode("\n", $errors) . "\n";
        }
        $this->assertTrue($populate, $errors);
        $dump = print_r($obj, true);
        $dump = str_replace(" \n", "\n", $dump);
        file_put_contents(dirname(__FILE__) . '/form-dump-actual.txt', $dump);
        $this->assertEquals(
            sha1_file(dirname(__FILE__) . '/form-dump-expect.txt'),
            sha1($dump)
        );
        $this->assertTrue(true);
	}

    /**
     * Integration test for form read/write.
     */
    public function testLoad() {
        $jsonFile = __DIR__ . '/form.json';
        $obj = Form::fromFile($jsonFile);
        // Save the result as JSON so we can compare
        NextForm::$jsonCompact = true;
        $resultJson = json_encode($obj, JSON_PRETTY_PRINT);
        file_put_contents(__DIR__ . '/form-out.json', $resultJson);

        // Stock JSON to stdClass for comparison; reload the config
        $result = json_decode($resultJson);
        $config = json_decode(file_get_contents($jsonFile));
        $this->assertTrue($this->jsonCompare($config, $result));
    }

    /**
     * Test duplicate name on form load
     */
    public function testLoadDuplicateName() {
        $jsonFile = __DIR__ . '/form-duplicate-name.json';
        $this->expectException('\RuntimeException');
        Form::fromFile($jsonFile);
    }

    /**
     * Test element access by name
     */
    public function testNamedElementExists() {
        $jsonFile = __DIR__ . '/form.json';
        $obj = Form::fromFile($jsonFile);

        $this->assertTrue($obj->elementExists('completeHeader'));
        $this->assertFalse($obj->elementExists('not a legal name'));
        $this->assertTrue(null === $obj->elementExists(''));
    }

    /**
     * Test element retrieval by name
     */
    public function testGetElement() {
        $jsonFile = __DIR__ . '/form.json';
        $obj = Form::fromFile($jsonFile);

        $this->assertTrue(null === $obj->getElement('not ok'));
        $phone = $obj->getElement('phone');
        $this->assertEquals('phone', $phone->getName());

    }

    /**
     * Test element access by name/Element
     */
    public function testGetElementIndex() {
        $jsonFile = __DIR__ . '/form.json';
        $obj = Form::fromFile($jsonFile);

        $this->assertTrue(null === $obj->findElementIndex('not ok'));
        $phoneIndex = $obj->findElementIndex('phone');
        $this->assertEquals(3, $phoneIndex);
        $phoneElement = $obj[3];
        $phoneIndex = $obj->findElementIndex($phoneElement);
        $this->assertEquals(3, $phoneIndex);

        $newElement = new \Abivia\NextForm\Form\Element\StaticElement();
        $this->assertTrue(null === $obj->findElementIndex($newElement));
    }

    /**
     * Test array iteration
     */
    public function testIteration() {
        $jsonFile = __DIR__ . '/form.json';
        $obj = Form::fromFile($jsonFile);

        foreach ($obj as $key => $element) {
            if ($key === 0) {
                $this->assertTrue($element->getName() === 'intro');
            } elseif ($key === 1) {
                $this->assertTrue($element->getName() === 'completeHeader');
            } elseif ($key === 3) {
                $this->assertTrue($element->getName() === 'phone');
            } elseif ($key === 4) {
                $this->assertTrue($element->getName() === 'mysection');
            } else {
                $this->assertTrue($element->getName() === '');
            }

        }
    }

    /**
     * Test the Countable interface
     */
    public function testCountable() {
        $jsonFile = __DIR__ . '/form.json';
        $obj = Form::fromFile($jsonFile);

        $this->assertEquals(6, count($obj));
    }

    /**
     * Test the ArrayAccess interface
     */
    public function testArrayAccess() {
        $jsonFile = __DIR__ . '/form.json';
        $obj = Form::fromFile($jsonFile);

        $this->assertTrue(isset($obj[3]));
        $this->assertFalse(isset($obj[9993]));

        $count = $obj->count();
        $element = $obj[3];
        $this->assertEquals('phone', $element->getName());
        $newElement = $element->copy();
        $newElement->name('phoneHome');
        $obj[] = $newElement;
        $this->assertEquals('phoneHome', $obj[$count]->getName());

        unset($obj[3]);
        $this->assertTrue(null === $obj->findElementIndex('phone'));

        $this->assertTrue(null === $obj[$count + 1]);

        $obj[3] = $element;
        $this->assertFalse(null === $obj->findElementIndex('phone'));

        $this->expectException('\RuntimeException');
        $obj[1] = 'foo';
    }

    /**
     * Test the append element function - trap duplicate names
     */
    public function testAppendNoDup()
    {
        $jsonFile = __DIR__ . '/form.json';
        $obj = Form::fromFile($jsonFile);

        $element = $obj[3]->copy();

        $this->expectException('\RuntimeException');
        $obj->append($element);
    }

    /**
     * Test the append element function
     */
    public function testAppend()
    {
        $jsonFile = __DIR__ . '/form.json';
        $obj = Form::fromFile($jsonFile);

        $count = $obj->count();
        $element = $obj[2]->copy();
        $element->name('etPhoneHome');

        $obj->append($element);
        $this->assertEquals($count, $obj->findElementIndex($element));
    }

    /**
     * Test the append element in place function
     */
    public function testAppendInPlace()
    {
        $jsonFile = __DIR__ . '/form.json';
        $obj = Form::fromFile($jsonFile);

        $element = $obj[3]->copy();
        $element->name('etPhoneHome');

        $obj->append($element, 'phone');
        $this->assertEquals(4, $obj->findElementIndex($element));
    }

    /**
     * Test the append element in place function with an array
     */
    public function testAppendInPlaceArray()
    {
        $jsonFile = __DIR__ . '/form.json';
        $obj = Form::fromFile($jsonFile);

        $elements = [$obj[3]->copy(), $obj[3]->copy()];
        $elements[0]->name('etPhoneHome');
        $elements[1]->name('etPhoneWork');

        $obj->append($elements, 'phone');
        $this->assertEquals(4, $obj->findElementIndex($elements[0]));
        $this->assertEquals(5, $obj->findElementIndex($elements[1]));
    }

    /**
     * Test the prepend element function
     */
    public function testPrepend()
    {
        $jsonFile = __DIR__ . '/form.json';
        $obj = Form::fromFile($jsonFile);

        $element = $obj[2]->copy();
        $element->name('etPhoneHome');

        $obj->prepend($element);
        $this->assertEquals(0, $obj->findElementIndex($element));
    }

    /**
     * Test the prepend element in place function
     */
    public function testPrependInPlace()
    {
        $jsonFile = __DIR__ . '/form.json';
        $obj = Form::fromFile($jsonFile);

        $element = $obj[3]->copy();
        $element->name('etPhoneHome');

        $obj->prepend($element, 'phone');
        $this->assertEquals(3, $obj->findElementIndex($element));
    }

    /**
     * Test the prepend element in place function with an array
     */
    public function testPrependInPlaceArray()
    {
        $jsonFile = __DIR__ . '/form.json';
        $obj = Form::fromFile($jsonFile);

        $elements = [$obj[3]->copy(), $obj[3]->copy()];
        $elements[0]->name('etPhoneHome');
        $elements[1]->name('etPhoneWork');

        $obj->prepend($elements, 'phone');
        $this->assertEquals(3, $obj->findElementIndex($elements[0]));
        $this->assertEquals(4, $obj->findElementIndex($elements[1]));
    }

}
