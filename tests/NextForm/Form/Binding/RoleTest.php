<?php

namespace NextFormTests\Form\Binding;

// render object that just records the form element names
require_once __DIR__ . '/../../../test-tools/LoggingRender.php';
require_once __DIR__ . '/../../../test-tools/MockTranslate.php';

use Abivia\NextForm\NextForm;
use Abivia\NextForm\Data\Schema;
use Abivia\NextForm\Form\Form;
use NextFormTests\Test_Tools\LoggingRender;
use NextFormTests\Test_Tools\MockTranslate;

/**
 * @covers Abivia\NextForm\Form\Binding\Binding
 */
class RoleTest extends \PHPUnit\Framework\TestCase
{
    public $expect;
    public $manager;
    public $sectionParts;

    protected function makeSeg(...$bits) {
        return implode(NextForm::SEGMENT_DELIM, $bits);
    }

    public function setUp() : void {
        NextForm::boot();

        $manager = new NextForm();
        $manager->useOptions(
            [
                'segmentNameMode' => 'off',
                'wire' => [
                    'Render' => LoggingRender::class,
                ]
            ]
        );
        $manager->addForm(
            Form::fromFile(__DIR__ . '/role-test-form.json'),
            ['action' => 'myform.php']
        );
        $manager->addSchema(Schema::fromFile(__DIR__ . '/role-test-schema.json'));
        $this->manager = $manager;

        $this->expect = [
            'admin' => [
                'button.button' => 'write',
                'simpleCell.simpleCell' => 'write',
                'elementTest_cellOne.cellFieldOne' => 'write',
                'elementTest_cellTwo.cellFieldTwo' => 'write',
                'elementTest_textOne.textOne' => 'write',
                'htmlOne.htmlOne' => 'write',
                'SectionOne.SectionOne' => 'write',
                'elementTest_textSectionOne.textInSectionOne' => 'write',
                'cellInSectionOne.cellInSectionOne' => 'write',
                'elementTest_cellThree.cellFieldThree' => 'write',
                'elementTest_cellFour.cellFieldFour' => 'write',
                'elementTest_cellFive.cellFieldFive' => 'write',
                'staticOne.staticOne' => 'none',
            ],
            'agent' => [
                'button.button' => 'write',
                'simpleCell.simpleCell' => 'write',
                'elementTest_cellOne.cellFieldOne' => 'write',
                'elementTest_cellTwo.cellFieldTwo' => 'write',
                'elementTest_textOne.textOne' => 'write',
                'htmlOne.htmlOne' => 'write',
                'SectionOne.SectionOne' => 'view',
                'elementTest_textSectionOne.textInSectionOne' => 'view',
                'cellInSectionOne.cellInSectionOne' => 'view',
                'elementTest_cellThree.cellFieldThree' => 'view',
                'elementTest_cellFour.cellFieldFour' => 'view',
                'elementTest_cellFive.cellFieldFive' => 'view',
                'staticOne.staticOne' => 'view',
            ],
            'guest' => [
                'button.button' => 'none',
                'simpleCell.simpleCell' => 'view',
                'elementTest_cellOne.cellFieldOne' => 'view',
                'elementTest_cellTwo.cellFieldTwo' => 'view',
                'elementTest_textOne.textOne' => 'view',
                'htmlOne.htmlOne' => 'write',
                'SectionOne.SectionOne' => 'view',
                'elementTest_textSectionOne.textInSectionOne' => 'view',
                'cellInSectionOne.cellInSectionOne' => 'view',
                'elementTest_cellThree.cellFieldThree' => 'view',
                'elementTest_cellFour.cellFieldFour' => 'view',
                'elementTest_cellFive.cellFieldFive' => 'view',
                'staticOne.staticOne' => 'view',
            ],
        ];

        $this->sectionParts = [
            'SectionOne.SectionOne',
            'elementTest_textSectionOne.textInSectionOne',
            'cellInSectionOne.cellInSectionOne',
            'elementTest_cellThree.cellFieldThree',
            'elementTest_cellFour.cellFieldFour',
            'elementTest_cellFive.cellFieldFive',
        ];
    }

    /**
     * Admin access test.
     */
    public function testAdmin()
    {
        $this->manager->generate(null, ['role' => 'admin']);
        $this->assertEquals($this->expect['admin'], LoggingRender::getLog());
    }

    /**
     * Agent access test.
     */
    public function testAgent()
    {
        $this->manager->generate(null, ['role' => 'agent']);
        $this->assertEquals($this->expect['agent'], LoggingRender::getLog());
    }

    /**
     * Default access test.
     */
    public function testGuest()
    {
        $this->manager->generate(null, ['role' => 'guest']);
        $this->assertEquals($this->expect['guest'], LoggingRender::getLog());
    }

}