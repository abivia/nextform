<?php

namespace NextFormTests\Form\Binding;

use Abivia\NextForm\Data\Labels;
use Abivia\NextForm\Form\Binding\Binding;
use Abivia\NextForm\Form\Element\ButtonElement;
use Abivia\NextForm\Form\Element\CellElement;
use Abivia\NextForm\Form\Element\FieldElement;
use Abivia\NextForm\Form\Element\HtmlElement;
use Abivia\NextForm\Form\Element\SectionElement;
use Abivia\NextForm\Form\Element\StaticElement;
use Abivia\NextForm\NextForm;


/**
 * @covers Abivia\NextForm\Form\Binding\Binding
 */
class BindingTest extends \PHPUnit\Framework\TestCase
{

    /**
     * Test instantiation with a button element
     */
	public function testInstantiateButton()
    {
        $element = new ButtonElement();
        $binding = Binding::fromElement($element);
        $this->assertInstanceOf('\Abivia\NextForm\Form\Binding\Binding', $binding);
        $this->assertEquals($element, $binding->getElement());
	}

    /**
     * Test instantiation with a cell element
     */
	public function testInstantiateCell()
    {
        $element = new CellElement();
        $binding = Binding::fromElement($element);
        $this->assertInstanceOf('\Abivia\NextForm\Form\Binding\ContainerBinding', $binding);
        $this->assertEquals($element, $binding->getElement());
	}

    /**
     * Test instantiation with a field element
     */
	public function testInstantiateField()
    {
        $element = new FieldElement();
        $binding = Binding::fromElement($element);
        $this->assertInstanceOf('\Abivia\NextForm\Form\Binding\FieldBinding', $binding);
        $this->assertEquals($element, $binding->getElement());
	}

    /**
     * Test instantiation with a html element
     */
	public function testInstantiateHtml()
    {
        $element = new HtmlElement();
        $binding = Binding::fromElement($element);
        $this->assertInstanceOf('\Abivia\NextForm\Form\Binding\SimpleBinding', $binding);
        $this->assertEquals($element, $binding->getElement());
	}

    /**
     * Test instantiation with a section element
     */
	public function testInstantiateSection()
    {
        $element = new SectionElement();
        $binding = Binding::fromElement($element);
        $this->assertInstanceOf('\Abivia\NextForm\Form\Binding\ContainerBinding', $binding);
        $this->assertEquals($element, $binding->getElement());
	}

    /**
     * Test instantiation with a static element
     */
	public function testInstantiateStatic()
    {
        $element = new StaticElement();
        $binding = Binding::fromElement($element);
        $this->assertInstanceOf('\Abivia\NextForm\Form\Binding\SimpleBinding', $binding);
        $this->assertEquals($element, $binding->getElement());
	}

    /**
     * Test copy with a field element
     */
	public function testCopy()
    {
        $element = new FieldElement();
        $binding = Binding::fromElement($element);
        $bCopy = $binding->copy();
        $this->assertEquals($bCopy, $binding);
	}

    /**
     * Create a field element and set some labels
     */
	public function testLabelsField()
    {
        $element = new FieldElement();
        $binding = Binding::fromElement($element);
        $binding->label('heading', 'some text');
        $this->assertEquals('some text', $binding->getLabels()->get('heading'));
        $binding->label('error', ['a', 'b']);
        $this->assertEquals(['a', 'b'], $binding->getLabels()->get('error'));
        $this->assertNull($binding->getLabels()->get('inner'));
        $this->expectException('\RuntimeException');
        $binding->getLabels()->get('fubar');
	}

    /**
     * Test autofocus with a field element
     */
	public function testAutofocus()
    {
        $element = new FieldElement();
        $binding = Binding::fromElement($element);
        $this->assertFalse($binding->getAutofocus());
	}

    /**
     * Test getForm with a field element
     */
	public function testGetForm()
    {
        $element = new FieldElement();
        $binding = Binding::fromElement($element);
        $this->assertTrue($binding->getForm() === null);
	}

    /**
     * Test getId with a field element
     */
	public function testGetId()
    {
        NextForm::boot();
        $element = new FieldElement();
        $binding = Binding::fromElement($element);
        $binding->id('foo');
        $this->assertEquals('foo', $binding->getId());
        $binding->id('');
        $this->assertEquals('field_1', $binding->getId());
	}

    /**
     * Test getLabels
     */
	public function testGetLabels()
    {
        $binding = new Binding();
        $this->assertInstanceOf(Labels::class, $binding->getLabels());
        $this->assertInstanceOf(Labels::class, $binding->getLabels(true));
        $binding = new Binding();
        $this->assertInstanceOf(Labels::class, $binding->getLabels(true));
	}

    /**
     * Test getLinkedForm with a field element
     */
	public function testGetLinkedForm()
    {
        $element = new FieldElement();
        $binding = Binding::fromElement($element);
        $this->assertTrue($binding->getLinkedForm() === null);
	}

    /**
     * Test getNameOnForm with a section element
     */
	public function testGetNameOnFormSection()
    {
        NextForm::boot();
        $element = new SectionElement();
        $element->name('something');
        $binding = Binding::fromElement($element);
        $this->assertEquals('something', $binding->getNameOnForm());

        $binding->nameOnForm('somethingElse');
        $this->assertEquals('somethingElse', $binding->getNameOnForm());
	}

    /**
     * Test getNameOnForm with a section element
     */
	public function testGetNameOnFormSectionDefault()
    {
        NextForm::boot();
        $element = new SectionElement();
        $binding = Binding::fromElement($element);
        $this->assertEquals('section_1', $binding->getNameOnForm());
	}

    /**
     * Test getObject with a section element
     */
	public function testGetObject()
    {
        NextForm::boot();
        $element = new SectionElement();
        $binding = Binding::fromElement($element);
        $this->assertTrue(null === $binding->getObject());
	}

    /**
     * Test getTranslator with a section element
     */
	public function testGetTranslator()
    {
        NextForm::boot();
        $element = new SectionElement();
        $binding = Binding::fromElement($element);
        $this->assertTrue(null === $binding->getTranslator());
	}

    /**
     * Test getValid/valid with a section element
     */
	public function testGetValid()
    {
        NextForm::boot();
        $element = new SectionElement();
        $binding = Binding::fromElement($element);
        $this->assertTrue(null === $binding->getValid());
        $this->assertEquals($binding, $binding->valid(true));
        $this->assertTrue($binding->getValid());
	}

    /**
     * Test getValue with a section element
     */
	public function testGetValue()
    {
        NextForm::boot();
        $element = new SectionElement();
        $binding = Binding::fromElement($element);
        $this->assertTrue(null === $binding->getValue());
        $this->assertEquals($binding, $binding->value('foo'));
        $this->assertEquals('foo', $binding->getValue());
	}

    /**
     * Test labels
     */
	public function testLabels()
    {
        $binding = new Binding();
        $labels = new Labels();
        $binding->labels($labels);
        $this->assertEquals($labels, $binding->getLabels());

        $binding = new Binding();
        $binding->label('heading');
        $this->assertInstanceOf(Labels::class, $binding->getLabels());
	}

    /**
     * Test linkedForm with a field element
     */
	public function testLinkedForm()
    {
        $element = new FieldElement();
        $binding = Binding::fromElement($element);
        $this->assertTrue($binding->getLinkedForm() === null);
        $form = new \Abivia\NextForm\LinkedForm();
        $binding->linkedForm($form);
        $this->assertEquals($form, $binding->getLinkedForm());
	}

}
