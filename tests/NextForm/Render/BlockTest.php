<?php

namespace NextFormTests\Render;

use Abivia\NextForm\Render\Block;

/**
 * @covers \Abivia\NextForm\Render\Block
 */
class BlockTest extends \PHPUnit\Framework\TestCase {

	public function testFormRenderBlockInstantiation() {
        $obj = new Block();
		$this->assertInstanceOf('\Abivia\NextForm\Render\Block', $obj);
	}

	public function testFormRenderBlockClose() {
        $obj = new Block();
        $obj->body('body');
        $obj->post('post');
        $obj->close();
		$this->assertEquals('bodypost', $obj->getBody());
		$this->assertEquals('', $obj->getPost());
    }

	public function testFormRenderBlockMerge() {
        $main = new Block();
        $main->body('mainbody');
        $main->post('mainpost');
        $mainScripts = [
            'main' => 'somescript',
            'redundant' => 'redundant',
        ];
        foreach ($mainScripts as $use => $code) {
            $main->scriptFile($use, $code);
        }
        $obj2 = new Block();
        $obj2->body('obj2body');
        $obj2->post('obj2post');
        $obj2Scripts = [
            'obj2' => 'anotherscript',
            'redundant' => 'redundant',
        ];
        foreach ($obj2Scripts as $use => $code) {
            $obj2->scriptFile($use, $code);
        }
        $main->merge($obj2);
        $this->assertEquals('mainbodyobj2body', $main->getBody());
        $this->assertEquals('obj2postmainpost', $main->getPost());
        $this->assertEquals(
            array_merge($mainScripts, $obj2Scripts),
            $main->getScriptFiles()
        );
    }

}
