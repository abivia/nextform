<?php

namespace NextFormTests\Render;

use Abivia\NextForm\Data\Schema;
use Abivia\NextForm\Data\SchemaCollection;
use Abivia\NextForm\Form\Binding\Binding;
use Abivia\NextForm\Form\Element\ButtonElement;
use Abivia\NextForm\Form\Element\CellElement;
use Abivia\NextForm\Form\Element\FieldElement;
use Abivia\NextForm\Form\Element\HtmlElement;
use Abivia\NextForm\Form\Element\SectionElement;
use Abivia\NextForm\Form\Element\StaticElement;

/**
 * Generate standard test cases for use in testing multiple render classes.
 */
class RenderCaseGenerator {

    /**
     * Iterate through various label permutations
     * @param Binding $bBase Base element used to generate cases
     * @param string $prefix Test case prefix
     * @param string $namePrefix Test label prefix
     * @return array label-none|label-inner|label-before|label-after|label-head|label-help|label-all
     */
    protected static function addLabels(Binding $bBase, $prefix = '', $namePrefix = '', $skip = []) {
        $cases = [];

        // No changes
        $cases[$prefix . 'label-none'] = [$bBase, [], $namePrefix . 'label none'];

        if (!in_array('inner', $skip)) {
            $b1 = $bBase->copy();
            $b1->label('inner', 'inner');
            $cases[$prefix . 'label-inner'] = [$b1, [], $namePrefix . 'label inner'];
        }

        // A before label
        if (!in_array('before', $skip)) {
            $b2 = $bBase->copy();
            $b2->label('before', 'prefix');
            $cases[$prefix . 'label-before'] = [$b2, [], $namePrefix . 'label before'];
        }

        // Some text after
        if (!in_array('after', $skip)) {
            $b3 = $bBase->copy();
            $b3->label('after', 'suffix');
            $cases[$prefix . 'label-after'] = [$b3, [], $namePrefix . 'label after'];
        }

        // A heading
        if (!in_array('heading', $skip)) {
            $b4 = $bBase->copy();
            $b4->label('heading', 'Header');
            $cases[$prefix . 'label-head'] = [$b4, [], $namePrefix . 'label heading'];
        }

        // Help
        if (!in_array('help', $skip)) {
            $b5 = $bBase->copy();
            $b5->label('help', 'Helpful');
            $cases[$prefix . 'label-help'] = [$b5, [], $namePrefix . 'label help'];
        }

        // All the labels
        $b6 = $bBase->copy();
        if (!in_array('inner', $skip)) {
            $b6->label('inner', 'inner');
        }
        if (!in_array('heading', $skip)) {
            $b6->label('heading', 'Header');
        }
        if (!in_array('help', $skip)) {
            $b6->label('help', 'Helpful');
        }
        if (!in_array('before', $skip)) {
            $b6->label('before', 'prefix');
        }
        if (!in_array('after', $skip)) {
            $b6->label('after', 'suffix');
        }
        $cases[$prefix . 'label-all'] = [$b6, [], $namePrefix . 'all labels'];

        return self::normalizeCases($cases);
    }

    /**
     * Button test cases
     */
	public static function html_Button() {
        $cases = [];
        $config = json_decode(
            '{"type":"button","labels":{"inner":"I am Button!"}}'
        );

        $baseButton = new ButtonElement();
        $baseButton->configure($config);
        $baseBinding = Binding::fromElement($baseButton);
        $e1 = $baseButton->copy()->showString('purpose:success');
        $b1 = Binding::fromElement($e1);
        $cases['bda'] = [$b1, [], 'button default access'];
        $cases['bwa'] = [$b1, ['access' => 'write'], 'button write access'];

        // Make it a reset
        $b2 = Binding::fromElement($baseButton->copy()->function('reset'));
        $cases['rbda'] = [$b2, [], 'reset button default access'];

        // Make it a submit
        $e3 = $baseButton->copy()->function('submit');
        $b3 = Binding::fromElement($e3);
        $cases['sbda'] = [$b3, [], 'submit button default access'];

        // Set it back to button
        $b4 = Binding::fromElement($baseButton->copy()->function('button'));
        $cases['bda2'] = [$b4, [], 'button default access #2'];

        // Test view access
        $cases['bva'] = [$baseBinding, ['access' => 'view'], 'button view access'];

        // Test mask access
        $cases['bma'] = [$baseBinding, ['access' => 'mask'], 'button mask access'];

        // Test hidden access
        $cases['bra'] = [$baseBinding, ['access' => 'hide'], 'button hidden access'];

        // Test success with smaller size
        $b5 = Binding::fromElement($e1->copy()->addShow('size:small'));
        $cases['small'] = [$b5];

        // Test submit with larger size
        $b6 = Binding::fromElement($e3->copy()->addShow('size:large'));
        $cases['large'] = [$b6];

        // How about a large outline warning?
        $b7 = Binding::fromElement(
            $baseButton->copy()->showString('purpose:warning|size:large|fill:outline')
        );
        $cases['lg-warn-out'] = [$b7, [], 'button large warning outline'];

        // Disabled button
        $cases['disabled'] = Binding::fromElement(
            $baseButton->copy()->enable(false)
        );

        // Hidden button
        $cases['hidden'] = Binding::fromElement(
            $baseButton->copy()->display(false)
        );

        return self::normalizeCases($cases);
    }

    /**
     * Iterate through labels on a button
     * @return array
     */
	public static function html_ButtonLabels() {
        $config = json_decode('{"type":"button"}');
        $eBase = new ButtonElement();
        $eBase->configure($config);
        $bBase = Binding::fromElement($eBase);
        $cases = self::addLabels($bBase, '', 'Button ');
        return self::normalizeCases($cases);
    }

    public static function html_Cell() {
        $cases = [];
        $element = new CellElement();
        $binding = Binding::fromElement($element);
        $cases['basic'] = $binding;
        return self::normalizeCases($cases);
    }

    public static function html_FieldButton() {
        $cases = [];
        $schema = new SchemaCollection(
            Schema::fromFile(__DIR__ . '/../../test-data/test-schema.json')
        );
        //
        // Modify the schema to change test.text to a button
        //
        $schema->getProperty('test.text')->getPresentation()->type('button');
        $config = json_decode('{"type": "field","object": "test.text"}');
        $e1 = new FieldElement();
        $e1->configure($config);
        $b1 = Binding::fromElement($e1);
        $b1->bindSchema($schema);
        $b1->value('Ok Bob');

        $cases['value'] = [$b1, [], 'with value'];

        $s2 = $schema->copy();
        $e2 = new FieldElement();
        $e2->configure($config);
        $b2 = Binding::fromElement($e2);
        $b2->bindSchema($s2);
        $b2->value('Ok Bob');
        $s2->getProperty('test.text')->getPresentation()->type('reset');
        $cases['reset'] = [$b2];

        $s3 = $schema->copy();
        $e3 = new FieldElement();
        $e3->configure($config);
        $b3 = Binding::fromElement($e3);
        $b3->bindSchema($s3);
        $b3->value('Ok Bob');
        $s3->getProperty('test.text')->getPresentation()->type('submit');
        $cases['submit'] = [$b3];

        $cases['valid'] = [$b1->copy()->valid(true)];

        $cases['invalid'] = [$b1->copy()->valid(false)];

        return self::normalizeCases($cases);
    }

    public static function html_FieldCheckbox() {
        $cases = [];
        $schema = new SchemaCollection(
            Schema::fromFile(__DIR__ . '/../../test-data/test-schema.json')
        );
        //
        // Modify the schema to change test.text to a checkbox
        //
        $presentation = $schema->getProperty('test.text')->getPresentation();
        $presentation->type('checkbox');
        $config = json_decode('{"type": "field","object": "test.text"}');
        $element = new FieldElement();
        $element->configure($config);
        $binding = Binding::fromElement($element);
        $binding->bindSchema($schema);
        //
        // Give the binding a label
        //
        $binding->label('inner', '<Stand-alone> checkbox');

        // No access specification assumes write access
        $cases['basic'] = [$binding];

        // Same result with explicit write access
        $cases['write'] = [$binding, ['access' => 'write']];

        // Test view access
        $cases['view'] = [$binding, ['access' => 'view']];

        // Test mask access
        $cases['mask'] = [$binding, ['access' => 'mask']];

        // Test hidden access
        $cases['hide'] = [$binding, ['access' => 'hide']];

        // Set a value (e2 gets used below)
        $b2 = $binding->copy();
        $b2->value(3);
        $b2b = $b2->copy();
        $b2b->getDataProperty()->getPopulation()->sidecar('foo');
        $cases['value'] = [$b2b];
        $cases['value-view'] = [$b2b, ['access' => 'view']];
        $cases['value-mask'] = [$b2b, ['access' => 'mask']];
        $cases['value-hide'] = [$b2b, ['access' => 'hide']];

        // Set the default to the same as the value to render it as checked
        $b2c = $b2->copy();
        $b2c->getElement()->default(3);
        $cases['checked'] = [$b2c];

        // Render inline
        $b3 = $binding->copy();
        $b3->getElement()->addShow('layout:inline');
        $cases['inline'] = [$b3];

        // Render inline with no labels
        $b4 = $b3->copy();
        $b4->getElement()->addShow('appearance:no-label');
        $cases['inline-nolabel'] = [$b4];

        // Test headings
        $cases = array_merge($cases, self::addLabels($b2));

        $cases['valid'] = [$binding->copy()->valid(true)];

        $cases['invalid'] = [$binding->copy()->valid(false)];

        return self::normalizeCases($cases);
    }

    public static function html_FieldCheckboxButton() {
        $cases = [];
        $schema = new SchemaCollection(
            Schema::fromFile(__DIR__ . '/../../test-data/test-schema.json')
        );
        //
        // Modify the schema to change test.text to a checkbox
        //
        $presentation = $schema->getProperty('test.text')->getPresentation();
        $presentation->type('checkbox');
        $config = json_decode('{"type": "field","object": "test.text"}');
        $element = new FieldElement();
        $element->configure($config);
        $binding = Binding::fromElement($element);
        $binding->bindSchema($schema);

        // Give the binding a label
        $binding->label('inner', 'CheckButton!');

        // Make one show as a toggle
        $b1 = $binding->copy();
        $b1->getElement()->addShow('appearance:toggle');
        $cases['toggle'] = $b1;
        $cases = array_merge($cases, self::addLabels($b1, '', '', ['inner']));

        $cases['valid'] = [$b1->copy()->valid(true)];

        $cases['invalid'] = [$b1->copy()->valid(false)];

        return self::normalizeCases($cases);
    }

    public static function html_FieldCheckboxButtonList() {
        $cases = [];
        $schema = new SchemaCollection(
            Schema::fromFile(__DIR__ . '/../../test-data/test-schema.json')
        );

        // Modify the schema to change textWithList to a checkbox, mix up item attributes
        $schema->getProperty('test.textWithList')->getPresentation()->type('checkbox');

        $config = json_decode('{"type": "field","object": "test.textWithList"}');
        $element = new FieldElement();
        $element->configure($config);
        $binding = Binding::fromElement($element);
        $binding->bindSchema($schema);

        $list = $binding->getList(true);

        // Disable the second item
        $list[1]->enable(false);

        // Set appearance on the last list item
        $list[3]->showString('purpose:danger');
        // Make the list show as a toggle
        $b2 = $binding->copy();
        $b2->getElement()->addShow('appearance:toggle');
        $cases['toggle-list'] = $b2;

        $cases = array_merge($cases, self::addLabels($b2, 'list-', 'list-', ['inner']));

        $cases['valid'] = [$b2->copy()->valid(true)];

        $cases['invalid'] = [$b2->copy()->valid(false)];

        return self::normalizeCases($cases);
    }

    public static function html_FieldCheckboxList() {
        $cases = [];
        $schema = new SchemaCollection(
            Schema::fromFile(__DIR__ . '/../../test-data/test-schema.json')
        );
        //
        // Modify the schema to change textWithList to a checkbox
        //
        $schema->getProperty('test.textWithList')->getPresentation()->type('checkbox');

        $config = json_decode('{"type": "field","object": "test.textWithList"}');
        $element = new FieldElement();
        $element->configure($config);
        $binding = Binding::fromElement($element);
        $binding->bindSchema($schema);

        $list = $binding->getList(true);

        // Disable the second item
        $list[1]->enable(false);

        // No access specification assumes write access
        $cases['basic'] = [$binding];

        // Same result with explicit write access
        $cases['write'] = [$binding, ['access' => 'write']];

        // Test view access
        $cases['view'] = [$binding, ['access' => 'view']];

        // Test mask access
        $cases['mask'] = [$binding, ['access' => 'mask']];

        // Test hidden access
        $cases['hide'] = [$binding, ['access' => 'hide']];

        // Set a value to trigger the checked option
        $b2 = $binding->copy()->value('textlist 4');
        $cases['single-value'] = [$b2];

        // Test hidden access
        $cases['single-value-hide'] = [$b2, ['access' => 'hide']];

        // Set a second value to trigger the checked option
        $b3 = $binding->copy()->value(['textlist 1', 'textlist 4']);
        $cases['dual-value'] = [$b3];

        // Test view access
        $cases['dual-value-view'] = [$b3, ['access' => 'view']];

        // Test mask access
        $cases['dual-value-mask'] = [$b3, ['access' => 'mask']];

        // Test hidden access
        $cases['dual-value-hide'] = [$b3, ['access' => 'hide']];

        // Render inline
        $b4 = $binding->copy();
        $b4->getElement()->addShow('layout:inline');
        $cases['inline'] = [$b4];

        // Render inline with no labels
        $b5 = $b4->copy();
        $b5->getElement()->addShow('appearance:no-label');
        $cases['inline-nolabel'] = [$b5];

        // Render with smaller items
        $b5 = $binding->copy();
        $b5->getElement()->addShow('optionwidth:sm-3:md-4');
        $cases['optionwidth'] = [$b5];

        $cases['valid'] = [$binding->copy()->valid(true)];

        $cases['invalid'] = [$binding->copy()->valid(false)];

        return self::normalizeCases($cases);
    }

    public static function html_FieldColor() {
        $cases = [];
        $schema = new SchemaCollection(
            Schema::fromFile(__DIR__ . '/../../test-data/test-schema.json')
        );
        $presentation = $schema->getProperty('test.text')->getPresentation();
        $presentation->type('color');
        $config = json_decode('{"type": "field","object": "test.text"}');

        $element = new FieldElement();
        $element->configure($config);
        $binding = Binding::fromElement($element);
        $binding->bindSchema($schema);

        $cases['default'] = $binding;

        // Set a value
        //
        $b1 = $binding->copy();
        $b1->value('#F0F0F0');
        $cases['value'] = $b1;

        // Same result with explicit write access
        $cases['value-write'] = [$b1, ['access' => 'write']];

        // Now with view access
        $cases['value-view'] = [$b1, ['access' => 'view']];

        // Mask access
        $cases['value-mask'] = [$b1, ['access' => 'mask']];

        // Hidden access
        $cases['value-hide'] = [$b1, ['access' => 'hide']];

        $cases['valid'] = [$binding->copy()->valid(true)];

        $cases['invalid'] = [$binding->copy()->valid(false)];

        return self::normalizeCases($cases);
    }

    public static function html_FieldDate() {
        $cases = [];
        $schema = new SchemaCollection(
            Schema::fromFile(__DIR__ . '/../../test-data/test-schema.json')
        );

        $presentation = $schema->getProperty('test.text')->getPresentation();
        $presentation->type('date');
        $config = json_decode('{"type": "field","object": "test.text"}');
        $element = new FieldElement();
        $element->configure($config);
        $binding = Binding::fromElement($element);
        $binding->bindSchema($schema);

        // No access specification assumes write access
        $cases['basic'] = $binding;

        // Set a value
        $b1 = $binding->copy();
        $b1->value('2010-10-10');
        $cases['value'] = $b1;

        // Same result with explicit write access
        //
        $cases['write'] = [$b1, ['access' => 'write']];

        // Now test validation
        $b2 = $b1->copy();
        $validation = $b2->getDataProperty()->getValidation();
        $validation->set('minValue', '1957-10-08');
        $validation->set('maxValue', 'Nov 6th 2099');
        $cases['minmax'] = $b2;

        // Now with view access
        $cases['view'] = [$b2, ['access' => 'view']];

        // Mask access
        $cases['mask'] = [$b2, ['access' => 'mask']];

        // Convert to hidden access
        $cases['hide'] = [$b2, ['access' => 'hide']];

        $cases['valid'] = [$binding->copy()->valid(true)];

        $cases['invalid'] = [$binding->copy()->valid(false)];

        return self::normalizeCases($cases);
    }

    public static function html_FieldDatetimeLocal() {
        $cases = [];

        $schema = new SchemaCollection(
            Schema::fromFile(__DIR__ . '/../../test-data/test-schema.json')
        );

        $presentation = $schema->getProperty('test.text')->getPresentation();
        $presentation->type('datetime-local');
        $config = json_decode('{"type": "field","object": "test.text"}');
        $element = new FieldElement();
        $element->configure($config);
        $binding = Binding::fromElement($element);
        $binding->bindSchema($schema);

        // No access specification assumes write access
        $cases['basic'] = $binding;

        // Set a value
        $b1 = $binding->copy();
        $b1->value('2010-10-10');
        $cases['value'] = $b1;

        // Same result with explicit write access
        $cases['write'] = [$b1, ['access' => 'write']];

        // Now test validation
        $b2 = $b1->copy();
        $validation = $b2->getDataProperty()->getValidation();
        $validation->set('minValue', '1957-10-08');
        $validation->set('maxValue', '2:15 pm Nov 6th 2099');
        $cases['minmax'] = $b2;

        // Now with view access
        $cases['view'] = [$b2, ['access' => 'view']];

        // Mask access
        $cases['mask'] = [$b2, ['access' => 'mask']];

        // Convert to hidden access
        $cases['hide'] = [$b2, ['access' => 'hide']];

        $cases['valid'] = [$binding->copy()->valid(true)];

        $cases['invalid'] = [$binding->copy()->valid(false)];

        return self::normalizeCases($cases);
    }

    public static function html_FieldEmail() {
        $cases = [];

        $schema = new SchemaCollection(
            Schema::fromFile(__DIR__ . '/../../test-data/test-schema.json')
        );

        $presentation = $schema->getProperty('test.text')->getPresentation();
        $presentation->type('email');
        $config = json_decode('{"type": "field","object": "test.text"}');

        $element = new FieldElement();
        $element->configure($config);
        $binding = Binding::fromElement($element);
        $binding->bindSchema($schema);

        // No access specification assumes write access
        $cases['basic'] = $binding;

        // Now test validation
        $b1 = $binding->copy();
        $validation = $b1->getDataProperty()->getValidation();
        $validation->set('multiple', true);
        $cases['multiple'] = $b1;

        // Turn confirmation on and set some test labels
        $s2 = $schema->copy();
        $e2 = new FieldElement();
        $e2->configure($config);
        $b2 = Binding::fromElement($e2);
        $b2->bindSchema($s2);
        $s2->getProperty('test.text')->getPresentation()->confirm(true);
        $b2->label('heading', 'Yer email');
        $b2->label('heading', 'Confirm yer email', ['asConfirm' => true]);
        $cases['confirm'] = $b2;

        // Test view access
        $b3 = $b2->copy();
        $b3->value('snafu@fub.ar');
        $cases['view'] = [$b3, ['access' => 'view']];

        // Mask access
        $cases['mask'] = [$b3, ['access' => 'mask']];

        // Hidden access
        $cases['hide'] = [$b3, ['access' => 'hide']];

        $cases['valid'] = [$binding->copy()->valid(true)];

        $cases['invalid'] = [$binding->copy()->valid(false)];

        return self::normalizeCases($cases);
    }

    public static function html_FieldFile() {
        $cases = [];

        $schema = new SchemaCollection(
            Schema::fromFile(__DIR__ . '/../../test-data/test-schema.json')
        );

        $presentation = $schema->getProperty('test.text')->getPresentation();
        $presentation->type('file');
        $config = json_decode('{"type": "field","object": "test.text"}');
        $element = new FieldElement();
        $element->configure($config);
        $binding = Binding::fromElement($element);
        $binding->bindSchema($schema);

        // No access specification assumes write access
        $cases['basic'] = $binding;

        // Now test validation
        //
        $b1 = $binding->copy();
        $validation = $b1->getDataProperty()->getValidation();
        $validation->set('accept', '*.png,*.jpg');
        $validation->set('multiple', true);
        $cases['valid'] = $b1;

        // Test view access
        $cases['view'] = [$b1, ['access' => 'view']];

        // Mask access
        $cases['mask'] = [$b1, ['access' => 'mask']];

        // Test view with a value
        $b2 = $b1->copy();
        $b2->value(['file1.png', 'file2.jpg']);
        $cases['value-view'] = [$b2, ['access' => 'view']];

        // Mask access
        $cases['value-mask'] = [$b2, ['access' => 'mask']];

        // Test hidden access
        $cases['value-hide'] = [$b2, ['access' => 'hide']];

        $cases['valid'] = [$binding->copy()->valid(true)];

        $cases['invalid'] = [$binding->copy()->valid(false)];

        return self::normalizeCases($cases);
    }

    public static function html_FieldHidden() {
        $cases = [];

        $schema = new SchemaCollection(
            Schema::fromFile(__DIR__ . '/../../test-data/test-schema.json')
        );

        $presentation = $schema->getProperty('test.text')->getPresentation();
        $presentation->type('hidden');
        $config = json_decode('{"type": "field","object": "test.text"}');
        $element = new FieldElement();
        $element->configure($config);
        $binding = Binding::fromElement($element);
        $binding->bindSchema($schema);

        // No access specification assumes write access
        $cases['basic'] = $binding;

        // Same result with explicit write access
        $cases['write'] = [$binding, ['access' => 'write']];

        // Same result with view access
        $cases['view'] = [$binding, ['access' => 'view']];

        // Mask access
        $cases['mask'] = [$binding, ['access' => 'mask']];

        // Same result for hidden access
        $cases['hide'] = [$binding, ['access' => 'hide']];

        // Scalar valued
        $b2 = $binding->copy();
        $b2->value(3);
        $cases['scalar'] = $b2;

        // Array valued
        $b3 = $binding->copy();
        $b3->value([3, 4]);
        $cases['array'] = $b3;

        // Scalar with sidecar
        $b4 = $b2->copy();
        $b4->getDataProperty()->getPopulation()->sidecar('foo');
        $cases['sidecar'] = $b4;

        $cases['valid'] = [$binding->copy()->valid(true)];

        $cases['invalid'] = [$binding->copy()->valid(false)];

        return self::normalizeCases($cases);
    }

    public static function html_FieldHiddenLabels() {
        $schema = new SchemaCollection(
            Schema::fromFile(__DIR__ . '/../../test-data/test-schema.json')
        );

        $presentation = $schema->getProperty('test.text')->getPresentation();
        $presentation->type('hidden');
        $config = json_decode('{"type": "field","object": "test.text"}');
        $element = new FieldElement();
        $element->configure($config);
        $binding = Binding::fromElement($element);
        $binding->bindSchema($schema);
        $binding->value('the value');

        $cases = self::addLabels($binding);

        $cases['valid'] = [$binding->copy()->valid(true)];

        $cases['invalid'] = [$binding->copy()->valid(false)];

        return self::normalizeCases($cases);
    }

    public static function html_FieldHtml() {
        $cases = [];

        $schema = new SchemaCollection(
            Schema::fromFile(__DIR__ . '/../../test-data/test-schema.json')
        );

        // Modify the schema to change test.text to HTML
        $presentation = $schema->getProperty('test.text')->getPresentation();
        $presentation->type('html');
        $config = json_decode('{"type": "field","object": "test.text"}');
        $element = new FieldElement();
        $element->configure($config);
        $binding = Binding::fromElement($element);
        $binding->bindSchema($schema);
        $binding->value('<span>Some HTML.</span>');

        // No access specification assumes write access
        $cases['basic'] = $binding;

        // Same result with explicit write access
        $cases['write'] = [$binding, ['access' => 'write']];

        // Test view access
        $cases['view'] = [$binding, ['access' => 'view']];

        // Mask access
        $cases['mask'] = [$binding, ['access' => 'mask']];

        // Test hidden access
        $cases['hide'] = [$binding, ['access' => 'hide']];

        $cases['valid'] = [$binding->copy()->valid(true)];

        $cases['invalid'] = [$binding->copy()->valid(false)];

        $cases = array_merge($cases, self::addLabels($binding, '', '', ['inner']));

        return self::normalizeCases($cases);
    }

    public static function html_FieldMonth() {
        $cases = [];

        $schema = new SchemaCollection(
            Schema::fromFile(__DIR__ . '/../../test-data/test-schema.json')
        );

        $presentation = $schema->getProperty('test.text')->getPresentation();
        $presentation->type('month');
        $config = json_decode('{"type": "field","object": "test.text"}');
        $element = new FieldElement();
        $element->configure($config);
        $binding = Binding::fromElement($element);
        $binding->bindSchema($schema);

        // No access specification assumes write access
        $cases['basic'] = $binding;

        // Set a value
        $b1 = $binding->copy();
        $b1->value('2010-10');
        $cases['value'] = $b1;

        // Same result with explicit write access
        $cases['value-write'] = [$b1, ['access' => 'write']];

        // Now test validation
        $b2 = $b1->copy();
        $validation = $b2->getDataProperty()->getValidation();
        $validation->set('minValue', '1957-10');
        $validation->set('maxValue', 'Nov 2099');
        $cases['minmax'] = $b2;

        // Now with view access
        $cases['minmax-view'] = [$b2, ['access' => 'view']];

        // Mask access
        $cases['minmax-mask'] = [$b2, ['access' => 'mask']];

        // Convert to hidden access
        $cases['hide'] = [$b2, ['access' => 'hide']];

        $cases['valid'] = [$binding->copy()->valid(true)];

        $cases['invalid'] = [$binding->copy()->valid(false)];

        return self::normalizeCases($cases);
    }

    public static function html_FieldNumber() {
        $cases = [];

        $schema = new SchemaCollection(
            Schema::fromFile(__DIR__ . '/../../test-data/test-schema.json')
        );

        // Modify the schema to change test.text to a number
        //
        $presentation = $schema->getProperty('test.text')->getPresentation();
        $presentation->type('number');
        $config = json_decode('{"type": "field","object": "test.text"}');
        $element = new FieldElement();
        $element->configure($config);
        $binding = Binding::fromElement($element);
        $binding->bindSchema($schema);
        $binding->value('200');

        $cases['basic'] = $binding;

        // Make the field required
        $b1 = $binding->copy();
        $validation = $b1->getDataProperty()->getValidation();
        $validation->set('required', true);
        $cases['required'] = $b1->copy();

        // Set minimum/maximum values
        $validation->set('minValue', -1000);
        $validation->set('maxValue', 999.45);
        $cases['minmax'] = $b1->copy();

        // Add a step
        $validation->set('step', 1.23);
        $cases['step'] = $b1->copy();

        // Settng a pattern should have no effect!
        $validation->set('pattern', '/[+\-]?[0-9]+/');
        $cases['pattern'] = $b1;

        // Test view access
        $cases['view'] = [$b1, ['access' => 'view']];

        // Mask access
        $cases['mask'] = [$b1, ['access' => 'mask']];

        $cases['valid'] = [$binding->copy()->valid(true)];

        $cases['invalid'] = [$binding->copy()->valid(false)];

        return self::normalizeCases($cases);
    }

    public static function html_FieldPassword() {
        $cases = [];

        $schema = new SchemaCollection(
            Schema::fromFile(__DIR__ . '/../../test-data/test-schema.json')
        );

        // Modify the schema to change test.text to a password
        $presentation = $schema->getProperty('test.text')->getPresentation();
        $presentation->type('password');
        $config = json_decode('{"type": "field","object": "test.text"}');
        $element = new FieldElement();
        $element->configure($config);
        $binding = Binding::fromElement($element);
        $binding->bindSchema($schema);

        // No access specification assumes write access
        $cases['basic'] = $binding;

        // Same result with explicit write access
        $cases['write'] = [$binding, ['access' => 'write']];

        // Test view access
        $cases['view'] = [$binding, ['access' => 'view']];

        // Mask access
        $cases['mask'] = [$binding, ['access' => 'mask']];

        // Test view with a value
        $b1 = $binding->copy();
        $b1->value('secret');
        $cases['value-view'] = [$b1, ['access' => 'view']];

        // Mask access
        $cases['value-mask'] = [$b1, ['access' => 'mask']];

        // Test hidden access
        $cases['value-hide'] = [$b1, ['access' => 'hide']];

        $cases['valid'] = [$binding->copy()->valid(true)];

        $cases['invalid'] = [$binding->copy()->valid(false)];

        return self::normalizeCases($cases);
    }

    public static function html_FieldRadio() {
        $cases = [];

        $schema = new SchemaCollection(
            Schema::fromFile(__DIR__ . '/../../test-data/test-schema.json')
        );

        // Modify the schema to change test.text to a radio
        $presentation = $schema->getProperty('test.text')->getPresentation();
        $presentation->type('radio');
        $config = json_decode('{"type": "field","object": "test.text"}');
        $element = new FieldElement();
        $element->configure($config);
        $binding = Binding::fromElement($element);
        $binding->bindSchema($schema);

        // Give the element a label
        $binding->label('inner', '<Stand-alone> radio');
        //
        // No access specification assumes write access
        $cases['basic'] = $binding;

        // Same result with explicit write access
        $cases['write'] = [$binding, ['access' => 'write']];

        // Set a value
        $b1 = $binding->copy();
        $b1->value(3);
        $cases['value'] = $b1;

        // Test view access
        $cases['value-view'] = [$b1, ['access' => 'view']];

        // Mask access
        $cases['value-mask'] = [$b1, ['access' => 'mask']];

        // Test hidden access
        $cases['value-hide'] = [$b1, ['access' => 'hide']];

        $cases['valid'] = [$binding->copy()->valid(true)];

        $cases['invalid'] = [$binding->copy()->valid(false)];

        return self::normalizeCases($cases);
    }

    public static function html_FieldRadioLabels() {
        $cases = [];

        $schema = new SchemaCollection(
            Schema::fromFile(__DIR__ . '/../../test-data/test-schema.json')
        );

        // Modify the schema to change test.text to a radio
        $presentation = $schema->getProperty('test.text')->getPresentation();
        $presentation->type('radio');
        $config = json_decode('{"type": "field","object": "test.text"}');
        $element = new FieldElement();
        $element->configure($config);
        $binding = Binding::fromElement($element);
        $binding->bindSchema($schema);

        // Give the element some labels and a value
        $binding->label('before', 'No need to fear');
        $binding->label('heading', 'Very Important Choice');
        $binding->label('inner', '<Stand-alone> radio');
        $binding->label('after', 'See? No problem!');
        $binding->value(3);
        $cases['labels-value'] = $binding;

        // Test view access
        $cases['labels-value-view'] = [$binding, ['access' => 'view']];

        // Mask access
        $cases['labels-value-mask'] = [$binding, ['access' => 'mask']];

        // Test hidden access
        $cases['labels-value-hide'] = [$binding, ['access' => 'hide']];

        $cases['valid'] = [$binding->copy()->valid(true)];

        $cases['invalid'] = [$binding->copy()->valid(false)];

        return self::normalizeCases($cases);
    }

    public static function html_FieldRadioList() {
        $cases = [];

        $schema = new SchemaCollection(
            Schema::fromFile(__DIR__ . '/../../test-data/test-schema.json')
        );

        // Modify the schema to change textWithList to a radio
        $schema->getProperty('test.textWithList')->getPresentation()->type('radio');
        $config = json_decode('{"type": "field","object": "test.textWithList"}');
        $element = new FieldElement();
        $element->configure($config);
        $binding = Binding::fromElement($element);
        $binding->bindSchema($schema);

        // No access specification assumes write access
        $cases['basic'] = $binding->copy();

        // Same result with explicit write access
        $cases['write'] = [$binding->copy(), ['access' => 'write']];

        // Set a value to trigger the checked option
        $binding->value('textlist 3');
        $cases['value'] = $binding->copy();

        //
        // Test view access
        $cases['value-view'] = [$binding->copy(), ['access' => 'view']];

        // Mask access
        $cases['value-mask'] = [$binding->copy(), ['access' => 'mask']];

        // Test hidden access
        $cases['value-hide'] = [$binding->copy(), ['access' => 'hide']];

        $cases['valid'] = [$binding->copy()->valid(true)];

        $cases['invalid'] = [$binding->copy()->valid(false)];

        return self::normalizeCases($cases);
    }

    public static function html_FieldRadioListLabels() {
        $cases = [];

        $schema = new SchemaCollection(
            Schema::fromFile(__DIR__ . '/../../test-data/test-schema.json')
        );

        // Modify the schema to change test.text to a radio
        $presentation = $schema->getProperty('test.textWithList')->getPresentation();
        $presentation->type('radio');
        $config = json_decode('{"type": "field","object": "test.textWithList"}');
        $element = new FieldElement();
        $element->configure($config);
        $binding = Binding::fromElement($element);
        $binding->bindSchema($schema);
        //
        // Give the element some labels and a value
        //
        $binding->label('before', 'No need to fear');
        $binding->label('heading', 'Very Important Choice');
        $binding->label('inner', '<Stand-alone> radio');
        $binding->label('after', 'See? No problem!');
        $binding->value('textlist 3');
        $cases['labels-value'] = $binding;

        // Test view access
        $cases['labels-value-view'] = [$binding, ['access' => 'view']];

        // Mask access
        $cases['labels-value-mask'] = [$binding, ['access' => 'mask']];

        // Test hidden access
        $cases['labels-value-hide'] = [$binding, ['access' => 'hide']];

        $cases['valid'] = [$binding->copy()->valid(true)];

        $cases['invalid'] = [$binding->copy()->valid(false)];

        return self::normalizeCases($cases);
    }

    public static function html_FieldRange() {
        $cases = [];

        $schema = new SchemaCollection(
            Schema::fromFile(__DIR__ . '/../../test-data/test-schema.json')
        );

        // Modify the schema to change test.text to a range
        //
        $presentation = $schema->getProperty('test.text')->getPresentation();
        $presentation->type('range');
        $config = json_decode('{"type": "field","object": "test.text"}');
        $element = new FieldElement();
        $element->configure($config);
        $binding = Binding::fromElement($element);
        $binding->bindSchema($schema);
        $binding->value('200');

        $cases['basic'] = $binding->copy();

        $cases['valid'] = [$binding->copy()->valid(true)];

        $cases['invalid'] = [$binding->copy()->valid(false)];

        // Making the field required should have no effect
        $validation = $binding->getDataProperty()->getValidation();
        $validation->set('required', true);
        $cases['required'] = $binding->copy();

        // Set minimum/maximum values
        $validation->set('minValue', -1000);
        $validation->set('maxValue', 999.45);
        $cases['minmax'] = $binding->copy();

        // Add a step
        //
        $validation->set('step', 20);
        $cases['step'] = $binding->copy();

        // Setting a pattern should have no effect!
        $validation->set('pattern', '/[+\-]?[0-9]+/');
        $cases['pattern'] = $binding->copy();

        // Test view access
        $cases['view'] = [$binding, ['access' => 'view']];

        // Mask access
        $cases['mask'] = [$binding, ['access' => 'mask']];

        return self::normalizeCases($cases);
    }

    public static function html_FieldSearch() {
        $cases = [];

        $schema = new SchemaCollection(
            Schema::fromFile(__DIR__ . '/../../test-data/test-schema.json')
        );

        // Modify the schema to change test.text to a search
        $presentation = $schema->getProperty('test.text')->getPresentation();
        $presentation->type('search');
        $config = json_decode('{"type": "field","object": "test.text"}');
        $element = new FieldElement();
        $element->configure($config);
        $binding = Binding::fromElement($element);
        $binding->bindSchema($schema);

        // No access specification assumes write access
        $cases['basic'] = $binding;

        // Same result with explicit write access
        $cases['write'] = [$binding, ['access' => 'write']];

        // Test view access
        $cases['view'] = [$binding, ['access' => 'view']];

        // Mask access
        $cases['mask'] = [$binding, ['access' => 'mask']];

        // Test hidden access
        $cases['hide'] = [$binding, ['access' => 'hide']];

        $cases['valid'] = [$binding->copy()->valid(true)];

        $cases['invalid'] = [$binding->copy()->valid(false)];

        return self::normalizeCases($cases);
    }

    public static function html_FieldSelect() {
        $cases = [];

        $schema = new SchemaCollection(
            Schema::fromFile(__DIR__ . '/../../test-data/test-schema.json')
        );

        // Modify the schema to change test.text to a select
        $schema->getProperty('test.textWithList')
            ->getPresentation()->type('select');
        $config = json_decode('{"type": "field","object": "test.textWithList"}');
        $element = new FieldElement();
        $element->configure($config);
        $binding = Binding::fromElement($element);
        $binding->bindSchema($schema);

        // No access specification assumes write access
        $cases['basic'] = $binding->copy();

        // Same result with explicit write access
        $cases['write'] = $cases['basic'];

        // Set valid/invalid
        $cases['valid'] = [$binding->copy()->valid(true)];

        $cases['invalid'] = [$binding->copy()->valid(false)];

        // Test view access
        $cases['view'] = [$binding->copy(), ['access' => 'view']];

        // Mask access
        $cases['mask'] = [$binding, ['access' => 'mask']];

        // Test hidden access
        $cases['hide'] = [$binding->copy(), ['access' => 'hide']];

        // Now let's give it a value...
        $b1 = $binding->copy();
        $b1->value('textlist 2');
        $cases['value'] = $b1->copy();

        // Test view access
        $cases['value-view'] = [$b1->copy(), ['access' => 'view']];

        // Mask access
        $cases['value-mask'] = [$b1->copy(), ['access' => 'mask']];

        // Test hidden access
        $cases['value-hide'] = [$b1->copy(), ['access' => 'hide']];

        // Test the BS custom presentation
        $b2 = $b1->copy();
        $b2->getElement()->showString('appearance:custom');
        $cases['value-bs4custom'] = $b2;

        // Set multiple and give it two values
        $validation = $b1->getDataProperty()->getValidation();
        $validation->set('multiple', true);
        $b1->value(['textlist 2', 'textlist 4']);
        $cases['multivalue'] = $b1->copy();


        // Test view access
        $cases['multivalue-view'] = [$b1->copy(), ['access' => 'view']];

        // Mask access
        $cases['multivalue-mask'] = [$b1->copy(), ['access' => 'mask']];

        // Test hidden access
        $cases['multivalue-hide'] = [$b1->copy(), ['access' => 'hide']];

        // Clone the schema and set the presentation to six rows
        $s2 = $schema->copy();
        $s2->getProperty('test.textWithList')
            ->getPresentation()->rows(6);
        $config = json_decode('{"type": "field","object": "test.textWithList"}');
        $element = new FieldElement();
        $element->configure($config);
        $b3 = Binding::fromElement($element);
        $b3->bindSchema($s2);
        $b3->getDataProperty()->getValidation()->set('multiple', true);
        $b3->value(['textlist 2', 'textlist 4']);
        $cases['sixrow'] = $b3;

        return self::normalizeCases($cases);
    }

    public static function html_FieldSelectNested() {
        $cases = [];

        $schema = new SchemaCollection(
            Schema::fromFile(__DIR__ . '/../../test-data/test-schema.json')
        );

        // Modify the schema to change test.text to a select
        $presentation = $schema->getProperty('test.textWithNestedList')->getPresentation();
        $presentation->type('select');
        $config = json_decode('{"type": "field","object": "test.textWithNestedList"}');
        $element = new FieldElement();
        $element->configure($config);
        $binding = Binding::fromElement($element);
        $binding->bindSchema($schema);

        // No access specification assumes write access
        $cases['basic'] = $binding->copy();

        // Same result with explicit write access
        $cases['write'] = [$binding->copy(), ['access' => 'write']];

        $cases['valid'] = [$binding->copy()->valid(true)];

        $cases['invalid'] = [$binding->copy()->valid(false)];

        // Test view access
        $cases['view'] = [$binding->copy(), ['access' => 'view']];

        // Mask access
        $cases['mask'] = [$binding->copy(), ['access' => 'mask']];

        // Test hidden access
        $cases['hide'] = [$binding->copy(), ['access' => 'hide']];

        // Now let's give it a value...
        $binding->value('S2I1');
        $cases['value'] = $binding->copy();

        // Test the BS custom presentation
        $b2 = $binding->copy();
        $b2->getElement()->showString('appearance:custom');
        $cases['value-bs4custom'] = $b2;

        // Test view access
        $cases['value-view'] = [$binding->copy(), ['access' => 'view']];

        // Mask access
        $cases['value-mask'] = [$binding->copy(), ['access' => 'mask']];

        // Test hidden access
        $cases['value-hide'] = [$binding->copy(), ['access' => 'hide']];

        // Set multiple and give it two values
        $validation = $binding->getDataProperty()->getValidation();
        $validation->set('multiple', true);
        $binding->value(['S2I1', 'Sub One Item One']);
        $cases['multivalue'] = $binding->copy();

        // Test view access
        $cases['multivalue-view'] = [$binding->copy(), ['access' => 'view']];

        // Mask access
        $cases['multivalue-mask'] = [$binding->copy(), ['access' => 'mask']];

        // Test hidden access
        $cases['multivalue-hide'] = [$binding->copy(), ['access' => 'hide']];

        return self::normalizeCases($cases);
    }

    public static function html_FieldTel() {
        $cases = [];

        $schema = new SchemaCollection(
            Schema::fromFile(__DIR__ . '/../../test-data/test-schema.json')
        );

        // Modify the schema to change test.text to a tel
        $presentation = $schema->getProperty('test.text')->getPresentation();
        $presentation->type('tel');
        $config = json_decode('{"type": "field","object": "test.text"}');
        $element = new FieldElement();
        $element->configure($config);
        $binding = Binding::fromElement($element);
        $binding->bindSchema($schema);

        // No access specification assumes write access
        $cases['basic'] = $binding;

        // Same result with explicit write access
        $cases['write'] = [$binding, ['access' => 'write']];

        // Test view access
        $cases['view'] = [$binding, ['access' => 'view']];

        // Mask access
        $cases['mask'] = [$binding, ['access' => 'mask']];

        // Test hidden access
        $cases['hide'] = [$binding, ['access' => 'hide']];

        $cases['valid'] = [$binding->copy()->valid(true)];

        $cases['invalid'] = [$binding->copy()->valid(false)];

        return self::normalizeCases($cases);
    }

    public static function html_FieldText() {
        $cases = [];

        $schema = new SchemaCollection(
            Schema::fromFile(__DIR__ . '/../../test-data/test-schema.json')
        );

        $config = json_decode('{"type": "field","object": "test.text"}');
        $element = new FieldElement();
        $element->configure($config);
        $binding = Binding::fromElement($element);
        $binding->bindSchema($schema);

        $cases['default'] = [$binding, [], 'default access'];
        $cases['write'] = [$binding, ['access' => 'write'], 'explicit write access'];
        $cases['view'] = [$binding, ['access' => 'view'], 'explicit view access'];

        // Mask access
        $cases['mask'] = [$binding, ['access' => 'mask']];

        $cases['hide'] = [$binding, ['access' => 'hide'], 'explicit hidden access'];

        $cases['valid'] = [$binding->copy()->valid(true)];

        $cases['invalid'] = [$binding->copy()->valid(false)];

        return self::normalizeCases($cases);
    }

    public static function html_FieldTextDataList() {
        $cases = [];

        $schema = new SchemaCollection(
            Schema::fromFile(__DIR__ . '/../../test-data/test-schema.json')
        );

        $config = json_decode('{"type": "field","object": "test.textWithList"}');
        $element = new FieldElement();
        $element->configure($config);
        $binding = Binding::fromElement($element);
        $binding->bindSchema($schema);

        // No access assumes write access
        $cases['basic'] = $binding;

        // Test view access: No list is required
        $cases['view'] = [$binding, ['access' => 'view']];

        // Mask access
        $cases['mask'] = [$binding, ['access' => 'mask']];

        // Test read  access
        $cases['hide'] = [$binding, ['access' => 'hide']];

        $cases['valid'] = [$binding->copy()->valid(true)];

        $cases['invalid'] = [$binding->copy()->valid(false)];

        return self::normalizeCases($cases);
    }

    /**
     * Iterate through labels on a text field
     * @return array
     */
	public static function html_FieldTextLabels() {
        $schema = new SchemaCollection(
            Schema::fromFile(__DIR__ . '/../../test-data/test-schema.json')
        );

        $config = json_decode('{"type": "field","object": "test.text"}');
        $element = new FieldElement();
        $element->configure($config);
        $binding = Binding::fromElement($element);
        $binding->bindSchema($schema);
        $binding->value('the value');
        $cases = self::addLabels($binding, '', 'Text ');

        $cases['valid'] = [$binding->copy()->valid(true)];

        $cases['invalid'] = [$binding->copy()->valid(false)];

        return self::normalizeCases($cases);
    }

    public static function html_FieldTextValidation() {
        $cases = [];

        $schema = new SchemaCollection(
            Schema::fromFile(__DIR__ . '/../../test-data/test-schema.json')
        );

        $config = json_decode('{"type": "field","object": "test.text"}');
        $element = new FieldElement();
        $element->configure($config);
        $binding = Binding::fromElement($element);
        $binding->bindSchema($schema);
        $validation = $binding->getDataProperty()->getValidation();

        // Make the field required
        $validation->set('required', true);
        $cases['required'] = $binding->copy();

        // Set a maximum length
        $validation->set('maxLength', 10);
        $cases['max'] = $binding->copy();

        // Set a minimum length
        $validation->set('minLength', 3);
        $cases['minmax'] = $binding->copy();

        // Make it match a postal code
        $validation->set('pattern', '/[a-z][0-9][a-z] ?[0-9][a-z][0-9]/');
        $cases['pattern'] = $binding->copy();

        return self::normalizeCases($cases);
    }

    public static function html_FieldTextarea() {
        $cases = [];

        $schema = new SchemaCollection(
            Schema::fromFile(__DIR__ . '/../../test-data/test-schema.json')
        );

        // Modify the schema to change test.text to a textarea
        $presentation = $schema->getProperty('test.text')->getPresentation();
        $presentation->type('textarea');
        $config = json_decode('{"type": "field","object": "test.text"}');
        $element = new FieldElement();
        $element->configure($config);
        $binding = Binding::fromElement($element);
        $binding->bindSchema($schema);

        // No access specification assumes write access
        $cases['basic'] = $binding;

        // Same result with explicit write access
        $cases['write'] = [$binding, ['access' => 'write']];

        // Test view access
        $cases['view'] = [$binding, ['access' => 'view']];

        // Mask access
        $cases['mask'] = [$binding, ['access' => 'mask']];

        // Test hidden access
        $cases['hide'] = [$binding, ['access' => 'hide']];

        $cases['valid'] = [$binding->copy()->valid(true)];

        $cases['invalid'] = [$binding->copy()->valid(false)];

        return self::normalizeCases($cases);
    }

    public static function html_FieldTime() {
        $cases = [];

        $schema = new SchemaCollection(
            Schema::fromFile(__DIR__ . '/../../test-data/test-schema.json')
        );

        $presentation = $schema->getProperty('test.text')->getPresentation();
        $presentation->type('time');
        $config = json_decode('{"type": "field","object": "test.text"}');
        $element = new FieldElement();
        $element->configure($config);
        $binding = Binding::fromElement($element);
        $binding->bindSchema($schema);

        // No access specification assumes write access
        $cases['basic'] = $binding->copy();

        $cases['valid'] = [$binding->copy()->valid(true)];

        $cases['invalid'] = [$binding->copy()->valid(false)];

        // Set a value
        $binding->value('20:10');
        $cases['value'] = $binding->copy();

        // Same result with explicit write access
        //
        $cases['value-write'] = [$binding->copy(), ['access' => 'write']];

        // Now test validation
        $validation = $binding->getDataProperty()->getValidation();
        $validation->set('minValue', '19:57');
        $validation->set('maxValue', '20:19');
        $cases['minmax'] = $binding->copy();

        // Now with view access
        $cases['minmax-view'] = [$binding->copy(), ['access' => 'view']];

        // Mask access
        $cases['minmax-mask'] = [$binding, ['access' => 'mask']];

        // Convert to hidden access
        $cases['hide'] = [$binding, ['access' => 'hide']];

        return self::normalizeCases($cases);
    }

    public static function html_FieldUrl() {
        $cases = [];

        $schema = new SchemaCollection(
            Schema::fromFile(__DIR__ . '/../../test-data/test-schema.json')
        );

        // Modify the schema to change test.text to a search
        $presentation = $schema->getProperty('test.text')->getPresentation();
        $presentation->type('url');
        $config = json_decode('{"type": "field","object": "test.text"}');
        $element = new FieldElement();
        $element->configure($config);
        $binding = Binding::fromElement($element);
        $binding->bindSchema($schema);

        // No access specification assumes write access
        $cases['basic'] = $binding;

        // Same result with explicit write access
        $cases['write'] = [$binding, ['access' => 'write']];

        // Test view access
        $cases['view'] = [$binding, ['access' => 'view']];

        // Mask access
        $cases['mask'] = [$binding, ['access' => 'mask']];

        // Test hidden access
        $cases['hide'] = [$binding, ['access' => 'hide']];

        $cases['valid'] = [$binding->copy()->valid(true)];

        $cases['invalid'] = [$binding->copy()->valid(false)];

        return self::normalizeCases($cases);
    }

    public static function html_FieldWeek() {
        $cases = [];

        $schema = new SchemaCollection(
            Schema::fromFile(__DIR__ . '/../../test-data/test-schema.json')
        );

        $presentation = $schema->getProperty('test.text')->getPresentation();
        $presentation->type('week');
        $config = json_decode('{"type": "field","object": "test.text"}');
        $element = new FieldElement();
        $element->configure($config);
        $binding = Binding::fromElement($element);
        $binding->bindSchema($schema);

        // No access specification assumes write access
        $cases['basic'] = $binding->copy();

        $cases['valid'] = [$binding->copy()->valid(true)];

        $cases['invalid'] = [$binding->copy()->valid(false)];

        // Set a value
        $binding->value('2010-W37');
        $cases['value'] = $binding->copy();

        // Same result with explicit write access
        $cases['value-write'] = [$binding->copy(), ['access' => 'write']];

        // Now test validation
        $validation = $binding->getDataProperty()->getValidation();
        $validation->set('minValue', '1957-W30');
        $validation->set('maxValue', '2099-W42');
        $cases['minmax'] = $binding->copy();

        // Now with view access
        $cases['minmax-view'] = [$binding->copy(), ['access' => 'view']];

        // Mask access
        $cases['minmax-mask'] = [$binding->copy(), ['access' => 'mask']];

        // Convert to hidden access
        $cases['hide'] = [$binding, ['access' => 'hide']];

        return self::normalizeCases($cases);
    }

    public static function html_Html() {
        $cases = [];

        $config = json_decode('{"type":"html","value":"<p>This is some raw html &amp;<\/p>"}');
        $element = new HtmlElement();
        $element->configure($config);
        $binding = Binding::fromElement($element);

        // No access specification assumes write access
        $cases['basic'] = $binding;

        // Same result with explicit write access
        $cases['write'] = [$binding, ['access' => 'write']];

        // Test view access
        $cases['view'] = [$binding, ['access' => 'view']];

        // Mask access
        $cases['mask'] = [$binding, ['access' => 'mask']];

        // Test hidden access
        $cases['hide'] = [$binding, ['access' => 'hide']];

        return self::normalizeCases($cases);
    }

    public static function html_Section() {
        $cases = [];

        $element = new SectionElement();
        $element->name('mysection');
        $binding = Binding::fromElement($element);

        // Start with an empty section
        $cases['empty'] = $binding->copy();

        // Add a label
        $binding->label('heading', 'This is legendary');
        $cases['label'] = $binding->copy();

        // Same for view access
        $cases['label-view'] = [$binding->copy(), ['access' => 'view']];

        // Mask access
        $cases['label-mask'] = [$binding->copy(), ['access' => 'mask']];

        // Same for hidden access
        $cases['label-hide'] = [$binding->copy(), ['access' => 'hide']];

        return self::normalizeCases($cases);
    }

    public static function html_Static() {
        $cases = [];

        $config = json_decode('{"type":"static","value":"This is unescaped text with <stuff>!"}');
        $element = new StaticElement();
        $element->configure($config);
        $binding = Binding::fromElement($element);

        // No access specification assumes write access
        $cases['basic'] = $binding->copy();

        // Add a heading
        $binding->label('heading', 'Header');
        $cases['head'] = $binding;

        // Same result with explicit write access
        $cases['write'] = [$binding, ['access' => 'write']];

        // Test view access
        $cases['view'] = [$binding, ['access' => 'view']];

        // Mask access
        $cases['mask'] = [$binding, ['access' => 'mask']];

        // Test hidden access
        $cases['hide'] = [$binding, ['access' => 'hide']];

        // Now with raw HTML
        $config = json_decode('{"type":"static","value":"This is <strong>raw html</strong>!","html":true}');
        $element = new StaticElement();
        $element->configure($config);
        $binding = Binding::fromElement($element);

        $cases['raw'] = $binding->copy();

        // Add a heading
        $binding->label('heading', 'Header');
        $cases['raw-head'] = $binding;

        return self::normalizeCases($cases);
    }

    public static function normalizeCases($cases) {
        foreach ($cases as $key => &$info) {
            if (!is_array($info)) {
                $info = [$info];
            }
            if (!isset($info[1])) {
                $info[1] = [];
            }
            if (!isset($info[2])) {
                $info[2] = $key;
            }
        }
        return $cases;
    }

}
