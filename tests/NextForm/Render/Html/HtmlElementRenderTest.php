<?php

namespace NextFormTests\Render\Html;

include_once __DIR__ . '/HtmlRenderFrame.php';

use Abivia\NextForm\Form\Binding\Binding;
use Abivia\NextForm\Render\Html\HtmlElementRender;
use Abivia\NextForm\Render\Block;
use Abivia\NextForm\Render\Html\Render;
use NextFormTests\Render\RenderCaseGenerator;

/**
 * @covers \Abivia\NextForm\Render\Html\HtmlElementRender
 */
class HtmlElementRenderTest extends HtmlRenderFrame
{
    public $testObj;

    public function setUp() : void
    {
        $this->testObj = new HtmlElementRender(new Render(), new Binding());
    }

    public static function setUpBeforeClass() : void
    {
        parent::setUpBeforeClass();
    }

    public static function tearDownAfterClass() : void
    {
        self::generatePage(__FILE__, new Render());
    }

	public function testInstantiation()
    {
		$this->assertInstanceOf(
            '\Abivia\NextForm\Render\Html\HtmlElementRender', $this->testObj
        );
	}

	public function testRenderNone()
    {
        $block = $this->testObj->render(['access' => 'none']);
		$this->assertEquals('', $block->getBody());
    }

    /**
     * Check the standard cases for a HTML element
     */
	public function testHtmlSuite()
    {
        $this->logMethod(__METHOD__);
        $cases = RenderCaseGenerator::html_Html();
        foreach ($cases as &$case) {
            $case[0] = new HtmlElementRender(new Render(), $case[0]);
        }

        $expect = [];

        $expect['basic'] = Block::fromString(
            '<p>This is some raw html &amp;</p>'
        );

        // Same result with explicit write access
        $expect['write'] = $expect['basic'];

        // Test view access
        $expect['view'] = $expect['basic'];

        // Test mask access
        $expect['mask'] = $expect['basic'];

        // Test hidden access
        $expect['hide'] = new Block();

        $this->runElementCases($cases, $expect);
    }

}
