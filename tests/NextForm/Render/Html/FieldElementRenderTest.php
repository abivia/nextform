<?php

namespace NextFormTests\Render\Html;

include_once __DIR__ . '/HtmlRenderFrame.php';

use Abivia\NextForm\Form\Binding\Binding;
use Abivia\NextForm\Render\Html\FieldElementRender;
use Abivia\NextForm\Render\Block;
use Abivia\NextForm\Render\Html\Render;
use NextFormTests\Render\RenderCaseGenerator;


class MockHtmlCheckbox {
    public function render($options) {
        return Block::fromString('[' . __CLASS__ . ']');
    }
}

class MockHtmlDefault {
    public function render($options) {
        return Block::fromString('[' . __CLASS__ . ']');
    }
}

/**
 * @covers \Abivia\NextForm\Render\Html\FieldElementRender
 */
class FieldElementRenderTest extends HtmlRenderFrame
{
    public static function setUpBeforeClass() : void
    {
        parent::setUpBeforeClass();
    }

    public static function tearDownAfterClass() : void
    {
        self::generatePage(__FILE__, new Render());
    }

    public function testDataList()
    {
        // Get the standard test cases
        $cases = RenderCaseGenerator::html_FieldTextDataList();

        // Create a renderer from the basic case
        $obj = new FieldElementRender(new Render(), $cases['basic'][0]);

        // Set the required attributes
        $attrs = new \Abivia\NextForm\Render\Attributes('id', 'list_test_1');

        $block = $obj->dataList($attrs, 'text', []);
        $expect = Block::fromString(
            '<datalist id="list_test_1_list">' . "\n"
            . '<option value="textlist 1"/>' . "\n"
            . '<option value="textlist 2"'
            . ' data-nf-group="[&quot;grpX&quot;]"/>' . "\n"
            . '<option value="textlist 3"'
            . ' data-nf-name="tl3"/>' . "\n"
            . '<option value="textlist 4" data-nf-sidecar="[1,2,3,4]"/>' . "\n"
            . '</datalist>' . "\n"
        );
		$this->assertEquals($expect, $block);
    }

	public function testRenderCheckbox()
    {
        // Get the standard test cases
        $cases = RenderCaseGenerator::html_FieldCheckbox();

        // Create a renderer from the basic case
        $obj = new FieldElementRender(new Render(), $cases['basic'][0]);

        // Make it use our mock for the render operation
        $obj->setFieldHandler('checkbox', MockHtmlCheckbox::class);

        $block = $obj->render();
		$this->assertEquals(
            '[NextFormTests\Render\Html\MockHtmlCheckbox]',
            $block->getBody()
        );
    }

	public function testRenderDefault()
    {
        // Get the standard test cases
        $cases = RenderCaseGenerator::html_FieldTel();

        // Create a renderer from the basic case
        $obj = new FieldElementRender(new Render(), $cases['basic'][0]);

        // Make it use our mock for the render operation
        $obj->setFieldHandler('tel', MockHtmlDefault::class);

        $block = $obj->render();
		$this->assertEquals(
            '[NextFormTests\Render\Html\MockHtmlDefault]',
            $block->getBody()
        );
    }

	public function testRenderDefaultConfirm()
    {
        // Get the standard test cases
        $cases = RenderCaseGenerator::html_FieldEmail();

        // Create a renderer from the "confirm" case
        $obj = new FieldElementRender(new Render(), $cases['confirm'][0]);

        // Make it use our mock for the render operation
        $obj->setFieldHandler('email', MockHtmlDefault::class);

        $block = $obj->render();
		$this->assertEquals(
            '[NextFormTests\Render\Html\MockHtmlDefault]'
            . '[NextFormTests\Render\Html\MockHtmlDefault]',
            $block->getBody()
        );
    }

}
