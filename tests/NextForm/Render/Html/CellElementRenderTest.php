<?php

namespace NextFormTests\Render\Html;

include_once __DIR__ . '/HtmlRenderFrame.php';

use Abivia\NextForm\Form\Binding\Binding;
use Abivia\NextForm\Form\Element\CellElement;
use Abivia\NextForm\Render\Html\CellElementRender;
use Abivia\NextForm\Render\Block;
use Abivia\NextForm\Render\Html\Render;
use NextFormTests\Render\RenderCaseGenerator;

/**
 * @covers \Abivia\NextForm\Render\Html\CellElementRender
 */
class CellElementRenderTest extends HtmlRenderFrame
{
    public $testObj;

    public function setUp() : void
    {
        $this->testObj = new CellElementRender(new Render(), new Binding());
    }

    public static function setUpBeforeClass() : void {
        parent::setUpBeforeClass();
    }

    public static function tearDownAfterClass() : void
    {
        self::generatePage(__FILE__, new Render());
    }

	public function testInstantiation()
    {
		$this->assertInstanceOf(
            '\Abivia\NextForm\Render\Html\CellElementRender', $this->testObj
        );
	}

	public function testContext() {
        $this->logMethod(__METHOD__);
        $render = new Render();
        $element = new CellElement();
        $binding = Binding::fromElement($element);
        $obj = new CellElementRender($render, $binding);
        $this->assertFalse($render->queryContext('inCell'));
        $block = $obj->render();
        $this->assertTrue($render->queryContext('inCell'));
        $block->close();
        $this->assertFalse($render->queryContext('inCell'));
    }

	public function testContextHidden() {
        $this->logMethod(__METHOD__);
        $render = new Render();
        $element = new CellElement();
        $binding = Binding::fromElement($element);
        $obj = new CellElementRender($render, $binding);
        $this->assertFalse($render->queryContext('inCell'));
        $block = $obj->render(['access' => 'hide']);
        $this->assertTrue($render->queryContext('inCell'));
        $block->close();
        $this->assertFalse($render->queryContext('inCell'));
    }

	public function testRenderNone()
    {
        $block = $this->testObj->render(['access' => 'none']);
		$this->assertEquals('', $block->getBody());
    }

    /**
     * Check the standard cases for a static element
     */
	public function testCellSuite() {
        $this->logMethod(__METHOD__);
        $cases = RenderCaseGenerator::html_Cell();
        foreach ($cases as &$case) {
            $case[0] = new CellElementRender(new Render(), $case[0]);
        }

        $expect = [];

        $expect['basic'] = Block::fromString(
            '<div id="cell_1_container" data-nf-for="cell_1">' . "\n"
            . '<div>' . "\n",
            '</div>' . "\n" . '</div>' . "\n"
        );

        $this->runElementCases($cases, $expect);
    }

}
