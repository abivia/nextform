<?php

namespace NextFormTests\Render;

include_once __DIR__ . '/SimpleHtmlRenderFrame.php';

use Abivia\NextForm\NextForm;
use Abivia\NextForm\Form\Binding\Binding;
use Abivia\NextForm\Form\Element\CellElement;
use Abivia\NextForm\Render\Attributes;
use Abivia\NextForm\Render\Block;
use Abivia\NextForm\Render\Html\SimpleHtml\Render;
use NextFormTests\Render\Html\SimpleHtml\SimpleHtmlRenderFrame;
use NextFormTests\Test_Tools\HtmlTestLogger;

/**
 * @covers \Abivia\NextForm\Render\Html\SimpleHtml
 */
class SimpleHorizontalTest extends SimpleHtmlRenderFrame {
    use HtmlTestLogger;

    protected $emptyLabel;
    protected $testObj;

    protected function setUp() : void {
        NextForm::boot();
        $this->testObj = new Render();
        $this->testObj->show()->set('layout:hor:20%:40%');
    }

    public static function setUpBeforeClass() : void {
        parent::setUpBeforeClass();
        self::$defaultFormGroupClass = '';
    }

    public static function tearDownAfterClass() : void
    {
        self::generatePage(__FILE__, new Render());
    }

	public function testInstantiation() {
		$this->assertInstanceOf(Render::class, $this->testObj);
	}

	public function testCell() {
        $this->logMethod(__METHOD__);
        $cases = RenderCaseGenerator::html_Cell();

        $expect['basic'] = Block::fromString(
            '<div id="cell_1_container" data-nf-for="cell_1">' . "\n"
            . '<div style="display:inline-block; vertical-align:top;'
            . ' width:20%">&nbsp;</div>' . "\n"
            . '<div style="display:inline-block; vertical-align:top;'
            . ' width:40%">' . "\n",
            '</div>' . "\n" . '</div>' . "\n"
        );

        $this->runCases($cases, $expect);
    }

	public function testCellContext() {
        $this->logMethod(__METHOD__);
        $element = new CellElement();
        $binding = Binding::fromElement($element);
        $this->assertFalse($this->testObj->queryContext('inCell'));
        $this->testObj->render($binding);
        $this->assertTrue($this->testObj->queryContext('inCell'));
    }

    /**
     * @doesNotPerformAssertions
     */
	public function testSetOptions() {

        $this->testObj->useOptions();
    }

}
