<?php

namespace NextFormTests\Render;

use Abivia\NextForm\NextForm;
use Abivia\NextForm\Form\Binding\FieldBinding;
use Abivia\NextForm\Render\Attributes;
use Abivia\NextForm\Render\Block;
use Abivia\NextForm\Render\Html\SimpleHtml\Render;
use NextFormTests\Render\Html\SimpleHtml\SimpleHtmlRenderFrame;

include_once __DIR__ . '/SimpleHtmlRenderFrame.php';

/**
 * @covers \Abivia\NextForm\Render\Html\SimpleHtml
 */
class SimpleTest extends SimpleHtmlRenderFrame {

    protected function setUp() : void {
        NextForm::boot();
        $this->testObj = new Render();
    }

    public static function setUpBeforeClass() : void {
        parent::setUpBeforeClass();
         self::$defaultFormGroupClass = '';
   }

    public static function tearDownAfterClass() : void
    {
        self::generatePage(__FILE__, new Render());
    }

	public function testInstantiation()
    {
		$this->assertInstanceOf(Render::class, $this->testObj);
	}

    public function testEpilog()
    {
        $block = $this->testObj->epilog();
        $this->assertEquals("<br/>\n", $block->getBody());

        $this->testObj->context('inCell', true);
        $block = $this->testObj->epilog();
        $this->assertEquals('&nbsp;', $block->getBody());
    }

    public function testRenderTriggers()
    {
        $block = $this->testObj->renderTriggers(new FieldBinding());
        $this->assertInstanceof('\Abivia\NextForm\Render\Block', $block);
    }

    public function testOptions()
    {
        // Yes this is just a stub.
        $this->testObj->useOptions([]);
        $this->assertTrue(true);
    }

    /**
     * Exercise the protected checkShowState() function by introducing
     * a nonstandard scope
     */
    public function testCheckShowState()
    {
        $this->testObj->show()->doCellspacing('new*state', 'a', ['1']);
        $attrs = $this->testObj->show()->get('new*state', 'cellspacing');
        $style = $attrs->get('style');
        $this->assertEquals('1rem', $style['padding-left']);
    }

    public function testShowDoCellspacing()
    {
        $this->testObj->show()->doCellspacing('form', 'a', ['1']);
        $attrs = $this->testObj->show()->get('form', 'cellspacing');
        $style = $attrs->get('style');
        $this->assertEquals('1rem', $style['padding-left']);

        $this->testObj->show()->doCellspacing('form', 'b', ['sm-1', 'sh-md-2', 'b4-md-5']);
        $attrs = $this->testObj->show()->get('form', 'cellspacing');
        $style = $attrs->get('style');
        $this->assertEquals('1.6rem', $style['padding-left']);
    }

    public function testShowDoLayout()
    {
        $this->testObj->show()->doLayout('form', 'horizontal', ['h']);
        $attrs = $this->testObj->show()->get('form', 'cellspacing');
        $style = $attrs->get('style');
        $this->assertEquals('1.2rem', $style['padding-left']);

        $this->testObj->show()->doLayout('form', 'vertical', ['v']);
        $attrs = $this->testObj->show()->get('form', 'cellspacing');
        $style = $attrs->get('style');
        $this->assertEquals('1.2rem', $style['padding-left']);
    }

    public function testShowDoLayoutAnyHorizontal()
    {
        $this->testObj->show()->doLayoutAnyHorizontal('form', ['h']);
        $attrs = $this->testObj->show()->get('form', 'headingAttributes');
        $style = $attrs->get('style');
        $this->assertEquals('25%', $style['width']);
        $attrs = $this->testObj->show()->get('form', 'inputWrapperAttributes');
        $style = $attrs->get('style');
        $this->assertEquals('75%', $style['width']);

    }

    public function testShowDoLayoutAnyHorizontalCss1()
    {
        $this->testObj->show()->doLayoutAnyHorizontal('form', ['h', '100px']);
        $attrs = $this->testObj->show()->get('form', 'headingAttributes');
        $style = $attrs->get('style');
        $this->assertEquals('100px', $style['width']);
        $attrs = $this->testObj->show()->get('form', 'inputWrapperAttributes');
        $this->assertEquals(null, $attrs);

    }

    public function testShowDoLayoutAnyHorizontalClass1()
    {
        $this->testObj->show()->doLayoutAnyHorizontal('form', ['h', '.foo']);
        $attrs = $this->testObj->show()->get('form', 'headingAttributes');
        $class = $attrs->get('class');
        $this->assertEquals('foo', $class[0]);
        $attrs = $this->testObj->show()->get('form', 'inputWrapperAttributes');
        $this->assertEquals(null, $attrs);

    }

    public function testShowDoLayoutAnyHorizontalCss2()
    {
        $this->testObj->show()->doLayoutAnyHorizontal('form', ['h', '100px', '300px']);
        $attrs = $this->testObj->show()->get('form', 'headingAttributes');
        $style = $attrs->get('style');
        $this->assertEquals('100px', $style['width']);
        $attrs = $this->testObj->show()->get('form', 'inputWrapperAttributes');
        $style = $attrs->get('style');
        $this->assertEquals('300px', $style['width']);

    }

    public function testShowDoLayoutAnyHorizontalClass2()
    {
        $this->testObj->show()->doLayoutAnyHorizontal('form', ['h', '.foo', '.bar']);
        $attrs = $this->testObj->show()->get('form', 'headingAttributes');
        $class = $attrs->get('class');
        $this->assertEquals('foo', $class[0]);
        $attrs = $this->testObj->show()->get('form', 'inputWrapperAttributes');
        $class = $attrs->get('class');
        $this->assertEquals('bar', $class[0]);

    }

    public function testShowDoLayoutAnyHorizontalRatio2()
    {
        $this->testObj->show()->doLayoutAnyHorizontal('form', ['h', '1', '2.5']);
        $attrs = $this->testObj->show()->get('form', 'headingAttributes');
        $style = $attrs->get('style');
        $this->assertEquals('28.571%', $style['width']);
        $attrs = $this->testObj->show()->get('form', 'inputWrapperAttributes');
        $style = $attrs->get('style');
        $this->assertEquals('71.429%', $style['width']);

        $this->expectException('\RuntimeException');
        $this->testObj->show()->doLayoutAnyHorizontal('form', ['h', '1', '0']);
    }

    public function testShowDoLayoutAnyHorizontalRatio3()
    {
        $this->testObj->show()->doLayoutAnyHorizontal('form', ['h', '1', '2', '5']);
        $attrs = $this->testObj->show()->get('form', 'headingAttributes');
        $style = $attrs->get('style');
        $this->assertEquals('20%', $style['width']);
        $attrs = $this->testObj->show()->get('form', 'inputWrapperAttributes');
        $style = $attrs->get('style');
        $this->assertEquals('40%', $style['width']);

    }

    public function testShowDoLayoutAnyVertical()
    {
        $this->testObj->show()->doLayoutAnyVertical('form', ['v']);
        $attrs = $this->testObj->show()->get('form', 'inputWrapperAttributes');
        $this->assertEquals(null, $attrs);

    }

    public function testShowDoLayoutAnyVerticalCss1()
    {
        $this->testObj->show()->doLayoutAnyVertical('form', ['v', '500px']);
        $attrs = $this->testObj->show()->get('form', 'inputWrapperAttributes');
        $style = $attrs->get('style');
        $this->assertEquals('500px', $style['width']);

    }

    public function testShowDoLayoutAnyVerticalClass1()
    {
        $this->testObj->show()->doLayoutAnyVertical('form', ['v', '.foo']);
        $attrs = $this->testObj->show()->get('form', 'inputWrapperAttributes');
        $class = $attrs->get('class');
        $this->assertEquals('foo', $class[0]);

    }

    public function testShowDoLayoutAnyVerticalRatio2()
    {
        $this->testObj->show()->doLayoutAnyVertical('form', ['v', '6', '10']);
        $attrs = $this->testObj->show()->get('form', 'inputWrapperAttributes');
        $style = $attrs->get('style');
        $this->assertEquals('60%', $style['width']);

        $this->expectException('\RuntimeException');
        $this->testObj->show()->doLayoutAnyVertical('form', ['v', '1', '0']);
    }

    public function emptyToken() {
        return ['', ''];
    }

    public function fooToken() {
        return ['_nf_token', 'foo'];
    }

    public function georgeToken() {
        return ['george', 'foo'];
    }

	public function testStart()
    {
        $attrs = new Attributes();
        $attrs->set('id', 'form_1');
        $attrs->set('name', 'form_1');
        NextForm::csrfGenerator([$this, 'emptyToken']);
        $data = $this->testObj->start(['attributes' => $attrs]);
        $this->assertEquals("<form id=\"form_1\" name=\"form_1\" method=\"post\">\n", $data->getBody());
        $this->assertEquals("</form>\n", $data->getPost());

        NextForm::csrfGenerator([$this, 'fooToken']);
        NextForm::generateCsrfToken();
        $data = $this->testObj->start(['attributes' => $attrs]);
        $this->assertEquals(
            "<form id=\"form_1\" name=\"form_1\" method=\"post\">\n"
            . '<input id="_nf_token" name="_nf_token" type="hidden" value="foo">' . "\n",
            $data->getBody()
        );

        NextForm::csrfGenerator([$this, 'georgeToken']);
        $data = $this->testObj->start(['attributes' => $attrs]);
        $this->assertEquals(
            "<form id=\"form_1\" name=\"form_1\" method=\"post\">\n"
            . '<input id="george" name="george" type="hidden" value="foo">' . "\n",
            $data->getBody()
        );

        NextForm::csrfGenerator([$this, 'emptyToken']);
        $data = $this->testObj->start(['attributes' => $attrs, 'method' => 'put']);
        $this->assertEquals("<form id=\"form_1\" name=\"form_1\" method=\"put\">\n", $data->getBody());

        $data = $this->testObj->start(
            ['action' => 'https://localhost/some file.php', 'attributes' => $attrs]
        );
        $this->assertEquals(
            "<form id=\"form_1\" name=\"form_1\" action=\"https://localhost/some file.php\" method=\"post\">\n",
            $data->getBody()
        );

        $attrs->set('name', 'bad<name');
        $data = $this->testObj->start(['attributes' => $attrs]);
        $this->assertEquals(
            "<form id=\"form_1\" name=\"bad&lt;name\""
            . " action=\"https://localhost/some"
            . " file.php\" method=\"post\">\n",
            $data->getBody()
        );

    }

    /**
     * @doesNotPerformAssertions
     */
	public function testSetOptions() {

        $this->testObj->useOptions();
    }

}
