<?php

namespace NextFormTests\Render\Html\FieldElementRender;

include_once __DIR__ . '/../SimpleHtmlRenderFrame.php';

use Abivia\NextForm\Render\Html\FieldElementRender;
use Abivia\NextForm\Render\Block;
use Abivia\NextForm\Render\Html\SimpleHtml\Render;
use NextFormTests\Render\RenderCaseGenerator;
use NextFormTests\Render\Html\SimpleHtml\SimpleHtmlRenderFrame;

/**
 * @covers \Abivia\NextForm\Render\Html\FieldElementRender\Html
 * @covers \Abivia\NextForm\Render\Html\FieldElementRender\Html
 */
class FieldElementHtmlTest
extends SimpleHtmlRenderFrame
{
    public $render;

    public function setUp() : void
    {
        $this->render = new Render();
    }

    public static function setUpBeforeClass() : void {
        parent::setUpBeforeClass();
    }

    public static function tearDownAfterClass() : void
    {
        self::generatePage(__FILE__, new Render());
    }

    /**
     * Check textarea element, Horizontal layout
     */
	public function testHtmlSuiteHorizontal()
    {
        $this->logMethod(__METHOD__);
        $this->setMode('h');

        $cases = RenderCaseGenerator::html_FieldHtml();
        foreach ($cases as &$case) {
            $case[0] = new FieldElementRender($this->render, $case[0]);
        }

        $expect = [];

        $expect['basic'] = Block::fromString(
            $this->formGroup(
                $this->column1('')
                . '<span>Some HTML.</span>' . "\n"
            )
        );

        // Same result with explicit write access
        $expect['write'] = $expect['basic'];

        // Test view access
        $expect['view'] = Block::fromString(
            $this->formGroup(
                $this->column1('')
                . '<span>Some HTML.</span>' . "\n"
            )
        );

        // Test mask access
        $expect['mask'] = Block::fromString(
            $this->formGroup(
                $this->column1('')
                . '*****' . "\n"
            )
        );

        // Test hidden access
        $expect['hide'] = Block::fromString('');

        $expect['valid'] = Block::fromString(
            $this->formGroup(
                $this->column1('')
                . '<span>Some HTML.</span>' . "\n"
            )
        );

        $expect['invalid'] = Block::fromString(
            $this->formGroup(
                $this->column1('')
                . '<span>Some HTML.</span>' . "\n"
            )
        );

        // Label cases
        $expect['label-none'] = $expect['basic'];
        $expect['label-after'] = $expect['basic'];
        $expect['label-before'] = $expect['basic'];
        $expect['label-help'] = $expect['basic'];

        $expect['label-head'] = Block::fromString(
            $this->formGroup(
                $this->column1('Header')
                . '<span>Some HTML.</span>' . "\n"
            )
        );

        $expect['label-all'] = Block::fromString(
            $this->formGroup(
                $this->column1('Header')
                . '<span>Some HTML.</span>' . "\n"
            )
        );

        $this->runElementCases($cases, $expect);
    }

    /**
     * Check textarea element, Vertical layout
     */
	public function testHtmlSuiteVertical()
    {
        $this->logMethod(__METHOD__);
        $this->setMode('v');

        $cases = RenderCaseGenerator::html_FieldHtml();
        foreach ($cases as &$case) {
            $case[0] = new FieldElementRender($this->render, $case[0]);
        }

        $expect = [];

        $expect['basic'] = Block::fromString(
            $this->formGroup(
                '<span>Some HTML.</span>' . "\n"
            )

        );

        // Same result with explicit write access
        $expect['write'] = $expect['basic'];

        // Test view access
        $expect['view'] = Block::fromString(
            $this->formGroup(
                '<span>Some HTML.</span>' . "\n"
            )
        );

        // Test mask access
        $expect['mask'] = Block::fromString(
            $this->formGroup(
                '*****' . "\n"
            )
        );

        // Test hidden access
        $expect['hide'] = Block::fromString('');

        $expect['valid'] = Block::fromString(
            $this->formGroup(
                '<span>Some HTML.</span>' . "\n"
            )

        );

        $expect['invalid'] = Block::fromString(
            $this->formGroup(
                '<span>Some HTML.</span>' . "\n"
            )

        );

        // Label cases
        $expect['label-none'] = $expect['basic'];
        $expect['label-after'] = $expect['basic'];
        $expect['label-before'] = $expect['basic'];
        $expect['label-help'] = $expect['basic'];

        $expect['label-head'] = Block::fromString(
            $this->formGroup(
                '<label for="field_1">Header</label>' . "\n"
                . '<span>Some HTML.</span>' . "\n"
            )
        );

        $expect['label-all'] = Block::fromString(
            $this->formGroup(
                '<label for="field_1">Header</label>' . "\n"
                . '<span>Some HTML.</span>' . "\n"
            )
        );

        $this->runElementCases($cases, $expect);
    }

}
