<?php

namespace NextFormTests\Render\Html\FieldElementRender;

include_once __DIR__ . '/../SimpleHtmlRenderFrame.php';

use Abivia\NextForm\Render\Html\FieldElementRender;
use Abivia\NextForm\Render\Block;
use Abivia\NextForm\Render\Html\SimpleHtml\Render;
use NextFormTests\Render\RenderCaseGenerator;
use NextFormTests\Render\Html\SimpleHtml\SimpleHtmlRenderFrame;

/**
 * @covers \Abivia\NextForm\Render\Html\FieldElementRender\Common
 * @covers \Abivia\NextForm\Render\Html\FieldElementRender\Common
 */
class FieldElementRangeTest
extends SimpleHtmlRenderFrame
{
    public $render;

    public function setUp() : void
    {
        $this->render = new Render();
    }

    public static function setUpBeforeClass() : void {
        parent::setUpBeforeClass();
    }

    public static function tearDownAfterClass() : void
    {
        self::generatePage(__FILE__, new Render());
    }

    /**
     * Check range element, Horizontal layout
     */
	public function testRangeSuiteHorizontal()
    {
        $this->logMethod(__METHOD__);
        $this->setMode('h');

        $cases = RenderCaseGenerator::html_FieldRange();
        foreach ($cases as &$case) {
            $case[0] = new FieldElementRender($this->render, $case[0]);
        }

        $expect = [];

        $expect['basic'] = Block::fromString(
            $this->formGroup(
                $this->column1('')
                . $this->column2(
                    '<input id="field_1" name="field_1" type="range" value="200"/>' . "\n"
                )
            )
            . '<br/>' . "\n"
        );

        // Making the field required should have no effect
        $expect['required'] = $expect['basic'];

        // Set minimum/maximum values
        $expect['minmax'] = Block::fromString(
            $this->formGroup(
                $this->column1('')
                . $this->column2(
                    '<input id="field_1" name="field_1" type="range"'
                    . ' value="200"'
                    . ' min="-1000" max="999.45"/>' . "\n"
                )
            )
            . '<br/>' . "\n"
        );

        // Add a step
        $expect['step'] = Block::fromString(
            $this->formGroup(
                $this->column1('')
                . $this->column2(
                    '<input id="field_1" name="field_1" type="range"'
                    . ' value="200"'
                    . ' min="-1000" max="999.45" step="20"/>' . "\n"
                )
            )
            . '<br/>' . "\n"
        );

        // Settng a pattern should have no effect!
        $expect['pattern'] = $expect['step'];


        // Test view access
        $expect['view'] = Block::fromString(
            $this->formGroup(
                $this->column1('')
                . $this->column2(
                    '<input id="field_1" name="field_1" type="text"'
                    . ' value="200" readonly/>' . "\n"
                )
            )
            . '<br/>' . "\n"
        );

        // Mask access
        $expect['mask'] = Block::fromString(
            $this->formGroup(
                $this->column1('')
                . $this->column2(
                    '<input id="field_1" name="field_1" type="text"'
                    . ' value="*****" disabled readonly/>' . "\n"
                )
            )
            . "<br/>\n"
        );

        $expect['valid'] = Block::fromString(
            $this->formGroup(
                $this->column1('')
                . $this->column2(
                    '<input id="field_1" name="field_1" type="range"'
                    . ' class="nf-valid" value="200"/>' . "\n"
                )
            )
            . '<br/>' . "\n"
        );

        $expect['invalid'] = Block::fromString(
            $this->formGroup(
                $this->column1('')
                . $this->column2(
                    '<input id="field_1" name="field_1" type="range"'
                    . ' class="nf-invalid" value="200"/>' . "\n"
                )
            )
            . '<br/>' . "\n"
        );

        $this->runElementCases($cases, $expect);
    }

    /**
     * Check range element, Vertical layout
     */
	public function testRangeSuiteVertical()
    {
        $this->logMethod(__METHOD__);
        $this->setMode('v');

        $cases = RenderCaseGenerator::html_FieldRange();
        foreach ($cases as &$case) {
            $case[0] = new FieldElementRender($this->render, $case[0]);
        }

        $expect = [];

        $expect['basic'] = Block::fromString(
            $this->formGroup(
                '<input id="field_1" name="field_1" type="range" value="200"/>' . "\n"
            )
            . '<br/>' . "\n"
        );

        // Making the field required should have no effect
        $expect['required'] = $expect['basic'];

        // Set minimum/maximum values
        $expect['minmax'] = Block::fromString(
            $this->formGroup(
                '<input id="field_1" name="field_1" type="range"'
                . ' value="200"'
                . ' min="-1000" max="999.45"/>' . "\n"
            )
            . '<br/>' . "\n"
        );

        // Add a step
        $expect['step'] = Block::fromString(
            $this->formGroup(
                '<input id="field_1" name="field_1" type="range"'
                . ' value="200"'
                . ' min="-1000" max="999.45" step="20"/>' . "\n"
            )
            . '<br/>' . "\n"
        );

        // Settng a pattern should have no effect!
        $expect['pattern'] = $expect['step'];


        // Test view access
        $expect['view'] = Block::fromString(
            $this->formGroup(
                '<input id="field_1" name="field_1" type="text"'
                . ' value="200" readonly/>' . "\n"
            )
            . '<br/>' . "\n"
        );

        // Mask access
        $expect['mask'] = Block::fromString(
            $this->formGroup(
                '<input id="field_1" name="field_1" type="text"'
                . ' value="*****" disabled readonly/>' . "\n"
            )
            . '<br/>' . "\n"
        );

        $expect['valid'] = Block::fromString(
            $this->formGroup(
                '<input id="field_1" name="field_1" type="range"'
                . ' class="nf-valid" value="200"/>' . "\n"
            )
            . '<br/>' . "\n"
        );

        $expect['invalid'] = Block::fromString(
            $this->formGroup(
                '<input id="field_1" name="field_1" type="range"'
                . ' class="nf-invalid" value="200"/>' . "\n"
            )
            . '<br/>' . "\n"
        );

        $this->runElementCases($cases, $expect);
    }

}
