<?php

namespace NextFormTests\Render\Html\Bootstrap\Bs4;

include_once __DIR__ . '/Bootstrap4RenderFrame.php';

use Abivia\NextForm\NextForm;
//use Abivia\NextForm\Data\Property;
use Abivia\NextForm\Form\Binding\Binding;
use Abivia\NextForm\Form\Element\CellElement;
use Abivia\NextForm\Render\Block;
use Abivia\NextForm\Render\Html\Bootstrap\Bs4\Render;
use NextFormTests\Render\RenderCaseGenerator;
use NextFormTests\Render\RenderCaseRunner;
use NextFormTests\Test_Tools\HtmlTestLogger;

/**
 * @covers \Abivia\NextForm\Render\Html\Bootstrap\Bs4\Render
 */
class Bootstrap4HorizontalTest extends Bootstrap4RenderFrame
{
    use HtmlTestLogger;
    use RenderCaseRunner;

    protected $testObj;

    protected function setUp() : void
    {
        NextForm::boot();
        $this->testObj = new Render();
        $this->testObj->show()->set('layout:horizontal:2:10');
    }

    public static function setUpBeforeClass() : void
    {
        parent::setUpBeforeClass();
        self::$defaultFormGroupClass = 'form-group row';
    }

    public static function tearDownAfterClass() : void
    {
        self::generatePage(__FILE__, new Render());
    }

	public function testInstantiation()
    {
		$this->assertInstanceOf(Render::class, $this->testObj);
	}

	public function testCell() {
        $this->logMethod(__METHOD__);
        $cases = RenderCaseGenerator::html_Cell();

        $expect['basic'] = Block::fromString(
            '<div id="cell_1_container" class="form-group px-1 row"'
            . ' data-nf-for="cell_1">' . "\n"
            . '<div class="col-form-label col-sm-2 text-md-right">'
            . '&nbsp;</div>' . "\n"
            . '<div class="col-sm-10 form-row">' . "\n",
            '</div>' . "\n" . '</div>' . "\n"
        );

        $this->runCases($cases, $expect);
    }

	public function testCellContext() {
        $this->logMethod(__METHOD__);
        $element = new CellElement();
        $binding = Binding::fromElement($element);
        $this->assertFalse($this->testObj->queryContext('inCell'));
        $this->testObj->render($binding);
        $this->assertTrue($this->testObj->queryContext('inCell'));
    }

    /**
     * @doesNotPerformAssertions
     */
	public function testSetOptions() {

        $this->testObj->useOptions();
    }

}
