<?php

declare(strict_types=1);

namespace NextFormTests\Render;

if (!defined('NF_TEST_ROOT')) {
    define('NF_TEST_ROOT', dirname(__DIR__, 5) . '/');
}

include_once __DIR__ . '/../../../RenderCaseGenerator.php';
include_once __DIR__ . '/../../../RenderCaseRunner.php';
include_once NF_TEST_ROOT . 'test-tools/Page.php';

use Abivia\NextForm\NextForm;
use Abivia\NextForm\Render\Attributes;
use Abivia\NextForm\Render\Block;
use Abivia\NextForm\Render\Html\Bootstrap\Bs4\Render;
use NextFormTests\Test_Tools\Page;

/**
 * Test functionality related to show settings.
 *
 * @covers \Abivia\NextForm\Render\Html\Bootstrap\Bs4
 */
class Bootstrap4ShowTest extends \PHPUnit\Framework\TestCase {

    protected $testObj;

    protected function setUp() : void
    {
        NextForm::boot();
        $this->testObj = new Render();
    }

	public function testCellSpacingDefault()
    {
        $show = $this->testObj->show();
        // First time: make sure default exists and that it contains Attributes
        $this->assertTrue($show->isset('form','cellspacing'));
        $cs = $show->get('form','cellspacing');
        $this->assertInstanceOf('\Abivia\NextForm\Render\Attributes', $cs);

        // Then check for the value we want
        $classes = implode(' ', $cs->get('class'));
        $this->assertEquals('ml-3', $classes);
        return;
    }

	public function testCellSpacingDigit()
    {
        // Change the value
        $this->testObj->show()->set('cellspacing:1');
        $show = $this->testObj->show();
        $classes = implode(' ', $show->get('form', 'cellspacing')->get('class'));
        $this->assertEquals('ml-1', $classes);

        // Change it again just to be sure
        $this->testObj->show()->set('cellspacing:4');
        $show = $this->testObj->show();
        $classes = implode(' ', $show->get('form', 'cellspacing')->get('class'));
        $this->assertEquals('ml-4', $classes);

        // Make sure a value for a different renderer changes nothing.
        $this->testObj->show()->set('cellspacing:xx-xs-2');
        $show = $this->testObj->show();
        $classes = implode(' ', $show->get('form', 'cellspacing')->get('class'));
        $this->assertEquals('ml-4', $classes);

        // Test out of range value changes nothing
        $this->testObj->show()->set('cellspacing:9');
        $show = $this->testObj->show();
        $classes = implode(' ', $show->get('form', 'cellspacing')->get('class'));
        $this->assertEquals('ml-4', $classes);
    }

	public function testCellSpacingResponsive()
    {
        // Change the value
        $this->testObj->show()->set('cellspacing:sm-1:md-3:xx-lg-4:b4-xl-2');
        $show = $this->testObj->show();
        $classes = implode(' ', $show->get('form', 'cellspacing')->get('class'));
        $this->assertEquals('ml-sm-1 ml-md-3 ml-xl-2', $classes);

        // Test unrecognized scheme
        $this->testObj->show()->set('cellspacing:qq-lg-3');
        $show = $this->testObj->show();
        $classes = implode(' ', $show->get('form', 'cellspacing')->get('class'));
        $this->assertEquals('ml-sm-1 ml-md-3 ml-xl-2', $classes);

        // Test invalid
        $this->expectException('RuntimeException');
        $this->testObj->show()->set('cellspacing:xx');
    }

    public function testShowInForm() {
        NextForm::csrfGenerator([Page::class, 'fixedToken']);
        NextForm::boot();
        $manager = new NextForm();
        $manager->useOptions(
            [
                'segmentNameMode' => 'off',
                'wire' => [
                    'Render' => Render::class,
                ],
            ]
        );
        $manager->addForm(
            dirname(__DIR__, 3) . '/login-form.json',
            ['action' => 'http://localhost/nextform/post.php']
        );
        $manager->addSchema(dirname(__DIR__, 3) . '/login-schema.json');

        $html = $manager->generate();

        // Write a log file if the folder exists.
        if (file_exists(NF_TEST_ROOT . 'logs')) {
            $relDir = substr(__DIR__, strlen(NF_TEST_ROOT));
            $logName = pathinfo(__FILE__, PATHINFO_FILENAME)
                . '.' . __FUNCTION__;
            $logFile = NF_TEST_ROOT . 'logs/' . $relDir . '/'
                . $logName . '.html';
            file_put_contents($logFile, Page::write($logName, $html));
        }

        $this->assertEquals(
            file_get_contents(dirname(__DIR__, 3) . '/login-expect.html'),
            $html->getBody()
        );
    }

}
