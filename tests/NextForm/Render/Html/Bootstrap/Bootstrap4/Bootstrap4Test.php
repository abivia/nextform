<?php

namespace NextFormTests\Render\Html\Bootstrap\Bs4;

include_once __DIR__ . '/Bootstrap4RenderFrame.php';

use Abivia\NextForm\NextForm;
use Abivia\NextForm\Form\Binding\Binding;
use Abivia\NextForm\Form\Element\CellElement;
use Abivia\NextForm\Render\Attributes;
use Abivia\NextForm\Render\Block;
use Abivia\NextForm\Render\Html\Bootstrap\Bs4\Render;
use NextFormTests\Render\RenderCaseGenerator;

/**
 * @covers \Abivia\NextForm\Render\Html\Bootstrap\Bs4
 */
class Bootstrap4Test extends Bootstrap4RenderFrame {

    protected function setUp() : void
    {
        NextForm::boot();
        $this->testObj = new Render();
        $this->testObj->show()->set('layout:vertical:10');
    }

    public static function setUpBeforeClass() : void
    {
        parent::setUpBeforeClass();
        self::$defaultFormGroupClass = 'form-group col-sm-10';
    }

    public static function tearDownAfterClass() : void
    {
        self::generatePage(__FILE__, new Render());
    }

	public function testInstantiation() {
		$this->assertInstanceOf(Render::class,
            $this->testObj
        );
	}

    public function emptyToken() {
        return ['', ''];
    }

    public function fooToken() {
        return ['_nf_token', 'foo'];
    }

    public function georgeToken() {
        return ['george', 'foo'];
    }

	public function testStart() {
        $attrs = new Attributes();
        $attrs->set('id', 'form_1');
        $attrs->set('name', 'form_1');

        NextForm::csrfGenerator([$this, 'emptyToken']);
        $data = $this->testObj->start(['attributes' => $attrs]);
        $this->assertEquals(
            '<form id="form_1" name="form_1" class="needs-validation"'
            . ' method="post" novalidate>'
            . "\n",
            $data->getBody()
        );
        $this->assertEquals("</form>\n", $data->getPost());

        NextForm::csrfGenerator([$this, 'fooToken']);
        $data = $this->testObj->start(['attributes' => $attrs]);
        $this->assertEquals(
            '<form id="form_1" name="form_1" class="needs-validation"'
            . ' method="post" novalidate>'
            . "\n"
            . '<input id="_nf_token" name="_nf_token" type="hidden" value="foo">' . "\n",
            $data->getBody()
        );

        NextForm::csrfGenerator([$this, 'georgeToken']);
        $data = $this->testObj->start(['attributes' => $attrs]);
        $this->assertEquals(
            '<form id="form_1" name="form_1" class="needs-validation"'
            . ' method="post" novalidate>'
            . "\n"
            . '<input id="george" name="george" type="hidden" value="foo">' . "\n",
            $data->getBody()
        );

        NextForm::csrfGenerator([$this, 'emptyToken']);
        $data = $this->testObj->start(['attributes' => $attrs, 'method' => 'put']);
        $this->assertEquals(
            '<form id="form_1" name="form_1" class="needs-validation"'
            . ' method="put" novalidate>'
            . "\n",
            $data->getBody()
        );

        $data = $this->testObj->start(
            ['action' => 'https://localhost/some file.php', 'attributes' => $attrs]
        );
        $this->assertEquals(
            "<form id=\"form_1\" name=\"form_1\" class=\"needs-validation\""
            . " action=\"https://localhost/some file.php\""
            . " method=\"post\" novalidate>\n",
            $data->getBody()
        );

        $attrs->set('name', 'bad<name');
        $data = $this->testObj->start(['attributes' => $attrs]);
        $this->assertEquals(
            "<form id=\"form_1\" name=\"bad&lt;name\" class=\"needs-validation\""
            . " action=\"https://localhost/some file.php\""
            . " method=\"post\" novalidate>\n",
            $data->getBody()
        );

    }

	public function testCell() {
        $this->logMethod(__METHOD__);
        $cases = RenderCaseGenerator::html_Cell();

        $expect['basic'] = Block::fromString(
            '<div id="cell_1_container" class="col-sm-10 form-group px-1"'
            . ' data-nf-for="cell_1">' . "\n"
            . '<div class="col-sm-10 form-row">' . "\n",
            '</div>' . "\n" . '</div>' . "\n"
        );

        $this->runCases($cases, $expect);
    }

	public function testCellContext() {
        $this->logMethod(__METHOD__);
        $element = new CellElement();
        $this->assertFalse($this->testObj->queryContext('inCell'));
        $binding = Binding::fromElement($element);
        $this->testObj->render($binding);
        $this->assertTrue($this->testObj->queryContext('inCell'));
    }

    /**
     * @doesNotPerformAssertions
     */
	public function testSetOptions() {

        $this->testObj->useOptions();
    }

}
