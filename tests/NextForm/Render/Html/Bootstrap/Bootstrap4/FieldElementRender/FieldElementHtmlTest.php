<?php

namespace NextFormTests\Render\Html\Bootstrap\Bs4\FieldElementRender;

include_once __DIR__ . '/../Bootstrap4RenderFrame.php';

use Abivia\NextForm\Render\Html\Bootstrap\Bs4\FieldElementRender;
use Abivia\NextForm\Render\Block;
use Abivia\NextForm\Render\Html\Bootstrap\Bs4\Render;
use NextFormTests\Render\RenderCaseGenerator;
use NextFormTests\Render\Html\Bootstrap\Bs4\Bootstrap4RenderFrame;

/**
 * @covers \Abivia\NextForm\Render\Html\Bootstrap\Bs4\FieldElementRender\Textarea
 * @covers \Abivia\NextForm\Render\Html\FieldElementRender\Textarea
 */
class FieldElementHtmlTest
extends Bootstrap4RenderFrame
{
    public $render;

    public function setUp() : void
    {
        $this->render = new Render();
    }

    public static function setUpBeforeClass() : void {
        parent::setUpBeforeClass();
    }

    public static function tearDownAfterClass() : void
    {
        self::generatePage(__FILE__, new Render());
    }

    /**
     * Check HTML element, Horizontal layout
     */
	public function testTextareaSuiteHorizontal()
    {
        $this->logMethod(__METHOD__);
        $this->setMode('h');

        $cases = RenderCaseGenerator::html_FieldHtml();
        foreach ($cases as &$case) {
            $case[0] = new FieldElementRender($this->render, $case[0]);
        }

        $groupOptions = ['invalid' => ''];

        $expect = [];

        $expect['basic'] = Block::fromString(
            $this->formGroup(
                $this->column1h('')
                . $this->column2h(
                    '<span>Some HTML.</span>' . "\n"
                ),
                $groupOptions
            )
        );

        // Same result with explicit write access
        $expect['write'] = $expect['basic'];

        // Test view access
        $expect['view'] = Block::fromString(
            $this->formGroup(
                $this->column1h('')
                . $this->column2h(
                    '<span>Some HTML.</span>' . "\n"
                ),
                $groupOptions
            )
        );

        // Test mask access
        $expect['mask'] = Block::fromString(
            $this->formGroup(
                $this->column1h('')
                . $this->column2h(
                    '*****' . "\n"
                ),
                $groupOptions
            )
        );

        // Test hidden access
        $expect['hide'] = Block::fromString('');

        $expect['valid'] = Block::fromString(
            $this->formGroup(
                $this->column1h('')
                . $this->column2h(
                    '<span>Some HTML.</span>' . "\n"
                ),
                $groupOptions
            )
        );

        $expect['invalid'] = Block::fromString(
            $this->formGroup(
                $this->column1h('')
                . $this->column2h(
                    '<span>Some HTML.</span>' . "\n"
                ),
                $groupOptions
            )
        );

        // Label cases
        $expect['label-none'] = $expect['basic'];
        $expect['label-after'] = $expect['basic'];
        $expect['label-before'] = $expect['basic'];
        $expect['label-help'] = $expect['basic'];

        $expect['label-head'] = Block::fromString(
            $this->formGroup(
                $this->column1h('Header')
                . $this->column2h(
                    '<span>Some HTML.</span>' . "\n"
                ),
                $groupOptions
            )
        );

        $expect['label-all'] = Block::fromString(
            $this->formGroup(
                $this->column1h('Header')
                . $this->column2h(
                    '<span>Some HTML.</span>' . "\n"
                ),
                $groupOptions
            )
        );

        $this->runElementCases($cases, $expect);
    }

    /**
     * Check textarea element, Vertical layout
     */
	public function testTextareaSuiteVertical()
    {
        $this->logMethod(__METHOD__);
        $this->setMode('v');

        $cases = RenderCaseGenerator::html_FieldHtml();
        foreach ($cases as &$case) {
            $case[0] = new FieldElementRender($this->render, $case[0]);
        }

        $expect = [];

        $expect['basic'] = Block::fromString(
            $this->formGroup(
                $this->column1('')
                . $this->column2(
                    '<span>Some HTML.</span>' . "\n"
                )
            )
        );

        // Same result with explicit write access
        $expect['write'] = $expect['basic'];

        // Test view access
        $expect['view'] = Block::fromString(
            $this->formGroup(
                $this->column1('')
                . $this->column2(
                    '<span>Some HTML.</span>' . "\n"
                )
            )
        );

        // Test mask access
        $expect['mask'] = Block::fromString(
            $this->formGroup(
                $this->column1('')
                . $this->column2(
                    '*****' . "\n"
                )
            )
        );

        // Test hidden access
        $expect['hide'] = Block::fromString('');

        $expect['valid'] = Block::fromString(
            $this->formGroup(
                $this->column1('')
                . $this->column2(
                    '<span>Some HTML.</span>' . "\n"
                )
            )
        );

        $expect['invalid'] = Block::fromString(
            $this->formGroup(
                $this->column1('')
                . $this->column2(
                    '<span>Some HTML.</span>' . "\n"
                )
            )
        );

        // Label cases
        $expect['label-none'] = $expect['basic'];
        $expect['label-after'] = $expect['basic'];
        $expect['label-before'] = $expect['basic'];
        $expect['label-help'] = $expect['basic'];

        $expect['label-head'] = Block::fromString(
            $this->formGroup(
                $this->column1('Header')
                . $this->column2(
                    '<span>Some HTML.</span>' . "\n"
                )
            )
        );

        $expect['label-all'] = Block::fromString(
            $this->formGroup(
                $this->column1('Header')
                . $this->column2(
                    '<span>Some HTML.</span>' . "\n"
                )
            )
        );

        $this->runElementCases($cases, $expect);
    }

}
