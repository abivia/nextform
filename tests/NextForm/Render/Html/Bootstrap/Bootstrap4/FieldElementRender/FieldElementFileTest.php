<?php

namespace NextFormTests\Render\Html\Bootstrap\Bs4\FieldElementRender;

include_once __DIR__ . '/../Bootstrap4RenderFrame.php';

use Abivia\NextForm\Render\Html\Bootstrap\Bs4\FieldElementRender;
use Abivia\NextForm\Render\Block;
use Abivia\NextForm\Render\Html\Bootstrap\Bs4\Render;
use NextFormTests\Render\RenderCaseGenerator;
use NextFormTests\Render\Html\Bootstrap\Bs4\Bootstrap4RenderFrame;

/**
 * @covers \Abivia\NextForm\Render\Html\Bootstrap\Bs4\FieldElementRender\File
 * @covers \Abivia\NextForm\Render\Html\FieldElementRender\File
 */
class FieldElementFileTest
extends Bootstrap4RenderFrame
{
    public $render;

    public function setUp() : void
    {
        $this->render = new Render();
    }

    public static function setUpBeforeClass() : void {
        parent::setUpBeforeClass();
    }

    public static function tearDownAfterClass() : void
    {
        self::generatePage(__FILE__, new Render());
    }

    /**
     * Check file element, Horizontal layout
     */
	public function testFileSuiteHorizontal()
    {
        $this->logMethod(__METHOD__);
        $this->setMode('h');

        $cases = RenderCaseGenerator::html_FieldFile();
        foreach ($cases as &$case) {
            $case[0] = new FieldElementRender($this->render, $case[0]);
        }

        $groupOptions = ['invalid' => ''];

        $expect = [];

        // No access specification assumes write access
        $expect['basic'] = Block::fromString(
            $this->formGroup(
                $this->column1h('', 'label')
                . $this->column2h(
                    '<input id="field_1" name="field_1" type="file"'
                    . ' class="form-control-file"/>' . "\n"
                ),
                $groupOptions
            )
        );

        // Now test validation
        $expect['valid'] = Block::fromString(
            $this->formGroup(
                $this->column1h('', 'label')
                . $this->column2h(
                    '<input id="field_1" name="field_1[]" type="file"'
                    . ' class="form-control-file"'
                    . ' accept="*.png,*.jpg" multiple/>' . "\n"
                ),
                $groupOptions
            )
        );

        // Test view access
        $expect['view'] = Block::fromString(
            $this->formGroup(
                $this->column1h('', 'label')
                . $this->column2h(
                    '<input id="field_1" name="field_1[]" type="text"'
                    . ' class="form-control-file" disabled readonly/>' . "\n"
                ),
                $groupOptions
            )
        );

        // Test view with a value
        $expect['value-view'] = Block::fromString(
            $this->formGroup(
                $this->column1h('', 'label')
                . $this->column2h(
                    '<input id="field_1" name="field_1[]" type="text"'
                    . ' class="form-control-file"'
                    . ' value="file1.png,file2.jpg" disabled readonly/>' . "\n"
                ),
                $groupOptions
            )
        );

        // Test mask access
        $expect['mask'] = Block::fromString(
            $this->formGroup(
                $this->column1h('', 'label')
                . $this->column2h(
                    '<input id="field_1" name="field_1[]" type="text"'
                    . ' class="form-control-file"'
                    . ' value="*****" disabled readonly/>' . "\n"
                ),
                $groupOptions
            )
        );

        // Test mask with a value
        $expect['value-mask'] = Block::fromString(
            $this->formGroup(
                $this->column1h('', 'label')
                . $this->column2h(
                    '<input id="field_1" name="field_1[]" type="text"'
                    . ' class="form-control-file"'
                    . ' value="*****" disabled readonly/>' . "\n"
                ),
                $groupOptions
            )
        );

        // Test hidden access
        //
        $expect['value-hide'] = Block::fromString(
            '<input id="field_1_opt0" name="field_1[0]" type="hidden" value="file1.png"/>' . "\n"
            . '<input id="field_1_opt1" name="field_1[1]" type="hidden" value="file2.jpg"/>' . "\n"
        );

        $expect['valid'] = Block::fromString(
            $this->formGroup(
                $this->column1h('', 'label')
                . $this->column2h(
                    '<input id="field_1" name="field_1" type="file"'
                    . ' class="form-control-file is-valid"/>' . "\n"
                ),
                $groupOptions
            )
        );

        $expect['invalid'] = Block::fromString(
            $this->formGroup(
                $this->column1h('', 'label')
                . $this->column2h(
                    '<input id="field_1" name="field_1" type="file"'
                    . ' class="form-control-file is-invalid"/>' . "\n"
                ),
                $groupOptions
            )
        );

        $this->runElementCases($cases, $expect);
    }

    /**
     * Check file element, Vertical layout
     */
	public function testFileSuiteVertical()
    {
        $this->logMethod(__METHOD__);
        $this->setMode('v');

        $cases = RenderCaseGenerator::html_FieldFile();
        foreach ($cases as &$case) {
            $case[0] = new FieldElementRender($this->render, $case[0]);
        }

        $expect = [];

        // No access specification assumes write access
        $expect['basic'] = Block::fromString(
            $this->formGroup(
                '<input id="field_1" name="field_1" type="file" class="form-control-file"/>' . "\n"
            )
        );

        // Now test validation
        $expect['valid'] = Block::fromString(
            $this->formGroup(
                '<input id="field_1" name="field_1[]" type="file" class="form-control-file"'
                . ' accept="*.png,*.jpg" multiple/>' . "\n"
            )
        );

        // Test view access
        $expect['view'] = Block::fromString(
            $this->formGroup(
                '<input id="field_1" name="field_1[]" type="text"'
                . ' class="form-control-file" disabled readonly/>' . "\n"
            )
        );

        // Test view with a value
        $expect['value-view'] = Block::fromString(
            $this->formGroup(
                '<input id="field_1" name="field_1[]" type="text" class="form-control-file"'
                . ' value="file1.png,file2.jpg" disabled readonly/>' . "\n"
            )
        );

        // Test mask access
        $expect['mask'] = Block::fromString(
            $this->formGroup(
                '<input id="field_1" name="field_1[]" type="text"'
                . ' class="form-control-file" value="*****" disabled readonly/>' . "\n"
            )
        );

        // Test mask with a value
        $expect['value-mask'] = Block::fromString(
            $this->formGroup(
                '<input id="field_1" name="field_1[]" type="text" class="form-control-file"'
                . ' value="*****" disabled readonly/>' . "\n"
            )
        );

        // Test hidden access
        //
        $expect['value-hide'] = Block::fromString(
            '<input id="field_1_opt0" name="field_1[0]" type="hidden" value="file1.png"/>' . "\n"
            . '<input id="field_1_opt1" name="field_1[1]" type="hidden" value="file2.jpg"/>' . "\n"
        );

        $expect['valid'] = Block::fromString(
            $this->formGroup(
                '<input id="field_1" name="field_1" type="file"'
                . ' class="form-control-file is-valid"/>' . "\n"
            )
        );

        $expect['invalid'] = Block::fromString(
            $this->formGroup(
                '<input id="field_1" name="field_1" type="file"'
                . ' class="form-control-file is-invalid"/>' . "\n"
            )
        );

        $this->runElementCases($cases, $expect);
    }

}
