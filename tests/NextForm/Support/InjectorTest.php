<?php

namespace NextFormTests\Support;

require_once __DIR__ . '/../../test-tools/MockBase.php';

use Abivia\NextForm\Support\Injector;

class ConcreteInjector extends Injector
{
    public function test1($obj)
    {
        $this->wireToInstance('Cement', $obj);
    }
}

class Cement
{
    public $thingy = 'init';

    public function __construct(?string $thingy = '')
    {
        $this->thingy = $thingy;
    }

}

class CementSubstitute
{

}

/**
 * @covers Abivia\NextForm\Support\Injector
 */
class InjectorTest extends \PHPUnit\Framework\TestCase
{
    public function testInstantiation()
    {
        $obj = new ConcreteInjector();
        $this->assertTrue(is_subclass_of($obj, Injector::class));

        $obj = new ConcreteInjector(['Cement' => Cement::class]);
        $this->assertTrue(is_subclass_of($obj, Injector::class));
    }

    public function testMake()
    {
        $obj = new ConcreteInjector([
            'Cement' => Cement::class,
            'Null' => null
        ]);
        $made = $obj->make('Cement');
        $this->assertInstanceOf(Cement::class, $made);
        $madeAnother = $obj->make('Cement');
        $this->assertEquals($made, $madeAnother);
        $made->thingy = 'differ';
        $this->assertNotEquals($made, $madeAnother);

        $missing = $obj->make('Null');
        $this->assertNull($missing);
    }

    public function testServiceClass()
    {
        $obj = new ConcreteInjector(
            [
                'Cement1' => Cement::class,
                'Cement2' => [
                    Cement::class,
                    function () {
                        $obj = new Cement;
                        $obj->thingy = 'bar';
                        return $obj;
                    }
                ]
            ]
        );
        $this->assertEquals(Cement::class, $obj->serviceClass('Cement1'));
        $this->assertEquals(Cement::class, $obj->serviceClass('Cement2'));
        $this->expectException('\RuntimeException');
        $obj->serviceClass('foo');
    }

    public function testServiceProvider()
    {
        $obj = new ConcreteInjector(
            [
                'Cement1' => Cement::class,
                'Cement2' => [
                    Cement::class,
                    function () {
                        $obj = new Cement;
                        $obj->thingy = 'bar';
                        return $obj;
                    }
                ]
            ]
        );
        $this->assertEquals(Cement::class, $obj->serviceProvider('Cement1'));
        $this->assertIsArray($obj->serviceProvider('Cement2'));
    }

    public function testSingleton()
    {
        $obj = new ConcreteInjector(['Cement' => Cement::class]);
        $made = $obj->singleton('Cement');
        $this->assertInstanceOf(Cement::class, $made);
        $this->assertEquals('', $made->thingy);

        $madeAnother = $obj->singleton('Cement');
        $this->assertEquals($made, $madeAnother);

        $made->thingy = 'same';
        $this->assertEquals($made, $madeAnother);

        $obj = new ConcreteInjector(['Cement' => Cement::class]);
        $made = $obj->singleton('Cement', 'foo');
        $this->assertEquals('foo', $made->thingy);

        $obj = new ConcreteInjector(
            [
                'Cement' => [
                    Cement::class,
                    function () {
                        $obj = new Cement;
                        $obj->thingy = 'bar';
                        return $obj;
                    }
                ]
            ]
        );
        $made = $obj->singleton('Cement');
        $this->assertEquals('bar', $made->thingy);
    }

    public function testWire()
    {
        $obj = new ConcreteInjector();
        $obj->wire(['Cement' => Cement::class]);
        $made = $obj->singleton('Cement');
        $this->assertInstanceOf(Cement::class, $made);

        $this->expectException('\RuntimeException');
        $obj->wire(['Bad' => ['okay', 5]]);
    }

    public function testWireToInstance()
    {
        $obj = new ConcreteInjector(['Cement' => Cement::class]);

        // wireToInstance is protected so we use the test1() method.
        $obj->test1(new CementSubstitute());

        $made = $obj->singleton('Cement');
        $this->assertInstanceOf(CementSubstitute::class, $made);
    }

}