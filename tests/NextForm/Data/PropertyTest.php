<?php

namespace NextFormTests\Data;

use Abivia\NextForm\Data\Labels;
use Abivia\NextForm\Data\Property;
use Abivia\NextForm\NextForm;

/**
 * @covers \Abivia\NextForm\Data\Property
 */
class PropertyTest extends \PHPUnit\Framework\TestCase {

	public function testInstantiation() {
        $obj = new Property();
		$this->assertInstanceOf('\Abivia\NextForm\Data\Property', $obj);
	}

    public function testBuild()
    {
        $obj = Property::build();
        $this->assertEquals(null, $obj->getName());
        $this->assertEquals(null, $obj->getLabels()->get('heading'));
        $this->assertEquals(null, $obj->getPresentation());

        $obj = Property::build('radio');
        $this->assertEquals(null, $obj->getName());
        $this->assertEquals(null, $obj->getLabels()->get('heading'));
        $this->assertEquals('radio', $obj->getPresentation()->getType());

        $obj = Property::build('radio', 'picker');
        $this->assertEquals('picker', $obj->getName());
        $this->assertEquals(null, $obj->getLabels()->get('heading'));
        $this->assertEquals('radio', $obj->getPresentation()->getType());

        $obj = Property::build('radio', 'packer', 'Choose one');
        $this->assertEquals('packer', $obj->getName());
        $this->assertEquals('Choose one', $obj->getLabels()->get('heading'));
        $this->assertEquals('radio', $obj->getPresentation()->getType());

        $obj = Property::build(
            'text', 'pucker', ['heading' => 'head', 'inner' => 'inside']
        );
        $this->assertEquals('pucker', $obj->getName());
        $this->assertEquals('head', $obj->getLabels()->get('heading'));
        $this->assertEquals('inside', $obj->getLabels()->get('inner'));
        $this->assertEquals('text', $obj->getPresentation()->getType());

        $lbl = Labels::build(['heading' => 'head', 'inner' => 'inside']);
        $obj = Property::build('text', 'pucker', $lbl);
        $this->assertEquals('pucker', $obj->getName());
        $this->assertEquals('head', $obj->getLabels()->get('heading'));
        $this->assertEquals('inside', $obj->getLabels()->get('inner'));
        $this->assertEquals('text', $obj->getPresentation()->getType());

    }

    public function testMethods()
    {
        $obj = new Property();
        $this->assertInstanceOf('\Abivia\NextForm\Data\Labels', $obj->getLabels());
        $this->assertEquals(null, $obj->getName());
        $obj->name('test');
        $this->assertEquals('test', $obj->getName());
		$this->assertInstanceOf('\Abivia\NextForm\Data\Population', $obj->getPopulation());
        $this->assertEquals(null, $obj->getStore());
        $this->assertEquals(null, $obj->getPresentation());
    }

    public function testLoadAndSave()
    {
        $obj = new Property();
        $config = json_decode(file_get_contents(dirname(__FILE__) . '/property.json'));
        $this->assertTrue(false !== $config, 'JSON error!');
        $result = $obj->configure($config, true);
        if (!$result) {
            print_r($obj->configureGetErrors());
        }
		$this->assertTrue($result);
		$this->assertInstanceOf('\Abivia\NextForm\Data\Population', $obj->getPopulation());
		$this->assertInstanceOf('\Abivia\NextForm\Data\Presentation', $obj->getPresentation());

        NextForm::$jsonCompact = false;
        $result = json_encode($obj, JSON_PRETTY_PRINT);
        file_put_contents(__DIR__ . '/property_actual.json', $result);
        $this->assertJsonStringEqualsJsonFile(
            __DIR__ . '/property_expect.json',
            $result
        );

        $result = NextForm::toJson(
            $obj, ['compact' => true, 'flags' => JSON_PRETTY_PRINT]
        );
        file_put_contents(__DIR__ . '/property_actual_compact.json', $result);
        $this->assertJsonStringEqualsJsonFile(
            __DIR__ . '/property_expect_compact.json',
            $result
        );
    }

    public function testAccess()
    {
        $obj = new Property();
        $config = json_decode(file_get_contents(dirname(__FILE__) . '/property.json'));
        $this->assertTrue(false !== $config, 'JSON error!');
        $obj->configure($config, true);

        $this->assertEquals('none', $obj->getAccess('public'));
        $this->assertEquals('view', $obj->getAccess('admin'));
        $this->assertEquals('write', $obj->getAccess('super'));
        $this->assertEquals('hide', $obj->getAccess('unspecified'));
        $this->assertEquals('hide', $obj->getAccess('*'));

        $this->assertEquals('view', $obj->getAccess(['admin', 'public']));
        $this->assertEquals('write', $obj->getAccess(['admin', 'super']));
    }

    public function testAccessBad1()
    {
        $json = '{"name":"id","roles":{"*":"write", "foo":"badval"}}';
        $config = json_decode($json);
        $this->assertTrue(false !== $config, 'JSON error!');
        $obj = new Property();
        $result = $obj->configure($config, true);
        $this->assertFalse($result);
    }

    public function testAccessNoDefault()
    {
        $json = '{"name":"id","roles":{"foo":"write"}}';
        $config = json_decode($json);
        $this->assertTrue(false !== $config, 'JSON error!');
        $obj = new Property();
        $obj->configure($config, true);

        $this->assertEquals('none', $obj->getAccess('bob'));
    }

    public function testRole()
    {
        $obj = new Property();
        $obj->role('*', 'view');
        $obj->role('foo', 'write');
        $this->assertEquals(
            ['*' => 'view', 'foo' => 'write'],
            $obj->getRoles()
        );
    }

    public function testRoles()
    {
        $obj = new Property();
        $obj->roles('*:view|foo:write');
        $this->assertEquals(
            ['*' => 'view', 'foo' => 'write'],
            $obj->getRoles()
        );
    }

    public function testRoleBad1()
    {
        $obj = new Property();
        $this->expectException('\RuntimeException');
        $obj->role('foo', 'view');
    }

    public function testRoleBad2()
    {
        $obj = new Property();
        $this->expectException('\RuntimeException');
        $obj->role('*', 'splork');
    }

    public function testRolesGet()
    {
        $json = '{"name":"id","roles":{"*":"view", "foo":"write"}}';
        $config = json_decode($json);
        $this->assertTrue(false !== $config, 'JSON error!');
        $obj = new Property();
        $obj->configure($config, true);
        $this->assertEquals(
            ['*' => 'view', 'foo' => 'write'],
            $obj->getRoles()
        );
    }

    public function testRolesDefault()
    {
        $json = '{"name":"id"}';
        $config = json_decode($json);
        $this->assertTrue(false !== $config, 'JSON error!');
        $obj = new Property();
        $obj->configure($config, true);

        $this->assertEquals('write', $obj->getAccess('super'));
        $this->assertEquals('write', $obj->getAccess('unspecified'));

        $jsonOut = json_encode($obj);
        $this->assertEquals($json, $jsonOut);
    }

}
