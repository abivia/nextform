<?php
namespace NextFormTests\Data;

use Abivia\NextForm\Data\Store;

/**
 * @covers \Abivia\NextForm\Data\Store
 */
class StoreTest extends \PHPUnit\Framework\TestCase {

    public function testBuild()
    {
        $obj = Store::build();
        $this->assertTrue($obj->isEmpty());

        $obj = Store::build('int');
		$this->assertEquals('int', $obj->getType());
		$this->assertEquals(null, $obj->getSize());

        $obj = Store::build('int', 10);
		$this->assertEquals('int', $obj->getType());
		$this->assertEquals('10', $obj->getSize());

        $this->expectException('\RuntimeException');
        $obj = Store::build('dingdong');

    }

    public function testEmpty()
    {
        $obj = new Store;
        $this->assertTrue($obj->isEmpty());

        $obj->type('blob');
        $this->assertFalse($obj->isEmpty());

        $obj = new Store;
        $obj->size('large');
        $this->assertFalse($obj->isEmpty());
    }

    public function testLoad()
    {
        $obj = new Store();
        $config = json_decode('{"type":"int","size":"10"}');
        $this->assertTrue(false !== $config, 'JSON error!');
        $this->assertTrue($obj->configure($config));
		$this->assertEquals('int', $obj->getType());
		$this->assertEquals('10', $obj->getSize());
    }

    public function testLoadBadType()
    {
        $obj = new Store();
        $config = json_decode('{"type":"jinglebells","size":"10"}');
        $this->assertTrue(false !== $config, 'JSON error!');
        $this->assertFalse($obj->configure($config));
    }

    public function testLoadCompact()
    {
        $obj = new Store();
        $config = json_decode('"int|size:10"');
        $this->assertTrue(false !== $config, 'JSON error!');
        $this->assertTrue($obj->configure($config));
		$this->assertEquals('int', $obj->getType());
		$this->assertEquals('10', $obj->getSize());
    }

	public function testInstantiation()
    {
        $obj = new Store();
		$this->assertInstanceOf('\Abivia\NextForm\Data\Store', $obj);
	}

    public function testSize()
    {
        $obj = new Store;
        $this->assertEquals(null, $obj->getSize());

        $obj->size('large');
        $this->assertEquals('large', $obj->getSize());
    }

    public function testType()
    {
        $obj = new Store;
        $this->assertEquals(null, $obj->getType());

        $obj->type('blob');
        $this->assertEquals('blob', $obj->getType());

        $this->expectException('\RuntimeException');
        $obj->type('invalid');

    }

}
