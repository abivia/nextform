Abivia\NextForm\Data\Segment Object
(
    [autoLabels:protected] => Abivia\NextForm\Data\Labels Object
        (
            [accept:protected] =>
            [after:protected] =>
            [autoSets:protected] => Array
                (
                )

            [before:protected] =>
            [confirm:protected] =>
            [error:protected] =>
            [heading:protected] => auto.*.head
            [help:protected] =>
            [inner:protected] =>
            [isHtml:protected] => Array
                (
                    [accept] =>
                    [after] =>
                    [before] =>
                    [error] =>
                    [heading] =>
                    [help] =>
                    [inner] =>
                    [mask] =>
                )

            [iterPosn:Abivia\NextForm\Data\Labels:private] => 8
            [mask:protected] =>
            [replacements:protected] => Array
                (
                )

            [schema:protected] =>
            [translate:protected] => 1
            [configureErrors:Abivia\NextForm\Data\Labels:private] => Array
                (
                )

            [configureOptions:Abivia\NextForm\Data\Labels:private] => Array
                (
                    [strict] => 1
                    [newlog] =>
                    [parent] => Abivia\NextForm\Data\Segment Object
 *RECURSION*
                )

        )

    [name:protected] => ObjectOne
    [properties:protected] => Array
        (
            [id] => Abivia\NextForm\Data\Property Object
                (
                    [description:protected] => The first object
                    [labels:protected] => Abivia\NextForm\Data\Labels Object
                        (
                            [accept:protected] =>
                            [after:protected] =>
                            [autoSets:protected] => Array
                                (
                                    [heading] => auto.id.head
                                )

                            [before:protected] =>
                            [confirm:protected] =>
                            [error:protected] =>
                            [heading:protected] => auto.id.head
                            [help:protected] =>
                            [inner:protected] =>
                            [isHtml:protected] => Array
                                (
                                    [accept] =>
                                    [after] =>
                                    [before] =>
                                    [error] =>
                                    [heading] =>
                                    [help] =>
                                    [inner] =>
                                    [mask] =>
                                )

                            [iterPosn:Abivia\NextForm\Data\Labels:private] => 0
                            [mask:protected] =>
                            [replacements:protected] => Array
                                (
                                )

                            [schema:protected] =>
                            [translate:protected] => 1
                            [configureErrors:Abivia\NextForm\Data\Labels:private] => Array
                                (
                                )

                            [configureOptions:Abivia\NextForm\Data\Labels:private] =>
                        )

                    [linkedBindings:protected] => Array
                        (
                        )

                    [name:protected] => id
                    [population:protected] =>
                    [presentation:protected] => Abivia\NextForm\Data\Presentation Object
                        (
                            [cols:protected] => 1
                            [confirm:protected] =>
                            [rows:protected] =>
                            [type:protected] => text
                            [configureErrors:Abivia\NextForm\Data\Presentation:private] => Array
                                (
                                )

                            [configureOptions:Abivia\NextForm\Data\Presentation:private] => Array
                                (
                                    [strict] => 1
                                    [newlog] =>
                                    [parent] => Abivia\NextForm\Data\Property Object
 *RECURSION*
                                )

                        )

                    [segment:protected] => Abivia\NextForm\Data\Segment Object
 *RECURSION*
                    [store:protected] => Abivia\NextForm\Data\Store Object
                        (
                            [size:protected] => 10
                            [type:protected] => int
                            [configureErrors:Abivia\NextForm\Data\Store:private] => Array
                                (
                                )

                            [configureOptions:Abivia\NextForm\Data\Store:private] => Array
                                (
                                    [strict] => 1
                                    [newlog] =>
                                    [parent] => Abivia\NextForm\Data\Property Object
 *RECURSION*
                                )

                        )

                    [validation:protected] =>
                    [configureErrors:Abivia\NextForm\Data\Property:private] => Array
                        (
                        )

                    [configureOptions:Abivia\NextForm\Data\Property:private] => Array
                        (
                            [strict] => 1
                            [newlog] =>
                            [parent] => Abivia\NextForm\Data\Segment Object
 *RECURSION*
                        )

                    [roles:protected] => Array
                        (
                            [*] => view
                            [admin] => write
                        )

                )

            [id2] => Abivia\NextForm\Data\Property Object
                (
                    [description:protected] =>
                    [labels:protected] => Abivia\NextForm\Data\Labels Object
                        (
                            [accept:protected] =>
                            [after:protected] =>
                            [autoSets:protected] => Array
                                (
                                    [heading] => auto.id2.head
                                )

                            [before:protected] =>
                            [confirm:protected] =>
                            [error:protected] =>
                            [heading:protected] => auto.id2.head
                            [help:protected] =>
                            [inner:protected] =>
                            [isHtml:protected] => Array
                                (
                                    [accept] =>
                                    [after] =>
                                    [before] =>
                                    [error] =>
                                    [heading] =>
                                    [help] =>
                                    [inner] =>
                                    [mask] =>
                                )

                            [iterPosn:Abivia\NextForm\Data\Labels:private] => 0
                            [mask:protected] =>
                            [replacements:protected] => Array
                                (
                                )

                            [schema:protected] =>
                            [translate:protected] => 1
                            [configureErrors:Abivia\NextForm\Data\Labels:private] => Array
                                (
                                )

                            [configureOptions:Abivia\NextForm\Data\Labels:private] =>
                        )

                    [linkedBindings:protected] => Array
                        (
                        )

                    [name:protected] => id2
                    [population:protected] =>
                    [presentation:protected] => Abivia\NextForm\Data\Presentation Object
                        (
                            [cols:protected] => 1
                            [confirm:protected] =>
                            [rows:protected] =>
                            [type:protected] => text
                            [configureErrors:Abivia\NextForm\Data\Presentation:private] => Array
                                (
                                )

                            [configureOptions:Abivia\NextForm\Data\Presentation:private] => Array
                                (
                                    [strict] => 1
                                    [newlog] =>
                                    [parent] => Abivia\NextForm\Data\Property Object
 *RECURSION*
                                )

                        )

                    [segment:protected] => Abivia\NextForm\Data\Segment Object
 *RECURSION*
                    [store:protected] => Abivia\NextForm\Data\Store Object
                        (
                            [size:protected] => 10
                            [type:protected] => int
                            [configureErrors:Abivia\NextForm\Data\Store:private] => Array
                                (
                                )

                            [configureOptions:Abivia\NextForm\Data\Store:private] => Array
                                (
                                    [strict] => 1
                                    [newlog] =>
                                    [parent] => Abivia\NextForm\Data\Property Object
 *RECURSION*
                                )

                        )

                    [validation:protected] =>
                    [configureErrors:Abivia\NextForm\Data\Property:private] => Array
                        (
                        )

                    [configureOptions:Abivia\NextForm\Data\Property:private] => Array
                        (
                            [strict] => 1
                            [newlog] =>
                            [parent] => Abivia\NextForm\Data\Segment Object
 *RECURSION*
                        )

                    [roles:protected] => Array
                        (
                            [*] => view
                            [admin] => view
                        )

                )

        )

    [primary:protected] => Array
        (
            [0] => id
        )

    [configureErrors:Abivia\NextForm\Data\Segment:private] => Array
        (
        )

    [configureOptions:Abivia\NextForm\Data\Segment:private] => Array
        (
            [strict] => 1
        )

    [roles:protected] => Array
        (
            [*] => view
            [admin] => write
        )

)
