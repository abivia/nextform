<?php

namespace NextFormTests\Data;

use Abivia\NextForm\Data\Property;
use Abivia\NextForm\Data\Segment;
use Abivia\NextForm\NextForm;

/**
 * @covers \Abivia\NextForm\Data\Segment
 */
class SegmentTest extends \PHPUnit\Framework\TestCase {

    protected function getProperty($instance, $property)
    {
        $reflector = new \ReflectionClass($instance);
        $reflectorProperty = $reflector->getProperty($property);
        $reflectorProperty->setAccessible(true);

        return $reflectorProperty->getValue($instance);
    }

	public function testInstantiation()
    {
        $obj = new Segment();
		$this->assertInstanceOf('\Abivia\NextForm\Data\Segment', $obj);
	}

    public function testBuild()
    {
        $obj = Segment::build();
        $this->assertEquals(null, $obj->getName());
        $this->assertEquals(0, $obj->count());

        $obj = Segment::build('someSeg');
        $this->assertEquals('someSeg', $obj->getName());
        $this->assertEquals(0, $obj->count());

        $obj = Segment::build('someSeg', Property::build('text', 'bob'));
        $this->assertEquals('someSeg', $obj->getName());
        $this->assertEquals(1, $obj->count());

    }

    public function testLoad()
    {
        $obj = new Segment();
        $config = json_decode(file_get_contents(dirname(__FILE__) . '/data-segment.json'));
        $this->assertTrue(false !== $config, 'JSON error!');
        $populate = $obj->configure($config, true);
        if ($populate) {
            $errors = '';
        } else {
            $errors = $obj->configureGetErrors();
            $errors = 'Segment load:' . "\n" . implode("\n", $errors) . "\n";
        }
        $this->assertTrue($populate, $errors);
        $this->assertEquals('ObjectOne', $obj->getName());
        $this->assertInstanceOf(
            '\Abivia\NextForm\Data\Property', $obj->getProperty('id')
        );
        $this->assertNull($obj->getProperty('some-nonexistent-property'));
        $dump = print_r($obj, true);
        $dump = str_replace(" \n", "\n", $dump);
        file_put_contents(dirname(__FILE__) . '/segment-dump_actual.txt', $dump);
        $this->assertStringEqualsFile(dirname(__FILE__) . '/segment-dump_expect.txt', $dump);
    }

    public function testSave()
    {
        $obj = new Segment();
        $config = json_decode(file_get_contents(dirname(__FILE__) . '/data-segment.json'));
        $this->assertTrue(false !== $config, 'JSON error!');
        $obj->configure($config, true);
        $result = NextForm::toJson(
            $obj, ['flags' => JSON_PRETTY_PRINT]
        );
        file_put_contents(__DIR__ . '/data-segment_actual.json', $result);
        $this->assertJsonStringEqualsJsonFile(
            __DIR__ . '/data-segment_expect.json',
            $result
        );

        $result = NextForm::toJson(
            $obj, ['compact' => true, 'flags' => JSON_PRETTY_PRINT]
        );
        file_put_contents(__DIR__ . '/data-segment_actual_compact.json', $result);
        $this->assertJsonStringEqualsJsonFile(
            __DIR__ . '/data-segment_expect_compact.json',
            $result
        );
    }

    public function testLoadBadPrimary()
    {
        $obj = new Segment();
        $config = json_decode(
            file_get_contents(
                dirname(__FILE__) . '/data-segment-bad-primary.json'
            )
        );
        $this->assertTrue(false !== $config, 'JSON error!');
        $this->assertFalse($obj->configure($config, true));

    }

    public function testName()
    {
        $obj = new Segment();
        $config = json_decode(file_get_contents(dirname(__FILE__) . '/data-segment.json'));
        $this->assertTrue(false !== $config, 'JSON error!');
        $obj->configure($config, true);

        $this->assertEquals('ObjectOne', $obj->getName());

        $obj->name('foo');
        $this->assertEquals('foo', $obj->getName());
    }

    public function testPrimary()
    {
        $obj = new Segment();
        $config = json_decode(file_get_contents(dirname(__FILE__) . '/data-segment.json'));
        $this->assertTrue(false !== $config, 'JSON error!');
        $obj->configure($config, true);
        $obj->primary('id');
        $this->assertEquals(['id'], $obj->getPrimary());

        $this->expectException('\RuntimeException');
        $obj->primary('nonexistent');
    }

    public function testAutoLabel()
    {
        $obj = new Segment();
        $config = json_decode(
            file_get_contents(dirname(__FILE__) . '/data-segment.json')
        );
        $this->assertTrue(false !== $config, 'JSON error!');
        $obj->configure($config, true);
        $labels = $obj->getProperty('id2')->getLabels();
        $this->assertEquals('auto.id2.head', $labels->get('heading'));
    }

    public function testIterator()
    {
        $obj = new Segment();
        $config = json_decode(
            file_get_contents(dirname(__FILE__) . '/data-segment.json')
        );
        $this->assertTrue(false !== $config, 'JSON error!');
        $obj->configure($config, true);

        $expect = ['id' => true, 'id2' => true];
        foreach ($obj as $propName => $prop) {
            $this->assertTrue(isset($expect[$propName]));
            unset($expect[$propName]);
        }
        $this->assertEquals(0, count($expect));
    }

    public function testProperty()
    {
        $obj = new Segment();
        $config = json_decode(file_get_contents(dirname(__FILE__) . '/data-segment.json'));
        $this->assertTrue(false !== $config, 'JSON error!');
        $obj->configure($config, true);

        $prop = new Property();
        $prop->name('someprop');
        $obj->property($prop);

        $prop = new Property();
        $this->expectException('\RuntimeException');
        $obj->property($prop);
    }

}
