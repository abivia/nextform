<?php

namespace NextFormTests\Integrated\Bs4;

if (!defined('NF_TEST_ROOT')) {
    define('NF_TEST_ROOT', dirname(__DIR__, 2) . '/');
}

include_once NF_TEST_ROOT . 'test-tools/JsonComparison.php';
include_once NF_TEST_ROOT . 'test-tools/MockTranslate.php';
include_once NF_TEST_ROOT . 'test-tools/NullCaptcha.php';
include_once NF_TEST_ROOT . 'test-tools/Page.php';

use Abivia\NextForm\Data\Schema;
use Abivia\NextForm\Form\Form;
use Abivia\NextForm\NextForm;
use Abivia\NextForm\Render\Html\Bootstrap\Bs4\Render;
use NextFormTests\Test_Tools\JsonComparison;
use NextFormTests\Test_Tools\MockTranslate;
use NextFormTests\Test_Tools\NullCaptcha;
use NextFormTests\Test_Tools\Page;

class AccessTest extends \PHPUnit\Framework\TestCase {
    use JsonComparison;

    public $accessForm;
    public $accessSchema;

    public $expect = [
        'admin' => [
            'field_2' => [
                'name' => 'accessTest_sysId',
                'readonly' => true,
                'type' => 'text',
            ],
            'field_3' => [
                'name' => 'accessTest_username',
                'readonly' => false,
                'type' => 'text',
            ],
            'field_4' => [
                'name' => 'accessTest_phone',
                'readonly' => true,
                'type' => 'tel',
            ],
        ],
        'owner' => [
            'field_5' => [
                'name' => 'accessTest_sysId',
                'readonly' => false,
                'type' => 'hidden',
            ],
            'field_6' => [
                'name' => 'accessTest_username',
                'readonly' => false,
                'type' => 'text',
            ],
            'field_7' => [
                'name' => 'accessTest_phone',
                'readonly' => false,
                'type' => 'tel',
            ],
        ],
        'public' => [
            'field_8' => [
                'name' => 'accessTest_sysId',
                'readonly' => false,
                'type' => 'hidden',
            ],
            'field_9' => [
                'name' => 'accessTest_username',
                'readonly' => true,
                'type' => 'text',
            ],
            'field_10' => [
                'name' => 'accessTest_phone',
                'readonly' => true,
                'type' => 'text',
            ],
        ],
    ];

    public $roleList = ['admin', 'owner', 'public'];

    public function setUp() : void {
        NextForm::wireStatic(['Captcha' => NullCaptcha::class]);
        $this->accessForm  = Form::fromFile(__DIR__ . '/../access-form.json');
        $this->accessSchema = Schema::fromFile(__DIR__ . '/../access-schema.json');
    }

    public function checkPage($role, $page)
    {
        $doc = new \DOMDocument();
        $this->assertTrue($doc->loadHTML($page), $role);
        foreach ($this->expect[$role] as $id => $data) {
            $input = $doc->getElementById($id);
            $this->assertTrue($input !== null);
            $this->assertEquals(
                $data['name'], $input->getAttribute('name'), "{$role}.{$id}"
            );
            $this->assertEquals(
                $data['type'], $input->getAttribute('type'), "{$role}.{$id}"
            );
            $this->assertEquals(
                $data['readonly'], $input->hasAttribute('readonly'),
                "{$role}.{$id}"
            );
        }
    }

    public function testRender() {
        NextForm::csrfGenerator([Page::class, 'fixedToken']);
        NextForm::boot();
        $manager = new NextForm();
        $manager->useOptions(['segmentNameMode' => 'off']);
        $manager->useOptions(
            [
                'segmentNameMode' => 'off',
                'wire' => [
                    'Render' => Render::class,
                    'Translate' => MockTranslate::class
                ],
            ]
        );
        $manager->addForm(
            $this->accessForm,
            ['action' => 'http://localhost/nextform/post.php']
        );
        $manager->addSchema($this->accessSchema);


        foreach ($this->roleList as $role) {
            $html = $manager->generate(null, ['role' => $role]);
            $html->addScript(
                file_get_contents(__DIR__ . '/memberform.js'),
                false
            );
            $page = Page::write(__FUNCTION__, $html);
            $this->checkPage($role, $page);
            file_put_contents(
                Page::logFolder(__FILE__) . __FUNCTION__
                . "_{$role}.html",
                $page
            );
        }

        $this->assertTrue(true);
    }

}
