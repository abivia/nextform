<?php

namespace NextFormTests\Integrated\Bs4;

if (!defined('NF_TEST_ROOT')) {
    define('NF_TEST_ROOT', dirname(__DIR__, 2) . '/');
}

include_once NF_TEST_ROOT . 'test-tools/JsonComparison.php';
include_once NF_TEST_ROOT . 'test-tools/MockTranslate.php';
include_once NF_TEST_ROOT . 'test-tools/NullCaptcha.php';
include_once NF_TEST_ROOT . 'test-tools/Page.php';

use Abivia\NextForm\Data\Schema;
use Abivia\NextForm\Form\Form;
use Abivia\NextForm\NextForm;
use Abivia\NextForm\Render\Html\Bootstrap\Bs4\Render;
use NextFormTests\Test_Tools\JsonComparison;
use NextFormTests\Test_Tools\MockTranslate;
use NextFormTests\Test_Tools\NullCaptcha;
use NextFormTests\Test_Tools\Page;

class MemberTest extends \PHPUnit\Framework\TestCase {
    use JsonComparison;

    public $memberForm;
    public $memberSchema;

    public function setUp() : void {
        NextForm::wireStatic(['Captcha' => NullCaptcha::class]);
        $this->memberForm  = Form::fromFile(__DIR__ . '/../member-form.json');
        $this->memberSchema = Schema::fromFile(__DIR__ . '/../member-schema.json');
    }

    public function testUnpopulated() {
        NextForm::csrfGenerator([Page::class, 'fixedToken']);
        NextForm::boot();
        $manager = new NextForm();
        $manager->useOptions(['segmentNameMode' => 'off']);
        $manager->useOptions(
            [
                'segmentNameMode' => 'off',
                'wire' => [
                    'Render' => Render::class,
                    'Translate' => MockTranslate::class
                ],
            ]
        );
        $manager->addForm(
            $this->memberForm,
            ['action' => 'http://localhost/nextform/post.php']
        );
        $manager->addSchema($this->memberSchema);

        $html = $manager->generate();
        $html->addScript(file_get_contents(__DIR__ . '/memberform.js'), false);
        file_put_contents(
            Page::logFolder(__FILE__) . __FUNCTION__ . '.html',
            Page::write(__FUNCTION__, $html)
        );

        NextForm::boot();
        MockTranslate::$append = ' (lang2)';
        $html = $manager->generate();
        $html->addScript(file_get_contents(__DIR__ . '/memberform.js'), false);
        file_put_contents(
            Page::logFolder(__FILE__) . __FUNCTION__ . '_lang2.html',
            Page::write(__FUNCTION__, $html)
        );

        $this->assertTrue(true);
    }

    public function testPopulated() {
        NextForm::csrfGenerator([Page::class, 'fixedToken']);
        NextForm::boot();
        $manager = new NextForm();
        $manager->useOptions(['segmentNameMode' => 'off']);
        $manager->useOptions(
            [
                'segmentNameMode' => 'off',
                'wire' => [
                    'Render' => Render::class,
                    'Translate' => MockTranslate::class
                ],
            ]
        );
        $manager->addForm(
            $this->memberForm,
            ['action' => 'http://localhost/nextform/post.php']
        );
        $manager->addSchema($this->memberSchema);

        $data = [
            'id' => 70,
            'primaryFirstName' => 'Jane',
            'primaryLastName' => 'Duh',
        ];
        $manager->populate($data, 'members');

        $html = $manager->generate();
        $body = $html->getBody();
        $this->assertEquals(1, preg_match(
            '/name="members_primaryFirstName"[^>]*?value="Jane"/',
            $body
        ));
        $this->assertEquals(1, preg_match(
            '/name="members_primaryLastName"[^>]*?value="Duh"/',
            $body
        ));
        $html->addScript(file_get_contents(__DIR__ . '/memberform.js'), false);
        file_put_contents(
            Page::logFolder(__FILE__) . __FUNCTION__ . '.html',
            Page::write(__FUNCTION__, $html)
        );

        NextForm::boot();
        MockTranslate::$append = ' (lang2)';
        $html = $manager->generate();
        $html->addScript(file_get_contents(__DIR__ . '/memberform.js'), false);
        file_put_contents(
            Page::logFolder(__FILE__) . __FUNCTION__ . '_lang2.html',
            Page::write(__FUNCTION__, $html)
        );

        $this->assertTrue(true);
    }

    public function testRequest() {
        NextForm::csrfGenerator([Page::class, 'fixedToken']);
        NextForm::boot();
        $manager = new NextForm();
        $manager->useOptions(['segmentNameMode' => 'off']);
        $manager->useOptions(
            [
                'segmentNameMode' => 'off',
                'wire' => [
                    'Render' => Render::class,
                    'Translate' => MockTranslate::class
                ],
            ]
        );
        $manager->addForm(
            $this->memberForm,
            ['action' => 'http://localhost/nextform/post.php']
        );
        $manager->addSchema($this->memberSchema);

        $data = [
            'id' => 70,
            'primaryFirstName' => 'Jane',
            'primaryLastName' => 'Duh',
        ];
        $manager->populate($data, 'members');

        //$manager->generate();

        $mockRequest = [
            'members_id' => 70,
            'members_primaryFirstName' => 'Jane',
            'members_primaryLastName' => 'Duh',
            'members_primaryEmail' => null,
            'members_primaryVisualArtsMedia' => ['ACR', 'PHO'],
            'members_bogus' => 'not valid',
        ];
        $mapped = $manager->request($mockRequest);
        $this->assertEquals(
            [
                'members.primaryFirstName' => 'Jane',
                'members.primaryLastName' => 'Duh',
                'members.primaryVisualArtsMedia' => ['ACR', 'PHO'],
                'members.id' => 70,
            ],
            $mapped
        );
        $mapped = $manager->request($mockRequest, ['segment' => true]);
        $this->assertEquals(
            ['members' =>
                [
                    'primaryFirstName' => 'Jane',
                    'primaryLastName' => 'Duh',
                    'primaryVisualArtsMedia' => ['ACR', 'PHO'],
                    'id' => 70,
                ]
            ],
            $mapped
        );
    }

}
