<?php

namespace NextFormTests\Integrated\Tw2;

if (!defined('NF_TEST_ROOT')) {
    define('NF_TEST_ROOT', dirname(__DIR__, 2) . '/');
}

include_once NF_TEST_ROOT . 'test-tools/JsonComparison.php';
include_once NF_TEST_ROOT . 'test-tools/MockTranslate.php';
include_once NF_TEST_ROOT . 'test-tools/NullCaptcha.php';
include_once NF_TEST_ROOT . 'test-tools/Page.php';

use Abivia\NextForm\Data\Schema;
use Abivia\NextForm\Form\Form;
use Abivia\NextForm\NextForm;
use Abivia\NextForm\Render\Html\Tailwind\Tw2\Render;
use NextFormTests\Test_Tools\JsonComparison;
use NextFormTests\Test_Tools\MockTranslate;
use NextFormTests\Test_Tools\NullCaptcha;
use NextFormTests\Test_Tools\Page;

class MemberTest extends \PHPUnit\Framework\TestCase {
    use JsonComparison;

    protected $cssFiles;
    public $memberForm;
    public $memberSchema;

    /**
     * If Tailwind and Tailwind forms are present, copy the latest CSS
     * files into this directory, so remote CI will run without a full
     * NPM install.
     */
    protected function copyTailwindCss()
    {
        $base = __DIR__;
        $found = false;
        while(!($found = file_exists($base . '/package.json')) && $base !== '/')  {
            $base = dirname($base);
        }
        if ($found) {
            $npmDir = $base . '/node_modules';
            $this->cssFiles = [
                '/tailwind.min.css' => '/tailwindcss/dist/tailwind.min.css',
                '/forms.min.css' => '/@tailwindcss/forms/dist/forms.min.css'
            ];
            foreach ($this->cssFiles as $name => $path) {
                if (file_exists($npmDir . $path)) {
                    copy($npmDir . $path, __DIR__ . $name);
                }
            }
        }
    }

    public function setUp() : void
    {
        $this->copyTailwindCss();
        NextForm::wireStatic(['Captcha' => NullCaptcha::class]);
        $this->memberForm  = Form::fromFile(__DIR__ . '/../member-form.json');
        $this->memberSchema = Schema::fromFile(__DIR__ . '/../member-schema.json');
    }

    public function testUnpopulated() {
        NextForm::csrfGenerator([Page::class, 'fixedToken']);
        NextForm::boot();
        $manager = new NextForm();
        $manager->useOptions(['segmentNameMode' => 'off']);
        $manager->useOptions(
            [
                'segmentNameMode' => 'off',
                'wire' => [
                    'Render' => Render::class,
                    'Translate' => MockTranslate::class
                ],
            ]
        );
        $manager->addForm(
            $this->memberForm,
            ['action' => 'http://localhost/nextform/post.php']
        );
        $manager->addSchema($this->memberSchema);

        $html = $manager->generate();

        // Bypass all the build layers and just jam in the whole CSS.
        foreach (array_keys($this->cssFiles) as $name) {
            $html->addStyle(file_get_contents(__DIR__ . $name));
        }

        $html->addScript(file_get_contents(__DIR__ . '/memberform.js'), false);
        file_put_contents(
            Page::logFolder(__FILE__) . __FUNCTION__ . '.html',
            Page::write(__FUNCTION__, $html)
        );

        NextForm::boot();
        MockTranslate::$append = ' (lang2)';
        $html = $manager->generate();
        $html->addScript(file_get_contents(__DIR__ . '/memberform.js'), false);
        file_put_contents(
            Page::logFolder(__FILE__) . __FUNCTION__ . '_lang2.html',
            Page::write(__FUNCTION__, $html)
        );

        $this->assertTrue(true);
    }

}
