<?php

namespace NextFormTests\Integrated;

if (!defined('NF_TEST_ROOT')) {
    define('NF_TEST_ROOT', dirname(__DIR__) . '/');
}

include_once NF_TEST_ROOT . 'test-tools/JsonComparison.php';
include_once NF_TEST_ROOT . 'test-tools/MockTranslate.php';
include_once NF_TEST_ROOT . 'test-tools/NullCaptcha.php';
include_once NF_TEST_ROOT . 'test-tools/Page.php';

use Abivia\NextForm\Data\Schema;
use Abivia\NextForm\Form\Form;
use Abivia\NextForm\NextForm;
use NextFormTests\Test_Tools\JsonComparison;
use NextFormTests\Test_Tools\NullCaptcha;

class MemberTest extends \PHPUnit\Framework\TestCase {
    use JsonComparison;

    public $memberForm;
    public $memberSchema;

    public function setUp() : void {
        NextForm::wireStatic(['Captcha' => NullCaptcha::class]);
        $this->memberForm  = Form::fromFile(__DIR__ . '/member-form.json');
        $this->memberSchema = Schema::fromFile(__DIR__ . '/member-schema.json');
    }

    /**
     * Integration test for schema read/write.
     * @coversNothing
     */
    public function testSchemaLoad() {
        $obj = new Schema();
        $jsonFile = __DIR__ . '/member-schema.json';
        $config = json_decode(file_get_contents($jsonFile));
        $this->assertTrue(false !== $config, 'Error JSON decoding schema.');
        $populate = $obj->configure($config, true);
        if ($populate) {
            $errors = '';
        } else {
            $errors = $obj->configureGetErrors();
            $errors = 'Schema load:' . "\n" . implode("\n", $errors) . "\n";
        }
        $this->assertTrue($populate, $errors);
        // Save the result as JSON so we can compare
        $resultJson = NextForm::toJson(
            $obj, ['compact' => true, 'flags' => JSON_PRETTY_PRINT]
        );
        file_put_contents(__DIR__ . '/member-schema-out.json', $resultJson);
        // Stock JSON to stdClass for comparison
        NextForm::$jsonCompact = false;
        $result = json_decode($resultJson);
        // Reload the original configuration
        $config = json_decode(file_get_contents($jsonFile));
        $this->assertTrue($this->jsonCompare($config, $result));
    }

    /**
     * Integration test for form read/write.
     * @coversNothing
     */
    public function testFormLoad() {
        NextForm::boot();
        $obj = new Form();
        $jsonFile = __DIR__ . '/member-form.json';
        $config = json_decode(file_get_contents($jsonFile));
        $this->assertTrue(false !== $config, 'Error JSON decoding form.');
        $populate = $obj->configure($config, true);
        if ($populate) {
            $errors = '';
        } else {
            $errors = $obj->configureGetErrors();
            $errors = 'Form load:' . "\n" . implode("\n", $errors) . "\n";
        }
        $this->assertTrue($populate, $errors);

        // Save the result as JSON so we can compare
        NextForm::$jsonCompact = false;
        $resultJson = json_encode($obj, JSON_PRETTY_PRINT);
        file_put_contents(__DIR__ . '/member-form-out.json', $resultJson);

        // Stock JSON to stdClass for comparison; reload the config
        $result = json_decode($resultJson);
        $jsonFile = __DIR__ . '/member-form-baseline.json';
        $config = json_decode(file_get_contents($jsonFile));
        $this->assertTrue($this->jsonCompare($config, $result));

        // Repeat JSON test in compact mode
        $resultJson = NextForm::toJson(
            $obj, ['flags' =>JSON_PRETTY_PRINT, 'compact' => true]
        );
        file_put_contents(__DIR__ . '/member-form-out_compact.json', $resultJson);

        // Stock JSON to stdClass for comparison; reload the config
        $result = json_decode($resultJson);
        $jsonFile = __DIR__ . '/member-form-baseline_compact.json';
        $config = json_decode(file_get_contents($jsonFile));
        $this->assertTrue($this->jsonCompare($config, $result));
    }

}
