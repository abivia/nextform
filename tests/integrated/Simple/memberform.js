/*
 * Custom support for the member form
 */

/**
 * Update limits on user-entered fees based on a membership level change.
 *
 * @param {HtmlElement} element
 * @returns {undefined}
 */
function setFeeAttributes(element) {
    var form = element.form;
    var fees = JSON.parse(element.dataset.nfSidecar).fee;
    var target = document.querySelector('[name=scratch_feeSelect]', form);
    if (fees.minValue === undefined) {
        target.setAttribute('min', 15);
    } else {
        target.setAttribute('min', fees.minValue);
    }
    if (fees.maxValue === undefined) {
        target.removeAttribute('max');
    } else {
        target.setAttribute('max', fees.maxValue);
    }
    target.value = fees.default;
    document.querySelector('[name=members_membershipFee]', form).value = fees.default;
    var help = document.querySelector('#' + target.id + '_help');
    if (help !== null) {
        help.text(document.querySelector('[for=' + element.id + ']', form).text());
    }
}

function updateFee(element) {
    var value = Number.parseInt(element.getAttribute('value'));
    var test = element.getAttribute('min');
    if (test !== undefined && value < test) return;
    var test = element.getAttribute('max');
    if (test !== undefined && value > test) return;
    document.querySelector('[name=members_membershipFee]', element.form).value = value;
}

