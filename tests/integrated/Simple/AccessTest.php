<?php

namespace NextFormTests\Integrated\Simple;

if (!defined('NF_TEST_ROOT')) {
    define('NF_TEST_ROOT', dirname(__DIR__, 2) . '/');
}

include_once NF_TEST_ROOT . 'test-tools/JsonComparison.php';
include_once NF_TEST_ROOT . 'test-tools/MockTranslate.php';
include_once NF_TEST_ROOT . 'test-tools/NullCaptcha.php';
include_once NF_TEST_ROOT . 'test-tools/Page.php';

use Abivia\NextForm\Data\Schema;
use Abivia\NextForm\Form\Form;
use Abivia\NextForm\NextForm;
use Abivia\NextForm\Render\Html\SimpleHtml\Render;
use NextFormTests\Test_Tools\JsonComparison;
use NextFormTests\Test_Tools\MockTranslate;
use NextFormTests\Test_Tools\NullCaptcha;
use NextFormTests\Test_Tools\Page;

class AccessTest extends \PHPUnit\Framework\TestCase {
    use JsonComparison;

    public $accessForm;
    public $accessSchema;

    public $roleList = ['admin', 'owner', 'public'];

    public function setUp() : void {
        NextForm::wireStatic(['Captcha' => NullCaptcha::class]);
        $this->accessForm  = Form::fromFile(__DIR__ . '/../access-form.json');
        $this->accessSchema = Schema::fromFile(__DIR__ . '/../access-schema.json');
    }

    public function testRender() {
        NextForm::boot();
        NextForm::csrfGenerator([Page::class, 'fixedToken']);
        $manager = new NextForm();
        $manager->useOptions(
            [
                'segmentNameMode' => 'off',
                'wire' => [
                    'Render' => Render::class,
                    'Translate' => MockTranslate::class
                ],
            ]
        );
        $manager->addForm(
            $this->accessForm,
            ['action' => 'http://localhost/nextform/post.php']
        );
        $manager->addSchema($this->accessSchema);
        foreach ($this->roleList as $role) {
            $html = $manager->generate(null, ['role' => $role]);
            file_put_contents(
                Page::logFolder(__FILE__) . __FUNCTION__
                . "_{$role}.html",
                Page::write(__FUNCTION__, $html)
            );
        }
        $this->assertTrue(true);
    }

}
