<?php

namespace NextFormTests\Integrated\Flat;

if (!defined('NF_TEST_ROOT')) {
    define('NF_TEST_ROOT', dirname(__DIR__, 2) . '/');
}

include_once NF_TEST_ROOT . 'test-tools/JsonComparison.php';
include_once NF_TEST_ROOT . 'test-tools/MockTranslate.php';
include_once NF_TEST_ROOT . 'test-tools/NullCaptcha.php';
include_once NF_TEST_ROOT . 'test-tools/Page.php';

use Abivia\NextForm\Contracts\RenderInterface;
use Abivia\NextForm\Data\Schema;
use Abivia\NextForm\Form\Binding\Binding;
use Abivia\NextForm\Form\Binding\ContainerBinding;
use Abivia\NextForm\Form\Binding\FieldBinding;
use Abivia\NextForm\Form\Form;
use Abivia\NextForm\NextForm;
use Abivia\NextForm\Render\Block;
use Abivia\NextForm\Render\Html\Show;
use NextFormTests\Test_Tools\JsonComparison;
use NextFormTests\Test_Tools\NullCaptcha;
use NextFormTests\Test_Tools\Page;

class FlatRender implements RenderInterface {

    public function __construct($options = []) {

    }

    public function popContext() {
    }

    public function pushContext() {

    }

    public function render(Binding $binding, $options = []) : Block {
        $result = new Block();
        $type = $binding->getElement()->getType();
        $result->appendBody($type);
        $name = $binding->getNameOnForm();
        if ($name) {
            $result->appendBody(' (' . $name . ')');
        }
        if ($binding instanceof FieldBinding) {
            $result->appendBody(' object = ' . $binding->getObject());
        }
        $result->appendLine();
        if ($binding instanceof ContainerBinding) {
            $result->appendPostLine('Close ' . $type);
        }
        return $result;
    }

    public function useOption($key, $value) : RenderInterface
    {
        return $this;
    }

    public function useOptions($options = []) {

    }

    public function show() {
        return new Show('form');
    }

    public function start($options = []) : Block {
        $result = new Block();
        $result->appendLine('Form');
        $result->appendPostLine('End form');
        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public static function stateData($state) : Block
    {
        $result = new Block();
        foreach ($state as $name => $value) {
            $result->appendLine("state: {$name} = {$value}");
        }
        return $result;
    }

    public static function writeList($list = [], $options = []) : string
    {
        return '';
    }

}

class MemberTest extends \PHPUnit\Framework\TestCase {
    use JsonComparison;

    public $memberForm;
    public $memberSchema;

    public function setUp() : void {
        NextForm::wireStatic(['Captcha' => NullCaptcha::class]);
        $this->memberForm  = Form::fromFile(__DIR__ . '/../member-form.json');
        $this->memberSchema = Schema::fromFile(__DIR__ . '/../member-schema.json');
    }

    /**
     * Integration test for form generation
     * @coversNothing
     */
    public function testUnpopulated() {
        NextForm::boot();
        NextForm::csrfGenerator([Page::class, 'fixedToken']);

        $manager = new NextForm();
        $manager->useOptions(
            [
                'segmentNameMode' => 'off',
                'wire' => ['Render' => FlatRender::class]
            ]
        );
        $manager->addForm($this->memberForm, ['action' => 'myform.php']);
        $manager->addSchema($this->memberSchema);
        $manager->generate();
        $this->assertTrue(true);
    }

    /**
     * Integration test for form generation
     * @coversNothing
     */
    public function testPopulated() {
        NextForm::boot();
        NextForm::csrfGenerator([Page::class, 'fixedToken']);
        $manager = new NextForm();
        $manager->useOptions(
            [
                'segmentNameMode' => 'off',
                'wire' => ['Render' => FlatRender::class]
            ]
        );
        $manager->addSchema($this->memberSchema);
        $manager->addForm($this->memberForm, ['action' => 'myform.php']);
        $data = [
            'id' => 70,
            'primaryFirstName' => 'Jane',
            'primaryLastName' => 'Duh',
        ];
        $manager->populate($data, 'members');
        $manager->generate();
        $this->assertTrue(true);
    }

}
