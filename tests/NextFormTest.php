<?php

namespace NextFormTests;

require_once __DIR__ . '/test-tools/MockBase.php';
require_once __DIR__ . '/test-tools/MockTranslate.php';

use Abivia\NextForm\Contracts\FormInterface;
use Abivia\NextForm\Contracts\SchemaInterface;
use Abivia\NextForm\NextForm;
use Abivia\NextForm\Data\Schema;
use Abivia\NextForm\Data\Segment;
use Abivia\NextForm\Data\Property;
use Abivia\NextForm\Form\Form;
use PHPUnit\Framework\TestCase;

class MockForm extends Test_Tools\MockBase implements FormInterface
{

    public function bind(NextForm $manager)
    {
        self::_MockBase_log(__METHOD__, [$manager]);
        return $this;
    }

    public static function fromFile(string $path) : ?FormInterface
    {
        self::_MockBase_log(__METHOD__, [$path]);
        return new MockForm();
    }

    /**
     * Get a list of top level elements in the form.
     * @return Element[]
     */
    public function getElements()
    {
        self::_MockBase_log(__METHOD__);
        return [];
    }

    public function getName() : ?string
    {
        self::_MockBase_log(__METHOD__);
        return 'someFormName';
    }

    public function getSegment() : ?string
    {
        self::_MockBase_log(__METHOD__);
        return 'someSegmentName';
    }
}

class MockSchema extends Test_Tools\MockBase implements SchemaInterface
{
    public static $schemaCount = 0;

    public function __construct(...$args) {
        self::_MockBase_log(__METHOD__, $args);
    }

    public static function _MockBase_reset()
    {
        parent::_MockBase_reset();
        static::$schemaCount = 0;
    }

    public static function fromFile($path) : ?SchemaInterface
    {
        self::_MockBase_log(__METHOD__, [$path]);
        return new MockSchema();
    }

    public function getDefault($setting = null)
    {
        self::_MockBase_log(__METHOD__, [$setting]);
        return null;
    }

    public function getProperty($segProp, $name = '') : ?Property
    {
        self::_MockBase_log(__METHOD__, [$segProp, $name]);
    }

    public function getSegment($segName) : ?Segment
    {
        self::_MockBase_log(__METHOD__, [$segName]);
        return null;
    }

    public function getSegmentNames()
    {
        self::_MockBase_log(__METHOD__);
        return ['someSegment'. ++self::$schemaCount];
    }

    public function segment($segName, Segment $segment) : SchemaInterface
    {
        self::_MockBase_log(__METHOD__, [$segName, $segment]);
        return $this;
    }

}

class FakeAccess
{
    public static $instances = 0;

    public function __construct()
    {
        ++self::$instances;
    }
}


/**
 * @covers Abivia\NextForm\NextForm
 */
class NextFormTest extends TestCase
{

    public function testInstantiation()
    {
        $obj = new NextForm();
		$this->assertInstanceOf('\Abivia\NextForm\NextForm', $obj);
    }

    public function testWiring() {
        MockForm::_MockBase_reset();
        MockSchema::_MockBase_reset();
        NextForm::wireStatic([
            'Form' => MockForm::class,
            'Schema' => MockSchema::class
        ]);
        $obj = new NextForm();
        $linkedForm = $obj->addForm('foo.json');
        $this->assertInstanceOf(NextForm::class, $linkedForm);

        $obj->addSchema('foo.json');

        // Put the global static settings back
        NextForm::wireStatic([
            'Form' => Form::class,
            'Schema' => Schema::class
        ]);

        $this->expectException('\RuntimeException');
        NextForm::wireStatic(['erroneous service' => 'something']);
    }

    public function testWiringCallable() {
        $expect = FakeAccess::$instances + 1;
        $obj = new NextForm(
            ['wire' =>
                [
                    'Access' => [
                        FakeAccess::class,
                        function () { return new FakeAccess();}
                    ]
                ]
            ]
        );
        $obj->singleton('Access');
        $this->assertEquals($expect, FakeAccess::$instances);
    }

    public function testAddForm()
    {
        MockForm::_MockBase_reset();
        MockSchema::_MockBase_reset();
        $obj = new NextForm();
        $obj->useOptions(['wire' => ['Form' => MockForm::class]]);
        $obj->addForm('foo.json');
        $this->assertEquals(
            [
                ['NextFormTests\MockForm::fromFile', ['foo.json']],
                ['NextFormTests\MockForm::getName', []]
            ],
            MockForm::_MockBase_getLog()
        );
    }

    public function testAddFormArray()
    {
        MockForm::_MockBase_reset();
        MockSchema::_MockBase_reset();
        $obj = new NextForm();
        $obj->useOptions(['wire' => ['Form' => MockForm::class]]);
        $obj->addForm(['foo.json', 'bar.json']);
        $this->assertEquals(
            [
                ['NextFormTests\MockForm::fromFile', ['foo.json']],
                ['NextFormTests\MockForm::getName', []],
                ['NextFormTests\MockForm::fromFile', ['bar.json']],
                ['NextFormTests\MockForm::getName', []],
            ],
            MockForm::_MockBase_getLog()
        );
    }

    public function testAddFormArrayOption()
    {
        MockForm::_MockBase_reset();
        MockSchema::_MockBase_reset();
        $obj = new NextForm();
        $obj->useOptions(['wire' => ['Form' => MockForm::class]]);
        $obj->addForm([
            ['foo.json', ['opt' => 1]],
            'bar.json',
            ['bat.json'],
        ]);
        $this->assertEquals(
            [
                ['NextFormTests\MockForm::fromFile', ['foo.json']],
                ['NextFormTests\MockForm::getName', []],
                ['NextFormTests\MockForm::fromFile', ['bar.json']],
                ['NextFormTests\MockForm::getName', []],
                ['NextFormTests\MockForm::fromFile', ['bat.json']],
                ['NextFormTests\MockForm::getName', []],
            ],
            MockForm::_MockBase_getLog()
        );
    }

    public function testAddFormOption()
    {
        MockForm::_MockBase_reset();
        MockSchema::_MockBase_reset();
        $obj = new NextForm();
        $obj->useOptions(['wire' => ['Form' => MockForm::class]]);
        $obj->addForm('foo.json', ['opt' => 1]);
        $this->assertEquals(
            [
                ['NextFormTests\MockForm::fromFile', ['foo.json']],
                ['NextFormTests\MockForm::getName', []]
            ],
            MockForm::_MockBase_getLog()
        );
    }

    public function testAddSchema()
    {
        MockForm::_MockBase_reset();
        MockSchema::_MockBase_reset();
        $obj = new NextForm();
        $obj->useOptions(['wire' => ['Schema' => MockSchema::class]]);
        $obj->addSchema('foo.json');
        $this->assertEquals(
            [
                ['NextFormTests\MockSchema::fromFile', ['foo.json']],
                ['NextFormTests\MockSchema::__construct', []],
                ['NextFormTests\MockSchema::getSegmentNames', []],
                ['NextFormTests\MockSchema::getSegmentNames', []],
            ],
            MockSchema::_MockBase_getLog()
        );
    }

    public function testAddSchemaArray()
    {
        MockForm::_MockBase_reset();
        MockSchema::_MockBase_reset();
        $obj = new NextForm();
        $obj->useOptions(['wire' => ['Schema' => MockSchema::class]]);
        $obj->addSchema(['foo.json', 'bar.json']);
        $this->assertEquals(
            [
                ['NextFormTests\MockSchema::fromFile', ['foo.json']],
                ['NextFormTests\MockSchema::__construct', []],
                ['NextFormTests\MockSchema::getSegmentNames', []],
                ['NextFormTests\MockSchema::getSegmentNames', []],
                ['NextFormTests\MockSchema::fromFile', ['bar.json']],
                ['NextFormTests\MockSchema::__construct', []],
                ['NextFormTests\MockSchema::getSegmentNames', []],
            ],
            MockSchema::_MockBase_getLog()
        );
    }

    public function testGetSegment()
    {
        MockForm::_MockBase_reset();
        MockSchema::_MockBase_reset();
        $obj = new NextForm();
        $obj->useOptions(['wire' => ['Schema' => MockSchema::class]]);
        $obj->addSchema(['foo.json']);
        $obj->getSegment('someSegment1');
        $actual = MockSchema::_MockBase_getLog();
        $this->assertEquals(
            [
                ['NextFormTests\MockSchema::fromFile', ['foo.json']],
                ['NextFormTests\MockSchema::__construct', []],
                ['NextFormTests\MockSchema::getSegmentNames', []],
                ['NextFormTests\MockSchema::getSegmentNames', []],
                ['NextFormTests\MockSchema::getSegment', ['someSegment1']],
            ],
            $actual
        );
    }

    public function testBinding()
    {
        $schema = Schema::fromFile(__DIR__ . '/test-data/test-schema.json');
        $form = Form::fromFile(__DIR__ . '/test-data/newform.json');
        $manager = new NextForm();
        $manager->addSchema($schema)->addForm($form);
        $manager->bind();
        $data = $manager->getData();
        $this->assertEquals(['test.text' => null], $data);
    }

    public function testBindingEmpty()
    {
        $manager = new NextForm();
        $this->expectException('\RuntimeException');
        $manager->bind();
    }

    public function testGenerate()
    {
        MockForm::_MockBase_reset();
        MockSchema::_MockBase_reset();
        $obj = new NextForm();
        $obj->useOptions(
            [
                'wire' => [
                    'Form' => MockForm::class,
                    'Schema' => MockSchema::class,
                    'Translate' => Test_Tools\MockTranslate::class,
                ]
            ]
        );
        $obj->addSchema('foo.json');
        $page = $obj->generate('foo2.json');
        $this->assertEquals(
            [
                ['NextFormTests\MockSchema::fromFile', ['foo.json']],
                ['NextFormTests\MockSchema::__construct', []],
                ['NextFormTests\MockSchema::getSegmentNames', []],
                ['NextFormTests\MockSchema::getSegmentNames', []],
            ],
            MockSchema::_MockBase_getLog()
        );

        // Test retrieval functions
        $this->assertEquals($page, $obj->getBlock());
        $this->assertNull($obj->getBlock('doesNotExist'));
        $this->assertInstanceOf(
            '\Abivia\NextForm\Render\Block',
            $obj->getBlock('someFormName')
        );

        $this->assertEquals($page->getHead(), $obj->getHead());

        $this->assertEquals($page->getBody(), $obj->getBody());

        $text = \implode("\n", $page->getLinkedFiles());
        $this->assertEquals($text, $obj->getLinks());

        $this->assertEquals($page->getScript(), $obj->getScript());

        $text = \implode("\n", $page->getScriptFiles());
        $this->assertEquals($text, $obj->getScriptFiles());

        $this->assertEquals($page->getStyles(), $obj->getStyles());

        return [$page, $obj];
    }

    /**
     * @depends testGenerate
     * @param array $pair
     */
    public function testGenerateBody($pair) {
        [$page, $obj] = $pair;
        $this->expectOutputString($page->getBody());
        $obj->body();
    }

    /**
     * @depends testGenerate
     * @param array $pair
     */
    public function testGenerateHead($pair) {
        list($page, $obj) = $pair;
        $this->expectOutputString($page->getHead());
        $obj->head();
    }

    /**
     * @depends testGenerate
     * @param array $pair
     */
    public function testGenerateLinkedFiles($pair) {
        list($page, $obj) = $pair;
        $text = \implode("\n", $page->getLinkedFiles());
        $this->assertEquals($text, $obj->getLinks());
        $this->expectOutputString($text);
        $obj->links();
    }

    /**
     * @depends testGenerate
     * @param array $pair
     */
    public function testGenerateScript($pair) {
        list($page, $obj) = $pair;
        $this->expectOutputString($page->getScript());
        $obj->script();
    }

    /**
     * @depends testGenerate
     * @param array $pair
     */
    public function testGenerateScriptFiles($pair) {
        list($page, $obj) = $pair;
        $text = \implode("\n", $page->getScriptFiles());
        $this->assertEquals($text, $obj->getScriptFiles());
        $this->expectOutputString($text);
        $obj->scriptFiles();
    }

    /**
     * @depends testGenerate
     * @param array $pair
     */
    public function testGenerateStyles($pair) {
        list($page, $obj) = $pair;
        $this->expectOutputString($page->getStyles());
        $obj->styles();
    }

    public function testHtmlIdentifier()
    {
        NextForm::boot();
        $this->assertEquals('nf_1', NextForm::htmlIdentifier());
        $this->assertEquals('nf_2', NextForm::htmlIdentifier());
        $this->assertEquals('foo', NextForm::htmlIdentifier('foo'));
        $this->assertEquals('foo_3', NextForm::htmlIdentifier('foo', true));
        $this->assertEquals('f_o_o', NextForm::htmlIdentifier('f#o*o'));
        NextForm::boot();
        $this->assertEquals('nf_1', NextForm::htmlIdentifier());
    }

    public function testFormErrors() {
        $manager = new NextForm();
        $expect = [
            'seg1' => [
                's1f1' => ['bad things'],
            ],
            'seg2' => [
                's2f1' => ['other bad things'],
            ],
            'seg3' => [
                's3f1' => ['more bad things'],
            ],
        ];
        $part1 = $expect;
        unset($part1['seg2']);
        $manager->populateErrors($part1);
        $part2 = $expect['seg2'];
        $manager->populateErrors('seg2', $part2);

        $this->assertEquals($expect, $manager->getFormErrors());
    }

}
