<?php

namespace NextFormTests\Test_Tools;

use Abivia\NextForm\Contracts\CaptchaInterface;
use Abivia\NextForm\Contracts\RenderInterface;
use Abivia\NextForm\Form\Binding\Binding;
use Abivia\NextForm\Render\Block;

class NullCaptcha implements CaptchaInterface
{
    private $answer;

    /**
     * Render the captcha binding.
     *
     * @param RenderInterface $engine
     * @param Binding $binding
     * @param array $options
     * @return Block
     */
    public function render(
        RenderInterface $engine,
        Binding $binding,
        $options = []
    ) : Block {

        $block = Block::fromString('<div>Your visible captcha part here</div>');

        return $block;
    }

    public function reset()
    {
        $this->answer = null;
    }
}
