<?php

namespace NextFormTests\Test_Tools;

use Abivia\NextForm\Render\Block;

/**
 *
 */
class Page {
    public static function fixedToken() {
        return ['_nf_token', 'not-so-random'];
    }

    /**
     * Get the folder where results for this file should be logged, if any.
     *
     * @param string $forTestFile
     * @return string|null
     */
    public static function logFolder($forTestFile) {
        if (!defined('NF_TEST_ROOT')) {
            return null;
        }
        if (!file_exists(NF_TEST_ROOT . 'logs')) {
            return null;
        }
        $relDir = substr(dirname($forTestFile), strlen(NF_TEST_ROOT));
        $logDir = NF_TEST_ROOT . 'logs/' . $relDir . '/';
        if (!is_dir($logDir)) {
            mkdir($logDir, 0777, true);
        }
        return $logDir;
    }

    public static function write($title, Block $html, $template = 'boilerplate') {
        $page = file_get_contents(__DIR__ . '/' . $template . '.html');
        $page = str_replace(
            [
                '{{title}}',
                '<!--{{head}}-->',
                '{{form}}',
                '<!--{{scripts}}-->',
                '{{context}}'
            ],
            [
                $title,
                $html->getHead() . implode("\n", $html->getLinkedFiles()) . "\n"
                . ($html->getStyles() ? "<style>\n" . $html->getStyles() . "</style>" : ''),
                $html->getBody(),
                implode("\n", $html->getScriptFiles())
                . (
                    $html->getScript()
                    ? "<script>\n" . $html->getScript() . "</script>"
                    : ''
                ),
                str_replace('--', '- -', print_r($html->getContext(), true)),
            ],
            $page
        );
        return $page;
    }
}
