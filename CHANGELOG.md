Change log for Abivia\NextForm
====

1.10.0
---
- Restructure rendering
- Add Bootstrap 5 support
- Add native JS generation to remove jQuery dependency
- Initial Tailwind support (incomplete)

1.9.1
---
- Bugfix on field width; fix test.

1.9.0
---
- Correct compound syntax in show strings
- Implement show:width for input fields

1.8.3
---
- Bugfix: render cell contents flush with other form elements.

1.8.2
---
- Fix issue with access control in rendering.

1.8.1
---
- Fix issues with access control on container elements (cell, section).

1.8.0
---
- Support roles on non-field form elements.

1.7.1
---
- Bugfix: fix error when segment has no properties.
- Bugfix: compact form roles at the segment level were not being processed
correctly.

1.7.0
---
- Rename "compound" JSON output to "compact".
- Apply compact JSON mode to all compound and shorthand attributes.
- Implement compact syntax for roles in a Data\Property.

1.6.0
---
- Implement more compact, compound definition syntax for Data\Presentation and
Data\Store objects.
- Allow a Data\Validation object to be represented by a single rules string.
- Add NextForm::toJson() with options to control use of compound forms when
writing as JSON.

1.5.0
---
- Add role attribute to Segments to set defaults for all objects.
- Fix: don't write resolved auto-labels when saving a Segment in JSON.

1.4.1
---
- Fix composer dependency

1.4.0
---
- Refactor `NextForm::populateErrors()` to facilitate multiple calls and
multiple segments per call. Add the getter `NextForm::getFormErrors()`

1.3.3
---
- Support all NextForm rules in the `rules` string, when JSON encoding, do
not emit properties that match the values set in a rules string.

1.3.2
---
- Added Laravel-like "rules" property to validations.

1.3.1
---
- Added the `autoLabels` property to segment definitions. Objects in a segment
can now inherit default labels that automatically incorporate the object name.

1.3.0
---
- Minimum PHP now 7.4
- Bugfix: autofocus was being ignored.
- New form property: autofocus. Used to specify autofocus field by name.
- Improve test coverage

1.2.3
---
- Bufgix: No name attribute on Containers (sections, cells)
- Added NextForm specific exceptions

1.2.2
---
- Add options to NexfForm::reset to improve application code readability.

1.2.1
---
- Support arrays of role names for access resolution.

1.2.0
---
- New role-based system for simplified access control.
- Deprecated the original access control system.
- New "mask" access will display the field but masks the value and
makes it read-only

1.1.0
----
- Add arguments to the build() methods in most Data classes.
- All changes backwards compatible.

1.0.1
----
- Bugfix in NextForm::reset()

1.0.0
----
- Stable release
- Add NextForm::reset() to help with use as a singleton.
- Add some features to BasicAccess
