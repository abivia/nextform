<?php

declare(strict_types=1);

namespace Abivia\NextForm\Access;

use Abivia\Configurable\Configurable;

/**
 * A simple user for the BasicAccess class.
 *
 * @deprecated since version 1.2.0
 *
 */
class User
{
    use Configurable;

    /**
     * The user identifier.
     * @var string
     */
    protected $id;

    /**
     * A list of roles this user belongs to.
     * @var string[]
     */
    protected $roles = [];

    /**
     * Add a role to this user.
     *
     * @param string $name The role name.
     * @return $this
     */
    public function addRole(string $name)
    {
        if (!in_array($name, $this->roles)) {
            $this->roles[] = $name;
        }
        return $this;
    }

    /**
     * Delete a role from this user.
     *
     * @param string $name The name of the rile to remove.
     * @return $this
     */
    public function deleteRole($name)
    {
        if (($key = array_search($name, $this->roles))) {
            unset($this->roles[$key]);
        }
        return $this;
    }

    /**
     * Get the user ID.
     *
     * @return int|string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the user's roles.
     *
     * @return string[]
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Set the user ID.
     * @param int|string $id The user identifier.
     * @return $this
     */
    public function id($id)
    {
        $this->id = $id;
        return $this;
    }

}
