<?php

declare(strict_types=1);

namespace Abivia\NextForm\Access;

use Abivia\Configurable\Configurable;
use Abivia\NextForm\Contracts\AccessInterface;

/**
 * This a very skeletal access provider driven by a simple configuration file.
 *
 * @deprecated since version 1.2.0
 *
 */
class BasicAccess implements AccessInterface
{
    use Configurable;

    /**
     * The default user to check when no explicit user identifier is passed to allows().
     * @var string
     */
    protected $currentUser = null;

    /**
     * Role definitions.
     * @var Role[]
     */
    protected $roles;

    /**
     * User roles.
     * @var User[]
     */
    protected $users;

    /**
     * Determine if the a user is allowed to perform an operation on an object.
     *
     * @param string $segment The segment that the requested object belongs to.
     * @param string $objectName The name of the object.
     * @param string $operation The operation we're asking permission for (read|write).
     * @param int|string $user User to query, uses current user if null
     * @return bool True if access is granted.
     */
    public function allows(
        string $segment,
        string $objectName,
        string $operation,
        $user = null
    ): bool {
        $user = $this->checkUser($user);
        $composite = $segment . '/'. $objectName;
        $segAccess = null;
        $objAccess = null;
        // Check the roles for the current user
        foreach ($this->users[$user]->getRoles() as $role) {
            if (isset($this->roles[$role])) {
                $has = $this->roles[$role]->has($segment, $operation);
                if ($has !== null) {
                    $segAccess = $has;
                }
                if ($segAccess === false) {
                    break;
                }
                $has = $this->roles[$role]->has($composite, $operation);
                if ($has !== null) {
                    $objAccess = $has;
                }
                if ($objAccess === false) {
                    break;
                }
            }
        }
        if ($objAccess !== null) {
            $access = $objAccess;
        } else {
            $access = $segAccess === true;
        }
        return $access;
    }

    /**
     * Check that the user is valid or that there's a current user set.
     *
     * @param scalar $user
     * @return scalar
     * @throws \LogicException
     */
    protected function checkUser($user = null)
    {
        if ($user === null) {
            $user = $this->currentUser;
        }
        if ($user === null || !isset($this->users[$user])) {
            throw new \LogicException('No valid user has been selected.');
        }

        return $user;
    }

    /**
     * Map a property to a class.
     *
     * @staticvar array $classMap Maps property names to a set of instantiation rules
     * @param string $property The name of the property to check.
     * @param scalar|array|object $value The current value of the named property.
     * @return object|false An instantiation rule object or false if no rule applies.
     */
    protected function configureClassMap($property, $value)
    {
        static $classMap = [
            'roles' => [
                'className' => '\Abivia\NextForm\Access\Role',
                'key' => 'getName',
                'keyIsMethod' => true
            ],
            'users' => [
                'className' => '\Abivia\NextForm\Access\User',
                'key' => 'getId',
                'keyIsMethod' => true
            ],
        ];
        if (isset($classMap[$property])) {
            return (object) $classMap[$property];
        }
        // @codeCoverageIgnoreStart
        return false;
        // @codeCoverageIgnoreEnd
    }

    /**
     * Check if the property can be loaded from configuration.
     *
     * @param string $property
     * @return bool true if the property is allowed.
     */
    protected function configurePropertyAllow($property)
    {
        return in_array($property, ['roles', 'users']);
    }

    /**
     * Determine if the specified/current user has the named role.
     *
     * @param string $role The role name to look for.
     * @param scalar|null $user The user identifier, null to use current user.
     *
     * @return bool
     */
    public function hasRole(string $role, $user = null): bool
    {
        $user = $this->checkUser($user);
        return in_array($role, $this->users[$user]);
    }

    /**
     * Set a default user for subsequent access requests.
     *
     * @param int|string $user The user identifier
     * @return $this
     * @throws \LogicException
     */
    public function user($user) : AccessInterface
    {
        if ($user === null || isset($this->users[$user])) {
            $this->currentUser = $user;
            return $this;
        }
        $this->currentUser = null;
        throw new \LogicException('User not found.');
    }
}
