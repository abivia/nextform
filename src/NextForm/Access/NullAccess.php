<?php

declare(strict_types=1);

namespace Abivia\NextForm\Access;

use Abivia\NextForm\Contracts\AccessInterface;

/**
 * This the default null access provider.
 *
 * @deprecated since version 1.2.0
 *
 */
class NullAccess implements AccessInterface
{

    /**
     * Null access provider: grants access to everything.
     *
     * @param string $segment The segment that the requested object belongs to.
     * @param string $objectName The name of the object.
     * @param string $operation The operation we're asking permission for (read|write).
     * @param int|string $user User to query, uses current user if null
     * @return bool True if access is granted. Always true.
     */
    public function allows(
        string $segment,
        string $objectName,
        string $operation,
        $user = null
    ) : bool
    {
        return true;
    }

    /**
     * Provide a set user method.
     *
     * @param mixed $user
     * @return $this
     */
    public function user($user)
    {
        return $this;
    }

}
