<?php

declare(strict_types=1);

namespace Abivia\NextForm\Helpers;

/**
 * Helper for compact parameter strings.
 */
class CompactParameters
{
    /**
     * Break a presentation string up into a representative object.
     * @param string $compound
     * @return \stdClass
     */
    public static function decompose(string $compound, array $dictionary)
    : \stdClass {
        $result = new \stdClass();

        foreach (self::explode($compound) as $prop => $elements) {
            if (count($elements)) {
                $result->$prop = array_shift($elements);
            } elseif (in_array($prop, $dictionary)) {
                $result->type = $prop;
            } else {
                $result->$prop = true;
            }
        }

        return $result;
    }

    public static function explode(string $compound, $onlyFirst = false)
    : array {
        $result = [];

        foreach (explode('|', $compound) as $part) {
            $part = trim($part);
            if ($part === '') {
                continue;
            }
            $elements = explode(':', $part);
            $prop = array_shift($elements);
            $result[$prop] = $onlyFirst ? array_shift($elements) : $elements;
        }

        return $result;
    }

    public static function implode($params) : string
    {
        $props = [];
        foreach ($params as $prop => $elements) {
            if ($elements === null) {
                $props[] = $prop;
            } elseif (is_array($elements)) {
                $props[] = $prop . ':' . implode(',', $elements);
            } else {
                $props[] = $prop . ':' . $elements;
            }
        }

        return implode('|', $props);
    }

}