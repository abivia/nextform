<?php

declare(strict_types=1);

namespace Abivia\NextForm\Data;

use Abivia\Configurable\Configurable;
use Abivia\NextForm\Traits\HasRoles;
use Abivia\NextForm\Traits\JsonEncoderTrait;

/**
 * Describes a collection of Properties that come from the same source, for
 * example  columns are the properties that come from a database table. The
 * table represents a segment.
 */
class Segment implements \Countable, \IteratorAggregate, \JsonSerializable
{
    use Configurable;
    use HasRoles;
    use JsonEncoderTrait;

    /**
     * Default text for contained elements
     * @var \Abivia\NextForm\Data\Labels
     */
    protected $autoLabels = null;

    /**
     * Rules for the JsonEncoder
     * @var array
     */
    protected static $jsonEncodeMethod = [
        'name' => [],
        'primary' => ['drop:empty', 'scalarize'],
        'autoLabels' => ['drop:empty', 'drop:null'],
        'roles' => ['method:jsonCollapseRoles', 'drop:blank', 'drop:empty'],
        'properties' => ['map:objects','array'],
    ];

    /**
     * The name of this segment
     * @var string
     */
    protected $name;

    /**
     * A list of the properties in this segment.
     * @var Property[]
     */
    protected $properties;

    /**
     * Objects that constitute a primary key for the segment.
     * @var string[]
     */
    protected $primary = [];

    /**
     * Create a new segment, with optional name and properties.
     *
     * @param string|null $name The segment name.
     * @param Property|array|null $props Properties to be placed in this segment.
     *
     * @return Segment
     */
    public static function build(?string $name = null, $props = null) : Segment
    {
        $seg = new Segment();
        if ($name !== null) {
            $seg->name($name);
        }
        if (is_array($props)) {
            foreach ($props as $prop) {
                $seg->property($prop);
            }
        } elseif ($props instanceof Property) {
            $seg->property($props);
        }

        return $seg;
    }

    /**
     * Check that all elements in a list of property names are defined.
     *
     * @param string[] $keyList List of property names.
     * @return string Name of the first missing property or empty if no errors.
     */
    protected function checkPrimary($keyList)
    {
        foreach ($keyList as $propName) {
            if (!isset($this->properties[$propName])) {
                return $propName;
            }
        }
        return '';
    }

    /**
     * Map a property to a class.
     *
     * @param string $property The current class property name.
     * @param mixed $value The value to be stored in the property, made
     *  available for inspection.
     * @return mixed An object containing a class name and key, or false
     * @codeCoverageIgnore
     */
    protected function configureClassMap($property, $value)
    {
        static $classMap = [
            'autoLabels' => ['className' => Labels::class],
            'roles' => ['className' => 'array'],
            'properties' => [
                'className' => Property::class,
                'key' => 'getName',
                'keyIsMethod' => true
            ],
        ];
        if (isset($classMap[$property])) {
            return (object) $classMap[$property];
        }
        return false;
    }

    protected function configureComplete()
    {
        // Make sure the properties listed in primary exist.
        $badPropName = $this->checkPrimary($this->primary);
        if ($badPropName !== '') {
            $this->configureLogError(
                $badPropName . ' does not exist but is named as a primary key in segment '
                . $this->name
            );
            return false;
        }

        $hasAutoLabels = $this->autoLabels !== null;
        $hasRoleDefaults = count($this->roles) !== 0;
        foreach ($this->properties ?? [] as $prop) {
            // Link the segment to the property.
            $prop->segment($this);

            // Apply autolabels
            if ($hasAutoLabels) {
                $prop->getLabels()->autoLabel(
                    $this->autoLabels, $prop->getName()
                );
            }

            // Apply default roles
            if ($hasRoleDefaults) {
                $prop->roleDefaults($this->roles);
            }
        }

        return true;
    }

    /**
     * Rename "objects" in the configure file to "properties" in this class.
     *
     * @param string $property
     * @return string
     */
    protected function configurePropertyMap($property): string
    {
        if ($property === 'objects') {
            return 'properties';
        }
        return $property;
    }

    /**
     * Convert string as a primary key into a single valued array.
     *
     * @param string $property The property name.
     * @param mixed $value The current property value.
     * @return type
     */
    protected function configureValidate($property, &$value)
    {
        if ($property === 'primary' && !\is_array($value)) {
            $value = [$value];
        }
        if ($property === 'roles') {
            $value = $this->validateRoles($value);
            if ($value === false) {
                $this->configureLogError(
                    'Access must be one of '
                    . implode(', ', self::$accessLevels) . '.'
                );
                return false;
            }
        }
        return $value;
    }

    /**
     * Get the number of properties in this Segment.
     *
     * @return int
     */
    public function count() : int
    {
        return $this->properties ? count($this->properties) : 0;
    }

    /**
     * Get an iterator for looping.
     *
     * @return \ArrayIterator
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->properties);
    }

    /**
     * Get the name of this segment.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get the list of properties contributing to a primary key.
     *
     * @return string[]
     */
    public function getPrimary()
    {
        return $this->primary;
    }

    /**
     * Get a property by name.
     *
     * @param string $propName Name of the property to fetch.
     * @return Property|null
     */
    public function getProperty(string $propName) : ?Property
    {
        return isset($this->properties[$propName])
            ? $this->properties[$propName] : null;
    }

    public function getRoles() : array
    {
        return $this->roles;
    }

    /**
     * Set the name of this segment.
     *
     * @param string $name Name for the segment.
     * @return $this
     */
    public function name(string $name) :self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Set the primary key properties for this segment.
     *
     * @param string|array $keyList
     * @return $this
     * @throws \RuntimeException
     */
    public function primary($keyList)
    {
        // Check the format of the values, converting string to array.
        $keyList = $this->configureValidate('primary', $keyList);

        // Make sure all the properties are defined.
        $badPropName = $this->checkPrimary($keyList);
        if ($badPropName !== '') {
            throw new DefinitionException(
                "${badPropName} is not a valid primary key"
                . " in segment {$this->name}."
            );
        }
        $this->primary = $keyList;
        return $this;
    }

    /**
     * Set/replace a property.
     *
     * @param Property $prop The property to add/replace.
     * @return $this
     * @throws \RuntimeException
     */
    public function property(Property $prop)
    {
        $propName = $prop->getName();
        if ($propName === null || $propName === '') {
            throw new DefinitionException(
                "Cannot assign unnamed property to segment {$this->name}."
            );
        }

        // Apply autolabels to the property
        if ($this->autoLabels !== null) {
            $prop->getLabels()->autoLabel($this->autoLabels, $propName);
        }

        // Apply role defaults
        $prop->roleDefaults($this->roles);

        $this->properties[$propName] = $prop;

        return $this;
    }

}
