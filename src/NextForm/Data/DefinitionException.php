<?php

namespace Abivia\NextForm\Data;

use RuntimeException;

class DefinitionException extends RuntimeException
{
    //
}
