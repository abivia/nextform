<?php

declare(strict_types=1);

namespace Abivia\NextForm\Data;

use Abivia\Configurable\Configurable;
use Abivia\NextForm\Form\Binding\Binding;
use Abivia\NextForm\Helpers\CompactParameters;
use Abivia\NextForm\NextForm;
use Abivia\NextForm\Traits\HasRoles;
use Abivia\NextForm\Traits\JsonEncoderTrait;

/**
 * A Property describes a data object in the schema; objects are contained
 * in Segments.
 */
class Property implements \JsonSerializable
{
    use Configurable;
    use HasRoles;
    use JsonEncoderTrait;

    /**
     * Description of what this property is / is for.
     * @var string
     */
    protected $description;

    /**
     * Rules for the JsonEncoder
     * @var array
     */
    protected static $jsonEncodeMethod = [
        'name' => [],
        'description' => ['drop:null'],
        'labels' => ['drop:empty', 'drop:null'],
        'population' => ['drop:empty', 'drop:null'],
        'presentation' => ['drop:empty', 'drop:null'],
        'roles' => ['method:jsonCollapseRoles', 'drop:empty', 'drop:blank'],
        'store' => ['drop:empty', 'drop:null'],
        'validation' => ['drop:empty', 'drop:null'],
    ];

    /**
     * Text associated with an element
     * @var \Abivia\NextForm\Data\Labels
     */
    protected $labels;

    /**
     * A list of form bindings that use this property
     * @var Binding[]
     */
    protected $linkedBindings = [];

    /**
     * The segment-unique name of this property.
     * @var string
     */
    protected $name;

    /**
     * Description of the data contained in this property.
     * @var Population
     */
    protected $population;

    /**
     * Description on how this property should be presented on a form.
     * @var Presentation
     */
    protected $presentation;

    /**
     * Reference to the segment this property belongs to.
     * @var Segment
     */
    protected $segment;

    /**
     * Definition of where the data is stored.
     * @var Store
     */
    protected $store;

    /**
     * Rules used to validate values for this property.
     * @var Validation
     */
    protected $validation;

    /**
     * Initialize a new Property.
     */
    public function __construct()
    {
        $this->labels = new Labels();
    }

    /**
     * Create a new property, with optional parameters for type, name, heading
     *
     * @param string|null $type
     * @param string|null $name
     * @param string|array|Labels|null $labels Heading (string) or associative array.
     * @return Property
     */
    public static function build(
        ?string $type = null,
        ?string $name = null,
        $labels = null
    ) {
        $property = new Property();
        if ($type !== null) {
            $property->presentation(Presentation::build($type));
        }
        if ($name !== null) {
            $property->name($name);
        }
        if ($labels !== null) {
            if ($labels instanceof Labels) {
                $property->labels($labels);
            } else {
                $property->labels(Labels::build($labels));
            }
        }
        return $property;
    }

    /**
     * Map a property to a class.
     * @param string $property The current class property name.
     * @param mixed $value The value to be stored in the property, made available for inspection.
     * @return mixed An object containing a class name and key, or false
     * @codeCoverageIgnore
     */
    protected function configureClassMap($property, $value)
    {
        static $classMap = [
            'labels' => ['className' => '\Abivia\NextForm\Data\Labels'],
            'population' => ['className' => '\Abivia\NextForm\Data\Population'],
            'presentation' => ['className' => '\Abivia\NextForm\Data\Presentation'],
            'roles' => ['className' => 'array'],
            'store' => ['className' => '\Abivia\NextForm\Data\Store'],
            'validation' => ['className' => '\Abivia\NextForm\Data\Validation'],
        ];
        if (isset($classMap[$property])) {
            return (object) $classMap[$property];
        }
        return false;
    }

    /**
     * Ensure that we have a label object after configuration is completed
     * @return bool
     */
    protected function configureComplete()
    {
        if ($this->labels === null) {
            $this->labels = new Labels();
        }
        return true;
    }

    protected function configureValidate($property, &$value)
    {
        if ($property === 'roles') {
            $value = $this->validateRoles($value);
            if ($value === false) {
                $this->configureLogError(
                    'Access must be one of '
                    . implode(', ', self::$accessLevels) . '.'
                );
                return false;
            }
        }
        return true;
    }

    /**
     * Set the description for this property.
     *
     * @param string $text Descriptive text.
     * @return $this
     */
    public function description($text)
    {
        $this->description = $text;
        return $this;
    }

    /**
     * Get the description of this Property.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Get the schema-defined labels.
     *
     * @return Abivia\NextForm\Data\Labels
     */
    public function getLabels()
    {
        return $this->labels;
    }

    /**
     * Get the property name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get the property's population object.
     *
     * @return Population
     */
    public function getPopulation() : Population
    {
        if ($this->population === null) {
            $this->population = new Population();
        }
        return $this->population;
    }

    /**
     * Get the property's presentation object.
     *
     * @return Presentation|null
     */
    public function getPresentation() : ?Presentation
    {
        return $this->presentation;
    }

    /**
     * Get the property's Roles
     *
     * @return array
     */
    public function getRoles() : array
    {
        return $this->roles;
    }

    /**
     * Get the property's data store object.
     *
     * @return ?Store
     */
    public function getStore() : ?Store
    {
        return $this->store;
    }

    /**
     * Get the property's validation object.
     *
     * @return Validation
     */
    public function getValidation() : Validation
    {
        if ($this->validation === null) {
            $this->validation = Validation::build();
        }
        return $this->validation;
    }

    /**
     * Remove any segment defaults from JSON output.
     *
     * @param type $prop The current property
     * @param array $value The property value
     */
    public function jsonCollapseRoles($prop, &$value)
    {
        $inherit = $this->segment === null ? [] : $this->segment->getRoles();

        foreach ($value as $role => $right) {
            if (isset($inherit[$role]) && $inherit[$role] === $right) {
                unset($value[$role]);
            }
        }
        if (
            count($value) === 1
            && isset($value['*']) && $value['*'] === 'write'
        ) {
            $value = [];
        }
        if (NextForm::$jsonCompact) {
            $value = CompactParameters::implode($value);
        }
    }

    /**
     * Set the display texts for this Property.
     *
     * @param Labels $labels Object containing displayable labels.
     * @return $this
     */
    public function labels(Labels $labels)
    {
        $this->labels = $labels;
        return $this;
    }

    /**
     * Connect a form element to this property.
     *
     * @param Binding $binding The binding to be connected.
     * @return $this
     */
    public function linkBinding(Binding $binding)
    {
        if (!in_array($binding, $this->linkedBindings)) {
            $this->linkedBindings[] = $binding;
        }
        return $this;
    }

    /**
     * Set the Property name.
     *
     * @param string $name
     * @return $this
     */
    public function name($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * set the property's population object.
     *
     * @param Population $population
     */
    public function population(Population $population)
    {
        $this->population = $population;
        return $this;
    }

    /**
     * Set the property's presentation object.
     *
     * @param Presentation $presentation Presentation settings.
     * @return $this
     */
    public function presentation(Presentation $presentation)
    {
        $this->presentation = $presentation;
        return $this;
    }

    /**
     * Set access level for a role.
     *
     * @param string $role The name of the role
     * @param string $access The access level.
     *
     * @return $this
     *
     * @throws DefinitionException
     */
    public function role(string $role, string $access)
    {
        if ($role !== '*' && !isset($this->roles['*'])) {
            throw new DefinitionException('Default role not yet set.');
        }
        if (!in_array($access, self::$accessLevels)) {
            throw new DefinitionException(
                "{$access} is an invalid access level."
            );
        }
        $this->roles[$role] = $access;

        return $this;
    }

    /**
     * Merge inheritable roles from the segment
     *
     * @param array $roles
     *
     * @return $this
     */
    public function roleDefaults(array $roles)
    {
        $this->roles = array_merge($roles, $this->roles);
        return $this;
    }

    /**
     * Set one or more roles via a string.
     *
     * @param string $role Role settings in the form role:level|role2:level2...
     *
     * @return $this
     *
     * @throws DefinitionException
     */
    public function roles(string $roles)
    {
        $parts = CompactParameters::explode($roles, true);
        if (isset($parts['*'])) {
            $this->role('*', $parts['*']);
            unset($parts['*']);
        } elseif (!isset($this->roles['*'])) {
            throw new DefinitionException('Default role not set.');
        }
        foreach ($parts as $role => $access) {
            $this->role($role, $access);
        }

        return $this;
    }

    /**
     * Set the segment this property belongs to.
     *
     * @param Segment $segment
     *
     * @return $this
     */
    public function segment(Segment $segment)
    {
        $this->segment = $segment;

        return $this;
    }

    /**
     * Set the property's data store.
     *
     * @param Store $store
     * @return $this
     */

    public function store(Store $store)
    {
        $this->store = $store;
        return $this;
    }

    /**
     * Set the property's validation object.
     *
     * @param Validation $validation Validation rules.
     * @return $this
     */
    public function validation(Validation $validation)
    {
        $this->validation = $validation;
        return $this;
    }

}
