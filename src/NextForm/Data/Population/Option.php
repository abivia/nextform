<?php

declare(strict_types=1);

namespace Abivia\NextForm\Data\Population;

use Abivia\Configurable\Configurable;
use Abivia\NextForm\Data\DefinitionException;
use Abivia\NextForm\NextForm;
use Abivia\NextForm\Traits\JsonEncoderTrait;
use Abivia\NextForm\Traits\ShowableTrait;

use Illuminate\Contracts\Translation\Translator;

/**
 * Describes a value or list of values in a user selectable form object.
 */
class Option implements \JsonSerializable
{
    use Configurable;
    use JsonEncoderTrait;
    use ShowableTrait;

    /**
     * Set if this option should receive initial focus.
     * @var bool
     */
    protected $autofocus = false;

    /**
     * Whether or not this option is currently available.
     * @var bool
     */
    protected $enabled = true;

    /**
     * A list of groups that this option belongs to.
     * @var string[]
     */
    protected $groups = [];

    protected static $jsonEncodeMethod = [
        'label' => [],
        'value' => ['drop:null'],
        'groups' => ['drop:null', 'drop:empty', 'scalarize', 'map:memberOf'],
        'enabled' => ['drop:true'],
        'autofocus' => ['drop:false'],
        'name' => ['drop:blank','drop:null'],
        'sidecar' => ['drop:null'],
    ];

    /**
     * The user-visible text associated with this option.
     *
     * @var string
     */
    protected $label;

    protected $name;

    /**
     * Indicates if this is one of the selected options in a list.
     *
     * @var bool
     */
    protected $selected = false;

    /**
     * Arbitrary data associated with this field.
     * @var scalar|array|object
     */
    protected $sidecar;

    /**
     * The value to return for this option.
     *
     * @var int|string
     */
    protected $value;

    public function __construct()
    {
        //self::$showDefaultScope = 'option';
    }

    /**
     * Add this option to the named group.
     *
     * @param string $groupName Name of the group to be added.
     * @return $this
     */
    public function addGroup(string $groupName)
    {
        if (!in_array($groupName, $this->groups)) {
            $this->groups[] = $groupName;
            $this->configureValidate('groups', $this->groups);
        }
        return $this;
    }

    /**
     * Set the autofocus flag for this option.
     *
     * @param bool|null $autofocus
     *
     * @return $this
     */
    public function autofocus(?bool $autofocus = true)
    {
        $this->autofocus = $autofocus;
        return $this;
    }

    /**
     * Option factory.
     *
     * @return Option
     */
    public static function build(?string $label = null, $value = null) : Option
    {
        $option = new Option();
        if ($label !== null) {
            $option->label($label);
        }
        if ($value !== null) {
            $option->value($value);
        }

        return $option;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureClassMap($property, $value)
    {
        $result = false;
        if ($property === 'value' && is_array($value)) {
            $result = self::class;
        }
        return $result;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureComplete(): bool
    {
        if (\is_array($this->value)) {
            foreach($this->value as $option) {
                if (\is_array($option->getList())) {
                    throw new DefinitionException('Options can\'t be nested more than two levels deep.');
                }
            }
        }
        if ($this->label === null || $this->label === '') {
            throw new DefinitionException('Options must have a label.');
        }
        if ($this->value === null || $this->value === '') {
            $this->value = $this->label;
        }
        return true;
    }

    /**
     * Facilitates the use of simple string values of the form "label[:value]"
     * in the configuration by converting them to classes.
     *
     * @param \stdClass $config
     * @param array $context
     */
    protected function configureInitialize(&$config, ...$context)
    {
        // If the configuration is a string, treat it as label[:value]
        if (\is_string($config)) {
            $obj = new \stdClass();
            if (($posn = \strrpos($config, NextForm::GROUP_DELIM)) !== false) {
                $obj->label = \substr($config, 0, $posn);
                $obj->value = \substr($config, $posn + 1);

            } else {
                $obj->label = $config;
            }
            $config = $obj;
        } elseif (isset($config->value) && \is_array($config->value)) {
            // plain string labels are converted to objects with a label property
            foreach ($config->value as &$value) {
                if (is_string($value)) {
                    // Convert to a useful class
                    $obj = new \stdClass();
                    $obj->label = $value;
                    $value = $obj;
                }
            }
        }
    }

    /**
     * Map the config file's "memberOf" to "groups".
     *
     * @param string $property Name of the property.
     * @return string
     */
    protected function configurePropertyMap($property)
    {
        if ($property === 'memberOf') {
            $property = 'groups';
        }
        return $property;
    }

    /**
     * Ensure groups is an array of valid names.
     *
     * @param string $property Name of the property.
     * @param type $value Current value of the property.
     * @return bool True when the property is valid.
     */
    protected function configureValidate($property, &$value)
    {
        if ($property === 'groups') {
            if (!is_array($value)) {
                $value = [$value];
            }
            foreach ($value as $key => &$item) {
                $item = trim($item);
                if (!preg_match('/^[a-z0-9\-_]+$/i', $item)) {
                    unset($value[$key]);
                }
            }
            $value = array_values(array_unique($value));
        }
        return true;
    }

    /**
     * Delete this element from the named group.
     *
     * @param string $groupName Name of the group to be added.
     * @return $this
     */
    public function deleteGroup(string $groupName)
    {
        if (($key = array_search($groupName, $this->groups)) !== false) {
            unset($this->groups[$key]);
            $this->groups = array_values($this->groups);
        }
        return $this;
    }

    /**
     * Enable/disable the option.
     *
     * @param bool $enabled
     * @return $this
     */
    public function enable(bool $enabled)
    {
        $this->enabled = $enabled;
        return $this;
    }

    /**
     * Get the autofocus state of this element.
     *
     * @return bool
     */
    public function getAutofocus() : bool
    {
        return $this->autofocus;
    }

    /**
     * Get the option's enabled state.
     *
     * @return type
     */
    public function getEnabled() : bool
    {
        return $this->enabled;
    }

    /**
     * Get the list of groups this element is a member of.
     *
     * @return string[]
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * Get the user-visible text for this option.
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Get a list of sub-options (or null).
     *
     * @return ?array
     */
    public function getList()
    {
        if (!is_array($this->value)) {
            return null;
        }
        return $this->value;
    }

    /**
     * Get the name of this option on the form.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get the option's selected state.
     *
     * @return type
     */
    public function getSelected()
    {
        return $this->selected;
    }

    /**
     * Get the sidecar data
     *
     * @return scalar|array|object
     */
    public function getSidecar()
    {
        return $this->sidecar;
    }

    /**
     * Get the option's value.
     *
     * @return int|string
     */
    public function getValue()
    {
        if (is_array($this->value)) {
            return null;
        }
        return $this->value;
    }

    /**
     * Set the groups this element is a member of.
     *
     * @param string|string[] $groups The group or groups.
     * @return $this
     */
    public function groups($groups)
    {
        $this->configureValidate('groups', $groups);
        $this->groups = $groups;
        return $this;
    }

    /**
     * Determine if this option is empty (used by JsonEncoder).
     *
     * @return bool
     */
    public function isEmpty() : bool
    {
        if ($this->enabled === false) {
            return false;
        }
        if (!empty($this->groups)) {
            return false;
        }
        if ($this->label !== null && $this->label !== '') {
            return false;
        }
        if ($this->name !== null && $this->name !== '') {
            return false;
        }
        if ($this->sidecar !== null) {
            return false;
        }
        if ($this->value !== null) {
            return false;
        }
        return true;
    }

    /**
     * Determine if this option has a nested option list.
     *
     * @return bool
     */
    public function isNested() : bool
    {
        return is_array($this->value);
    }

    /**
     * Attempt to simplify the JSON encoded option as a string.
     *
     * @return string|$this
     */
    public function jsonCollapse()
    {
        if (!NextForm::$jsonCompact) {
            return $this;
        }
        if ($this->enabled === false) {
            return $this;
        }
        if ($this->name !== null && $this->name !== '') {
            return $this;
        }
        if ($this->sidecar !== null) {
            return $this;
        }
        if (!empty($this->groups)) {
            return $this;
        }
        if ($this->label === $this->value) {
            $result = $this->label;
        } else {
            if (\is_bool($this->value)) {
                $strVal = $this->value ? 'true' : 'false';
            } else {
                $strVal = $this->value;
            }
            $result = $this->label . NextForm::GROUP_DELIM . $strVal;
        }
        return $result;
    }

    /**
     * Set the user-visible text for this option.
     *
     * @param string $label The new display text.
     * @return $this
     */
    public function label(string $label)
    {
        $this->label = $label;
        if ($this->value === null) {
            $this->value = $label;
        }
        return $this;
    }

    /**
     * Set the name of this option on the form.
     *
     * @param string $name The new form name.
     * @return $this
     */
    public function name(string $name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Set the selected state for this option.
     *
     * @param bool $selected
     * @return $this
     */
    public function selected(bool $selected)
    {
        $this->selected = $selected;
        return $this;
    }

    /**
     * Set the sidecar data.
     *
     * @param scalar|object|array $data
     * @return $this
     */
    public function sidecar($data)
    {
        $this->sidecar = $data;
        return $this;
    }

    /**
     * Set the option value.
     *
     * @param scalar $value
     * @return $this
     */
    public function value($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * Get a copy of the option with the label translated.
     *
     * @param Translator|null $translator
     * @return Option
     */
    public function translate(?Translator $translator = null) : Option
    {
        $translated = clone $this;
        if ($translator !== null) {
            $translated->label($translator->get($this->label));
        }
        return $translated;
    }

}
