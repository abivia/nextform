<?php

declare(strict_types=1);

namespace Abivia\NextForm\Data;

use Abivia\Configurable\Configurable;
use Abivia\NextForm\Helpers\CompactParameters;
use Abivia\NextForm\NextForm;
use Abivia\NextForm\Traits\JsonEncoderTrait;

/**
 * Describes the storage characteristics of an object.
 */
class Store implements \JsonSerializable
{
    use Configurable;
    use JsonEncoderTrait;

    /**
     * Rules for the JsonEncoder
     * @var array
     */
    protected static $jsonEncodeMethod = [
        'type' => ['drop:null'],
        'size' => ['drop:null'],
    ];

    /**
     * A list of recognized storage types.
     * @var string[]
     */
    protected static $knownTypes = [
        'blob', 'date', 'datetime', 'decimal', 'float', 'int', 'string', 'text',
        'time',
    ];

    /**
     * The size in the data store. Can be non-numeric.
     * @var string
     */
    protected $size;

    /**
     * The storage type.
     * @var string
     */
    protected $type;

    /**
     * Create a new Store object, with optional properties.
     *
     * @param string|null $type The storage type
     * @param string|null $size The storage size specification
     *
     * @return Store
     */
    public static function build(?string $type = null, ?string $size = null
    ) : Store {
        $store = new Store();
        if ($type !== null) {
            $store->type($type);
        }
        $store->size($size);

        return $store;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureInitialize(&$config, ...$context)
    {
        // Convert a simple string to a class with rules
        if (\is_string($config)) {
            $config = CompactParameters::decompose($config, self::$knownTypes);
        }
        return true;
    }

    /**
     * Check that the supplied type is valid.
     *
     * @param string $property The property to be validated.
     * @param mixed $value Current value for the property.
     * @return bool
     */
    protected function configureValidate($property, &$value)
    {
        $result = true;
        if ($property === 'type') {
            if (!($result = \in_array($value, self::$knownTypes))) {
                $this->configureLogError(
                    'Invalid value "'. $value . '" for property "' . $property . '".'
                );
                $result = false;
            }
        }
        return $result;
    }

    /**
     * Get the current storage size.
     * @return string
     */
    public function getSize()
    {
        return (string) $this->size;
    }

    /**
     * Get the current data type
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Determine if this object contributes nothing to a JSON encoding.
     * @return bool
     */
    public function isEmpty() : bool
    {
        if ($this->size !== null) {
            return false;
        }
        if ($this->type !== null && $this->type !== '') {
            return false;
        }
        return true;
    }

    /**
     * Compact the properties into a segmented string.
     *
     * @return string
     */
    public function jsonCollapse()
    {
        if (!NextForm::$jsonCompact) {
            return $this;
        }
        $result = [$this->type];
        if ($this->size !== null) {
            $result[] = 'size:' . $this->size;
        }

        return implode('|', $result);
    }

    /**
     * Set the storage size.
     *
     * @param string|null $size The desired storage size.
     * @return $this
     */
    public function size(?string $size)
    {
        $this->size = $size;
        return $this;
    }

    /**
     * Set the data type.
     *
     * @param string $type The desired data type.
     * @return $this
     * @throws RuntimeException If the type is not valid.
     */
    public function type(string $type)
    {
        if (!$this->configureValidate('type', $type)) {
            throw new DefinitionException($type . ' is not a valid value for type.');
        }
        $this->type = $type;
        return $this;
    }

}
