<?php

declare(strict_types=1);

namespace Abivia\NextForm\Data;

use Abivia\Configurable\Configurable;
use Abivia\NextForm\NextForm;
use Abivia\NextForm\Traits\JsonEncoderTrait;
use Illuminate\Support\Str;

/**
 * Describes acceptable values for a property
 */
class Validation implements \JsonSerializable
{
    use Configurable;
    use JsonEncoderTrait;

    /**
     * A list of file patterns for an input element of type file.
     * @var string[]
     */
    protected $accept = [];

    /**
     * Set when validation should be performed via a server request.
     * @var bool
     */
    protected $async = false;

    /**
     * Sets the source for image or video in an input element of type file.
     * @var string
     */
    protected $capture;

    /**
     * Test to determine if a property does not need to be JSON encoded.
     *
     * @var array
     */
    protected static $emptyState = [
        'accept' => 'empty',
        'async' => 'false',
        'capture' => 'false',
        'maxLength' => 'nonzero',
        'maxValue' => 'nonnull',
        'minLength' => 'nonzero',
        'minValue' => 'nonnull',
        'multiple' => 'false',
        'pattern' => 'empty',
        'required' => 'false',
        'rules' => 'empty',
        'step' => 'nonzero',
        'translatePattern' => 'false',
    ];

    /**
     * Rules for the JsonEncoder
     * @var array
     */
    protected static $jsonEncodeMethod = [
        'accept' => ['method:jsonRuleMap', 'drop:empty', 'drop:null'],
        'async' => ['method:jsonRuleMap', 'drop:false'],
        'capture' => ['method:jsonRuleMap', 'drop:null', 'drop:blank'],
        'maxLength' => ['method:jsonRuleMap', 'drop:null'],
        'maxValue' => ['method:jsonRuleMap', 'drop:null'],
        'minLength' => ['method:jsonRuleMap', 'drop:null'],
        'minValue' => ['method:jsonRuleMap', 'drop:null'],
        'multiple' => ['drop:false'],
        'pattern' => ['method:jsonRuleMap', 'drop:null', 'drop:blank'],
        'required' => ['method:jsonRuleMap', 'drop:false', 'drop:null'],
        'rules' => ['drop:null', 'drop:blank', 'order:5'],
        'step' => ['method:jsonRuleMap', 'drop:null','drop:zero'],
        'translatePattern' => ['method:jsonRuleMap', 'drop:false'],
    ];

    /**
     * The maximum acceptable input length.
     * @var int
     */
    protected $maxLength;

    /**
     * The maximum value for numeric/date input types.
     * @var float|string
     */
    protected $maxValue;

    /**
     * The minimum acceptable input length.
     * @var int
     */
    protected $minLength;

    /**
     * The minimum value for numeric/date input types.
     * @var float|string
     */
    protected $minValue;

    /**
     * Set when a check box list or select can have multiple values.
     * @var bool
     */
    protected $multiple = false;

    /**
     * Regular expression with PHP start-end delimiters (but still Javascript compatible)
     * for validating inputs.
     * @var string
     */
    protected $pattern = '';

    /**
     * Set when a value must be supplied.
     * @var bool
     */
    protected $required = false;

    /**
     * The field type for rules based validation.
     *
     * @var string
     */
    protected $ruleBaseType;

    /**
     * The parsed representation of the rules string. Each element is an array
     * of [rulename, args].
     *
     * @var array
     */
    protected $ruleList = [];

    /**
     * A record of property values as set by a rules string. Used to prevent
     * redundant properties when writing JSON.
     * @var array
     */
    protected $ruleMap = [];

    /**
     * A Laravel-like list of validation rules of the form
     * name1[:args]|name2[:args]...
     *
     * @var string
     */
    protected $rules = '';

    /**
     * The size of an increment for numeric/date input types.
     * @var float
     */
    protected $step;

    /**
     * Set when the validation regex should be translated before use.
     * @var bool
     */
    protected $translatePattern = false;

    /**
     * Properties that must have values of a given type.
     *
     * @var array
     */
    protected static $validateAs = [
        'bool' => [
            'async' => true, 'multiple' => true,
            'required' => true, 'translatePattern' => true
        ],
        'numdate' => [
            'maxValue' => true, 'minValue' => true, 'step' => true
        ],
        'numeric' => [
            'maxLength' => true, 'minLength' => true
        ],
        'regex' => [
            'pattern' => true
        ],
    ];

    /**
     * Process anything in the validation rules to set validation properties.
     * Rules we don't have a definition for have no effect.
     */
    protected function applyRules()
    {
        $this->ruleMap = [];
        $this->ruleBaseType = 'string';
        foreach ($this->ruleList as $pair) {
            $method = 'applyRules' . Str::studly($pair[0]);
            if (method_exists($this, $method)) {
                $this->$method($pair[1]);
            }

            // Most of the settings processing is determining the underlying
            // type so we can apply min and max properly.
            $setting = $pair[0];
            switch ($setting) {
                case 'accept':
                case 'async':
                case 'capture':
                case 'multiple':
                    $this->set($setting, $pair[1]);
                    $this->ruleMap[$setting] = $this->$setting;
                    break;

                case 'active_url':
                case 'alpha':
                case 'alpha_dash':
                case 'alpha_num':
                    $this->ruleBaseType = 'string';
                    break;

                case 'array':
                    $this->ruleBaseType = 'array';
                    break;

                case 'boolean':
                    $this->ruleBaseType = 'string';
                    break;

                case 'date':
                case 'date_format':
                    $this->ruleBaseType = 'date';
                    break;

                case 'email':
                    $this->ruleBaseType = 'string';
                    break;

                case 'filled':
                case 'required':
                    $this->required = true;
                    $this->ruleMap['required'] = true;
                    break;

                case 'integer':
                case 'numeric':
                    $this->ruleBaseType = 'number';
                    break;

                case 'ip':
                case 'ipv4':
                case 'ipv6':
                    $this->ruleBaseType = 'string';
                    break;

                case 'nullable':
                    $this->required = false;
                    $this->ruleMap['required'] = false;
                    break;

                case 'password':
                    $this->ruleBaseType = 'string';
                    break;

                case 'regex':
                    $this->set('pattern', $pair[1]);
                    $this->ruleMap['pattern'] = $this->pattern;
                    break;

                case 'regex_translate':
                    $this->set('translatePattern', $pair[1]);
                    $this->ruleMap['translatePattern'] = $this->translatePattern;
                    break;

                case 'string':
                case 'timezone':
                case 'url':
                case 'uuid':
                    $this->ruleBaseType = 'string';
                    break;

            }
        }

    }

    protected function applyRulesBetween($value) {
        $parts = explode(',', $value);
        if (count($parts) < 2) {
            $parts[] = $value;
        }
        $this->applyRulesMax($parts[1]);
        $this->applyRulesMin($parts[0]);
    }

    protected function applyRulesDigits($value) {
        $this->configureErrors = [];

        // We don't use set() here because we want to test both criteria first
        // to make the change atomic.
        if (!$this->configureValidate('maxLength', $value)) {
            throw new DefinitionException(implode("\n", $this->configureErrors));
        }
        if (!$this->configureValidate('minLength', $value)) {
            throw new DefinitionException(implode("\n", $this->configureErrors));
        }
        $this->maxLength = (int) $value;
        $this->ruleMap['maxLength'] = $this->maxLength;
        $this->minLength = (int) $value;
        $this->ruleMap['minLength'] = $this->minLength;
    }

    protected function applyRulesDigitsBetween($value) {
        $parts = explode(',', $value);
        if (count($parts) < 2) {
            $parts[] = $value;
        }
        $this->applyRulesMax($parts[1]);
        $this->applyRulesMin($parts[0]);
    }

    protected function applyRulesMax($value) {
        if ($this->ruleBaseType === 'string') {
            $this->set('maxLength', $value);
            $this->ruleMap['maxLength'] = $this->maxLength;
        } elseif (
            $this->ruleBaseType === 'date' || $this->ruleBaseType === 'number'
        ) {
            $this->set('maxValue', $value);
            $this->ruleMap['maxValue'] = $this->maxValue;
        }
    }

    protected function applyRulesMimes($value) {
        $this->accept = array_map(
            function ($ext) {
                return '*.' . $ext;
            },
            explode(',', $value)
        );
        $this->ruleMap['accept'] = $this->accept;
    }

    protected function applyRulesMin($value) {
        if ($this->ruleBaseType === 'string') {
            $this->set('minLength',$value);
            $this->ruleMap['minLength'] = $this->minLength;
        } elseif (
            $this->ruleBaseType === 'date' || $this->ruleBaseType === 'number'
        ) {
            $this->set('minValue', $value);
            $this->ruleMap['minValue'] = $this->minValue;
        }
    }

    protected function applyRulesSize($value) {
        $this->applyRulesMax($value);
        $this->applyRulesMin($value);
    }

    protected function applyRulesStep($value) {
        $this->configureErrors = [];
        $this->set('step', $value);
        $this->ruleMap['step'] = $this->step;
    }

    /**
     * Validation factory.
     *
     * @param bool|string|null $init Flag: sets the required state; string:
     * is a rules set.
     *
     * @return Validation
     */
    public static function build($init = null) : Validation
    {
        $val = new Validation();
        if (is_bool($init)) {
            $val->set('required', $init);
        } elseif (is_string($init)) {
            $val->set('rules', $init);
        }

        return $val;
    }

    protected function configureComplete()
    {
        if ($this->maxValue < $this->minValue) {
            $this->configureLogError(
                'Minimum value must be less than or equal to maximum value.'
            );
            return false;
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureInitialize(&$config, ...$context)
    {
        // Convert a simple string to a class with rules
        if (\is_string($config)) {
            $obj = new \stdClass();
            $obj->rules = $config;
            $config = $obj;
        }
        return true;
    }

    /**
     * Validate the value for a property.
     *
     * @param string $property
     * @param mixed $value
     *
     * @return boolean
     */
    protected function configureValidate($property, &$value)
    {
        $result = true;
        if (isset(self::$validateAs['bool'][$property])) {
            if (!is_bool($value)) {
                $this->configureLogError("{$property} must be a boolean.");
                $result = false;
            }
        } elseif (isset(self::$validateAs['regex'][$property])) {
            $result = $this->configureValidateRegex($property, $value);

        } elseif ($value === null) {
            $result = true;

        } elseif (isset(self::$validateAs['numeric'][$property])) {
            $result = $this->configureValidateNumeric($property, $value);

        } elseif (isset(self::$validateAs['numdate'][$property])) {
            $result = $this->configureValidateNumDate($property, $value);

        } elseif ($property === 'accept') {
            if (is_string($value)) {
                $value = explode(',', $value);
            }
            if (!is_array($value)) {
                $this->configureLogError(
                    "{$property} must be an array or comma-delimited list."
                );
                $result = false;
            }
        } elseif ($property === 'capture') {
            if ($value !== null && !in_array($value, ['environment', 'user'])) {
                $this->configureLogError(
                    "{$property} must be null, \"user\", or \"environment\"."
                );
                $result = false;
            }
        } elseif ($property === 'rules') {
            $result = $this->configureValidateRules($property, $value);
        }
        return $result;
    }

    /**
     * Validate the numeric/date properties.
     *
     * @param string $property
     * @param mixed $value
     *
     * @return boolean
     */
    protected function configureValidateNumDate($property, $value)
    {
        $result = true;
        if (
            !is_numeric($value)
            && (!is_string($value) || strtotime($value) === false)
        ) {
            $this->configureLogError("{$property} must be numeric or date.");
            $result = false;
        } else {
            if (is_numeric($value)) {
                $value = (float) $value;
            }
            if ($property === 'step' && $value <= 0) {
                $this->configureLogError(
                    "{$property} must be a positive number."
                );
                $result = false;
            }
        }

        return $result;
    }

    /**
     * Validate the numeric properties.
     *
     * @param string $property
     * @param mixed $value
     *
     * @return boolean
     */
    protected function configureValidateNumeric($property, $value)
    {
        if (!is_numeric($value)) {
            $this->configureLogError($property . ' must be numeric.');
            return false;
        }
        $value = (int) $value;
        if ($property === 'maxLength' && $value <= 0) {
            $this->configureLogError("{$property} must be a positive integer.");
            return false;
        }
        if ($property === 'minLength' && $value < 0) {
            $this->configureLogError(
                "{$property} must be a non-negative integer."
            );
            return false;
        }
        return true;
    }

    /**
     * Validate the regular expression properties.
     *
     * @param string $property
     * @param mixed $value
     *
     * @return boolean
     */
    protected function configureValidateRegex($property, $value)
    {
        $result = true;
        if (!is_string($value)) {
            $this->configureLogError("{$property} must be a string.");
            $result = false;
        } else {
            @preg_match($value, '');
            if (preg_last_error() !== 0) {
                $this->configureLogError(
                    "{$property} must be a valid regular expression."
                );
                $result = false;
            }
        }

        return $result;
    }

    /**
     * Validate the rules property and save in rulesList
     *
     * @param string $property
     * @param mixed $value
     *
     * @return boolean
     */
    protected function configureValidateRules($property, $value)
    {
        $result = true;
        $this->ruleList = [];
        $pairs = explode('|', $value);
        foreach ($pairs as $pair) {
            $split = strpos($pair, ':');
            if ($split === 0) {
                $result = false;
                break;
            } elseif ($split === false) {
                $this->ruleList[] = [$pair, null];
            } else {
                $this->ruleList[] = [
                    substr($pair, 0, $split), substr($pair, $split + 1)
                ];
            }
        }
        if ($result) {
            $this->applyRules();
        }

        return $result;
    }

    /**
     * Get the named property, with the ability to trim the pattern regex
     *  by using -pattern.
     * @param string $property The name of the property to get.
     * @return mixed
     * @throws \RuntimeException
     */
    public function get(string $property)
    {
        if ($property === '-pattern') {
            return $this->pattern !== '' ? substr($this->pattern, 1, -1) : '';
        }
        if (!isset(self::$jsonEncodeMethod[$property])) {
            throw new DefinitionException(
                $property . ' is not a recognized property.'
            );
        }
        return $this->$property;
    }

    /**
     * Determine if this object contributes nothing to a JSON encoding.
     *
     * @return bool
     */
    public function isEmpty() : bool
    {
        foreach (array_keys(self::$jsonEncodeMethod) as $prop) {
            if (!$this->isEmptyProperty($prop, $this->$prop)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Determine if this property contributes nothing to a JSON encoding.
     *
     * @return bool
     */
    protected function isEmptyProperty($prop, $value) : bool
    {
        switch(self::$emptyState[$prop]) {
            case 'empty':
                if (!empty($value)) {
                    return false;
                }
                break;

            case 'false':
                if ($value) {
                    return false;
                }
                break;

            case 'nonnull':
                if ($value !== null) {
                    return false;
                }
                break;

            case 'nonzero':
                if ($value !== null && $value !== 0) {
                    return false;
                }
                break;

        }

        return true;
    }

    /**
     * If we can represent this field in JSON as a string, return a string
     * otherwise $this.
     */
    public function jsonCollapse()
    {
        if (!NextForm::$jsonCompact) {
            return $this;
        }
        foreach (array_keys(self::$jsonEncodeMethod) as $prop) {
            if ($prop === 'rules') {
                continue;
            }
            $test = $this->$prop;
            if ($this->isEmptyProperty($prop, $test)) {
                continue;
            }
            $this->jsonRuleMap($prop, $test);
            if ($test !== null) {
                return $this;
            }
        }

        return $this->rules;
    }

    /**
     * Suppress a JSON property if the value matches one set by a rules
     * property.
     *
     * @param string $prop
     * @param mixed $value
     */
    protected function jsonRuleMap($prop, &$value) {
        if (isset($this->ruleMap[$prop]) && $this->ruleMap[$prop] === $value) {
            $value = null;
        }
    }

    /**
     * Set the rules string.
     *
     * @param string $rules
     */
    public function rules($rules) {
        $this->set('rules', $rules);
    }

    /**
     * Set a property, validating while doing so.
     *
     * @param string $property The property name.
     * @param int|string $value The property value.
     * @return $this
     * @throws \RuntimeException If the property has an invalid value.
     */
    public function set(string $property, $value)
    {
        if (!isset(self::$jsonEncodeMethod[$property])) {
            throw new DefinitionException(
                $property . ' is not a recognized property.'
            );
        }
        $this->configureErrors = [];
        if (!$this->configureValidate($property, $value)) {
            throw new DefinitionException(implode("\n", $this->configureErrors));
        }
        $this->$property = $value;
        return $this;
    }

}
