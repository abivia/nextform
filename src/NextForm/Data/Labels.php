<?php

declare(strict_types=1);

namespace Abivia\NextForm\Data;

use Abivia\Configurable\Configurable;
use Abivia\NextForm\NextForm;
use Abivia\NextForm\Traits\JsonEncoderTrait;
use Illuminate\Contracts\Translation\Translator;

use function DeepCopy\deep_copy;

/**
 * Text labels associated with a data object.
 *
 * Values default to null to allow inheritance form another level; an explicit blank
 * overwrites inheritance.
 */
class Labels implements \Iterator, \JsonSerializable
{
    use Configurable;
    use JsonEncoderTrait;

    /**
     * Text to display when validation passes.
     * @var string
     */
    protected $accept = null;

    /**
     * Text to display after the body of an item.
     * @var string
     */
    protected $after = null;

    /**
     * Cache of automatically computed labels.
     *
     * @var array
     */
    protected $autoSets = [];

    /**
     * Text to display before the body of an item.
     * @var string
     */
    protected $before = null;

    /**
     * Labels to use when asking for a confirmation.
     * @var Labels
     */
    protected $confirm = null;

    /**
     * Text to display when there is an error.
     * @var string
     */
    protected $error = null;

    /**
     * Text to display before as the item header.
     * @var string
     */
    protected $heading = null;

    /**
     * Text to display for item help.
     * @var string
     */
    protected $help = null;

    /**
     * Text to display "inside" an item: placeholder or label on a check/radio.
     * @var string
     */
    protected $inner = null;

    /**
     * Flags indicating if label elements are escaped HTML or not.
     * @var array
     */
    protected $isHtml = [];

    /**
     * The current Iterator position.
     *
     * @var int
     */
    private $iterPosn = 0;

    /**
     * Rules for the JsonEncoder
     * @var array
     */
    protected static $jsonEncodeMethod = [
        'translate' => ['drop:true'],
        'accept' => ['method:jsonAutoFilter', 'drop:null'],
        'accept.html' => ['method:jsonEscape', 'drop:false'],
        'after' => ['method:jsonAutoFilter', 'drop:null'],
        'after.html' => ['method:jsonEscape', 'drop:false'],
        'before' => ['method:jsonAutoFilter', 'drop:null'],
        'before.html' => ['method:jsonEscape', 'drop:false'],
        'confirm' => ['method:jsonAutoFilter', 'drop:null'],
        'error' => ['method:jsonAutoFilter', 'drop:null'],
        'error.html' => ['method:jsonEscape', 'drop:false'],
        'heading' => ['method:jsonAutoFilter', 'drop:null'],
        'heading.html' => ['method:jsonEscape', 'drop:false'],
        'help' => ['method:jsonAutoFilter', 'drop:null'],
        'help.html' => ['method:jsonEscape', 'drop:false'],
        'inner' => ['method:jsonAutoFilter', 'drop:null'],
        'inner.html' => ['method:jsonEscape', 'drop:false'],
        'mask' => ['method:jsonAutoFilter', 'drop:null'],
        'mask.html' => ['method:jsonEscape', 'drop:false'],
    ];

    /**
     * Text to display when the access level is "masked".
     *
     * @var string
     */
    protected $mask = null;

    /**
     * Substitution parameters, indexed by match string.
     * @var array
     */
    protected $replacements = [];

    /**
     * Reference to the schema this property is part of.
     * @var Schema
     */
    protected $schema;

    /**
     * A list of the properties that contain text.
     * @var string[]
     */
    private static $textProperties = [
        'accept', 'after', 'before', 'error', 'heading', 'help', 'inner',
        'mask',
    ];

    /**
     * Flag indicating if these labels should be translated or not.
     * @var bool
     */
    protected $translate = true;

    public function __construct()
    {
        $this->init();
    }


    /**
     * Apply auto-labels to any missing labels.
     *
     * @param Labels $pattern The auto label definitions.
     * @param string $name The replacement string.
     *
     * @return $this
     */
    public function autoLabel(Labels $pattern, string $name) : Labels
    {
        foreach ($pattern as $labelName => $text) {
            if ($text === null || $this->has($labelName)) {
                continue;
            }
            $this->$labelName = str_replace('*', $name, $text);
            $this->autoSets[$labelName] = $this->$labelName;
        }
        if ($this->confirm !== null && $pattern->getConfirm() !== null) {
            $this->confirm->autoLabel($pattern->getConfirm(), $name);
        }

        return $this;
    }

    /**
     * Label factory.
     *
     * @param string|array|null $data Heading (string) or associative array
     * @return Labels
     */
    public static function build($data = null) : Labels
    {
        $labels = new Labels();
        if (is_string($data)) {
            $labels->set('heading', $data);
        } elseif (is_array($data)) {
            foreach ($data as $labelName => $text) {
                $labels->set($labelName, $text);
            }
        }
        return $labels;
    }

    /**
     * Ensure that a text property is valid.
     *
     * @param string $labelName
     * @throws \RuntimeException
     */
    protected function checkTextProperty(string $labelName)
    {
        if (!in_array($labelName, self::$textProperties)) {
            throw new DefinitionException(
                "${labelName} isn't a valid label property."
            );
        }
    }

    /**
     * Map a property to a class.
     *
     * @param string $property The current class property name.
     * @param scalar $value The value to be stored in the property, made available for inspection.
     * @return object|false An object containing a class name and key, or false
     * @codeCoverageIgnore
     */
    protected function configureClassMap(string $property, $value)
    {
        static $classMap = [
            'confirm' => ['className' => self::class],
        ];
        if (isset($classMap[$property])) {
            return (object) $classMap[$property];
        }
        return false;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureInitialize(&$config, ...$context)
    {
        $this->init();
        if (isset($this->configureOptions['_schema'])) {
            $this->schema = $this->configureOptions['_schema'];
        }

        // Convert a simple string to a class with heading
        if (\is_string($config)) {
            $obj = new \stdClass();
            $obj->heading = $config;
            $config = $obj;
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureComplete()
    {
        if ($this->confirm) {
            if ($this->confirm->confirm !== null) {
                $this->configureLogError(
                    "Label confirm strings can't be nested."
                );
                return false;
            }
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    protected function configurePropertyMap($property)
    {
        // Convert label.isHtml to isHtml[label]
        $parts = explode('.', $property);
        if (count($parts) === 1) {
            return $property;
        }

        return ['isHtml', $parts[0]];
    }

    /**
     * Create a deep clone of the labels, copying any connected objects.
     *
     * @return Labels
     */
    public function copy() : Labels
    {
        // deep copy is TOO deep... need to not copy schema!!
        //return deep_copy($this);
        return clone $this;
    }

    /**
     * Get the value at the current Iterator position.
     *
     * @return string
     */
    public function current()
    {
        $iterProp = self::$textProperties[$this->iterPosn];

        return $this->$iterProp;
    }

    /**
     * Get the labels merged for use in a confirm context.
     *
     * @return Labels
     */
    public function forConfirm() : Labels
    {
        $newLabels = $this->copy();
        if ($newLabels->confirm !== null) {
            foreach (self::$textProperties as $prop) {
                if ($newLabels->confirm->has($prop)) {
                    $newLabels->$prop = $newLabels->confirm->get($prop);
                }
            }
            $newLabels->confirm = null;
        }
        return $newLabels;
    }

    /**
     * Get a label by name.
     *
     * @param string $labelName
     * @param ?bool $asConfirm When set, get the "confirm" version (if any).
     * @return ?string
     */
    public function get(
        string $labelName,
        ?bool $asConfirm = false,
        ?string $default = null
    ) {
        $this->checkTextProperty($labelName);
        if ($asConfirm && $this->confirm && $this->confirm->$labelName !== null) {
            return $this->confirm->get($labelName, false, $default);
        }
        if (
            $labelName === 'mask'
            && $this->$labelName === null
            && $default === null
        ) {
            return NextForm::MASK_TEXT_DEFAULT;
        }
        return $this->$labelName ?? $default;
    }

    /**
     * Get the confirm labels object, if any
     *
     * @return ?Labels
     */
    public function getConfirm() : ?Labels
    {
        return $this->confirm;
    }

    /**
     * Get a HTML escaped label by name.
     *
     * @param string $labelName
     * @param ?bool $asConfirm When set, get the "confirm" version (if any).
     * @return ?string
     * @throws \RuntimeException
     */
    public function getEscaped(string $labelName, ?bool $asConfirm = false)
    {
        $this->checkTextProperty($labelName);
        if ($asConfirm) {
            return $this->confirm->getEscaped($labelName);
        }
        $label = $this->$labelName;
        if (!$this->isHtml[$labelName]) {
            if (is_array($label)) {
                foreach ($label as &$item) {
                    $item = \htmlspecialchars($item);
                }
            } else {
                $label = \htmlspecialchars($label);
            }
        }
        return $label;
    }

    /**
     * Get the currently defined translation replacements.
     *
     * @return array
     */
    public function getReplacements()
    {
        return $this->replacements;
    }

    /**
     * Get the label's translate requirement.
     *
     * @return bool
     */
    public function getTranslate() : bool
    {
        return $this->translate;
    }

    /**
     * Determine if a label type has been set or not.
     *
     * @param string $labelName
     * @param ?bool $asConfirm When set, check the "confirm" version.
     * @return bool
     */
    public function has(string $labelName, ?bool $asConfirm = false) : bool
    {
        $this->checkTextProperty($labelName);
        if ($asConfirm) {
            if (!$this->confirm) {
                return false;
            }
            return $this->confirm->has($labelName);
        }
        return $this->$labelName !== null;
    }

    /**
     * Initialize the object's properties.
     *
     * @return $this
     */
    protected function init()
    {
        $this->confirm = null;
        $this->isHtml = array_fill_keys(self::$textProperties, false);
        $this->replacements = [];
        $this->schema = null;
        foreach (self::$textProperties as $prop) {
            $this->$prop = null;
        }
        $this->translate = true;
        $this->iterPosn = 0;

        return $this;
    }

    /**
     * Check to see if there are any non-null elements in the label.
     *
     * @return bool
     */
    public function isEmpty() : bool
    {
        foreach (self::$textProperties as $prop) {
            if (
                $this->$prop !== null
                && ($this->autoSets[$prop] ?? null) !== $this->$prop
            ) {
                return false;
            }
        }
        if ($this->confirm !== null) {
            return $this->confirm->isEmpty();
        }
        return true;
    }

    /**
     * Determine if a label type is escaped HTML.
     *
     * @param string $labelName
     * @param ?bool $asConfirm When set, check the "confirm" version.
     * @return bool
     * @throws \RuntimeException
     */
    public function isEscaped(string $labelName, ?bool $asConfirm = false) : bool
    {
        if (!$this->has($labelName, $asConfirm)) {
            return false;
        }
        if ($asConfirm) {
            if (!$this->confirm) {
                return false;
            }
            return $this->confirm->isEscaped($labelName);
        }
        return $this->isHtml[$labelName];
    }

    /**
     * Remove automatically calculated labels from a JSON output.
     *
     * @param string $property
     * @param int|string $value
     */
    protected function jsonAutoFilter(string $property, &$value)
    {
        if (
            $this->autoSets[$property] ?? false === $value
        ) {
            $value = null;
        }
    }

    /**
     * If we can represent this field in JSON as a string, return a string
     * otherwise $this.
     */
    public function jsonCollapse()
    {
        if ($this->confirm !== null) {
            return $this;
        }
        $collapsable = NextForm::$jsonCompact;
        foreach (self::$textProperties as $prop) {
            if (!$collapsable) {
                break;
            }
            if ($prop === 'heading') {
                continue;
            }
            if ($this->$prop !== null && $this->$prop !== '') {
                $collapsable = false;
            }
        }
        if (!$collapsable) {
            return $this;
        }

        $heading = $this->heading;
        $this->jsonAutoFilter('heading', $heading);

        return $heading;
    }

    /**
     * Get a component's escaped status for encoding in JSON.
     *
     * @param string $property
     * @param int|string $value
     */
    protected function jsonEscape(string &$property, &$value)
    {
        $parts = explode('.', $value);
        $value = $this->isHtml[$parts[0]];
    }

    /**
     * Get the Iterator key.
     *
     * @return string
     */
    public function key()
    {
        return self::$textProperties[$this->iterPosn];
    }

    /**
     * Merge another label set into this one and return a new merged object.
     *
     * @param ?Labels $merge
     * @return Labels
     */
    public function merge(?Labels $merge = null)
    {
        $newLabels = $this->copy();
        if ($merge !== null) {
            foreach (self::$textProperties as $prop) {
                if ($merge->has($prop)) {
                    $newLabels->set(
                        $prop,
                        $merge->get($prop),
                        [
                            'replacements' => $merge->getReplacements(),
                            'escaped' => $merge->isEscaped($prop)
                        ]
                    );
                }
            }
            $confirmLabels = $merge->getConfirm();
            if (($newConfirm = $newLabels->getConfirm()) !== null) {
                $confirmLabels = $newConfirm->merge($confirmLabels);
            }
            $newLabels->confirm($confirmLabels);
            $newLabels->mustTranslate(
                $newLabels->getTranslate() || $merge->getTranslate()
            );
        }
        return $newLabels;
    }

    /**
     * Advance the Iterator
     */
    public function next()
    {
        ++$this->iterPosn;
    }

    /**
     * Reset the Iterator
     */
    public function rewind()
    {
        $this->iterPosn = 0;
    }

    /**
     * Set a label by name.
     *
     * @param string $labelName
     * @param string|array $text The text value or an array of strings.
     * @param array $options Options are asConfirm:bool, replacements:[],
     *          escaped:bool
     * @return $this
     */
    public function set(
        string $labelName,
        $text,
        $options = []
    ) : Labels {
        $this->checkTextProperty($labelName);
        $asConfirm = $options['asConfirm'] ?? false;
        if ($asConfirm) {
            if ($this->confirm === null) {
                $this->confirm = new Labels();
            }
            $subOptions = $options;
            $subOptions['asConfirm'] = false;
            $this->confirm->set($labelName, $text, $subOptions);
        } else {
            $this->$labelName = $text;
            $this->isHtml[$labelName] = $options['escaped'] ?? false;
        }
        if (isset($options['replacements'])) {
            $this->replacements = array_merge(
                $this->replacements,
                $options['replacements']
            );
        }

        return $this;
    }

    /**
     * Set the confirmation labels.
     *
     * @param Labels|null $labels
     * @return Labels
     */
    public function confirm(?Labels $labels = null) : Labels
    {
        $this->confirm = $labels;
        return $this;
    }

    /**
     * Set the label translation requirement.
     *
     * @param bool $translate
     * @return $this
     */
    public function mustTranslate(bool $translate) : Labels
    {
        $this->translate = $translate;
        return $this;
    }

    /**
     * Create a translated version of the labels.
     *
     * @param ?Translator $translator The translation facility.
     * @param array $replacements Substitutions for translation arguments.
     * @return Labels
     */
    public function translate(
        ?Translator $translator = null,
        $replacements = []
    ) : Labels {
        // Create a copy for the translated strings
        $newLabels = $this->copy();

        // Merge in any schema defaults
        if ($this->schema) {
            $newLabels = $newLabels->merge($this->schema->getDefault('labels'));
        }

        // Perform the translation
        if ($newLabels->translate && $translator !== null) {
            $replacements = array_merge($this->replacements, $replacements);
            $newLabels->translateText($translator, $replacements);
            if ($this->confirm) {
                $newLabels->confirm = $this->confirm->translate(
                    $translator, $replacements
                );
            }
        }
        $newLabels->translate = false;
        return $newLabels;
    }

    /**
     * Translate text, with replacement strings.
     *
     * @param Translator $translator
     * @param array $replacements
     * @return $this
     */
    public function translateText(Translator $translator, $replacements)
    {
        foreach (self::$textProperties as $prop) {
            if (is_array($this->$prop)) {
                foreach($this->$prop as &$entry) {
                    $entry = $translator->get($entry, $replacements);
                }
            } elseif ($this->$prop !== null) {
                $this->$prop = $translator->get($this->$prop, $replacements);
            }
        }
        return $this;
    }

    /**
     * Determine if the current Iterator position exists.
     *
     * @return string
     */
    public function valid()
    {
        return isset(self::$textProperties[$this->iterPosn]);
    }

}
