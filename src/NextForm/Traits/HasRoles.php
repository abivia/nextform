<?php

declare(strict_types=1);

namespace Abivia\NextForm\Traits;

use Abivia\NextForm\Helpers\CompactParameters;
use Abivia\NextForm\NextForm;
use Abivia\NextForm\NextFormException;

/**
 * Trait for classes that offer compounded parameter strings.
 */
trait HasRoles
{
    /**
     * The valid access levels for role definitions. Order is important:
     * a higher index implies more access.
     *
     * @var array
     */
    public static $accessLevels = ['none', 'hide', 'mask', 'view', 'write'];

    /**
     * Access level by role. "*" is the default role.
     * @var array
     */
    protected $roles = [];

    /**
     * Cap an access level.
     *
     * @param string $level The level to be capped.
     * @param string $cap The maximum access allowed.
     *
     * @return string
     */
    public static function accessCap(string $level, string $cap)
    {
        return self::accessIndex($cap) < self::accessIndex($level)
            ? $cap : $level;
    }

    /**
     * Get the index for an access level.
     *
     * @param string $level The access level.
     *
     * @return int
     *
     * @throws NextFormException When the level is not valid.
     */
    public static function accessIndex(string $level) : int
    {
        $index = array_search($level, self::$accessLevels);
        if ($index === false) {
            throw new NextFormException("Invalid access level {$level}.");
        }

        return $index;
    }

    /**
     * Get the access level granted by one or more roles.
     *
     * @param string|array $role A role name or a list of role names.
     *
     * @return string
     */
    public function getAccess($role)
    {
        if (!is_array($role)) {
            $role = [$role];
        }

        // If no roles are defined * is write acess, otherwise * is none.
        $this->roles['*'] ??= count($this->roles) ? 'none' : 'write';

        // Find the maximum permission granted by the roles
        $maxIndex = -1;
        foreach ($role as $item) {
            if (!isset($this->roles[$item])) {
                continue;
            }
            $itemIndex = self::accessIndex($this->roles[$item]);
            $maxIndex = max($maxIndex, $itemIndex);
        }
        $access = $maxIndex === -1 ? $this->roles['*']
            : self::$accessLevels[$maxIndex];

        return $access;
    }

    /**
     * Remove any segment defaults from JSON output.
     *
     * @param type $prop The current property
     * @param array $value The property value
     */
    public function jsonCollapseRoles($prop, &$value)
    {
        if (
            count($value) === 1
            && isset($value['*']) && $value['*'] === 'write'
        ) {
            $value = [];
        }
        if (NextForm::$jsonCompact) {
            $value = CompactParameters::implode($value);
        }
    }

    /**
     * Validate a roles setting.
     *
     * @param array|string $roles
     *
     * @return array|boolean Roles array or false if setting is invalid.
     */
    protected function validateRoles($roles)
    {
        if (is_string($roles)) {
            $roles = CompactParameters::explode($roles, true);
        }
        if (is_object($roles)) {
            $roles = (array) $roles;
        }
        foreach ($roles as $access) {
            if (!in_array($access, self::$accessLevels)) {
                return false;
            }
        }

        return $roles;
    }

}