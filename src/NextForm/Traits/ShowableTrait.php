<?php

declare(strict_types=1);

namespace Abivia\NextForm\Traits;

use Abivia\NextForm\Helpers\CompactParameters;

/**
 * Trait for classes that support a show setting.
 */
trait ShowableTrait
{
    protected $show = '';

    protected static $defaultScope = 'undefined';

    public function addShow(string $show)
    {
        if ($this->show === '') {
            $this->show = trim($show);
        } else {
            $this->show .= '|' . trim($show);
        }
        return $this;
    }

    public function getShow() : string
    {
        return $this->show;
    }

    public function showString(?string $show)
    {
        $this->show = trim($show === null ? '' : $show);
        return $this;
    }

    /**
     * Extract the scope and setting from a show selector. Show selectors have
     * the form [scope.]setting[.specifier...] If no scope is present, the first
     * term in the expression is taken as the scope. Note that specifiers should
     * only be used when an explicit scope is present. If no scope is specified,
     * the optional $defaultScope is used. If not present, the global default
     * scope is used.
     *
     * @param string $text
     * @param string|null $defaultScope
     *
     * @return array
     */
    public static function getSetting(
        string $text,
        ?string $defaultScope = ''
    ) {
        if ($defaultScope === '') {
            $defaultScope = self::$defaultScope;
        }
        $first = explode('.', $text);
        if (count($first) === 1) {
            $scope = $defaultScope;
        } else {
            $scope = array_shift($first);
        }
        $setting = implode('.', $first);

        return [$scope, $setting];
    }

    public static function scopeToString($settings, ?string $setScope = '')
    {
        $parts = [];
        foreach ($settings as $setting => $info) {
            $info = implode(',', $info);
            $parts[] = ($setScope === '' ? '' : $setScope . '.')
                . $setting . ':' . $info;
        }
        return implode('|', $parts);
    }

}
