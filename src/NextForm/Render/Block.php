<?php

declare(strict_types=1);

namespace Abivia\NextForm\Render;

/**
 * Part of an output form that can enclose other blocks, and can contain elements of
 * a generated page, including header elements, scripts, style sheets, inline styles,
 * and inline script.
 */
class Block
{
    /**
     * Markup associated with an Element.
     *
     * @var string
     */
    protected $body = '';

    /**
     * Arbitrary application data.
     * @var array
     */
    protected $context = [];

    /**
     * Page header data associated with an Element.
     *
     * @var string
     */
    protected $head = '';

    /**
     * List of linked files
     * @var array
     */
    protected $linkedFiles = [];

    /**
     * Instructions on how properties are handled in a merge; missing
     * properties are ignored.
     * @var array
     */
    protected static $mergeRules = [
        'body' => 'append',
        'context' => 'data',
        'head' => 'append',
        'linkedFiles' => 'merge',
        'post' => 'prepend',
        'script' => 'append',
        'scriptFiles' => 'merge',
        'styles' => 'append',
    ];

    /**
     * On close completed event handler (pair with onCloseInit if needed).
     * @var callable
     */
    protected $onCloseDone;

    /**
     * Markup that follows any nested elements.
     * @var string
     */
    protected $post = '';

    /**
     * Executable script associated with an Element.
     * @var string
     */
    protected $script = '';

    /**
     * List of script files to link
     * @var array
     */
    protected $scriptFiles = [];

    /**
     * Inline styles
     * @var string
     */
    protected $styles = '';

    public function addHead($text) {
        $this->head .= $text;
        return $this;
    }

    public function addScript(string $text, ?bool $break = true) {
        $this->script .= $text . ($break ? "\n" : '');
        return $this;
    }

    public function addStyle($text) {
        $this->styles .= $text . "\n";
        return $this;
    }

    /**
     * Append some text to the block body.
     *
     * @param string $text
     *
     * @return $this
     */
    public function appendBody(string $text)
    {
        $this->body .= $text;
        return $this;
    }

    /**
     * Append a linefeed (and possibly some text) to the block body.
     *
     * @param string $text
     *
     * @return $this
     */
    public function appendLine(?string $text = '')
    {
        $this->body .= $text . "\n";
        return $this;
    }

    /**
     * Append some text to the block post text.
     *
     * @param string $text
     *
     * @return $this
     */
    public function appendPost(string $text)
    {
        $this->post .= $text;
        return $this;
    }

    /**
     * Append a linefeed (and possibly some text) to the block post text.
     *
     * @param string $text
     *
     * @return $this
     */
    public function appendPostLine(?string $text = '')
    {
        $this->post .= $text . "\n";
        return $this;
    }

    /**
     * Set the block body.
     *
     * @param string $text
     *
     * @return $this
     */
    public function body(string $text)
    {
        $this->body = $text;
        return $this;
    }

    /**
     * Combine the page body with any closing (post) text, execute any
     *  close handler.
     *
     * @return $this
     */
    public function close()
    {
        $this->body .= $this->post;
        $this->post = '';
        if (is_callable($this->onCloseDone)) {
            call_user_func($this->onCloseDone, $this);
        }
        return $this;
    }

    public function context(string $key, array $data) {
        $this->context[$key] = $data;
        return $this;
    }

    /**
     * Create a basic block from strings.
     *
     * @param string|null $body The opening/body of the block.
     * @param string|null $post Any closing text.
     * @return Block
     */
    public static function fromString(
        ?string $body = '',
        ?string $post = ''
    ) : Block {
        $that = new Block();
        $that->body = $body;
        $that->post = $post;
        return $that;
    }

    /**
     * Get the block body.
     *
     * @return string
     */
    public function getBody() : string {
        return $this->body;
    }

    /**
     * Get the block context.
     *
     * @return array
     */
    public function getContext() {
        return $this->context;
    }

    /**
     * Get the block head text.
     *
     * @return string
     */
    public function getHead() : string {
        return $this->head;
    }

    /**
     * Get the list of linked files.
     *
     * @return array
     */
    public function getLinkedFiles() {
        return $this->linkedFiles;
    }

    /**
     * Get the block post text.
     *
     * @return string
     */
    public function getPost() : string {
        return $this->post;
    }

    /**
     * Get the block script.
     *
     * @return string
     */
    public function getScript() : string {
        return $this->script;
    }

    /**
     * Get the list of script files.
     *
     * @return array
     */
    public function getScriptFiles() {
        return $this->scriptFiles;
    }

    /**
     * Get the block styles.
     *
     * @return string
     */
    public function getStyles() : string {
        return $this->styles;
    }

    /**
     * Add a linked file to the list.
     *
     * @return $this;
     */
    public function linkFile($file) {
        $this->linkedFiles[] = $file;
        return $this;
    }

    /**
     * Merge the contents of another block into this block.
     *
     * @param Block $block
     * @return $this
     */
    public function merge(Block $block)
    {
        foreach (self::$mergeRules as $prop => $operation)
        {
            switch ($operation) {
                case 'data':
                    // Recursive merge on application data.
                    $this->$prop = array_merge_recursive(
                        $this->$prop,
                        $block->$prop
                    );
                    break;

                case 'merge':
                    // Script and linked file lists are merged with no duplicates.
                    $this->$prop = array_unique(array_merge($this->$prop, $block->$prop));
                    break;

                case 'prepend':
                    // Merged block prefixes the existing data
                    $this->$prop = $block->$prop . $this->$prop;
                    break;

                case 'append':
                default:
                    // Merged block appended to the existing data
                    $this->$prop .= $block->$prop;
                    break;
            }
        }
        return $this;
    }

    public function onCloseDone($callable) {
        $this->onCloseDone = $callable;
    }

    /**
     * Set the block post text.
     *
     * @param string $text
     *
     * @return $this
     */
    public function post(string $text)
    {
        $this->post = $text;
        return $this;
    }

    /**
     * Add a file to the list of scripts.
     *
     * @param string $use The purpose of the script.
     * @param string $code The code required to include the file.
     *
     * @return $this
     */
    public function scriptFile(string $use, string $code) {
        $this->scriptFiles[$use] = $code;
        return $this;
    }

}
