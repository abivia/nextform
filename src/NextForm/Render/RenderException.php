<?php

namespace Abivia\NextForm\Render;

use RuntimeException;

class RenderException extends RuntimeException
{
    //
}
