<?php

declare(strict_types=1);

namespace Abivia\NextForm\Render\Html;

use Abivia\NextForm\Helpers\CompactParameters;
use Abivia\NextForm\Render\Attributes;
use Abivia\NextForm\Render\RenderException;
use Abivia\NextForm\Traits\ShowableTrait;
use Illuminate\Support\Str;

class Show
{
    use ShowableTrait;

    /**
     * Default values and validation rules for show settings. Validation
     * rules are organized by scope. Options are a list of valid strings,
     * each prefixed with |, an array indexed by integers with each element
     * being an array of [regex, replace], or an array indexed with the formal
     * value with a regex for that value being the array content. Rules are
     * evaluated sequentially, the first match is used.
     *
     * @var array
     */
    protected static $rules = [
        'appearance' => [
            'default' => 'default',
            'validate' => [
                'check' => [
                    'default' => '/^default$/i',
                    'no-label' => '/^no-?labels?$/i',
                    'toggle' => '/^toggle$/i',
                    'switch' => '/^switch$/i',
                ],
                'select' => [
                    'default' => '/^default$/i',
                    'custom' => '/^custom$/i',
                ],
            ],
        ],
        'cellspacing' => [
            'default' => '3',
            'validate' => [
                'form' => '@isSpan',
            ],
            'validateMode' => 'pack',
        ],
        'fill' => [
            'default' => 'solid',
            'validate' => [
                'form' => '|outline|solid|',
            ],
        ],
        'hidden' => [
            'default' => 'nf_hidden',
        ],
        'invalid' => [
            'default' => 'nf_isinvalid',
        ],
        'layout' => [
            'default' => 'vertical',
            'validate' => [
                'form' => [
                    'horizontal' => '/^hor/i',
                    'vertical' => '/^ver/i',
                    'inline' => '/^in/i'
                ],
            ],
        ],
        'optionwidth' => [
            'default' => '',
            'validate' => [
                'check' => '@isSpan',
            ],
            'validateMode' => 'pack',
        ],
        'purpose' => [
            'default' => 'primary',
            'validate' => [
                'form' => '|dark|danger|info|light|link|primary|secondary|success|warning|',
            ],
        ],
        'size' => [
            'default' => 'regular',
            'validate' => [
                'form' => [
                    'large' => '/^l/', 'regular' => '/^[mr]/', 'small' => '/^s/'
                ],
            ],
        ],
        'valid' => [
            'default' => 'nf_isvalid',
        ],
        'width' => [
            'default' => [],
            'validate' => [
                'field' => '@isSpan',
                'form' => '@isSpan',
            ],
        ],
    ];

    protected static $spanList = [];

    /**
     * Patterns used for validating a span show setting.
     * @var array
     */
    protected static $spanPatterns = [
        '(?<scheme>[a-z][a-z0-9]+)',
        '(?<size>[a-z][a-z0-9]+)',
        '(?<weight>[0-9]+)'
    ];

    /**
     * A LIFO stack of show settings so we can nest elements.
     * @var array
     */
    protected $stack = [];

    protected $state = [];

    function __construct($scope)
    {
        self::$defaultScope = $scope;
    }

    /**
     * Ensure we have a slot for the requested scope.
     *
     * @param string $scope
     */
    protected function checkState(string $scope)
    {
        if (!isset($this->state[$scope])) {
            $this->state[$scope] = [];
        }
    }

    /**
     * Process a show property, setting internal data structures as required.
     *
     * @param string $scope The scope that the setting applies to.
     * @param string $key The name of the setting
     * @param array $args A list of arguments
     * @throws \RuntimeError
     */
    public function do(string $scope, string $key, $args)
    {
        if (!isset(self::$rules[$key])) {
            throw new RenderException(
                'Invalid show: ' . $key . ' is not recognized.'
            );
        }
        $keyRules = self::$rules[$key];
        if (empty($args) && isset($keyRules['default'])) {
            $args[0] = $keyRules['default'];
        }
        if (isset($keyRules['validate'][$scope])) {
            $rules = $keyRules['validate'][$scope];
        } elseif (
            Attributes::isInput($scope) && isset($keyRules['validate']['field'])
        ) {
            $scope = 'field';
            $rules = $keyRules['validate']['field'];
        } elseif (isset($keyRules['validate']['form'])) {
            $rules = $keyRules['validate']['form'];
        } else {
            $rules = null;
        }
        if ($rules) {
            $validateMode = $keyRules['validateMode'] ?? 'choice';
            $choice = $this->validate($args, $validateMode, $rules, $key);
        } else {
            $choice = $args[0];
        }
        if (!isset($this->state[$scope])) {
            $this->state[$scope] = [];
        }
        // See if there's a method to process subsequent arguments,
        // if not, just store the setting in $choice
        $method = 'do' . Str::ucfirst($key);
        if (\method_exists($this, $method)) {
            $this->$method($scope, $choice, $args);
        } else {
            $this->state[$scope][$key] = $choice;
        }
    }

    /**
     * Default cell spacing options, called from do().
     *
     * @param string $scope Names the settings scope/element this applies to.
     * @param string $choice Primary option selection
     * @param array $values Array of colon-delimited settings including
     *  the initial keyword.
     */
    public function doCellspacing(
        string $scope,
        string $choice,
        $values = []
    ) {
        $this->state[$scope]['cellspacing']
            = new Attributes('class', ['cellspace']);
    }

    /**
     * Process hidden options, called from do().
     *
     * @param string $scope Names the settings scope/element this applies to.
     * @param string $choice Primary option selection
     * @param array $values Array of colon-delimited settings including the initial keyword.
     */
    public function doHidden(string $scope, string $choice, $values = [])
    {
        if (!isset($this->state[$scope])) {
            $this->state[$scope] = [];
        }
        // Use the choice as a class name
        $this->state[$scope]['hidden'] = new Attributes('class', $choice);
    }

    /**
     * Process options for invalid, called from do().
     *
     * @param string $scope Names the settings scope/element this applies to.
     * @param string $choice Primary option selection
     * @param array $values Array of colon-delimited settings including the initial keyword.
     */
    public function doInvalid(string $scope, string $choice, $values = [])
    {
        if (!isset($this->state[$scope])) {
            $this->state[$scope] = [];
        }
        // Use the choice as a class name
        $this->state[$scope]['invalid'] = $choice;
    }

    /**
     * Process layout options stub.
     *
     * @param string $scope Names the settings scope/element this applies to.
     * @param string $choice Primary option selection
     * @param array $values Array of colon-delimited settings including the initial keyword.
     */
    public function doLayout(string $scope, string $choice, $values = [])
    {
        $this->state[$scope]['layout'] = $choice;
    }

    /**
     * Process options for valid, called from do().
     *
     * @param string $scope Names the settings scope/element this applies to.
     * @param string $choice Primary option selection
     * @param array $values Array of colon-delimited settings including the initial keyword.
     */
    public function doValid(string $scope, string $choice, $values = [])
    {
        if (!isset($this->state[$scope])) {
            $this->state[$scope] = [];
        }
        // Use the choice as a class name
        $this->state[$scope]['valid'] = $choice;
    }

    /**
     * Width options, called from do().
     *
     * @param string $scope Names the settings scope/element this applies to.
     * @param string $choice Primary option selection
     * @param array $values Array of colon-delimited settings including
     *  the initial keyword.
     */
    public function doWidth(string $scope, string $choice, $values = []) {
        $this->state[$scope]['width'] = self::parseSpan($choice);
    }

    /**
     * Look for a show setting, falling back to the form if required.
     *
     * @param string $scope The scope to be searched for a value.
     * @param string $key The index of the value we want.
     * @return mixed
     */
    public function get(string $scope, string $key, $fallback = true)
    {

        if (($result = $this->getLocal($scope, $key)) !== null) {
            return $result;
        }
        if ($fallback) {
            if ($scope !== 'form') {
                // Look for something specified at the form level
                if (($result = $this->getLocal('form', $key)) !== null) {
                    return $result;
                }
            }
            if (isset(self::$rules[$key]['default'])) {
                $this->state['form'][$key]
                    = self::$rules[$key]['default'];
                return $this->state['form'][$key];
            }
        }
        return null;
    }

    /**
     * Look for a matching show setting.
     *
     * @param string $scope The scope to be searched for a value.
     * @param string $key The index of the value we want.
     * @return mixed
     */
    public function getLocal(string $scope, string $key)
    {
        if (!isset($this->state[$scope])) {
            return null;
        }
        if (!isset($this->state[$scope][$key])) {
            return null;
        }
        return $this->state[$scope][$key];
    }

    public function isset(string $scope, string $key)
    {
        return isset($this->state[$scope][$key]);
    }

    /**
     * Check to see if a string is a valid span.
     * @param string $value
     * @return bool
     */
    public static function isSpan(string $value) : bool
    {
        $segments = explode(',', $value);
        foreach ($segments as $segment) {
            $parts = explode('-', $segment);
            $numParts = count($parts);
            $pattern = '/' .
                implode('-', array_slice(self::$spanPatterns, 3 - $numParts))
                . '/';
            if (preg_match($pattern, $value) !== 1) {
                return false;
            }
        }
        return true;
    }

    /**
     * Break a generalized span string [engine-][viewport-]nnn down into
     * parts and validate.
     *
     * @param string $value span settings, multiples delimited by comma.
     * @return array[] Array containing arrays of match(bool), size,
     * weight(int), scheme.
     */
    public static function parseSpan(string $value) {
        $subValues = explode(',', $value);
        $results = [];
        foreach ($subValues as $subValue) {
            $results[] = static::parseSpanValue($subValue);
        }
        return $results;
    }

    /**
     * Validate a component of a span string has the scheme-size-weight form.
     *
     * @param string $subValue A single span setting.
     * @return array[] Elements of match(bool), scheme, size, weight(int), class.
     */
    public static function parseSpanValue(string $subValue) {
        static $unmatched = [
            'match' => false, 'scheme' => null, 'size' => null, 'weight' => null,
            'class' => null
        ];

        $result = $unmatched;
        $parts = explode('-', $subValue);
        $numParts = count($parts);
        $pattern = '/' .
            implode('-', array_slice(self::$spanPatterns, 3 - $numParts))
            . '/';
        if ($numParts === 3) {
            if (preg_match($pattern, $subValue, $match)) {
                $result['match'] = true;
                $result['scheme'] = $match['scheme'];
                $result['size'] = $match['size'];
                $result['weight'] = (int) $match['weight'];
            }
        } elseif ($numParts === 2) {
            if (preg_match($pattern, $subValue, $match)) {
                $result['match'] = true;
                $result['size'] = $match['size'];
                $result['weight'] = (int) $match['weight'];
            }
        } elseif ($numParts === 1) {
            if (preg_match($pattern, $parts[0])) {
                $result['match'] = true;
                $result['size'] = 'xs';
                $result['weight'] = (int) $parts[0];
            }
        }
        if ($result['match']) {
            if ($result['size'] === 'xs') {
                $result['class'] = (string) $result['weight'];
            } else {
                $result['class'] = $result['size'] . '-'
                    . $result['weight'];
            }
        }
        return $result;
    }

    public function pop()
    {
        $this->state = \array_pop($this->stack);
    }

    public function push()
    {
        \array_push($this->stack, $this->state);
    }

    /**
     * Convert a set of visual settings into rendering parameters.
     *
     * @param string $settings
     * @param string|null $defaultScope
     */
    public function set(string $settings, ?string $defaultScope = '')
    {
        $settings = self::tokenize($settings, $defaultScope);
        foreach ($settings as $scope => $list) {
            foreach ($list as $key => $value) {
                $this->do($scope, $key, $value);
            }
        }
    }

    /**
     * Filter a span list for those with no scheme or with a matching span id.
     *
     * @param array $spans Array of spans.
     *
     * @return array
     */
    public function spanFilter($spans)
    {
        $filtered = [];

        return $filtered;
    }

    public function spans(string $scope, string $key, \Closure $fn)
    {
        $spans = $this->get($scope, $key, false);
        if ($spans !== null) {

            // Apply the closure to spans that match the current Render class.
            foreach ($this->spanFilter($spans) as $span) {
                $fn($span);
            }
        }

        return $this;
    }

    /**
     * Break a "show" string down into a settings array.
     *
     * @param string $text String of the form
     * scope1.setting1:p1,p2...|scope2.setting2:p3,p4...
     * @return array A list of argument arrays indexed by setting.
     */
    public static function tokenize(string $text, ?string $defaultScope = '')
    {
        if ($defaultScope === '') {
            $defaultScope = self::$defaultScope;
        }
        $exprs = CompactParameters::explode($text);
        $settings = [];
        foreach ($exprs as $clause => $parts) {
            list($scope, $setting) = Show::getSetting($clause, $defaultScope);
            if ($setting === '') {
                continue;
            }
            if (!isset($settings[$scope])) {
                $settings[$scope] = [];
            }
            $settings[$scope][$setting] = $parts;
        }
        return $settings;
    }

    /**
     * Validate the arguments for a show setting.
     *
     * @param array $args The arguments provided by the user/application.
     * @param string $mode Validation mode: choice or pack
     * @param mixed $rules Validation rules (provided by $rules)
     * @param string $key The setting name.
     * @return string
     * @throws \RuntimeException
     */
    public function validate(
        $args, string $mode, $rules, string $key
    ) : string {
        if ($mode === 'pack') {
            $setting = implode(',', $args);
        } else {
            $setting = $args[0];
        }
        $valid = false;
        if (\is_array($rules)) {
            // Keyword selection or match/replace, depending on the value.
            // Scalar $match is keyword mode, Array $match is [match, replace],
            // return the key ($choice)
            foreach ($rules as $choice => $match) {
                if (\preg_match($match, $setting)) {
                    $valid = true;
                    break;
                }
            }
        } else {
            $process = $rules[0];
            // Plain string match
            $choice = $setting;
            if ($process === '|') {
                $valid = \strpos($rules, '|' . $choice . '|') !== false;
            } elseif ($process === '@') {
                $method = substr($rules, 1);
                $valid = $this->$method($choice);
            }
        }
        if (!$valid) {
            throw new RenderException(
                "Invalid show setting: {$setting} is not valid for {$key}"
            );
        }
        return $choice;
    }

}