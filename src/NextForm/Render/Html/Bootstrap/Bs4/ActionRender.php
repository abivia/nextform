<?php

declare(strict_types=1);

namespace Abivia\NextForm\Render\Html\Bootstrap\Bs4;

use Abivia\NextForm\Form\Binding\Binding;
use Abivia\NextForm\Trigger\Action;

/**
 * Action generator for Bs4
 */
class ActionRender
{
    /**
     * Generate the JS required for an action.
     *
     * @param string $formId
     * @param Binding $binding
     * @param string $action
     * @return string
     */
    public function render(
        string $formId,
        Binding $binding,
        Action $action
    ) : string {
        $subject = $action->getSubject();
        $method = 'render' . ucfirst($subject);
        if (\method_exists($this, $method)) {
            $script = '    ' . rtrim(
                implode("\n    ", $this->$method($formId, $action))
            );
        } elseif ($subject === 'script') {
            $script = '    ' . $action->getValue() . "\n";
        }

        return $script;
    }

    /**
     * Generate the JS required for a checked action.
     *
     * @param string $formId
     * @param string $action
     * @return string
     */
    protected function renderChecked(
        string $formId,
        Action $action
    ) : array {
        $value = $action->getValue();
        if (is_bool($value)) {
            $value = $value ? 'true' : 'false';
        } elseif ($value === 'checked') {
            $value = 'true';
        } elseif ($value === 'unchecked') {
            $value = 'false';
        }
        $script = [];
        foreach ($action->getTargets() as $target) {
            if (preg_match('/{(.*)}/', $target, $match)) {
                $target = $match[1];
                $script[] = "{$formId}.checkGroup('{$target}', {$value});";
            } elseif ($target[0] === '#') {
                $script[] = "{$formId}.check($('{$target}'), {$value});";
            } elseif ($target[0] === '&') {
                $script[] = "{$formId}.check($('#{$formId}"
                    . " [data-nf-name^=\"{$target}\"]'), {$value});";
            } else {
                $script[] = "{$formId}.check($('{$target}'), {$value});";
            }
        }

        return $script;
    }

    /**
     * Generate the JS required for a display action.
     *
     * @param string $formId
     * @param string $action
     * @return string
     */
    protected function renderDisplay(
        string $formId,
        Action $action
    ) : array {
        $value = $action->getValue();
        if (is_bool($value)) {
            $value = $value ? 'true' : 'false';
        } elseif ($value === 'checked') {
            $value = 'this.checked';
        } elseif ($value === 'unchecked') {
            $value = '!this.checked';
        }
        $script = [];
        foreach ($action->getTargets() as $target) {
            if (preg_match('/{(.*)}/', $target, $match)) {
                $target = $match[1];
                $script[] = "{$formId}.displayGroup('{$target}', {$value});";
            } elseif ($target[0] === '#') {
                $script[] = "$('{$target}').toggle({$value});";
            } elseif ($target[0] === '&') {
                $script[] = "$('#{$formId}[data-nf-name^=\"{$target}\"]')"
                    . ".toggle({$value});";
            } else {
                $script[] = "{$formId}.displayContainer('{$target}', {$value});";
            }
        }

        return $script;
    }

    /**
     * Generate the JS required for an enable action.
     *
     * @param string $formId
     * @param string $action
     * @return string
     */
    protected function renderEnable(
        string $formId,
        Action $action
    ) : array {
        $value = $action->getValue() ? 'false' : 'true';
        $script = [];
        foreach ($action->getTargets() as $target) {
            if (preg_match('/{(.*)}/', $target, $match)) {
                $target = $match[1];
                $script[] = "{$formId}.disableGroup('$target', $value);";
            } elseif ($target[0] === '#') {
                $script[] = "$('$target').prop('disabled', $value);";
            } elseif ($target[0] === '&') {
                $script[] = "$('#{$formId}\[data-nf-name^=\"{$target}\"]')"
                    . ".prop('disabled', {$value});";
            } else {
                $script[] = "{$formId}.disableContainer"
                    . "('{$target}', {$value});";
            }
        }

        return $script;
    }

}
