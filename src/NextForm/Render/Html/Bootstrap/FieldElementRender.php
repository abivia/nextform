<?php

declare(strict_types=1);

/**
 *
 */
namespace Abivia\NextForm\Render\Html\Bootstrap;

use Abivia\NextForm\Render\Html\FieldElementRender as FieldElementRenderBase;

/**
 * Has to exist so we can instantiate the correct field type classes.
 */
class FieldElementRender extends FieldElementRenderBase
{

}
