<?php

declare(strict_types=1);

/**
 *
 */
namespace Abivia\NextForm\Render\Html\Bootstrap\FieldElementRender;

use Abivia\NextForm\Data\Labels;
use Abivia\NextForm\Render\Attributes;
use Abivia\NextForm\Render\Block;
use Abivia\NextForm\Render\Html\FieldElementRender\Common as BaseCommon;

class Common extends BaseCommon
{

    /**
     * Get common attributes for the input element and add BS4 specifics.
     *
     * @return Attributes
     */
    protected function inputAttributes() : Attributes
    {
        $attrs = parent::inputAttributes();

        if ($this->labels->has('help')) {
            $attrs->set('aria-describedby', $attrs->get('id') . '_help');
        }
        if (in_array($this->inputType, ['button', 'reset', 'submit'])) {
            $attrs->itemAppend('class', $this->engine->getButtonClass());
        } else {
            $attrs->itemAppend('class', 'form-control');
        }

        return $attrs;
    }

    /**
     * Generate the input element and any wrapping/supporting code.
     *
     * @param Attributes $attrs
     * @return Block
     */
    protected function inputGroup(Attributes $attrs) : Block
    {
        $input = $this->engine->inputGroup($this->labels, $attrs);

        // Generate supporting messages
        $input->appendBody($this->engine->writeInputSupport($this->labels, $attrs));

        return $input;
    }

}
