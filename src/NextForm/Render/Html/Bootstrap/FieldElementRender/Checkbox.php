<?php

declare(strict_types=1);

/**
 *
 */
namespace Abivia\NextForm\Render\Html\Bootstrap\FieldElementRender;

use Abivia\NextForm\Data\Labels;
use Abivia\NextForm\Data\Population\Option;
use Abivia\NextForm\Form\Binding\FieldBinding;
use Abivia\NextForm\NextForm;
use Abivia\NextForm\Render\Attributes;
use Abivia\NextForm\Render\Block;
use Abivia\NextForm\Render\Html\FieldElementRender\Checkbox as BaseCheckbox;

class Checkbox extends BaseCheckbox
{
    /**
     * The show setting that determines how the element is displayed.
     * @var string
     */
    protected $appearance;

    /**
     * Attributes used for option labels.
     * @var Attributes
     */
    protected $labelAttrs;

    /**
     * Attributes used for each option.
     * @var Attributes
     */
    protected $listAttrs;

    /**
     * Additional classes for each list item.
     * @var string
     */
    protected $listClass;

    /**
     * The type of element we're rendering, checkbox or element.
     * @var string
     */
    protected $type;

    /**
     * Value of the current binding.
     * @var string|array
     */
    protected $value;

    /**
     * Generate a simple input element for a single-valued checkbox.
     *
     * @param FieldBinding $binding
     * @param Attributes $attrs
     * @return Block $block The output block.
     */
    protected function checkInput(FieldBinding $binding, Attributes $attrs)
    {
        // This is a single-valued element
        $attrs->set('id', $binding->getId());
        if ($this->access !== 'mask') {
            $attrs->setIfNotNull('value', $binding->getValue());
        }
        if (
            $binding->getValue() === $binding->getElement()->getDefault()
            && $binding->getValue() !== null
        ) {
            $attrs->flag('checked');
        }
        return Block::fromString($this->engine->writeTag('input', $attrs) . "\n");
    }

    /**
     * Generate check/radio HTML inputs from an element's data list.
     *
     * @return Block The output block.
     */
    protected function checkList() : Block
    {
        $this->listAttrs = $this->attrs->copy();
        $baseId = $this->binding->getId();
        $this->type = $this->binding->getDataProperty()->getPresentation()
            ->getType();
        if ($this->access === 'mask') {
            $this->value = null;
        } else {
            $this->value = $this->binding->getValue();
            if ($this->value === null) {
                $this->value = $this->binding->getElement()->getDefault();
            }
        }
        $this->appearance = $this->engine->show()->get('check', 'appearance');
        $optionWidth = $this->engine->show()->get('check', 'optionwidth');
        $listClasses = ['form-check'];
        if ($this->engine->show()->get('check', 'layout') === 'inline') {
            $listClasses[] = 'form-check-inline';
        }
        $this->labelAttrs = new Attributes();
        $this->labelAttrs->itemAppend('class', 'form-check-label');
        $block = new Block();
        if ($optionWidth !== '') {
            $spans = $this->engine->show()->parseSpan($optionWidth);
            foreach ($spans as $span) {
                if (
                    $span['match']
                    && ($span['scheme'] === null || $span['scheme'] === 'b4')
                ) {
                    $listClasses[] = "col-{$span['class']}";
                }
            }
            $block->merge(
                $this->engine->writeElement(
                    'div',
                    ['attributes' => new Attributes('class', 'row ml-0')]
                )
            );
        }
        $this->listClass = $listClasses;
        foreach ($this->binding->getList(true) as $optId => $radio) {
            $block->merge(
                $this->checkListItem("{$baseId}_opt{$optId}", $radio)
            );
        }
        $block->close();
        return $block;
    }

    /**
     * Generate a single item in a check/radio list.
     *
     * @param string $id The id for the HTML element
     * @param Option $radio The list item.
     * @return Block
     */
    protected function checkListItem(string $id, Option $radio) : Block
    {
        $block = new Block();
        $optAttrs = $this->listAttrs->copy();
        $optAttrs->set('id', $id);
        $value = $radio->getValue();
        $optAttrs->flag('disabled', !$radio->getEnabled());
        $optAttrs->flag('autofocus', $radio->getAutofocus());
        if ($this->access === 'view') {
            $optAttrs->flag('disabled', true);
        }
        if ($this->access === 'mask') {
            $optAttrs->flag('checked', false);
            $optAttrs->flag('disabled', true);
        } else {
            $optAttrs->set('value', $value);
            $optAttrs->flag('checked', $this->isChecked($value));
        }
        $optAttrs->setIfNotNull('data-nf-name', $radio->getName());
        $optAttrs->setIfNotEmpty('*data-nf-group', $radio->getGroups());
        $optAttrs->setIfNotNull('*data-nf-sidecar', $radio->getSidecar());

        $block->merge(
            $this->engine->writeElement(
                'div',
                ['attributes' => new Attributes('class', $this->listClass)]
            )
        );
        $optAttrs->itemAppend('class', 'form-check-input');
        if ($this->appearance === 'no-label') {
            $optAttrs->set('aria-label', $radio->getLabel());
        }
        $block->appendLine($this->engine->writeTag('input', $optAttrs));
        if ($this->appearance !== 'no-label') {
            $this->labelAttrs->set('!for', $id);
            $tempLabels = Labels::build();
            if ($this->access === 'mask') {
                $tempLabels->set('inner', $this->labels->get('mask'));
            } else {
                $tempLabels->set('inner', $radio->getLabel());
            }
            $block->appendBody(
                $this->engine->writeLabel(
                    'label',
                    $tempLabels,
                    'inner',
                    $this->labelAttrs,
                    ['break' => true]
                )
            );
        }
        $block->close();

        return $block;
    }

    /**
     * Generate check/radio HTML inputs as buttons from an element's data list.
     *
     * @param FieldBinding $binding The element we're generating for.
     * @param Attributes $attrs Parent element attributes.
     * @return Block $block The output block.
     */
    protected function checkListButtons(FieldBinding $binding, Attributes $attrs)
    {
        $baseId = $binding->getId();
        $type = $binding->getDataProperty()->getPresentation()->getType();
        $this->value = $binding->getValue();
        if ($this->value === null) {
            $this->value = $binding->getElement()->getDefault();
        }
        // We know the appearance is going to be button or toggle
        $block = new Block();
        foreach ($binding->getList(true) as $optId => $radio) {
            $this->labelAttrs = new Attributes();
            $optAttrs = $attrs->copy();
            $id = $baseId . '_opt' . $optId;
            $optAttrs->set('id', $id);
            if ($this->access === 'mask') {
                $value = null;
            } else {
                $value = $radio->getValue();
                $optAttrs->set('value', $value);
            }
            $optAttrs->setIfNotNull('data-nf-name', $radio->getName());
            $optAttrs->setIfNotEmpty('*data-nf-group', $radio->getGroups());
            $optAttrs->setIfNotNull('*data-nf-sidecar', $radio->getSidecar());
            if (
                $type === 'checkbox'
                && is_array($this->value) && in_array($value, $this->value)
            ) {
                $checked = true;
            } elseif ($value === $this->value) {
                $checked = true;
            } else {
                $checked = false;
            }
            if ($this->access === 'mask') {
                $checked = false;
            }
            if ($checked) {
                $optAttrs->flag('checked');
            }
            $show = $radio->getShow();
            if ($show) {
                $this->engine->pushContext();
                $this->engine->show()->set($show, 'radio');
            }
            $buttonClass = $this->engine->getButtonClass('radio');
            $this->labelAttrs->itemAppend(
                'class',
                $buttonClass . ($checked ? ' active' : '')
            );
            $block->merge(
                $this->engine->writeElement(
                    'label', ['attributes' => $this->labelAttrs]
                )
            );
            $block->appendBody(
                $this->engine->writeTag('input', $optAttrs) . "\n"
                . $radio->getLabel()
            );
            $block->close();
            if ($show) {
                $this->engine->popContext();
            }
        }
        return $block;
    }

    /**
     * Generate a single checkbox/radio input.
     *
     * @param FieldBinding $binding The element we're generating for.
     * @param Attributes $attrs
     * @param Attributes $groupAttrs
     * @return Block $block The output block.
     */
    protected function checkSingle(
        FieldBinding $binding,
        Attributes $attrs,
        Attributes $groupAttrs
    ) : Block {
        $baseId = $binding->getId();

        // Mask the label if required
        if ($this->access === 'mask') {
            $this->labels->set('inner', $this->labels->get('mask'));
        }

        $this->appearance = $this->engine->show()->get('check', 'appearance');
        $block = $this->engine->writeElement(
            'div', ['attributes' => $groupAttrs]
        );
        if ($this->labels->has('help')) {
            $attrs->set(
                'aria-describedby',
                $baseId . NextForm::HELP_LABEL
            );
        }
        $attrs->itemAppend('class', 'form-check-input');
        if ($this->appearance === 'no-label') {
            $attrs->setIfNotNull('aria-label', $this->labels->get('inner'));
            $block->merge($this->checkInput($binding, $attrs));
        } else {
            $block->merge($this->checkInput($binding, $attrs));
            $this->labelAttrs = new Attributes();
            $this->labelAttrs->set('!for', $baseId);
            $this->labelAttrs->itemAppend('class', 'form-check-label');
            $block->appendBody($this->engine->writeLabel(
                'label', $this->labels, 'inner',
                $this->labelAttrs, ['break' => true]
            ));
        }
        $block->close();
        return $block;
    }

    /**
     * Render a single-valued checkbox as a button.
     *
     * @param FieldBinding $binding The element we're generating for.
     * @param Attributes $attrs
     * @param Attributes $groupAttrs
     * @return Block $block The output block.
     */
    protected function checkSingleButton(
        FieldBinding $binding,
        Attributes $attrs,
        Attributes $groupAttrs
    ) : Block {
        $baseId = $binding->getId();
        $attrs->set('id', $baseId);
        $block = $this->engine->writeElement('div', ['attributes' => $groupAttrs]);
        if ($this->labels->has('help')) {
            $attrs->set('aria-describedby', $baseId . NextForm::HELP_LABEL);
        }
        $this->labelAttrs = new Attributes();
        $buttonClass = $this->engine->getButtonClass('radio');
        $checked = $binding->getValue() === $binding->getElement()->getDefault()
            && $binding->getValue() !== null;
        $this->labelAttrs->itemAppend(
            'class',
            $buttonClass . ($checked ? ' active' : '')
        );
        $block->merge($this->engine->writeElement(
            'label',
            ['attributes' => $this->labelAttrs]
        ));
        $block->appendBody(
            $this->engine->writeTag('input', $attrs) . "\n"
            . $this->labels->get('inner')
        );
        $block->close();

        return $block;
    }

    /**
     * Get common attributes for the input element and add BS4 specifics.
     *
     * @return Attributes
     */
    protected function inputAttributes() : Attributes
    {
        $attrs = parent::inputAttributes();

        return $attrs;
    }

    /**
     * Generate the input element and any wrapping/supporting code.
     *
     * @param Attributes $attrs
     * @return Block
     */
    protected function inputGroup(Attributes $attrs) : Block
    {
        $this->attrs = $attrs;
        if (empty($this->binding->getList(true))) {
            $input = $this->single();
        } else {
            $input = $this->multiple();
        }

        return $input;
    }

    /**
     * Determine if a list element is checked/selected.
     * @param scalar|array $value The list element value
     * @return bool True when the element is checked.
     */
    protected function isChecked($value) : bool
    {
        if (
            $this->type === 'checkbox'
            && is_array($this->value)
            && in_array($value, $this->value)
        ) {
            $checked = true;
        } elseif ($value === $this->value) {
            $checked = true;
        } else {
            $checked = false;
        }
        return $checked;
    }

    protected function multiple() : Block {
        $this->appearance = $this->engine->show()->get('check', 'appearance');
        $layout = $this->engine->show()->get('form', 'layout');

        $block = new Block();
        $baseId = $this->binding->getId();

        // If this is showing as a row of buttons change the group attributes
        $groupAttrs = new Attributes();
        if ($this->appearance === 'toggle') {
            $asButtons = true;
            $groupAttrs->itemAppend('class', 'btn-group btn-group-toggle');
            $groupAttrs->set('data-toggle', 'buttons');

            // For buttons, write before/after labels on the same line
            $labelElement = 'span';
        } else {
            $checkLayout = $this->engine->show()->get('check', 'layout');
            // Non-buttons can be stacked (default) or inline
            $asButtons = false;
            $groupAttrs->itemAppend(
                'class',
                'form-check' . ($checkLayout === 'inline' ? ' form-check-inline' : '')
            );
            $labelElement = 'div';
        }

        // Customize the header to align baselines in horizontal layouts
        $headerAttrs = new Attributes();
        if ($layout === 'vertical') {
            $rowBlock = $this->engine->writeElement(
                'fieldset', [
                    'attributes' => $this->engine->groupAttributes($this->binding),
                    'show' => 'formGroupAttributes'
                ]
            );
            $headerElement = 'div';
        } else {
            // Horizontal layouts has a fieldset with just the form group class
            $rowAttrs = new Attributes('class', 'form-group');
            $rowAttrs->merge($this->engine->groupAttributes($this->binding));
            $rowBlock = $this->engine->writeElement(
                'fieldset', ['attributes' => $rowAttrs]
            );
            // Horizontal layouts have another div for the row
            $rowBlock->merge($this->engine->writeElement(
                'div', ['attributes' => new Attributes('class', 'row')])
            );
            $headerElement = 'legend';
            if (!$asButtons && $this->access === 'write') {
                $headerAttrs->itemAppend('class', 'pt-0');
            }
        }

        // Write the heading. We added a pt-0 for horizontal layouts
        $block->appendBody($this->engine->writeLabel(
            $headerElement,
            $this->labels,
            ['heading' => 'headingAttributes'],
            $headerAttrs,
            ['break' => true]
        ));

        if ($layout === 'horizontal') {
            // Create the second column for a horizontal layout
            $block->merge($this->engine->writeElement(
                'div', ['show' => 'inputWrapperAttributes']
            ));
        }

        // Generate everything associated with the inputs, including before/after texts
        $input = new Block();
        $input->appendBody($this->engine->writeLabel(
            $labelElement,
            $this->labels,
            ['before' => 'before' . $labelElement]
        ));
        if ($this->labels->has('help')) {
            $this->attrs->set(
                'aria-describedby',
                $baseId . NextForm::HELP_LABEL
            );
        }
        if ($asButtons) {
            $input->merge($this->engine->writeElement(
                'div', ['attributes' => $groupAttrs]
            ));
            $input->merge($this->checkListButtons(
                $this->binding, clone $this->attrs
            ));
        } else {
            $input->merge($this->checkList());
        }
        $input->close();

        // Write any after-label
        $input->appendBody($this->engine->writeLabel(
            $labelElement,
            $this->labels,
            'after',
            null,
            ['break' => true]
        ));

        $block->merge($input);

        // Generate supporting messages
        $block->appendBody(
            $this->engine->writeInputSupport($this->labels, $this->attrs)
        );

        $block->close();
        $rowBlock->merge($block);
        $rowBlock->close();
        return $rowBlock;
    }

    /**
     * Render a single element field.
     *
     * @return Block
     */
    protected function single() : Block
    {
        $this->appearance = $this->engine->show()->get('check', 'appearance');
        $checkLayout = $this->engine->show()->get('check', 'layout');
        $block = new Block();

        // Generate hidden elements and return.
        if ($this->access === 'hide') {
            $this->attrs->set('type', 'hidden');
            $block->merge($this->checkInput($this->binding, $this->attrs));
            return $block;
        }
        if ($this->access === 'view') {
            $this->attrs->flag('readonly');
        }

        // If this is showing as a row of buttons change the group attributes
        $groupAttrs = new Attributes();
        if ($this->appearance === 'toggle') {
            $asButtons = true;
            $groupAttrs->itemAppend('class', 'btn-group btn-group-toggle');
            $groupAttrs->set('data-toggle', 'buttons');
        } else {
            // Non-buttons can be stacked (default) or inline
            $asButtons = false;
            $groupAttrs->set(
                'class',
                'form-check' . ($checkLayout === 'inline' ? ' form-check-inline' : '')
            );
        }

        // Customize the header to align baselines in horizontal layouts
        $headerAttrs = new Attributes();
        $rowBlock = $this->engine->writeElement(
            'div', [
                'attributes' => $this->engine->groupAttributes($this->binding),
                'show' => 'formGroupAttributes'
            ]
        );
        if ($this->engine->show()->get('form', 'layout') !== 'vertical') {
            if (!$asButtons && $this->access === 'write') {
                $headerAttrs->itemAppend('class', 'pt-0');
            }
        }

        // Write the heading. We added a pt-0 for horizontal non-button layouts
        $block->appendBody($this->engine->writeLabel(
            'div',
            $this->labels,
            ['heading' => 'headingAttributes'],
            $headerAttrs,
            ['break' => true]
        ));
        if ($this->engine->show()->get('form', 'layout') === 'horizontal') {
            // Create the second column for a horizontal layout
            $block->merge($this->engine->writeElement(
                'div', ['show' => 'inputWrapperAttributes']
            ));
        }

        // Generate everything associated with the inputs, including before/after texts
        $input = new Block();
        $input->appendBody($this->engine->writeLabel(
            'span', $this->labels, ['before' => 'beforespan']
        ));
        if ($asButtons) {
            $input->merge($this->checkSingleButton(
                $this->binding, $this->attrs, $groupAttrs
            ));
        } else {
            $input->merge($this->checkSingle(
                $this->binding, $this->attrs, $groupAttrs
            ));
        }
        $input->appendBody($this->engine->writeLabel(
            'span', $this->labels, 'after', null, ['break' => true]
        ));
        $input->close();
        $block->merge($input);

        // Generate supporting messages
        $block->appendBody(
            $this->engine->writeInputSupport($this->labels, $this->attrs)
        );

        $rowBlock->merge($block);
        $rowBlock->close();

        return $rowBlock;
    }

}
