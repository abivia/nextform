<?php

declare(strict_types=1);

/**
 *
 */
namespace Abivia\NextForm\Render\Html\Bootstrap\FieldElementRender;

use Abivia\NextForm\Data\Labels;
use Abivia\NextForm\Render\Attributes;
use Abivia\NextForm\Render\Block;
use Abivia\NextForm\Render\Html\FieldElementRender\Select as BaseSelect;

class Select extends BaseSelect
{

    /**
     * Get attributes for the input element and add BS4 specifics.
     *
     * @return Attributes
     */
    protected function inputAttributes() : Attributes
    {
        $attrs = parent::inputAttributes();

        if ($this->access === 'write') {
            if ($this->engine->show()->get('select', 'appearance') === 'custom') {
                $attrs->itemAppend('class', 'custom-select');
            } else {
                $attrs->itemAppend('class', 'form-control');
            }
        }

        return $attrs;
    }

    /**
     * Generate the input element and any wrapping/supporting code.
     */
    protected function inputGroup(
        Attributes $attrs
    ) : Block {
        $select = $this->engine->inputGroupPre($this->labels);

        if ($this->access === 'mask') {
            $select->merge($this->renderMask($attrs));
        } elseif ($this->access === 'view') {
            $select->merge($this->renderView($attrs));
        } else {
            // Write access: Add in any validation
            $attrs->addValidation('select', $this->dataProperty->getValidation());

            // Generate the actual input element.
            $select->merge(
                $this->engine->writeElement('select', ['attributes' => $attrs])
            );

            // Add the options
            $select->merge(
                $this->renderOptions(
                    $this->binding->getList(true), $this->value
                )
            );
        }

        $select->merge($this->engine->inputGroupPost($this->labels));
        $select->close();

        // Generate supporting messages
        $select->appendBody($this->engine->writeInputSupport(
            $this->labels, $attrs
        ));

        return $select;
    }

}
