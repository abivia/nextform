<?php

declare(strict_types=1);

/**
 *
 */
namespace Abivia\NextForm\Render\Html\Bootstrap\FieldElementRender;

use Abivia\NextForm\Data\Labels;
use Abivia\NextForm\Render\Attributes;
use Abivia\NextForm\Render\Block;
use Abivia\NextForm\Render\Html\FieldElementRender\File as BaseFile;

class File extends BaseFile
{

    /**
     * Get attributes for the input element and add BS4 specifics.
     *
     * @return Attributes
     */
    protected function inputAttributes() : Attributes
    {
        $attrs = parent::inputAttributes();
        $attrs->itemAppend('class', 'form-control-file');

        return $attrs;
    }

    /**
     * Generate the input element and any wrapping/supporting code.
     */
    protected function inputGroup(
        Attributes $attrs
    ) : Block {
        $input = $this->engine->inputGroup($this->labels, $attrs);

        // Generate supporting messages
        $input->appendBody($this->engine->writeInputSupport(
            $this->labels, $attrs
        ));

        return $input;
    }

}
