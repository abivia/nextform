<?php

declare(strict_types=1);

/**
 *
 */
namespace Abivia\NextForm\Render\Html\Bootstrap\FieldElementRender;

use Abivia\NextForm\Data\Labels;
use Abivia\NextForm\Render\Attributes;
use Abivia\NextForm\Render\Block;
use Abivia\NextForm\Render\Html\FieldElementRender\Textarea as BaseTextarea;

class Textarea extends BaseTextarea
{

    /**
     * Get attributes for the input element and add BS4 specifics.
     *
     * @return Attributes
     */
    protected function inputAttributes() : Attributes
    {
        $attrs = parent::inputAttributes();
        $attrs->itemAppend('class', 'form-control');

        return $attrs;
    }

    /**
     * Generate the input element and any wrapping/supporting code.
     */
    protected function inputGroup(
        Attributes $attrs,
        $value
    ) : Block {
        $input = $this->engine->inputGroupPre($this->labels);

        // Generate the textarea element
        $input->appendLine($this->engine->writeTag('textarea', $attrs, $value));

        $input->merge($this->engine->inputGroupPost($this->labels));

        // Generate supporting messages
        $input->appendBody($this->engine->writeInputSupport(
            $this->labels, $attrs
        ));

        return $input;
    }

}
