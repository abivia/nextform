<?php

declare(strict_types=1);

/**
 *
 */
namespace Abivia\NextForm\Render\Html\Bootstrap\FieldElementRender;

use Abivia\NextForm\Data\Labels;
use Abivia\NextForm\Render\Attributes;
use Abivia\NextForm\Render\Block;
use Abivia\NextForm\Render\Html\FieldElementRender\Html as BaseHtml;

class Html extends BaseHtml
{
    /**
     * Generate the input element and any wrapping/supporting code.
     */
    protected function inputGroup(
        Attributes $attrs,
        $text
    ): Block {
        $input = $this->engine->inputGroupPre();

        // Generate the textarea element
        $input->appendLine($text);

        $input->merge($this->engine->inputGroupPost());

        // Generate supporting messages
        $input->appendBody($this->engine->writeInputSupport(
            $this->labels, $attrs
        ));

        return $input;
    }

}
