<?php

declare(strict_types=1);

/**
 *
 */
namespace Abivia\NextForm\Render\Html\Bootstrap\Bs5;

use Abivia\NextForm\Render\Attributes;
use Abivia\NextForm\Render\Html\Bootstrap\Show as ShowBase;

class Show extends ShowBase
{

    public function __construct($scope)
    {
        parent::__construct($scope);
        self::$spanList[] = 'b5';
    }

    /**
     * Process layout options, called from do().
     *
     * @param string $scope Names the settings scope/element this applies to.
     * @param string $choice Primary option selection
     * @param array $values Array of colon-delimited settings including the
     * initial keyword.
     */
    public function doLayout(string $scope, string $choice, $values = [])
    {
        //
        // Structure of the layout elements
        // formGroupAttributes - An Attributes object associated with the
        //  element acting as a form group
        //
        // headingAttributes - Set in horizontal layouts to set heading widths
        //
        // inputWrapperAttributes - Set in horizontal layouts for giving an
        //  input element width
        //
        $this->checkState($scope);
        $this->state[$scope]['layout'] = $choice;
        if ($scope === 'form') {

            $form = &$this->state['form'];
            // Reset key settings
            unset($form['inputWrapperAttributes']);
            unset($form['headingAttributes']);

            // When before text is in a span, it has a trailing margin
            $form['beforespan'] = new Attributes('class', ['me-1']);

            // A cell element will appear as a row
            $form['cellElementAttributes'] = new Attributes(
                'class', ['mb-3']
            );

            // Group wrapper encloses the complete output for a field,
            // including labels
            $form['formGroupAttributes'] = new Attributes(
                'class', ['form-group']
            );

            // Cell wrapper is like a form group with less padding
            $form['formCellAttributes'] = new Attributes(
                'class', ['form-group', 'px-1']
            );
            if ($choice === 'horizontal') {
                $this->doLayoutAnyHorizontal($scope, $values);
            } elseif ($choice === 'vertical') {
                $this->doLayoutAnyVertical($scope, $values);
            }
        }
    }

}