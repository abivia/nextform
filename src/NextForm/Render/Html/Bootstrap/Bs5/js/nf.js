/*
 * https://flaviocopes.com/jquery/
    Selecting DOM elements using the # or . notation: document.querySelector('.button')
    Waiting for page DOM to load: document.addEventListener("DOMContentLoaded", () => { ... })
    Executing AJAX requests: fetch('/api.json').then(response => response.text()).then(body => console.log(body))

 */


