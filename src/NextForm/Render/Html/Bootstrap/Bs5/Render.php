<?php

declare(strict_types=1);

namespace Abivia\NextForm\Render\Html\Bootstrap\Bs5;

use Abivia\NextForm\Contracts\RenderInterface;
use Abivia\NextForm\Data\Labels;
use Abivia\NextForm\Form\Binding\FieldBinding;
use Abivia\NextForm\Render\Block;
use Abivia\NextForm\Render\Html\Bootstrap\Render as BsRender;

/**
 * Render for Bs5
 */
class Render extends BsRender implements RenderInterface
{
    protected static $links = [
        [
            'href' => 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css',
            'rel' => 'stylesheet',
            'integrity' => 'sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl',
            'crossorigin' => 'anonymous',
        ]
    ];

    protected static $scripts = [
        'bootstrap' => [
            'src' => 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js',
            'integrity' => 'sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0',
            'crossorigin' => 'anonymous',
        ]
    ];

    public function __construct($options = [])
    {
        $this->show = new Show('form');

        parent::__construct($options);
        $this->initialize();
        $this->useOptions($options);
    }

    protected function initialize() {
        parent::initialize();
        self::$coreJsPath = __DIR__ . '/js/nf.js';
    }

    /**
     * Write anything that comes after the input group.
     *
     * @param Labels $labels
     * @return Block
     */
    public function inputGroupPost(?Labels $labels = null): Block
    {
        if ($labels !== null && $labels->has('after')) {
            // Write the after label in the append group
            $group = $this->writeLabel(
                'span',
                $labels,
                ['after' => 'inputAfter'],
                new Attributes('class', ['input-group-text'])
            );
        } else {
            $group = new Block();
        }

        return $group;
    }

    /**
     * Write anything that comes before the input group.
     *
     * @param Labels $labels
     * @return Block
     */
    public function inputGroupPre(?Labels $labels = null): Block
    {
        if (
            $labels !== null
            && ($labels->has('before') || $labels->has('after'))
        ) {
            // We have before/after elements to attach, we need to create
            // an input group.
            $input = $this->writeElement(
                'div', ['attributes' => new Attributes('class', 'input-group'), 'show' => 'inputWrapperAttributes']
            );

            if ($labels->has('before')) {
                // Write the before label
                $input->merge($this->writeLabel(
                    'span',
                    $labels,
                    ['before' => 'inputBefore'],
                    new Attributes('class', ['input-group-text'])
                ));
            }
        } else {
            $input = $this->writeElement(
                'div', ['show' => 'inputWrapperAttributes']
            );
        }

        return $input;
    }

}

