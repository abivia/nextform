<?php

declare(strict_types=1);

/**
 *
 */
namespace Abivia\NextForm\Render\Html\Bootstrap;

use Abivia\NextForm\Data\Labels;
use Abivia\NextForm\NextForm;
use Abivia\NextForm\Render\Attributes;
use Abivia\NextForm\Render\Block;
use Abivia\NextForm\Render\Html\ButtonElementRender as ButtonElementRenderBase;

class ButtonElementRender extends ButtonElementRenderBase
{

    protected function inputAttributes() : Attributes
    {
        $attrs = parent::inputAttributes();
        $attrs->itemAppend('class', $this->engine->getButtonClass());
        if ($this->labels->has('help')) {
            $attrs->set('aria-describedby', $attrs->get('id') . '_help');
        }

        return $attrs;
    }

    protected function inputGroup(Attributes $attrs) : Block
    {
        if ($this->labels->has('help')) {
            $attrs->set(
                'aria-describedby',
                $attrs->get('id') . NextForm::HELP_LABEL
            );
        }

        // Generate the input wrapper, if required for a horizontal layout.
        $input = $this->engine->writeElement('div', ['show' => 'inputWrapperAttributes']);

        // Add in the input element and before/after labels
        $input->appendLine($this->engine->writeLabel(
                'span', $this->labels, ['before' => 'beforespan']
            )
            . $this->engine->writeTag('input', $attrs)
            . $this->engine->writeLabel('span', $this->labels, 'after')
        );

        // Generate supporting messages
        $input->appendBody($this->engine->writeInputSupport(
            $this->labels, $attrs
        ));

        return $input;
    }

}
