<?php

declare(strict_types=1);

namespace Abivia\NextForm\Render\Html;

use Abivia\NextForm\Render\Attributes;
use Abivia\NextForm\Render\RenderException;
use Abivia\NextForm\Render\Block;
use Illuminate\Support\Str;

class FieldElementRender extends AbstractElementRender
{

    protected const FIELD_CLASS = 'FieldElementRender';

    protected static $handlerCache = [];

    /**
     * Types of <input> that we'll auto-generate a confirmation for
     * @var array
     */
    protected static $inputConfirmable = [
        'email', 'number', 'password', 'tel', 'text',
    ];

    /**
     * Render a data list, if there is one.
     *
     * @param Attributes $attrs Parent attributes.
     * @param string $type The element type
     * @param array $options Options, specifically access rights.
     * @return Block
     */
    public function dataList(Attributes $attrs, string $type, $options) : Block
    {
        $block = new Block();
        // Check for a data list, if there is write access.
        $list = $this->engine->getAccess($options) === 'write'
            && Attributes::inputHas($type, 'list')
            ? $this->binding->getList(true) : [];
        if (!empty($list)) {
            $attrs->set('list', $attrs->get('id') . '_list');
            $block->appendLine(
                '<datalist id="' . $attrs->get('list') . '">'
            );
            foreach ($list as $option) {
                $optAttrs = new Attributes();
                $optAttrs->set('value', $option->getValue());
                $optAttrs->setIfNotNull('data-nf-name', $option->getName());
                $optAttrs->setIfNotEmpty('*data-nf-group', $option->getGroups());
                $optAttrs->setIfNotNull('*data-nf-sidecar', $option->getSidecar());
                $block->appendLine(
                    $this->engine->writeTag('option', $optAttrs)
                );
            }
            $block->appendLine('</datalist>');
        }
        return $block;
    }

    /**
     * Get the class that handles a field type for a rendering engine.
     *
     * @param string $engineClass The render engine class name
     * @param string $classType The field render class type
     * @return string The name of the class that will do the render
     * @throws \RuntimeException If there is nothing that will render the field.
     */
    protected function findHandler(
        string $engineClass,
        string $classType
    ) : string {
        foreach ([$classType, 'Common'] as $match) {
            // Walk up the render stack looking for a match
            $enginePath = $engineClass;
            while (true) {
                // Try the next engine up the stack, break if we hit the top.
                $lastPos = \strrpos($enginePath, '\\');
                if (
                    $lastPos === false
                    || \substr($enginePath, $lastPos) === '\\NextForm'
                ) {
                    break;
                }
                $enginePath = \substr($enginePath, 0, $lastPos);

                $fieldHandler = $enginePath . '\\' . self::FIELD_CLASS
                    . '\\' . $match;
                if (\class_exists($fieldHandler)) {
                    self::$handlerCache[$engineClass][$classType] = $fieldHandler;
                    return $fieldHandler;
                }
            }
        }
        throw new RenderException(
            'No render class for field type ' . $classType
        );
    }

    /**
     * Get an instance of the class that handles fields of the specified type.
     *
     * @param string $type The field type.
     * @return object An instance of the required handler.
     */
    protected function getFieldHandler(string $type)
    {
        $engineClass = \get_class($this->engine);
        $classType = Str::ucfirst($type);

        self::$handlerCache[$engineClass] ??= [];
        $fieldHandler = self::$handlerCache[$engineClass][$classType]
            ?? $this->findHandler($engineClass, $classType);

        return new $fieldHandler($this, $this->engine, $this->binding);
    }

    /**
     * Write a field element.
     *
     * @param array $options
     * @return Block
     */
    public function render($options = []) : Block
    {
        /*
            'image' remains incomplete...
        */
        $result = new Block();
        $presentation = $this->binding->getDataProperty()->getPresentation();
        $type = $presentation->getType();
        switch ($type) {
            case 'checkbox':
            case 'radio':
                $fieldHandler = $this->getFieldHandler('checkbox');
                break;

            default:
                $fieldHandler = $this->getFieldHandler($type);
                break;
        }

        $options['confirm'] = false;
        $repeater = true;
        while ($repeater) {
            $block = $fieldHandler->render($options);
            // Check to see if we need to generate a confirm field, and
            // haven't already done so...
            if (
                in_array($type, self::$inputConfirmable)
                && $presentation->getConfirm()
                && $this->engine->getAccess($options) === 'write'
                && !$options['confirm']
            ) {
                $options['confirm'] = true;
            } else {
                $repeater = false;
            }
            $result->merge($block);
        }

        $result->merge($this->engine->renderTriggers($this->binding));

        return $result;
    }

    /**
     * Set and cache the handler class for a field type.
     *
     * @param string $type
     * @param string $className
     * @return $this
     */
    public function setFieldHandler(string $type, string $className)
    {
        $engineClass = \get_class($this->engine);
        $classType = Str::ucfirst($type);

        if (!isset(self::$handlerCache[$engineClass])) {
            self::$handlerCache[$engineClass] = [];
        }
        self::$handlerCache[$engineClass][$classType] = $className;

        return $this;
    }

}
