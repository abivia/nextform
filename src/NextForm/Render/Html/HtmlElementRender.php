<?php

declare(strict_types=1);

/**
 *
 */
namespace Abivia\NextForm\Render\Html;

use Abivia\NextForm\Render\Block;

class HtmlElementRender extends AbstractElementRender
{

    /**
     * Write a HTML element.
     * @param array $options
     * @return \Abivia\NextForm\Render\Block
     */
    public function render($options = [])
    {
        $block = new Block();

        // There's no way to hide this element so if all we have is hidden access, skip it.
        $access = $this->engine->getAccess($options);
        if ($access === 'hide' || $access === 'none') {
            return $block;
        }

        $block->appendBody($this->binding->getElement()->getValue());

        return $block;
    }

}
