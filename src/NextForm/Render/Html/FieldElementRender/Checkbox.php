<?php

declare(strict_types=1);

/**
 *
 */
namespace Abivia\NextForm\Render\Html\FieldElementRender;

use Abivia\NextForm\Data\Labels;
use Abivia\NextForm\Render\Attributes;
use Abivia\NextForm\Render\Block;

class Checkbox extends AbstractFieldElement
{
    /**
     * Attributes for the main rendered element
     * @var Attributes
     */
    protected $attrs;

    /**
     * Generate a checkbox list.
     *
     * @param string $type
     * @param Attributes $attrs
     */
    protected function checkList()
    {
        $attrs = $this->attrs->copy();
        $type = $attrs->get('type');
        $baseId = $this->binding->getId();
        $select = $this->binding->getValue();
        if ($select === null) {
            $select = $this->binding->getElement()->getDefault();
        }
        $block = new Block();
        foreach ($this->binding->getList(true) as $optId => $radio) {
            $optAttrs = $attrs->copy();
            $id = $baseId . '_opt' . $optId;
            $optAttrs->set('id', $id);
            $value = $radio->getValue();
            if ($this->access !== 'mask') {
                $optAttrs->set('value', $value);
            }
            if ($this->access === 'mask' || $this->access === 'view') {
                $optAttrs->flag('disabled', true);
                $optAttrs->flag('readonly', true);
            } else {
                $optAttrs->flag('disabled', !$radio->getEnabled());
            }
            $optAttrs->flag('autofocus', $radio->getAutofocus());
            if (
                $type === 'checkbox'
                && is_array($select) && in_array($value, $select)
            ) {
                $optAttrs->flag('checked');
                $checked = true;
            } elseif ($value === $select) {
                $optAttrs->flag('checked');
                $checked = true;
            } else {
                $optAttrs->flag('checked', false);
                $checked = false;
            }
            if ($this->access === 'mask') {
                $optAttrs->flag('checked', false);
                $checked = false;
            }
            $optAttrs->setIfNotNull('data-nf-name', $radio->getName());
            $optAttrs->setIfNotEmpty('*data-nf-group', $radio->getGroups());
            $optAttrs->setIfNotNull('*data-nf-sidecar', $radio->getSidecar());
            if ($checked) {
                $optAttrs->flag('checked');
            } else {
                $optAttrs->flag('checked', false);
            }

            $tempLabels = Labels::build();
            $tempLabels->set(
                'inner',
                $this->access === 'mask' ? $this->labels->get('mask')
                    : $radio->getLabel()
            );
            $block->appendBody(
                "<div>\n"
                . $this->engine->writeTag('input', $optAttrs) . "\n"
                . $this->engine->writeLabel(
                    'label',
                    $tempLabels,
                    'inner',
                    new Attributes('!for',  $id),
                    ['break' => true]
                )
                . "</div>\n"
            );
        }

        return $block;
    }

    /**
     * Get common attributes for the input element.
     *
     * @return Attributes
     */
    protected function inputAttributes() : Attributes
    {
        $attrs = parent::inputAttributes();
        $data = $this->binding->getDataProperty();
        $type = $this->inputType;
        $attrs->set('type', $type);
        $attrs->set('name', $this->binding->getNameOnForm());
        $attrs->setIfNotNull('*data-nf-sidecar', $data->getPopulation()->getSidecar());
        if ($this->access === 'mask' || $this->access === 'view') {
            $attrs->flag('disabled');
            $attrs->flag('readonly');
        }
        return $attrs;
    }

    /**
     * Generate the input and any associated labels, inside a wrapping div.
     *
     * @param Attributes $attrs
     * @return Block
     */
    protected function inputGroup(Attributes $attrs) : Block
    {
        $this->attrs = $attrs;
        $baseId = $this->binding->getId();

        // Set attributes for the input
        if ($this->access === 'mask' || $this->access === 'view') {
            $attrs->flag('disabled', true);
            $attrs->flag('readonly', true);
        } else {
            $attrs->flag(
                'readonly', $this->binding->getElement()->getReadonly()
            );
        }
        $list = $this->binding->getList(true);
//        $attrs->setIfNotNull('*data-nf-sidecar', $data->getPopulation()->getSidecar());
//        $attrs->set('name', $binding->getNameOnForm());

        // Start generating output
        $block = $this->engine->writeElement(
            'div', [
                'attributes' => $this->engine->groupAttributes($this->binding)
            ]
        );

        // Write the heading
        $block->appendBody($this->engine->writeLabel(
            'div',
            $this->labels,
            ['heading' => 'headingAttributes'],
            null,
            ['break' => true]
        ));
        $block->merge($this->engine->writeElement(
            'div', ['show' => 'inputWrapperAttributes'])
        );

        // Enclose a single element with a span, a list with a div
        $bracketTag = empty($list) ? 'span' : 'div';
        $block->appendBody($this->engine->writeLabel(
            $bracketTag,
            $this->labels,
            'before',
            null,
            ['break' => !empty($list)]
        ));
        if (empty($list)) {
            $attrs->set('id', $baseId);
            if ($this->access === 'mask') {
                $this->labels->set('inner', $this->labels->get('mask'));
            } else {
                $value = $this->binding->getValue();
                if ($value !== null) {
                    $attrs->set('value', $value);
                    if ($value === $this->binding->getElement()->getDefault()) {
                        $attrs->flag('checked');
                    }
                }
            }
            $block->appendLine($this->engine->writeTag('input', $attrs));
            $block->appendBody($this->engine->writeLabel(
                'label',
                $this->labels,
                'inner',
                new Attributes('!for', $baseId),
                ['break' => true]
            ));
        } else {
            $block->merge($this->checkList());
        }
        $block->appendBody($this->engine->writeLabel(
            $bracketTag,
            $this->labels,
            'after',
            null,
            ['break' => !empty($list)]
        ));
        $block->close();
        return $block;
    }

    protected function multiple() : Block {
        return new Block();
    }

    /**
     * Render the element.
     *
     * @param array $options
     * @return Block
     */
    public function render($options = []) : Block
    {
        //  appearance = default|button|toggle (can't be multiple)|no-label
        //  layout = inline|vertical
        //  form.layout = horizontal|vertical|inline

        $this->access = $this->engine->getAccess($options);
        if ($this->access === 'hide') {

            // No write/view permissions, the field is hidden,
            // we don't need labels, etc.
            $block = $this->engine->elementHiddenList($this->binding);
            return $block;
        }

        $data = $this->binding->getDataProperty();
        $this->inputType = $data->getPresentation()->getType();
        $this->element = $this->binding->getElement();

        // Push and update the show context
        $show = $this->element->getShow();
        if ($show !== '') {
            $this->engine->pushContext();
            $this->engine->show()->set($show, 'check');
        }

        $attrs = $this->inputAttributes();

        $block = $this->inputGroup($attrs);

        $block->close();
        $block->merge($this->engine->epilog());

        // Restore show context and return.
        if ($show !== '') {
            $this->engine->popContext();
        }

        return $block;
    }

    /**
     * Generate any field grouping.
     *
     * @return Block
     */
    protected function renderContainer() : Block
    {
        // We can see or change the data. Create a form group.
        $block = $this->engine->writeElement(
            'div', [
                'attributes' => $this->engine->groupAttributes(
                    $this->binding,
                    ['id' => $this->binding->getId() . $this->confirmSuffix]
                ),
                'show' => 'formGroupAttributes'
            ]
        );
        return $block;
    }

    protected function single() : Block {
        return new Block();
    }

}
