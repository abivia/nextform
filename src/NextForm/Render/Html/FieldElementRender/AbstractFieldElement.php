<?php

declare(strict_types=1);

/**
 *
 */
namespace Abivia\NextForm\Render\Html\FieldElementRender;

use Abivia\NextForm\Contracts\RenderInterface;
use Abivia\NextForm\Data\Labels;
use Abivia\NextForm\Form\Binding\FieldBinding;
use Abivia\NextForm\Form\Element\Element;
use Abivia\NextForm\Render\Attributes;
use Abivia\NextForm\Render\Html\FieldElementRender as FieldElementRenderBase;

abstract class AbstractFieldElement
{
    /**
     * The binding's access setting.
     *
     * @var string
     */
    protected $access;

    /**
     * The binding we're rendering.
     *
     * @var FieldBinding
     */
    protected $binding;

    /**
     * The field element extracted from the binding.
     *
     * @var Element
     */
    protected $element;

    /**
     * The rendering engine.
     *
     * @var RenderInterface
     */
    protected $engine;

    /**
     * @var FieldElementRenderBase
     */
    protected $field;

    /**
     *
     * @var Labels
     */
    protected $labels;

    public function __construct(
        FieldElementRenderBase $field,
        RenderInterface $engine,
        FieldBinding $binding
    ) {
        $this->field = $field;
        $this->engine = $engine;
        $this->binding = $binding;

        // Get the non-confirm version of the labels as a convenience.
        $this->labels = $this->binding->getLabels(true)->copy();
    }

    /**
     * Get common attributes for the input element.
     *
     * @return Attributes
     */
    protected function inputAttributes() : Attributes
    {
        $attrs = new Attributes();

        // Apply field width settings
        $this->engine->show()->spans(
            'field', 'width',
            function ($span) use ($attrs) {
                $attrs->itemAppend('class', "col-{$span['class']}");
            }
        );
        $valid = $this->binding->getValid();
        if ($valid !== null) {
            $key = $valid ? 'valid' : 'invalid';
            $attrs->itemAppend('class', $this->engine->show()->get('form', $key));
        }
        $attrs->setIfNotEmpty('autofocus', $this->binding->getAutofocus());

        return $attrs;
    }

}
