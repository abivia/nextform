<?php

declare(strict_types=1);

/**
 *
 */
namespace Abivia\NextForm\Render\Html\FieldElementRender;

use Abivia\NextForm\Render\Attributes;
use Abivia\NextForm\Render\Block;

class File extends AbstractFieldElement
{
    protected $access;
    protected $inputType;

    /**
     * Get common attributes for the input element.
     *
     * @return Attributes
     */
    protected function inputAttributes() : Attributes
    {
        $attrs = parent::inputAttributes();
        $attrs->set('id', $this->binding->getId());
        $attrs->set('name', $this->binding->getNameOnForm());
        $attrs->set('type', $this->inputType);
        if ($this->access === 'mask' || $this->access === 'view') {
            $attrs->flag('readonly', true);
            $attrs->flag('disabled', true);
        } else {
            $attrs->flag('readonly', $this->element->getReadonly());
            $attrs->flag('disabled', !$this->element->getEnabled());
        }
        $attrs->flag('autofocus', $this->element->getAutofocus());
        $attrs->setIfNotNull(
            '*data-nf-sidecar',
            $this->binding->getDataProperty()->getPopulation()->getSidecar()
        );

        // If there's an inner label, use it as a placeholder
        $attrs->setIfNotNull('placeholder', $this->labels->get('inner'));

        return $attrs;
    }

    /**
     * Generate the input element and any wrapping/supporting code.
     */
    protected function inputGroup(
        Attributes $attrs
    ) : Block {
        // Start the input group
        $input = $this->engine->writeElement(
            'div',
            ['show' => 'inputWrapperAttributes']
        );

        $input->appendBody(
            $this->engine->writeLabel('span', $this->labels, 'before')
            . $this->engine->writeTag('input', $attrs) . "\n"
            . $this->engine->writeLabel('span', $this->labels, 'after')
        );

        // Generate help text, if any
        if ($this->labels->has('help')) {
            $helpAttrs = new Attributes();
            $helpAttrs->set('id', $attrs->get('aria-describedby'));
            $helpAttrs->itemAppend('class', 'form-text text-muted');
            $input->appendLine($this->engine->writeTag(
                'small',
                $helpAttrs,
                $this->labels->get('help')
            ));
        }

        return $input;
    }

    /**
     * Render the element.
     *
     * @param array $options
     * @return Block
     */
    public function render($options = []) : Block
    {
        $this->access = $this->engine->getAccess($options);
        $data = $this->binding->getDataProperty();
        $this->inputType = $data->getPresentation()->getType();
        if ($this->access === 'mask') {
            $value = $this->labels->get('mask');
        } else {
            $value = $this->binding->getValue();
        }
        if ($this->access === 'hide' || $this->inputType === 'hidden') {

            // No write/view permissions, the field is hidden,
            // we don't need labels, etc.
            $block = $this->engine->elementHidden(
                $this->binding, $value
            );
            return $block;
        }

        $this->element = $this->binding->getElement();

        // Push and update the show context
        $show = $this->element->getShow();
        if ($show !== '') {
            $this->engine->pushContext();
            $this->engine->show()->set($show, $this->inputType);
        }

        // Get attributes for the input element
        if ($this->access === 'mask' || $this->access === 'view') {
            $this->inputType = 'text';
        }
        $attrs = $this->inputAttributes();

        if ($this->access === 'write') {
            // Write access: Add in any validation
            $attrs->addValidation($this->inputType, $data->getValidation());
        }
        $attrs->set('type', $this->inputType);

        // We can see or change the data
        $attrs->setIfNotNull(
            'value', is_array($value) ? implode(',', $value) : $value
        );

        // Generate any field grouping.
        $block = $this->renderContainer();

        // Write the heading
        $block->appendBody($this->engine->writeLabel(
            'label',
            $this->labels,
            ['heading' => 'headingAttributes'],
            new Attributes('!for', $this->binding->getId()),
            ['break' => true]
        ));

        // Generate the actual input element
        $input = $this->inputGroup($attrs);

        $block->merge($input);
        $block->close();
        $block->merge($this->engine->epilog());

        // Restore show context and return.
        if ($show !== '') {
            $this->engine->popContext();
        }

        return $block;
    }

    /**
     * Generate any field grouping.
     *
     * @return Block
     */
    protected function renderContainer() : Block
    {
        // We can see or change the data. Create a form group.
        $block = $this->engine->writeElement(
            'div', [
                'attributes' => $this->engine->groupAttributes($this->binding),
                'show' => 'formGroupAttributes'
            ]
        );
        return $block;
    }

}
