<?php

declare(strict_types=1);

/**
 *
 */
namespace Abivia\NextForm\Render\Html\FieldElementRender;

use Abivia\NextForm\Data\Labels;
use Abivia\NextForm\Render\Attributes;
use Abivia\NextForm\Render\Block;

class Html extends AbstractFieldElement
{
    protected $access;

    /**
     * Get common attributes for the input element.
     *
     * @return Attributes
     */
    protected function inputAttributes() : Attributes
    {
        $attrs = parent::inputAttributes();
        $attrs->set('id', $this->binding->getId());
        $attrs->set('name', $this->binding->getNameOnForm());
        $attrs->setIfNotNull(
            '*data-nf-sidecar',
            $this->binding->getDataProperty()->getPopulation()->getSidecar()
        );

        return $attrs;
    }

    /**
     * Generate the input element and any wrapping/supporting code.
     *
     * @param Attributes $attrs
     * @param string $text
     * @return Block
     */
    protected function inputGroup(
        Attributes $attrs,
        $text
    ) : Block {
        $input = new Block();
        // Insert the HTML
        $input->appendLine($text);

        return $input;
    }

    /**
     * Render the element.
     *
     * @param array $options
     * @return Block
     */
    public function render($options = []) : Block
    {
        $this->access = $this->engine->getAccess($options);
        $data = $this->binding->getDataProperty();
        $value = $this->binding->getValue();
        if ($this->access === 'hide') {
            // No write/view permissions, the field is hidden,
            // we don't need labels, etc.
            return Block::fromString();
        }

        if ($this->access === 'mask') {
            $value = $this->labels->get('mask');
        }

        $this->element = $this->binding->getElement();

        // Push and update the show context
        $show = $this->element->getShow();
        if ($show !== '') {
            $this->engine->pushContext();
            $this->engine->show()->set($show, 'textarea');
        }

        // Get attributes for the input element
        $attrs = $this->inputAttributes();

        // We can see or change the data
        if ($value === null) {
            $value = '';
        }

        // Generate any field grouping.
        $block = $this->renderContainer();

        // Write the heading
        $block->appendBody($this->engine->writeLabel(
            'label',
            $this->labels,
            ['heading' => 'headingAttributes'],
            new Attributes('!for', $this->binding->getId()),
            ['break' => true]
        ));

        // Generate the actual input element.
        $input = $this->inputGroup($attrs, $value);

        $block->merge($input);
        $block->close();
        //$block->merge($this->engine->epilog());

        // Restore show context and return.
        if ($show !== '') {
            $this->engine->popContext();
        }

        return $block;
    }

    /**
     * Generate any field grouping.
     *
     * @return Block
     */
    protected function renderContainer() : Block
    {
        // We can see or change the data. Create a form group.
        $block = $this->engine->writeElement(
            'div', [
                'attributes' => $this->engine->groupAttributes($this->binding),
                'show' => 'formGroupAttributes'
            ]
        );
        return $block;
    }

}
