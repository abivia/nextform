<?php

declare(strict_types=1);

/**
 *
 */
namespace Abivia\NextForm\Render\Html\FieldElementRender;

use Abivia\NextForm\NextForm;
use Abivia\NextForm\Render\Attributes;
use Abivia\NextForm\Render\Block;

class Common extends AbstractFieldElement
{
    protected $access;
    protected $confirmSuffix;
    protected $inputType;

    /**
     * Get common attributes for the input element.
     *
     * @return Attributes
     */
    protected function inputAttributes() : Attributes
    {
        $attrs = parent::inputAttributes();
        $attrs->set('id', $this->binding->getId() . $this->confirmSuffix);
        $attrs->set('name', $this->binding->getNameOnForm() . $this->confirmSuffix);
        $attrs->set('type', $this->inputType);
        $attrs->flag(
            'readonly',
            $this->element->getReadonly() || $this->access === 'view'
        );
        $attrs->flag('disabled', !$this->element->getEnabled());
        $attrs->flag('autofocus', $this->element->getAutofocus());
        $value = $this->binding->getValue();
        if ($this->access === 'mask') {
            $attrs->flag('disabled', true);
            $attrs->flag('readonly', true);
            $attrs->set('value', $this->labels->get('mask'));
        } elseif ($value === null) {
            $attrs->setIfNotNull('value', $this->element->getDefault());
        } else {
            $attrs->set('value', $value);
        }
        $attrs->setIfNotNull(
            '*data-nf-sidecar',
            $this->binding->getDataProperty()->getPopulation()->getSidecar()
        );

        // If there's an inner label, use it as a placeholder
        $attrs->setIfNotNull('placeholder', $this->labels->get('inner'));

        return $attrs;
    }

    /**
     * Generate the input and any associated labels, inside a wrapping div.
     *
     * @param Attributes $attrs
     * @return Block
     */
    protected function inputGroup(Attributes $attrs) : Block
    {
        $input = $this->engine->writeElement(
            'div', ['show' => 'inputWrapperAttributes']
        );
        $input->appendBody($this->engine->writeLabel(
            'span', $this->labels, 'before'
        ));
        // Generate the input element
        $input->appendLine(
            $this->engine->writeTag('input', $attrs)
            . $this->engine->writeLabel('span', $this->labels, 'after')
        );
        return $input;
    }

    /**
     * Render the element.
     *
     * @param array $options
     * @return Block
     */
    public function render($options = []) : Block
    {
        $this->access = $this->engine->getAccess($options);
        $confirm = $options['confirm'];
        $this->confirmSuffix = $confirm ? NextForm::$confirmLabel : '';
        $data = $this->binding->getDataProperty();
        $this->inputType = $data->getPresentation()->getType();
        if ($this->access === 'hide' || $this->inputType === 'hidden') {

            // No write/view permissions, the field is hidden,
            // we don't need labels, etc.
            if ($confirm) {
                // No need to confirm a hidden element.
                $block = new Block();
            } else {
                $block = $this->engine->elementHidden(
                    $this->binding, $this->binding->getValue()
                );
            }
            return $block;
        }

        $this->element = $this->binding->getElement();

        // Push and update the show context
        $show = $this->element->getShow();
        if ($show !== '') {
            $this->engine->pushContext();
            $this->engine->show()->set($show, $this->inputType);
        }

        // Convert view-only range elements to text
        if ($this->access === 'view' && $this->inputType === 'range') {
            $this->inputType = 'text';
        }
        // Convert masked formatted elements to text
        if (
            $this->access === 'mask'
            && in_array(
                $this->inputType,
                ['color', 'date', 'datetime-local', 'month', 'number',
                'password', 'range', 'tel', 'time', 'url', 'week']
            )
        ) {
            $this->inputType = 'text';
        }

        // Generate any field grouping.
        $block = $this->renderContainer();

        // If we're generating a confirmation, use labels with the confirm texts
        if ($confirm) {
            $this->labels = $this->labels->forConfirm();
        }

        // Get attributes for the input element
        $attrs = $this->inputAttributes($this->labels);

        // Write the heading
        $block->appendBody($this->engine->writeLabel(
            'label',
            $this->labels,
            ['heading' => 'headingAttributes'],
            new Attributes('!for', $attrs->get('id')),
            ['break' => true]
        ));

        // Render the data list if there is one
        if ($this->access !== 'mask') {
            $block->merge(
                $this->field->dataList($attrs, $this->inputType, $options)
            );
        }
        if ($this->access === 'write') {
            // Write access: Add in any validation
            $attrs->addValidation($this->inputType, $data->getValidation());
        }

        // Generate the actual input element
        $input = $this->inputGroup($attrs);

        $block->merge($input);
        $block->close();
        $block->merge($this->engine->epilog());

        // Restore show context and return.
        if ($show !== '') {
            $this->engine->popContext();
        }

        return $block;
    }

    /**
     * Generate any field grouping.
     *
     * @return Block
     */
    protected function renderContainer() : Block
    {
        // We can see or change the data. Create a form group.
        $block = $this->engine->writeElement(
            'div', [
                'attributes' => $this->engine->groupAttributes(
                    $this->binding,
                    ['id' => $this->binding->getId() . $this->confirmSuffix]
                ),
                'show' => 'formGroupAttributes'
            ]
        );
        return $block;
    }

}
