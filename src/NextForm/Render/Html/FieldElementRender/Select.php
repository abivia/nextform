<?php

declare(strict_types=1);

/**
 *
 */
namespace Abivia\NextForm\Render\Html\FieldElementRender;

use Abivia\NextForm\Data\Labels;
use Abivia\NextForm\Render\Attributes;
use Abivia\NextForm\Render\Block;

class Select extends AbstractFieldElement
{
    protected $access;
    protected $dataProperty;
    protected $multiple;
    protected $value;

    /**
     * Get common attributes for the input element.
     *
     * @return Attributes
     */
    protected function inputAttributes() : Attributes
    {
        $attrs = parent::inputAttributes();
        $attrs->set('id', $this->binding->getId());
        $attrs->set('name', $this->binding->getNameOnForm());

        $attrs->flag('disabled', !$this->element->getEnabled());
        $attrs->flag('autofocus', $this->element->getAutofocus());
        $attrs->setIfNotNull(
            '*data-nf-sidecar',
            $this->binding->getDataProperty()->getPopulation()->getSidecar()
        );

        if (($rows = $this->dataProperty->getPresentation()->getRows()) !== null) {
            $attrs->set('size', $rows);
        }

        return $attrs;
    }

    /**
     * Generate the input element and any wrapping/supporting code.
     */
    protected function inputGroup(
        Attributes $attrs
    ) : Block {
        $select = $this->engine->writeElement(
            'div', ['show' => 'inputWrapperAttributes']
        );
        $select->appendBody($this->engine->writeLabel(
            'div', $this->labels, 'before', null, ['break' => true]
        ));

        if ($this->access === 'mask') {
            $select->merge($this->renderMask($attrs));
        } elseif ($this->access === 'view') {
            $select->merge($this->renderView($attrs));
        } else {
            // Write access: Add in any validation
            $attrs->addValidation('select', $this->dataProperty->getValidation());

            $select->merge(
                $this->engine->writeElement('select', ['attributes' => $attrs])
            );

            // Add the options
            $select->merge(
                $this->renderOptions(
                    $this->binding->getList(true), $this->value
                )
            );
        }

        $select->appendBody($this->engine->writeLabel(
            'div', $this->labels, 'after', null, ['break' => true]
        ));

        // Generate help text, if any
        if ($this->labels->has('help')) {
            $helpAttrs = new Attributes();
            $helpAttrs->set('id', $attrs->get('aria-describedby'));
            $helpAttrs->itemAppend('class', 'form-text text-muted');
            $select->appendLine($this->engine->writeTag(
                'small',
                $helpAttrs,
                $this->labels->get('help')
            ));
        }

        return $select;
    }

    /**
     * Render the element.
     *
     * @param array $options
     * @return Block
     */
    public function render($options = []) : Block
    {
        $this->access = $this->engine->getAccess($options);
        $this->dataProperty = $this->binding->getDataProperty();
        $this->multiple = $this->dataProperty->getValidation()->get('multiple');
        $this->element = $this->binding->getElement();

        // If there's no value set, see if there's a default
        $this->value = $this->binding->getValue();
        if ($this->value === null) {
            $this->value = $this->element->getDefault();
        }
        if (!is_array($this->value)) {
            $this->value = [$this->value];
        }
        if ($this->access === 'hide') {

            // No write/view permissions, the field is hidden,
            // we don't need labels, etc.
            $block = $this->engine->elementHidden(
                $this->binding, $this->binding->getValue()
            );
            return $block;
        }

        // Push and update the show context
        $show = $this->element->getShow();
        if ($show !== '') {
            $this->engine->pushContext();
            $this->engine->show()->set($show, 'select');
        }

        // Generate any field grouping.
        $block = $this->renderContainer();

        // Get attributes for the input element
        $attrs = $this->inputAttributes();

        $headAttrs = new Attributes();
        if ($this->access !== 'view') {
            $headAttrs->set('!for', $attrs->get('name'));
        }

        // Write the heading
        $block->appendBody($this->engine->writeLabel(
            'label',
            $this->labels,
            ['heading' => 'headingAttributes'],
            $headAttrs,
            ['break' => true]
        ));

        $select = $this->inputGroup($attrs);

        $block->merge($select);
        $block->close();
        $block->merge($this->engine->epilog());

        // Restore show context and return.
        if ($show !== '') {
            $this->engine->popContext();
        }

        return $block;
    }

    /**
     * Generate any field grouping.
     *
     * @return Block
     */
    protected function renderContainer() : Block
    {
        // We can see or change the data. Create a form group.
        $block = $this->engine->writeElement(
            'div', [
                'attributes' => $this->engine->groupAttributes(
                    $this->binding
                ),
                'show' => 'formGroupAttributes'
            ]
        );
        return $block;
    }

    /**
     * Render a masked version of the element.
     *
     * @param Attributes $attrs
     * @return Block
     */
    protected function renderMask(Attributes $attrs)
    {
        $list = $this->binding->getFlatList(true);
        // render as hidden with text
        $attrs->set('type', 'hidden');

        $input = new Block();
        $attrs->set('id', $this->binding->getId());
        $input->appendLine($this->engine->writeTag('input', $attrs));
        $input->appendLine($this->engine->writeTag(
            'span', null, $this->labels->get('mask')
        ));

        return $input;
    }

    protected function renderOption($option, $value)
    {
        $block = new Block();
        $attrs = new Attributes();
        $attrs->set('value', $option->getValue());
        $attrs->setIfNotNull('data-nf-name', $option->getName());
        $attrs->setIfNotEmpty('*data-nf-group', $option->getGroups());
        $attrs->setIfNotNull('*data-nf-sidecar', $option->getSidecar());
        if (in_array($attrs->get('value'), $value)) {
            $attrs->flag('selected');
        }
        $block->appendLine($this->engine->writeTag(
            'option', $attrs, $option->getLabel()
        ));
        return $block;
    }

    protected function renderOptions($list, $value) {
        $block = new Block();
        foreach ($list as $option) {
            if ($option->isNested()) {
                $attrs = new Attributes();
                $attrs->set('label', $option->getLabel());
                $attrs->setIfNotNull('data-nf-name', $option->getName());
                $attrs->setIfNotEmpty('*data-nf-group', $option->getGroups());
                $attrs->setIfNotNull('*data-nf-sidecar', $option->getSidecar());
                $block->appendLine($this->engine->writeTag('optgroup', $attrs));
                $block->merge($this->renderOptions($option->getList(), $value));
                $block->appendLine('</optgroup>');
            } else {
                $block->merge($this->renderOption($option, $value));
            }
        }
        return $block;
    }

    /**
     * Render a view-only version of the element.
     *
     * @param Attributes $attrs
     * @return Block
     */
    protected function renderView(Attributes $attrs)
    {
        $list = $this->binding->getFlatList(true);
        // render as hidden with text
        $attrs->set('type', 'hidden');

        $baseId = $this->binding->getId();
        $value = $this->binding->getValue();
        $input = new Block();
        if ($this->multiple) {
            // step through each possible value, output matches
            if (!is_array($value)) {
                $value = [$value];
            }
            $optId = 0;
            foreach ($list as $option) {
                $slot = array_search($option->getValue(), $value);
                if ($slot !== false) {
                    $id = $baseId . '_opt' . $optId;
                    $attrs->set('id', $id);
                    $attrs->set('value', $value[$slot]);
                    $input->appendLine(
                        $this->engine->writeTag('input', $attrs) . "\n"
                        . $this->engine->writeTag(
                            'span', null, $option->getLabel()
                        ) . '<br/>'
                    );
                    ++$optId;
                }
            }
        } else {
            $attrs->set('id', $baseId);
            $attrs->set('value', $value);
            $input->appendLine($this->engine->writeTag('input', $attrs));
            foreach ($list as $option) {
                if ($value === $option->getValue()) {
                    $input->appendLine(
                        $this->engine->writeTag('span')
                        . $option->getLabel() . '</span>'
                    );
                }
            }
        }
        return $input;
    }

}
