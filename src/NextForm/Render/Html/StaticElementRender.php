<?php

declare(strict_types=1);

namespace Abivia\NextForm\Render\Html;

use Abivia\NextForm\NextForm;
use Abivia\NextForm\Render\Attributes;
use Abivia\NextForm\Render\Block;

class StaticElementRender extends AbstractElementRender
{

    /**
     * Write a HTML element.
     *
     * @param array $options
     * @return \Abivia\NextForm\Render\Block
     */
    public function render($options = []) : Block
    {
        $block = new Block();

        // There's no way to hide this element so if all we have is hidden access, skip it.
        $access = $this->engine->getAccess($options);
        if ($access === 'hide' || $access === 'none') {
            return $block;
        }

        // Push and update the show context
        $element = $this->binding->getElement();
        $show = $element->getShow();
        if ($show !== '') {
            $this->engine->pushContext();
            $this->engine->show()->set($show, 'html');
        }

        // We can see or change the data. Create a form group.
        $block = $this->engine->writeElement(
            'div', [
                'attributes' => $this->engine->groupAttributes($this->binding),
                'show' => 'formGroupAttributes'
            ]
        );

        // Write a heading if there is one
        $block->appendBody($this->engine->writeLabel(
            'div',
            $this->binding->getLabels(true),
            ['heading' => 'headingAttributes'],
            null,
            ['break' => true]
        ));
        $block->merge($this->engine->writeElement(
            'div', ['show' => 'inputWrapperAttributes'])
        );

        $attrs = new Attributes('id', $this->binding->getId());
        $block->merge($this->engine->writeElement(
            'div', ['attributes' => $attrs])
        );
        // Escape the value if it's not listed as HTML
        if ($access === 'mask') {
            $value = NextForm::MASK_TEXT_DEFAULT . "\n";
        } else {
            $value = $this->binding->getValue() . "\n";
            $value = $element->getHtml() ? $value : htmlspecialchars($value);
        }
        $block->appendBody($value);
        $block->close();
        $block->merge($this->engine->epilog());

        // Restore show context and return.
        if ($show !== '') {
            $this->engine->popContext();
        }

        return $block;
    }

}
