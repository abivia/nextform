<?php

declare(strict_types=1);

namespace Abivia\NextForm\Render\Html\SimpleHtml;

use Abivia\NextForm\Contracts\RenderInterface;
use Abivia\NextForm\Form\Binding\FieldBinding;
use Abivia\NextForm\Render\Block;
use Abivia\NextForm\Render\Html\Render as HtmlRender;

/**
 * A skeletal renderer that generates a very basic form.
 */
class Render extends HtmlRender implements RenderInterface
{

    /**
     * Maps element types to render methods.
     * @var array
     */
    protected static $renderMethodCache = [];

    public function __construct($options = [])
    {
        $this->show = new Show('form');

        parent::__construct($options);
        $this->initialize();
    }

    /**
     * Delimit this element from the next as required.
     *
     * @return Block
     */
    public function epilog() : Block
    {
        $block = Block::fromString(
            $this->queryContext('inCell') ? '&nbsp;' : "<br/>\n"
        );
        return $block;
    }

    /*
     * Initialize the render engine.
     */
    protected function initialize()
    {
        parent::initialize();
        // Initialize custom settings
        $this->show->set('layout:vertical');
    }

}

