<?php

declare(strict_types=1);

namespace Abivia\NextForm\Render\Html\SimpleHtml;

use Abivia\NextForm\Render\Attributes;
use Abivia\NextForm\Render\RenderException;
use Abivia\NextForm\Render\Html\Show as ShowBase;

class Show extends ShowBase
{

    /**
     * Process cell spacing options, called from do().
     *
     * @param string $scope Names the settings scope/element this applies to.
     * @param string $choice Primary option selection
     * @param array $values Array of colon-delimited settings including the initial keyword.
     */
    public function doCellspacing(
        string $scope,
        string $choice,
        $values = []
    ) {
        // Expecting choice to be "a" or "b".
        // For "a", one or more space delimited single digits from 0 to 5,
        // optionally prefixed with rr-
        //
        // For "b" one or more space-delimited sets of [rr-]xx-n where rr is a
        // renderer selector (sh for simpleHtml), xx is a size specifier,
        // and n is 0 to 5.
        //
        // Specifiers other than bs are ignored the result is a list of
        // classes to be used when spacing between the second and subsequent
        // elements in a cell.
        $this->checkState($scope);

        $styleList = ['display' => 'inline-block', 'padding' => '0.5rem'];
        if ($choice === 'a') {
            $styleList['padding-left'] = '1rem';
        } else {
            foreach ($values as $value) {
                \preg_match(
                    '/(?<prefix>[a-z][a-z0-9]-)?(?<weight>[0-5])/',
                    $value, $match
                );
                if ($match['prefix'] !== '' && $match['prefix'] !== 'sh-') {
                    continue;
                }
                $weight = (int) $match['weight'];
            }
            $styleList['padding-left'] = round(2 * $weight / 5, 2) . 'rem';
        }
        if (!empty($styleList)) {
            $this->state[$scope]['cellspacing']
                = new Attributes('style', $styleList);
        }
    }

    /**
     * Process layout options, called from do().
     *
     * @param string $scope Names the settings scope/element this applies to.
     * @param string $choice Primary option selection
     * @param array $values Array of colon-delimited settings including the initial keyword.
     */
    public function doLayout(string $scope, string $choice, $values = [])
    {
        $this->checkState($scope);
        // Clear out anything that might have been set by previous commands.
        unset($this->state[$scope]['cellElementAttributes']);
        unset($this->state[$scope]['headingAttributes']);
        unset($this->state[$scope]['inputWrapperAttributes']);
        $this->state[$scope]['layout'] = $choice;
        if ($choice === 'horizontal') {
            $this->doLayoutAnyHorizontal($scope, $values);
        } elseif ($choice === 'vertical') {
            $this->doLayoutAnyVertical($scope, $values);
        }
    }

    /**
     * Process horizontal layout settings for any scope.
     *
     * @param string $scope Names the settings scope/element this applies to.
     * @param array $values Array of colon-delimited settings including the initial keyword.
     * @throws \RuntimeException
     */
    public function doLayoutAnyHorizontal(string $scope, $values)
    {
        // possible values for arguments:
        // h            - We get to decide
        // h:nxx        - First column width in CSS units
        // h:nxx:mxx    - CSS units for headers / input elements
        // h:n:m:t      - ratio of headers to inputs over space t. If no t, t=n+m
        // h:.c1        - Class for headers
        // h:.c1:.c2    - Class for headers / input elements
        $this->checkState($scope);

        $apply = &$this->state[$scope];
        switch (count($values)) {
            case 1:
                // No specification, use our default
                $apply['headingAttributes'] = new Attributes(
                    'style',
                    [
                        'display' => 'inline-block',
                        'vertical-align' => 'top',
                        'width' => '25%'
                    ]
                );
                $apply['inputWrapperAttributes'] = new Attributes(
                    'style',
                    [
                        'display' => 'inline-block',
                        'vertical-align' => 'top',
                        'width' => '75%'
                    ]
                );
                break;
            case 2:
                if ($values[1][0] === '.') {
                    // Single class specification
                    $apply['headingAttributes'] = new Attributes('class', [substr($values[1], 1)]);
                } else {
                    // Single CSS units
                    $apply['headingAttributes'] = new Attributes(
                        'style',
                        [
                            'display' => 'inline-block',
                            'vertical-align' => 'top',
                            'width' => $values[1]
                        ]
                    );
                }
                break;
            default:
                if ($values[1][0] === '.') {
                    // Dual class specification
                    $apply['headingAttributes'] = new Attributes('class', [substr($values[1], 1)]);
                    $apply['inputWrapperAttributes'] = new Attributes('class', [substr($values[2], 1)]);
                } elseif (preg_match('/^[+\-]?[0-9](\.[0-9]*)?$/', $values[1])) {
                    // ratio
                    $part1 = (float) $values[1];
                    $part2 = (float) $values[2];
                    if (!$part1 || !$part2) {
                        throw new RenderException(
                            "Invalid ratio: {$values[1]}:{$values[2]}"
                        );
                    }
                    $sum = $values[3] ?? ($part1 + $part2);
                    $apply['headingAttributes'] = new Attributes(
                        'style',
                        [
                            'display' => 'inline-block',
                            'vertical-align' => 'top',
                            'width' => round(100.0 * $part1 / $sum, 3) . '%'
                        ]
                    );
                    $apply['inputWrapperAttributes'] = new Attributes(
                        'style',
                        [
                            'display' => 'inline-block',
                            'vertical-align' => 'top',
                            'width' => round(100.0 * $part2 / $sum, 3) . '%'
                        ]
                    );
                } else {
                    // Dual CSS units
                    $apply['headingAttributes'] = new Attributes(
                        'style',
                        [
                            'display' => 'inline-block',
                            'vertical-align' => 'top',
                            'width' => $values[1]
                        ]
                    );
                    $apply['inputWrapperAttributes'] = new Attributes(
                        'style',
                        [
                            'display' => 'inline-block',
                            'vertical-align' => 'top',
                            'width' => $values[2]
                        ]
                    );
                }
                break;

        }
        if (isset($apply['inputWrapperAttributes'])) {
            $apply['cellElementAttributes'] = $apply['inputWrapperAttributes'];
        }
    }

    /**
     * Process vertical layout settings for any scope.
     *
     * @param string $scope Names the settings scope/element this applies to.
     * @param array $values Array of colon-delimited settings including the initial keyword.
     * @throws \RuntimeException
     */
    public function doLayoutAnyVertical(string $scope, $values)
    {
        // possible values for arguments:
        // v            - Default, nothing to do
        // v:mxx        - CSS units for input elements
        // v:.c2        - Class for input elements
        // v:m:t        - ratio of inputs over space t.
        $this->checkState($scope);
        $apply = &$this->state[$scope];
        switch (count($values)) {
            case 1:
                // No specification, nothing to do
                break;
            case 2:
                if ($values[1][0] === '.') {
                    // Single class specification
                    $apply['inputWrapperAttributes'] = new Attributes(
                        'class', [substr($values[1], 1)]
                    );
                } else {
                    // Single CSS units
                    $apply['inputWrapperAttributes'] = new Attributes(
                        'style',
                        [
                            'display' => 'inline-block',
                            'vertical-align' => 'top',
                            'width' => $values[1],
                        ]
                    );
                }
                break;
            default:
                if (preg_match('/^[+\-]?[0-9](\.[0-9]*)?$/', $values[1])) {
                    // ratio
                    $part1 = (float) $values[1];
                    $part2 = isset($values[2]) ? (float) $values[2] : $part1;
                    if (!$part1 || !$part2) {
                        throw new RenderException(
                            'Zero is invalid in a ratio.'
                        );
                    }
                    $apply['inputWrapperAttributes'] = new Attributes(
                        'style',
                        [
                            'display' => 'inline-block',
                            'vertical-align' => 'top',
                            'width' => round(100.0 * $part1 / $part2, 3) . '%'
                        ]
                    );
                }
                break;
        }
        if (isset($apply['inputWrapperAttributes'])) {
            $apply['cellElementAttributes'] = $apply['inputWrapperAttributes'];
        }
    }

    /**
     * Filter a span list for those with no scheme or with a matching span id.
     *
     * @param array $spans Array of spans.
     *
     * @return array
     */
    public function spanFilter($spans)
    {
        $filtered = [];

        return $filtered;
    }

}