<?php

declare(strict_types=1);

namespace Abivia\NextForm\Render\Html\Tailwind\Tw2;

use Abivia\NextForm\Contracts\RenderInterface;
use Abivia\NextForm\Data\Labels;
use Abivia\NextForm\Form\Binding\FieldBinding;
use Abivia\NextForm\Render\Attributes;
use Abivia\NextForm\Render\Block;
use Abivia\NextForm\Render\Html\Tailwind\Render as BsRender;

/**
 * Render for Tw2
 */
class Render extends BsRender implements RenderInterface
{
    protected static $links = [
    ];

    protected static $scripts = [
    ];

    public function __construct($options = [])
    {
        //$this->actionRender = new ActionRender();
        $this->show = new Show('form');

        parent::__construct($options);
        $this->initialize();
        $this->useOptions($options);
    }

    protected function initialize() {
        parent::initialize();
        //self::$coreJsPath = __DIR__ . '/js/nf.js';
    }

    /**
     * Write anything that comes after the input group.
     *
     * @param Labels $labels
     * @return Block
     */
    public function inputGroupPost(?Labels $labels = null): Block
    {
        if ($labels !== null && $labels->has('after')) {
            // Write an append group for the after label
            $group = $this->writeElement(
                'div', ['attributes' => new Attributes('class', ['input-group-append'])]
            );
            // Write the after label in the append group
            $group->appendLine($this->writeLabel(
                'span',
                $labels,
                ['after' => 'inputAfter'],
                new Attributes('class', ['input-group-text'])
            ));
            $group->close();
        } else {
            $group = new Block();
        }

        return $group;
    }

    /**
     * Write anything that comes before the input group.
     *
     * @param Labels $labels
     * @return Block
     */
    public function inputGroupPre(?Labels $labels = null): Block
    {
        if (
            $labels !== null
            && ($labels->has('before') || $labels->has('after'))
        ) {
            // We have before/after elements to attach, we need to create
            // an input group.
            $input = $this->writeElement(
                'div', ['attributes' => new Attributes('class', 'input-group'), 'show' => 'inputWrapperAttributes']
            );

            if ($labels->has('before')) {
                // Write a prepend group for the before label
                $group = $this->writeElement(
                    'div', ['attributes' => new Attributes('class', ['input-group-prepend'])]
                );
                // Write the before label in the prepend group
                $group->appendLine($this->writeLabel(
                    'span',
                    $labels,
                    ['before' => 'inputBefore'],
                    new Attributes('class', ['input-group-text'])
                ));
                $group->close();
                $input->merge($group);
            }
        } else {
            $input = $this->writeElement(
                'div', ['show' => 'inputWrapperAttributes']
            );
        }

        return $input;
    }

    /**
     * This method should be reworked to support different JS frameworks...
     *
     * @param FieldBinding $binding
     * @return Block
     */
    public function renderTriggers(FieldBinding $binding) : Block
    {
        $result = new Block;
        $triggers = $binding->getElement()->getTriggers();
        if (empty($triggers)) {
            return $result;
        }
        $formId = $binding->getLinkedForm()->getId();
        $script = "$('#" . $formId . ' [name^="' . $binding->getNameOnForm(true)
            . "\"]').change(function () {\n";
        foreach ($triggers as $trigger) {
            if ($trigger->getEvent() !== 'change') {
                continue;
            }
            $value = $trigger->getValue();
            $closing = "  }\n";
            if (is_array($value)) {
                $script .= '  if (' . json_encode($value) . ".includes(this.value)) {\n";
            } elseif ($value === null) {
                // Null implies no conditions.
                $closing = '';
            } else {
                $script .= '  if (this.value === ' . json_encode($value) . ") {\n";
            }
            foreach ($trigger->getActions() as $action) {
                $script .= $this->actionRender->render($formId, $binding, $action);
            }
            $script .= $closing;
        }
        $script .= "});\n";
        $result->addScript($script);

        return $result;
    }

}

