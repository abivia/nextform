<?php

declare(strict_types=1);

/**
 *
 */
namespace Abivia\NextForm\Render\Html\Tailwind;

use Abivia\NextForm\Render\Attributes;
use Abivia\NextForm\Render\Html\Show as ShowBase;

class Show extends ShowBase
{

    public function __construct($scope)
    {
        parent::__construct($scope);
        self::$spanList[] = 'tw';
    }

    /**
     * Process cell spacing options, called from do().
     *
     * @param string $scope Names the settings scope/element this applies to.
     * @param string $choice Primary option selection
     * @param array|null $values Array of colon-delimited settings including the initial keyword.
     */
    public function doCellspacing(string $scope, string $choice, $values = [])
    {
        // Schemes other than b4 are ignored the result is a list of
        // classes to be used when spacing between the second and subsequent
        // elements in a cell.
        $this->checkState($scope);

        $classList = [];
        foreach ($values as $value) {
            $parts = self::parseSpan($value)[0];
            if (!$parts['match'] || $parts['weight'] > 5) {
                continue;
            }
            if (
                $parts['scheme'] !== null
                && !in_array($parts['scheme'], self::$spanList)
            ) {
                continue;
            }
            $classList[] = 'ml-' . $parts['class'];
        }
        if (!empty($classList)) {
            $this->state[$scope]['cellspacing']
                = new Attributes('class', $classList);
        }
    }

    /**
     * Process horizontal layout settings for any scope.
     *
     * @param string $scope Names the settings scope/element this applies to.
     * @param array $values Array of colon-delimited settings including
     *  the initial keyword.
     * @throws \RuntimeException
     */
    public function doLayoutAnyHorizontal(string $scope, $values)
    {
        // possible values for arguments:
        // h            - We get to decide
        // h:nxx        - Ignored, we decide
        // h:nxx/mxx    - Ignored, we decide
        // h:n:m:t      - ratio of headers to inputs over space t. If no t, t=n+m
        // h:.c1        - Ignored, we decide
        // h:.c1:.c2    - Class for headers / input elements
        //
        // Adjusts:
        // cellElementAttributes - Add the input column class
        // formGroupAttributes - add the row class
        //
        // Creates an attribute set for:
        // headingAttributes -- to be used for input element headings
        //
        $this->checkState($scope);
        $apply = &$this->state[$scope];
        $default = true;
        $apply['formGroupAttributes']->itemAppend('class', 'row');
        if (count($values) >= 3) {
            if ($values[1][0] === '.') {
                // Dual class specification
                $apply['headingAttributes'] = new Attributes(
                    'class',
                    [substr($values[1], 1), 'col-form-label', 'text-md-right']
                );
                $col2 = substr($values[2], 1);
                $apply['inputWrapperAttributes'] = new Attributes(
                    'class', $col2
                );
                $apply['cellElementAttributes']->itemAppend('class', $col2);
                $default = false;
            } elseif (preg_match('/^[+\-]?[0-9](\.[0-9]*)?$/', $values[1])) {
                // ratio
                $part1 = (float) $values[1];
                $part2 = (float) $values[2];
                if (!$part1 || !$part2) {
                    throw new RenderException(
                        "Invalid ratio: {$values[1]}:{$values[2]}"
                    );
                }
                $sum = $values[3] ?? ($part1 + $part2);
                $factor = 12.0 / $sum;
                $total = round($factor * ($part1 + $part2));
                // Ensure columns are nonzero
                $width1 = ((int) round($factor * $part1)) ?: 1;
                $col1 = 'col-sm-' . $width1;
                $col2 = 'col-sm-' . (int) ($total - $width1 > 0 ? $total - $width1 : 1);
                $apply['headingAttributes'] = new Attributes(
                    'class',[$col1, 'col-form-label', 'text-md-right']
                );
                $apply['inputWrapperAttributes'] = new Attributes(
                    'class', [$col2]
                );
                $apply['cellElementAttributes']->itemAppend('class', $col2);
                $default = false;
            }
        }
        if ($default) {
            $apply['headingAttributes'] = new Attributes(
                'class',
                ['col-sm-2', 'col-form-label', 'text-md-right']
            );
            $apply['inputWrapperAttributes'] = new Attributes('class', ['col-sm-10']);
            $apply['cellElementAttributes']->itemAppend('class', 'col-sm-10');
        }
    }

    /**
     * Process vertical layout settings for any scope.
     *
     * @param string $scope Names the settings scope/element this applies to.
     * @param array $values Array of colon-delimited settings including
     *              the initial keyword.
     * @throws \RuntimeException
     */
    public function doLayoutAnyVertical(string $scope, $values)
    {
        // possible values for arguments:
        // v            - Default
        // v:.class
        // v:n          - Inputs use n columns in the 12 column grid
        // v:m:t        - ratio of inputs over space t, adjusted to the BS grid
        //
        // Adjusts:
        // cellElementAttributes - Add the input column class
        // formGroupAttributes - add the form width
        //
        $this->checkState($scope);
        $default = true;
        $apply = &$this->state[$scope];
        if (count($values) >= 2) {
            if ($values[1][0] === '.') {
                // class specification
                $col1 = substr($values[1], 1);
                $apply['formGroupAttributes']->itemAppend('class', $col1);
                $apply['cellElementAttributes']->itemAppend('class', $col1);
                $default = false;
            } elseif (preg_match('/^[+\-]?[0-9]+(\.[0-9]*)?$/', $values[1])) {
                // ratio
                $part1 = (float) $values[1];
                if (!$part1) {
                    throw new RenderException(
                        'Zero is invalid for a ratio.'
                    );
                }
                $sum = $values[2] ?? 12;
                $factor = 12.0 / $sum;
                // Ensure columns are nonzero
                $col1 = 'col-sm-' . ((int) round($factor * $part1)) ?: 1;
                $apply['formGroupAttributes']->itemAppend('class', $col1);
                $apply['cellElementAttributes']->itemAppend('class', $col1);
                $default = false;
            }
        }
        if ($default) {
            $apply['formGroupAttributes']->itemAppend('class', 'col-sm-12');
            $apply['cellElementAttributes']->itemAppend('class', 'col-sm-12');
        }

    }

    /**
     * Process purpose options, called from showValidate().
     *
     * @param string $scope Names the settings scope/element this applies to.
     * @param string $choice Primary option selection
     * @param array $value Array of colon-delimited settings including the initial keyword.
     */
    public function doPurpose(string $scope, string $choice, $value = [])
    {
        if (
            strpos(
                '|primary|secondary|success|danger|warning|info|'
                . 'light|dark|link',
                '|' . $choice
            ) === false
        ) {
            throw new RenderException(
                '{$choice} is an invalid value for purpose.'
            );
        }
        $this->checkState($scope);
        $this->state[$scope]['purpose'] = $choice;
    }

    /**
     * Filter a span list for those with no scheme or with a matching span id.
     *
     * @param array $spans Array of spans; each span contains scheme and size.
     *
     * @return array
     */
    public function spanFilter($spans)
    {
        $filtered = [];
        foreach ($spans as $span) {
            // If the scheme is unspecified or is one of ours, and the size
            // is recognized, retain the span.
            if (
                ($span['scheme'] === null
                    || in_array($span['scheme'], self::$spanList))
                && strpos('|sm|md|lg|xl|xs', '|' . $span['size']) !== false
            ) {
                $filtered[] = $span;
            }
        }

        return $filtered;
    }

}
