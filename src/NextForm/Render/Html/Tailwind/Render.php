<?php

declare(strict_types=1);

namespace Abivia\NextForm\Render\Html\Tailwind;

use Abivia\NextForm\Contracts\RenderInterface;
use Abivia\NextForm\Data\Labels;
use Abivia\NextForm\Render\Attributes;
use Abivia\NextForm\Render\Block;
use Abivia\NextForm\Render\Html\Render as HtmlRender;

/**
 * Base Render for Bootstrap
 */
abstract class Render extends HtmlRender implements RenderInterface
{

    protected static $buttonSizeClasses = [
        'large' => ' btn-lg', 'regular' => '', 'small' => ' btn-sm'
    ];

    public function __construct($options = [])
    {
        parent::__construct($options);
        $this->initialize();
        $this->useOptions($options);
    }

    /**
     * Use current show settings to build the button class
     *
     * @param string|null $scope What the button is used for.
     * @return string
     */
    public function getButtonClass(?string $scope = 'button') : string
    {
        $buttonClass = 'btn btn'
            . ($this->show()->get($scope, 'fill') === 'outline' ? '-outline' : '')
            . '-' . $this->show()->get($scope, 'purpose')
            . self::$buttonSizeClasses[$this->show()->get($scope, 'size')];
        return $buttonClass;
    }

    /**
     * Set up to start generating output.
     */
    protected function initialize()
    {
        parent::initialize();
        // Initialize custom settings
        $this->show->set('valid:is-valid');
        $this->show->set('layout:vertical');
        $this->show->set('purpose:primary');
        $this->show->set('invalid:is-invalid');
    }

    /**
     * Write a "standard" input element; if there are before/after labels,
     *  generate a group.
     *
     * @param Labels $labels
     * @param Attributes $attrs
     * @return Block
     */
    public function inputGroup(Labels $labels, Attributes $attrs)
    {
        // Generate the actual input element, with labels if provided.
        $input = $this->inputGroupPre($labels);
        // Generate the input element
        $input->appendLine($this->writeTag('input', $attrs));

        $input->merge($this->inputGroupPost($labels));

        // If there's help text we need to generate a break.
        if ($labels->has('help')) {
            $input->appendLine('<span class="w-100"></span>');
        }
        return $input;
    }

    /**
     * Start rendering a form.
     *
     * @param array|null  $options The 'attributes' option isn't really optional.
     * @return Block
     */
    public function start($options = []) : Block {
        $options['attributes']->flag('novalidate');
        $options['attributes']->itemAppend('class', 'needs-validation');
        $pageData = parent::start($options);

        return $pageData;
    }

    /**
     * Generate code for any help text.
     *
     * @param Labels $labels
     * @param Attributes $attrs
     * @return string
     */
    public function writeHelpMessage(Labels $labels, Attributes $attrs) : string
    {
        if ($labels->has('help') && $attrs->has('aria-describedby')) {
            $helpAttrs = new Attributes();
            $helpAttrs->set('id', $attrs->get('aria-describedby'));
            $helpAttrs->itemAppend('class', 'form-text text-muted');
            $body = $this->writeLabel(
                'small', $labels, 'help', $helpAttrs, ['break' => true]
            );
        } else {
            $body = '';
        }

        return $body;
    }

    /**
     * Generate code to support an input.
     *
     * @param Labels $labels
     * @param Attributes $attrs
     * @return string
     */
    public function writeInputSupport(Labels $labels, Attributes $attrs) : string
    {
        $body = $this->writeValidationMessages($labels);
        $body .= $this->writeHelpMessage($labels, $attrs);

        return $body;
    }

    /**
     * Generate code for any validation pass/fail messages.
     *
     * @param Labels $labels
     * @return string
     */
    public function writeValidationMessages(Labels $labels) {
        $body = '';
        foreach (['accept', 'error'] as $property) {
            $class = ($property === 'error' ? 'in' : '') . 'valid-feedback';
            if ($labels->has($property)) {
                $labelAttrs = new Attributes('class', $class);
                $body .= $this->writeLabel(
                    'div', $labels, $property, $labelAttrs, ['break' => true]
                );
            }
        }
        return $body;
    }

}

