<?php

declare(strict_types=1);

/**
 *
 */
namespace Abivia\NextForm\Render\Html;

use Abivia\NextForm\Contracts\RenderInterface;
use Abivia\NextForm\Form\Binding\Binding;

abstract class AbstractElementRender
{

    /**
     * The binding to be rendered.
     *
     * @var Binding
     */
    protected $binding;

    /**
     * The render engine to use.
     *
     * @var RenderInterface
     */
    protected $engine;

    public function __construct(RenderInterface $engine, Binding $binding)
    {
        $this->engine = $engine;
        $this->binding = $binding;
    }

}
