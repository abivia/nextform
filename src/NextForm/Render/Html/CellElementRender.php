<?php

declare(strict_types=1);

namespace Abivia\NextForm\Render\Html;

use Abivia\NextForm\Render\Block;

class CellElementRender extends AbstractElementRender
{

    /**
     * Write a cell element.
     *
     * @param array $options
     * @return \Abivia\NextForm\Render\Block
     */
    public function render($options = []) : Block
    {
        $access = $this->engine->getAccess($options);
        $block = new Block();
        if ($access !== 'none') {
            // Start with the form group
            $block = $this->engine->writeElement(
                'div', [
                    'attributes' => $this->engine->groupAttributes(
                        $this->binding
                    ),
                    'show' => ['formGroupAttributes', 'formCellAttributes']
                ]
            );
            // Write a heading if there is one
            $block->appendBody($this->engine->writeLabel(
                'div',
                $this->binding->getLabels(true),
                ['heading' => 'headingAttributes'],
                null,
                ['break' => true]
            ));
            $block->merge($this->engine->writeElement(
                'div', ['show' => 'cellElementAttributes', 'force' => true]
            ));
        }
        $block->onCloseDone([$this->engine, 'popContext']);
        $this->engine->pushContext();
        $this->engine->context('containerAccess', $access);
        $this->engine->context('inCell', true);
        $this->engine->context('cellFirstElement', true);
        $this->engine->show()->doLayout('form', 'inline');

        return $block;
    }

}
