<?php

declare(strict_types=1);

/**
 *
 */
namespace Abivia\NextForm\Render\Html;

use Abivia\NextForm\Data\Labels;
use Abivia\NextForm\NextForm;
use Abivia\NextForm\Render\Attributes;
use Abivia\NextForm\Render\Block;

class ButtonElementRender extends AbstractElementRender
{
    /**
     * The access level for this element.
     *
     * @var string
     */
    protected $access;

    protected $element;

    protected $inputType;

    protected $labels;

    /**
     * Get common attributes for the input element.
     *
     * @return Attributes
     */
    protected function inputAttributes() : Attributes
    {
        $attrs = new Attributes();
        $attrs->set('id', $this->binding->getId());
        $attrs->set('name', $this->binding->getNameOnForm());
        $attrs->set('type', $this->inputType);
        $restricted = $this->access === 'view' || $this->access === 'mask';
        $attrs->flag(
            'readonly',
            $this->element->getReadonly() || $restricted
        );
        if ($restricted || !$this->element->getEnabled()) {
            $attrs->flag('disabled');
        }
        $attrs->flag('autofocus', $this->element->getAutofocus());
        $attrs->setIfNotNull('value', $this->labels->get('inner'));
        $attrs->set('type', $this->element->getFunction());

        return $attrs;
    }

    /**
     * Generate the input element and any wrapping/supporting code.
     *
     * @param Attributes $attrs
     * @param string $text
     * @return Block
     */
    protected function inputGroup(Attributes $attrs) : Block
    {
        if ($this->labels->has('help')) {
            $attrs->set('aria-describedby',
                $attrs->get('id') . NextForm::HELP_LABEL
            );
        }
        $input = $this->engine->writeElement(
            'div', ['show' => 'inputWrapperAttributes']
        );
        $input->appendLine(
            $this->engine->writeLabel('span', $this->labels, 'before')
            . $this->engine->writeTag('input', $attrs)
            . $this->engine->writeLabel('span', $this->labels, 'after')
        );

        // Generate help text, if any
        if ($this->labels->has('help')) {
            $input->appendBody(
                $this->engine->queryContext('inCell') ? '&nbsp;' : "<br/>\n"
                . $this->engine->writeLabel(
                    'small',
                    $this->labels,
                    'help',
                    new Attributes('id', $attrs->get('aria-describedby')),
                    ['break' => true]
                )
            );
        }
        return $input;
    }

    /**
     * Write a button element.
     *
     * @param type $options
     * @return \Abivia\NextForm\Render\Block
     */
    public function render($options = []) : Block
    {
        $this->access = $this->engine->getAccess($options);

        // Get any labels associated with this element
        $this->labels = $this->binding->getLabels(true)->copy();

        // No write/view permissions, the field is hidden.
        if ($this->access === 'hide') {
            $block = $this->engine->elementHidden(
                $this->binding, $this->labels->get('inner')
            );
            return $block;
        }

        if ($this->access === 'mask') {
            $this->labels->set('inner', $this->labels->get('mask'));
        }

        $this->element = $this->binding->getElement();

        // Push and update the show context
        $show = $this->element->getShow();
        if ($show !== '') {
            $this->engine->pushContext();
            $this->engine->show()->set($show, 'button');
        }

        // Generate any field grouping.
        $block = $this->renderContainer();

        // Get attributes for the input element
        $attrs = $this->inputAttributes();

        // Write the header.
        $block->appendBody($this->engine->writeLabel(
            'label',
            $this->labels,
            ['heading' => 'headingAttributes'],
            new Attributes('!for', $this->binding->getId()),
            ['break' => true]
        ));

        // Generate the actual input element.
        $input = $this->inputGroup($attrs);

        $block->merge($input);
        $block->close();
        $block->merge($this->engine->epilog());

        // Restore show context and return.
        if ($show !== '') {
            $this->engine->popContext();
        }

        return $block;
    }

    /**
     * Generate any field grouping.
     *
     * @return Block
     */
    protected function renderContainer() : Block
    {
        // We can see or change the data. Create a form group.
        $block = $this->engine->writeElement(
            'div', [
                'attributes' => $this->engine->groupAttributes($this->binding),
                'show' => 'formGroupAttributes'
            ]
        );
        return $block;
    }

}
