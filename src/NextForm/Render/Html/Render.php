<?php

declare(strict_types=1);

namespace Abivia\NextForm\Render\Html;

use Abivia\NextForm\Contracts\RenderInterface;
use Abivia\NextForm\Data\Labels;
use Abivia\NextForm\Form\Binding\Binding;
use Abivia\NextForm\Form\Binding\ContainerBinding;
use Abivia\NextForm\Form\Binding\FieldBinding;
use Abivia\NextForm\Form\Element\Element;
use Abivia\NextForm\NextForm;
use Abivia\NextForm\Render\Attributes;
use Abivia\NextForm\Render\Block;
use Abivia\NextForm\Render\Html\Show;
use Abivia\NextForm\Render\RenderException;
use Abivia\NextForm\Traits\HasRoles;

/**
 * A base for HTML rendering
 */
class Render implements RenderInterface
{
    /**
     * Object responsible for generating trigger code.
     * @var ActionRender
     */
    protected $actionRender;

    /**
     * An instance of a class that can render a Captcha.
     * @var Abivia\NextForm\Contracts\CaptchaInterface
     */
    protected $captchaInstance;

    /**
     * The name of a class that can render a Captcha.
     * @var string
     */
    protected $captchaProvider = '';

    protected $context = [];
    protected $contextStack = [];

    /**
     * Path to the Javascript file required to implement triggers.
     * @var string
     */
    protected static $coreJsPath;

    /**
     * Types of <input> that we'll auto-generate a confirmation for
     * @var array
     */
    protected static $inputConfirmable = [
        'email', 'number', 'password', 'tel', 'text',
    ];

    /**
     * Links to be added to a generated page.
     * @var array Each element an array of [attribute]=value
     */
    protected static $links = [];

    /**
     * Maps element types to render classes.
     * @var array
     */
    protected $renderClassCache = [];

    /**
     * Scripts to be added to a generated page.
     * @var array Indexed by use, each an array of [attribute]=value
     */
    protected static $scripts = [];

    /**
     * Quick lookup for self-closing elements
     * @var array
     */
    protected static $selfClose = ['input' => 1, 'option' => 2];

    /**
     * Custom classes and styles to apply to various form elements
     * @var Show
     */
    protected $show;

    public function __construct($options = [])
    {
        if (!$this->actionRender) {
            $this->actionRender = new ActionRender('form');
        }
        if (!$this->show) {
            $this->show = new Show('form');
        }
        $this->initialize();
    }

    /**
     * Generate a captcha field and any related data.
     *
     * @param Binding $binding
     * @return Block
     * @throws \RuntimeException
     */
    public function captcha(Binding $binding) : Block
    {
        if (!$this->captchaInstance) {
            if ($this->captchaProvider === '') {
                throw new RenderException(
                    'Unable to generate Captcha; no provider defined.'
                );
            }
            $this->captchaInstance = new $this->captchaProvider();
        }
        return $this->captchaInstance->render($this, $binding);
    }

    /**
     * Modify a setting in the current rendering context.
     *
     * @param string $selector The name of the setting to modify.
     * @param scalar|array|object $value The value to assign to the setting.
     * @return $this
     * @throws \RuntimeException
     */
    public function context(string $selector, $value)
    {
        $this->context[$selector] = $value;
        return $this;
    }

    /**
     * Generate a hidden element.
     *
     * @param Binding $binding
     * @param scalar|array $value
     * @return Block
     */
    public function elementHidden(Binding $binding, $value)
    {
        $block = new Block();
        $baseId = $binding->getId();
        $nameOnForm = $binding->getNameOnForm(true);
        $attrs = new Attributes('type', 'hidden');
        if ($binding instanceof \Abivia\NextForm\Form\Binding\FieldBinding) {
            $attrs->setIfNotNull(
                '*data-nf-sidecar',
                $binding->getDataProperty()->getPopulation()->getSidecar()
            );
        }
        if (\is_array($value)) {
            $optId = 0;
            foreach ($value as $key => $entry) {
                $attrs->set('id', $baseId . '_opt' . $optId);
                ++$optId;
                $attrs->set(
                    'name',
                    $nameOnForm . '[' . \htmlspecialchars((string) $key) . ']'
                );
                $attrs->set('value', $entry);
                $block->appendLine(static::writeTag('input', $attrs));
            }
        } else {
            $attrs->set('id', $baseId);
            $attrs->set('name', $nameOnForm);
            $attrs->setIfNotNull('value', $value);
            $block->appendLine(static::writeTag('input', $attrs));
        }
        return $block;
    }

    /**
     * Generate hidden elements for an option list.
     *
     * @param FieldBinding $binding The binding we're generating for.
     * @return Block The output block.
     */
    public function elementHiddenList(FieldBinding $binding) : Block
    {
        $needEmpty = true;
        $block = new Block();
        $baseId = $binding->getId();
        $select = $binding->getValue();
        $list = $binding->getList(true);
        $attrs = new Attributes('type', 'hidden');
        $attrs->set('name', $binding->getNameOnForm(true) . (empty($list) ? '' : '[]'));
        if ($select === null) {
            $select = $binding->getElement()->getDefault();
        }
        foreach ($list as $optId => $radio) {
            $optAttrs = $attrs->copy();
            $id = $baseId . '_opt' . $optId;
            $optAttrs->set('id', $id);
            $value = $radio->getValue();
            $optAttrs->set('value', $value);
            $optAttrs->setIfNotNull('*data-nf-sidecar', $radio->getSidecar());
            if (is_array($select)) {
                $checked = in_array($value, $select);
            } else {
                $checked = $value === $select;
            }
            if ($checked) {
                $block->appendLine(static::writeTag('input', $optAttrs));
                $needEmpty = false;
            }
        }
        if ($needEmpty) {
            $block = $this->elementHidden($binding, $select);
        }
        return $block;
    }

    /**
     * Generate anything that gets inserted after the field is completely
     *  rendered. Overridden by render engines.
     *
     * @return Block
     */
    public function epilog() : Block
    {
        return new Block();
    }

    /**
     * Get the current access level. A container can limit the option value.
     *
     * @param array $options Access is stored under the key "access".
     * @return string The access permission.
     */
    public function getAccess($options) : string
    {
        // The container access limits other settings
        $cap = $this->context['containerAccess'] !== false
            ? $this->context['containerAccess'] : 'write';

        // Get and cap any computed access
        $access = HasRoles::accessCap($options['access'] ?? 'write', $cap);

        return $access;
    }

    /**
     * Get the class responsible for rendering an element.
     *
     * @param Element $element
     * @return string
     */
    protected function getRenderClass(Element $element) : string
    {
        // Use the element class to find the render class
        $classPath = \get_class($element);

        // Extract the last part of the element class and append Render
        // to get the render base: FieldElement -> FieldElementRender
        $lastPos = \strrpos($classPath, '\\');
        $renderBase = \substr($classPath, $lastPos) . 'Render';

        $engineClass = \get_class($this);
        $this->renderClassCache[$engineClass] ??= [];
        // Check the cache
        if (isset($this->renderClassCache[$engineClass][$classPath])) {
            return $this->renderClassCache[$engineClass][$classPath];
        }

        $enginePath = $engineClass;
        while (true) {
            // Try the next engine up the stack, abort if we hit the top.
            $lastPos = \strrpos($enginePath, '\\');
            if (
                $lastPos === false
                || \substr($enginePath, $lastPos) === '\\NextForm'
            ) {
                throw new RenderException(
                    'Unable to render element ' . $classPath
                );
            }
            $enginePath = \substr($enginePath, 0, $lastPos);

            // If a render class exists, cache it and return it.
            $renderClass = $enginePath . $renderBase;
            if (\class_exists($renderClass)) {
                $this->renderClassCache[$engineClass][$classPath] = $renderClass;
                return $renderClass;
            }

        }

    }

    /**
     * Generate attributes for a group container.
     *
     * @param Binding $binding
     * @param array|null $options
     *
     * @return Attributes
     */
    public function groupAttributes(Binding $binding, $options = []) : Attributes
    {
        // Start wwith the ID since every container has one
        $id = $options['id'] ?? $binding->getId();
        $container = new Attributes('id', $id . NextForm::CONTAINER_LABEL);

        // If the container has its own name, add it
        $container->setIfNotEmpty('name', $options['name'] ?? null);

        $element = $binding->getElement();
        if (!$element->getDisplay()) {
            $container->merge($this->show->get('form', 'hidden'));
        }

        // If we're in a cell, check the context so we can apply different
        // styles to the first element
        if ($this->context['inCell']) {
            if ($this->context['cellFirstElement']) {
                $this->context['cellFirstElement'] = false;
            } else {
                $container->merge($this->show->get('form', 'cellspacing'));
            }
        }

        // Add our data elements to tie things together for the JS
        $container->setIfNotEmpty('*data-nf-group', $element->getGroups());
        $container->set('data-nf-for', $id);

        return $container;
    }

    /*
     * Initialize the render engine.
     */
    protected function initialize()
    {
        self::$coreJsPath = __DIR__ . '/js/nf.js';
        // Reset the context
        $this->context = [
            'containerAccess' => false,
            'inCell' => false
        ];
        // Initialize custom settings
        $this->show->set('cellspacing:3');
        $this->show->set('hidden:nf-hidden');
        $this->show->set('invalid:nf-invalid');
        $this->show->set('layout:vertical');
        $this->show->set('valid:nf-valid');
    }

    /**
     * Pop the rendering context
     */
    public function popContext()
    {
        if (count($this->contextStack)) {
            $this->context = \array_pop($this->contextStack);
            $this->show->pop();
        }
    }

    /**
     * Push the rendering context
     */
    public function pushContext()
    {
        \array_push($this->contextStack, $this->context);
        $this->show->push();
    }

    /**
     * Retrieve a value from the context.
     *
     * @param string $selector
     * @return scalar|array|object
     * @throws \RuntimeException
     */
    public function queryContext(string $selector)
    {
        if (!isset($this->context[$selector])) {
            throw new RenderException(
                "${selector} is not valid in the current context."
            );
        }
        return $this->context[$selector];
    }

    /**
     * Render a binding.
     *
     * @param Binding $binding
     * @param array|null $options
     * @return Block
     * @throws \RuntimeException
     */
    public function render(Binding $binding, $options = []) : Block
    {
        if (!isset($options['access'])) {
            $options['access'] = 'write';
        }
        $block = new Block();
        if ($this->context['containerAccess'] === 'none') {
            // We're in a no-output container
            return $block;
        }
        if ($options['access'] === 'none') {
            if ($binding instanceof ContainerBinding) {
                $block->onCloseDone([$this, 'popContext']);
                $this->pushContext();
                $this->context('containerAccess', 'none');
            }
            return $block;
        }
        $renderClass = $this->getRenderClass($binding->getElement());
        if ($renderClass) {
            $test = new $renderClass($this, $binding);
            $block = $test->render($options);
        } else {
        }
        return $block;
    }

    /**
     * This method should be reworked to support different JS frameworks...
     *
     * @param FieldBinding $binding
     * @return Block
     */
    public function renderTriggers(FieldBinding $binding) : Block
    {
        $result = new Block;
        $element = $binding->getElement();
        if (!$element) {
            return $result;
        }
        $triggers = $element->getTriggers();
        if (empty($triggers)) {
            return $result;
        }
        $formId = $binding->getLinkedForm()->getId();
        $script = "document.querySelectorAll('#" . $formId
            . ' [name^="' . $binding->getNameOnForm(true)
            . "\"]').forEach(function (element) {\n"
            . "  element.addEventListener('change', function () {\n";
        foreach ($triggers as $trigger) {
            if ($trigger->getEvent() !== 'change') {
                continue;
            }
            $value = $trigger->getValue();
            $closing = "    }\n";
            if (is_array($value)) {
                $script .= '    if (' . json_encode($value)
                    . ".includes(element.value)) {\n";
            } elseif ($value === null) {
                // Null implies no conditions.
                $closing = '';
            } else {
                $script .= '    if (this.value === ' . json_encode($value) . ") {\n";
            }
            foreach ($trigger->getActions() as $action) {
                $script .= $this->actionRender->render($formId, $binding, $action);
            }
            $script .= $closing;
        }
        $script .= "  });\n"
            . "});\n";
        $result->addScript($script);

        return $result;
    }

    public function show() : Show
    {
        return $this->show;
    }

    /**
     * Start form generation.
     *
     * @param array $options @see Manager
     * @return Block
     */
    public function start($options = []) : Block
    {
        $this->initialize();
        if (isset($options['attributes'])) {
            $attrs = $options['attributes'];
        } else {
            $attrs = new Attributes();
        }
        if (!$attrs->has('id') || !$attrs->has('name')) {
            throw new RenderException('Form must have name and id attributes');
        }
        $attrs->set('method', $options['method'] ?? 'post');
        $attrs->setIfSet('action', $options);

        $pageData = new Block();
        $pageData->addStyle('.nf-hidden {display:none}');
        $pageData->appendLine(static::writeTag('form', $attrs));
        $pageData->appendPostLine('</form>');
        $nfToken = NextForm::getCsrfToken();
        if ($nfToken[0] !== '') {
            $pageData->appendLine('<input id="' . $nfToken[0] . '"'
                . ' name="' . $nfToken[0] . '" type="hidden"'
                . ' value="' . $nfToken[1] . '">'
            );
        }

        // Add in required links
        foreach (static::$links as $link) {
            $html = '<link rel="stylesheet"'
                . Attributes::toHtml($link, false) . ">\n";
            $pageData->linkFile($html);
        }

        // Add in required scripts
        foreach (static::$scripts as $use => $link) {
            $html = '<script rel="stylesheet"'
                . Attributes::toHtml($link, false) . "></script>\n";
            $pageData->scriptFile($use, $html);
        }

        // Add our script (or an application override)
        if (isset($options['scriptpath'])) {
            $pageData->scriptFile(
                'nextform',
                '<script src="' . $options['scriptpath'] . '></script>'
            );
        } elseif (self::$coreJsPath) {
            $pageData->addScript(file_get_contents(self::$coreJsPath));
        }

        // Associate a NextForm object with the form
        $id = $options['attributes']->get('id');
        $pageData->addScript(
            "var ${id} = new NextForm(document.querySelector('#${id}'), '"
            . NextForm::CONTAINER_LABEL . "');"
        );

        return $pageData;
    }

    /**
     * Generate RESTful state data/context for embedding into a form.
     *
     * @param array $state
     * @return Block
     */
    public static function stateData($state) : Block
    {
        $block = new Block();
        foreach ($state as $name => $value) {
            $attrs = new Attributes();
            $attrs->set('type', 'hidden');
            $attrs->set('name', $name);
            $attrs->set('value', $value);
            $block->appendLine(static::writeTag('input', $attrs));
        }

        return $block;
    }

    /**
     * Set an option value.
     *
     * @param string $key
     * @param scalar\array|object $value
     * @return RenderInterface
     */
    public function useOption(string $key, $value) : RenderInterface {
        if ($key === 'Captcha') {
            $this->captchaProvider = $value;
        }
        return $this;
    }

    /**
     * Tell the render engine which options to use.
     *
     * @param array|null $options
     */
    public function useOptions($options = []) {
        foreach ($options as $key => $value) {
            $this->useOption($key, $value);
        }
    }

    /**
     * Conditionally write an element into an open Block suitable for merging.
     *
     * @param string $tag Name of the element to write (div, span, etc.)
     * @param array|null $options Name(type,default): append(string,''),
     *  force(bool,false), show(string|array,''), attrs(Attributes,null)
     * @return Block
     */
    public function writeElement(string $tag, $options = []) : block
    {
        $hasPost = false;
        $attrs = $options['attributes'] ?? new Attributes();
        $scopes = [];
        if (isset($options['show'])) {
            $showList = is_array($options['show']) ? $options['show']
                : [$options['show']];
            foreach ($showList as $show) {
                $scopes[] = Show::getSetting($show);
            }
        }
        foreach ($scopes as $scope) {
            if ($this->show->isset($scope[0], $scope[1])) {
                $attrs = $attrs->merge($this->show->get($scope[0], $scope[1]));
            }
        }
        $block = new Block();
        if (!$attrs->isEmpty()) {
            $block->appendLine(static::writeTag($tag, $attrs));
            $hasPost = true;
        } elseif ($options['force'] ?? false) {
            $block->appendLine("<{$tag}>");
            $hasPost = true;
        }
        if ($hasPost) {
            $block->post("</{$tag}>\n" . ($options['append'] ?? ''));
        }
        return $block;
    }

    /**
     * Write a label if required.
     *
     * @param string $tag The kind of HTML tag to wrap the label in.
     * @param Labels $labels The labels object to write.
     * @param string|array $prop The property to write or [prop => purpose].
     * @param Attributes $attrs HTML attributes to associate with the element.
     * @param type $options break(bool,''), div(string,classes).
     * @return string
     */
    public function writeLabel(
        string $tag,
        Labels $labels,
        $prop,
        ?Attributes $attrs = null,
        $options = []
    ) {
        if (is_array($prop)) {
            $labelName = array_key_first($prop);
            $purpose = $prop[$labelName];
        } else {
            $labelName = $prop;
            $purpose = $prop;
        }
        if (!$labels->has($labelName)) {
            // In horizontal layouts we always generate an element
            if (
                $this->show->get('form', 'layout') === 'horizontal'
                && $purpose === 'headingAttributes'
            ) {
                $text = '&nbsp;';
            } else {
                return '';
            }
        } else {
            $text = $labels->getEscaped($labelName);
            if (\is_array($text)) {
                // Don't make a single element array a list.
                if (\count($text) === 1) {
                    $text = \array_shift($text);
                } else {
                    $text = self::writeList($text, ['escape' => false]);
                }
            }
        }
        if ($this->show->isset('form', $purpose)) {
            $attrs = $attrs ?? new Attributes();
            $attrs = $attrs->merge($this->show->get('form', $purpose));
        }
        $breakTag = $options['break'] ?? false;
        $html = static::writeTag($tag, $attrs)
            . $text
            . '</' . $tag . '>' . ($breakTag ? "\n" : '')
        ;
        // Check to see if we should wrap this in a div.
        if (isset($options['div'])) {
            $html = '<div class="' . $options['div'] . '">' . "\n"
                . $html . ($breakTag ? '' : "\n") . "</div>\n";
        }
        return $html;
    }

    /**
     * Turn a list into a suitable display string.
     *
     * @param string[] $list
     * @param array $options Options include ul=>Attributes, li=>Attributes,
     *                       escape=>bool[true]
     * @return string
     */
    public static function writeList($list = [], $options = []) : string
    {
        if (empty($list)) {
            return '';
        }
        $html = [static::writeTag('ul', $options['ul'] ?? null)];
        foreach ($list as $entry) {
            $html[] = static::writeTag(
                'li',
                $options['li'] ?? null,
                $entry,
                $options
            );
        }
        $html[] = "</ul>\n";
        return implode("\n", $html);
    }

    /**
     * Write an element and attributes into escaped HTML. If text is provided,
     * a closing tag is also generated.
     *
     * @param string $tag
     * @param Attributes|null $attrs
     * @param string|null $text Text to be enclosed in the tag
     * @param array $options Options escape=>bool[true]
     * @return string
     */
    public static function writeTag(
        string $tag,
        ?Attributes $attrs = null,
        string $text = null,
        $options = []
    ) : string {
        $escape = $options['escape'] ?? true;
        $html = '<' . $tag . ($attrs ? $attrs->write($tag) : '');
        if (isset(self::$selfClose[$tag]) && $text === null) {
            $html .= '/>';
        } elseif ($text !== null) {
            $html .= '>'
                . ($escape ? \htmlspecialchars($text) : $text)
                . '</' . $tag . '>';
        } else {
            $html .= '>';
        }
        return $html;
    }

}

