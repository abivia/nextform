/*
 * https://flaviocopes.com/jquery/
    Selecting DOM elements using the # or . notation: document.querySelector('.button')
    Waiting for page DOM to load: document.addEventListener("DOMContentLoaded", () => { ... })
    Executing AJAX requests: fetch('/api.json').then(response => response.text()).then(body => console.log(body))

 */

'use strict';

(function () {
    document.addEventListener('DOMContentLoaded', () => function () {
        // Fetch all the forms we want to apply custom Bootstrap
        //  validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        Array.prototype.filter.call(forms, function (form) {
            form.addEventListener('submit', function (event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
})();

class NextForm {

    constructor(form, containerLabel) {
        this.form = form;
        this.containerLabel = containerLabel;
        this.groupCollector();
    }

    checkGroup(group, checked) {
        if (this.groupList[group] === undefined) return;
        this.groupList[group].forEach(function (element) {
            if (element.type === 'checkbox' || element.type === 'radio') {
                element.checked = checked;
                var event = new Event('change');
                element.dispatchEvent(event);
            }
        });
    }

    check(element, checked) {
        if (element.type === 'checkbox' || element.type === 'radio') {
            element.checked = checked;
            var event = new Event('change');
            element.dispatchEvent(event);
        }
    }

    disableContainer(name, disable) {
        var element = this.getContainer(name);
        if (element === undefined) return;
        element.disabled = disable;
    }

    disableGroup(group, disable) {
        if (this.groupList[group] === undefined) return;
        this.groupList[group].forEach(function (element) {
            element.disabled = disable;
        });
    }

    displayContainer(name, show) {
        var element = this.getContainer(name);
        if (element === undefined) return;
        if (show) {
            element.classList.remove('nf-hidden');
        } else {
            element.classList.add('nf-hidden');
        }
    }

    displayGroup(group, show) {
        if (this.groupList[group] === undefined) return;
        this.groupList[group].forEach(function (element) {
            if (show) {
                element.classList.remove('nf-hidden');
            } else {
                element.classList.add('nf-hidden');
            }
        });
    }

    getContainer(name) {
        var element = this.form.querySelector('[name=' + name + ']');
        if (element === undefined) return undefined;
        var container = document.querySelector('#' + element.id
            + this.containerLabel);
        return container;
    }

    /**
     * Get a list of objects for the elements in each group.
     *
     * @returns {Boolean}
     */
    groupCollector() {
        let rawGroups = [];
        this.form.querySelectorAll('*[data-nf-group]').forEach(function (element, i) {
            JSON.parse(element.dataset.nfGroup).forEach(function (group) {
                if (rawGroups[group] === undefined) {
                    rawGroups[group] = [];
                }
                rawGroups[group].push(element);
            });
        });
        this.groupList = rawGroups;
        return true;
    }

}

