<?php

declare(strict_types=1);

namespace Abivia\NextForm\Contracts;

/**
 *
 */
interface FormInterface
{

    /**
     * Generate a form object from a file.
     *
     * @param string $formFile
     * @return ?FormInterface
     */
    public static function fromFile(string $formFile) : ?FormInterface;

    /**
     * Get a list of top level elements in the form.
     * @return Element[]
     */
    public function getElements();

    public function getName() : ?string;

    public function getSegment() : ?string;

}
