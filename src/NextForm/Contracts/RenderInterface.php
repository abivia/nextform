<?php

declare(strict_types=1);

namespace Abivia\NextForm\Contracts;

use Abivia\NextForm\Form\Binding\Binding;
use Abivia\NextForm\Render\Block;

/**
 * Interface for a page renderer.
 * @codeCoverageIgnore
 */
interface RenderInterface
{

    /**
     * Pop the rendering context
     */
    public function popContext();

    /**
     * Push the rendering context
     */
    public function pushContext();

    /**
     * Generate form content for an element.
     *
     * @param Binding $binding The element and data context to be rendered.
     * @param array $options
     *  $options = [
     *      'access' => (string) Access level. One of hide|none|view|write. Default is write.
     *  ]
     * @return Block The generated text and any dependencies, scripts, etc.
     */
    public function render(Binding $binding, $options = []) : Block;

    /**
     * Initiate a form.
     * @param array $options Render-specific options.
     */
    public function start($options = []) : Block;

    /**
     * Generate RESTful state data/context for embedding into a form.
     *
     * @param array $state
     */
    public static function stateData($state) : Block;

    /**
     * Turn a list into a suitable output string.
     *
     * @param string[] $list
     * @param array $options Implementation dependent options.
     * @return array
     */
    public static function writeList($list = [], $options = []) : string;

    /**
     * Set a global option.
     * @param string $key Option name.
     * @param scalar|array|object $value Arbitrary value.
     */
    public function useOption(string $key, $value) : RenderInterface;

    /**
     * Set global options.
     * @param array $options Render-specific options.
     */
    public function useOptions($options = []);

}
