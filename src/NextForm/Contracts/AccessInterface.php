<?php

declare(strict_types=1);

namespace Abivia\NextForm\Contracts;

/**
 * Interface for access control.
 *
 * @deprecated since version 1.2.0
 *
 * @codeCoverageIgnore
 */
interface AccessInterface
{
    /**
     * Determine if the a user is allowed to perform an operation on an object.
     *
     * @param string $segment The segment that the requested object belongs to.
     * @param string $objectName The name of the object.
     * @param string $operation The operation we're asking permission for
     * (typically write, view, or hide).
     * @param scalar|array|object $user Overrides the current user to get another user's access.
     * @return bool
     */
    public function allows(
        string $segment,
        string $objectName,
        string $operation,
        $user = null
    ) : bool;

    /**
     * Set a default user for subsequent access requests.
     *
     * @param string $user The user identifier
     * @return \self
     */
    public function user($user);

}
