<?php

declare(strict_types=1);

namespace Abivia\NextForm\Contracts;

use Abivia\NextForm\Data\Property;
use Abivia\NextForm\Data\Segment;

/**
 * Describes the schema of a data set.
 */
interface SchemaInterface
{

    /**
     * Get the default schema settings
     * @param string $setting Gets the specified setting. If missing, all settings are returned.
     * @return mixed
     */
    public function getDefault(?string $setting = null);

    /**
     * Convenience function to fetch a property from a segment.
     * @param string|array $segProp A segment name, segment/property or [segment, property].
     * @param string $name Property name. Only required if $segProp is just a segment name.
     * @return \Abivia\NextForm\Data\Property|null Null if the property doesn't exist.
     */
    public function getProperty($segProp, ?string $name = '') : ?Property;

    /**
     * Get a segment by name.
     * @param string $segmentName Name of the segment to retrieve
     * @return \Abivia\NextForm\Data\Segment|null Null if the segment does not exist.
     */
    public function getSegment(string $segmentName) : ?Segment;

    /**
     * Get a list of segment names.
     * @return array A list of defined segment names.
     */
    public function getSegmentNames();

    /**
     * Set a segment in the schema.
     * @param string $segmentName Name of the segment.
     * @param \Abivia\NextForm\Data\Segment $segment Segment contents.
     * @return $this
     */
    public function segment(
        string $segmentName,
        Segment $segment
    ) : SchemaInterface;

}
