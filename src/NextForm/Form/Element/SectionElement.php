<?php

declare(strict_types=1);

namespace Abivia\NextForm\Form\Element;

use Abivia\Configurable\Configurable;
use Abivia\NextForm\Form\DefinitionException;
use Abivia\NextForm\Traits\JsonEncoderTrait;

/**
 * A section contains multiple elements in an area of the form.
 */
class SectionElement Extends ContainerElement
{
    use Configurable;
    use JsonEncoderTrait;

    protected $object;
    protected $triggers;

    public function __construct()
    {
        parent::__construct();
        if (empty(self::$jsonEncodeMethod)) {
            // This only runs on the first instantiation of the class.
            // @codeCoverageIgnoreStart
            self::$jsonEncodeMethod = parent::$parentJsonEncodeMethod;
            self::$jsonEncodeMethod['labels'] = '';
            self::$jsonEncodeMethod['object'] = '';
            self::$jsonEncodeMethod['triggers'] = '';
            // @codeCoverageIgnoreEnd
        }
        $this->type = 'section';
    }

    /**
     * Verify that an element can be added to the form without conflict.
     *
     * @param Element $element
     *
     * @throws \RuntimeException
     */
    protected function checkInsertable($element)
    {
        if (is_array($element)) {
            $list = $element;
        } else {
            $list = [$element];
        }
        foreach ($list as $item) {
            if ($item instanceof SectionElement) {
                throw new DefinitionException('Sections can\'t be nested.');
            }
            if ($this->elementExists($item->getName())) {
                throw new DefinitionException(
                    "Can't add element named " . $item->getName()
                    . ". An element with that name already exists"
                    . " in the section {$this->name}."
                );
            }
        }
    }

    /**
     * Verify that an element can be added to the form without conflict.
     *
     * @param Element $element
     *
     * @throws \RuntimeException
     */
    protected function checkReplaceable($offset, Element $element)
    {
        if (is_array($element)) {
            $list = $element;
        } else {
            $list = [$element];
        }
        foreach ($list as $item) {
            if ($item instanceof SectionElement) {
                throw new DefinitionException('Sections can\'t be nested.');
            }
            if (
                $this->elementExists($item->getName())
                && $this->elements[$offset]->getName() !== $item->getName()
            ) {
                throw new DefinitionException(
                    "Can't set element named " . $item->getName()
                    . ". An element with that name already exists"
                    . " in the section {$this->name}."
                );
            }
        }
    }

    protected function configureClassMap($property, $value)
    {
        return parent::configureClassMap($property, $value);
    }

    protected function configureComplete()
    {
        return parent::configureComplete();
    }

    /**
     * Extract the form if we have one. Not so DRY because we need local options
     */
    protected function configureInitialize(&$config, ...$context)
    {
        $this->expandElements($config);
        $this->registerElement($this->configureOptions);
        return true;
    }

    protected function configurePropertyIgnore($property)
    {
        return parent::configurePropertyIgnore($property);
    }

    protected function configurePropertyMap($property)
    {
        return parent::configurePropertyMap($property);
    }

    protected function configureValidate($property, &$value)
    {
        return parent::configureValidate($property, $value);
    }

    /**
     * Get an iterator for looping.
     *
     * @return \ArrayIterator
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->elements);
    }

}
