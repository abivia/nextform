<?php

declare(strict_types=1);

namespace Abivia\NextForm\Form\Element;

use Abivia\Configurable\Configurable;
use Abivia\NextForm\Form\DefinitionException;
use Abivia\NextForm\Form\Form;
use Abivia\NextForm\Traits\HasRoles;
use Abivia\NextForm\Traits\JsonEncoderTrait;

/**
 * Class for any element that contains a list of sub-elements.
 */
abstract class ContainerElement extends NamedElement
implements \ArrayAccess, \Countable, \IteratorAggregate
{
    use Configurable;
    use HasRoles;
    use JsonEncoderTrait;

    /**
     * The list of elements contained by this instance.
     * @var Element[]
     */
    protected $elements = [];

    /**
     * Rules for the JsonEncoder
     * @var array
     */
    protected static $jsonEncodeMethod = [];

    /**
     * All the contained elements with names.
     * @var array
     */
    protected $namedElements = [];

    /**
     * Build JSON encoder rules on the first instantiation.
     */
    public function __construct()
    {
        parent::__construct();
        if (empty(self::$jsonEncodeMethod)) {
            self::$jsonEncodeMethod = parent::$jsonEncodeMethod;
            self::$jsonEncodeMethod['labels'] = ['drop:empty', 'drop:null'];
            self::$jsonEncodeMethod['roles'] = [
                'method:jsonCollapseRoles', 'drop:blank', 'drop:empty'
            ];
            self::$jsonEncodeMethod['elements'] = [];
        }
    }

    /**
     * Add any named elements to the list of named elements.
     *
     * @param array $elements
     */
    protected function addNames($elements) {
        foreach ($elements as $element) {
            $name = $element->getName();
            if ($name !== null && $name !== '') {
                $this->namedElements[$name] = $element;
            }
        }
    }

    /**
     * Append an element to this container.
     *
     * @param Element|array $elements element or elements to append.
     * @param Element|string $after Name of an element to append after.
     * @return $this
     * @throws \RuntimeException
     */
    public function append($elements, $after = '')
    {
        $list = is_array($elements) ? $elements : [$elements];
        $this->checkInsertable($list);
        $index = $after === '' ? count($this->elements) - 1
            : $this->findElementIndex($after);
        array_splice($this->elements, $index + 1, 0, $list);
        $this->addNames($list);

        return $this;
    }


    /**
     * Connect data elements in a schema.
     *
     * @param \Abivia\NextForm\Data\Schema $schema
     */
    public function bindSchema(\Abivia\NextForm\Data\Schema $schema)
    {
        foreach ($this->elements as $element) {
            $element->bindSchema($schema);
        }
    }

    /**
     * Sub-elements have a procedural instantiation.
     *
     * @param string $property
     * @param mixed $value
     * @return mixed
     */
    protected function configureClassMap($property, $value)
    {
        $result = false;
        if ($property === 'elements') {
            $result = new \stdClass();
            $result->key = [$this, 'append'];
            $result->className = [Element::class, 'classFromType'];
        } else {
            $result = parent::configureClassMap($property, $value);
        }
        return $result;
    }

    /**
     * Make sure element names are unique.
     *
     * @return boolean
     */
    protected function configureComplete()
    {
        $this->namedElements = [];
        foreach ($this->elements as $element) {
            $name = $element->getName();
            if ($name === null || $name === '') {
                continue;
            }
            if (isset($this->namedElements[$name])) {
                $this->configureLogError(
                    "Duplicate element name \"{$name}\" found. "
                );
                return false;
            }
            $this->namedElements[$name] = $element;
        }
        return true;
    }

    protected function configurePropertyIgnore($property)
    {
        return parent::configurePropertyIgnore($property);
    }

    protected function configurePropertyMap($property)
    {
        return parent::configurePropertyMap($property);
    }

    protected function configureValidate($property, &$value)
    {
        if ($property === 'roles') {
            $value = $this->validateRoles($value);
            if ($value === false) {
                $this->configureLogError(
                    'Access must be one of '
                    . implode(', ', self::$accessLevels) . '.'
                );
                return false;
            }
        }
        return parent::configureValidate($property, $value);
    }

    public function count()
    {
        return count($this->elements);
    }

    /**
     * Set the display state of this and optionally all contained elements.
     *
     * @param bool|null $display
     * @param bool|null $descendants
     *
     * @return $this
     */
    public function display(?bool $display = true, bool $descendants = true)
    {
        if ($descendants) {
            foreach ($this->elements as $element) {
                $element->display($display);
            }
        }

        return parent::display($display);
    }

    /**
     * Determine if a named element exists in this container.
     *
     * @param string $name
     * @return bool|null
     */
    public function elementExists(string $name) {
        return $name === '' ? null : isset($this->namedElements[$name]);
    }

    /**
     * Set the enabled state of this and optionally all contained elements.
     *
     * @param bool|null $enable
     * @param bool|null $descendants
     *
     * @return $this
     */
    public function enable(?bool $enable = true, bool $descendants = true)
    {
        if ($descendants) {
            foreach ($this->elements as $element) {
                $element->enable($enable);
            }
        }
        return parent::enable($enable);
    }

    /**
     * Pre-process any compact elements.
     *
     * @param stdClass $config
     */
    protected function expandElements(&$config) {
        // Any elements that are simply strings are converted to basic field objects
        if (isset($config->elements) && is_array($config->elements)) {
            foreach ($config->elements as &$value) {
                if (is_string($value)) {
                    // Convert to a useful class
                    $value = Form::expandString($value);
                }
            }
        }
    }

    /**
     * Find a first level element by name or matching object.
     *
     * @param string|Element $element
     *
     * @return int|null
     */
    public function findElementIndex($element) {
        if (is_string($element)) {
            foreach ($this->elements as $key => $item) {
                if ($item->getName() === $element) {
                    return $key;
                }
            }
        } else {
            foreach ($this->elements as $key => $item) {
                if ($item === $element) {
                    return $key;
                }
            }
        }
        return null;
    }

    /**
     * Find a contained field by object name.
     *
     * @param string $objectName
     *
     * @return FieldElement|null
     */
    public function findField($objectName) : ?FieldElement
    {
        foreach ($this->elements as $item) {
            if ($item instanceof FieldElement) {
                if ($item->getObject() === $objectName) {
                    return $item;
                }
            } elseif ($item instanceof ContainerElement) {
                if (($subItem = $item->findField($objectName)) !== null) {
                    return $subItem;
                }
            }
        }

        return null;
    }

    /**
     * Get the elements in this container.
     *
     * @return Element[]
     */
    public function getElements()
    {
        return $this->elements;
    }

    /**
     * Get an iterator for looping.
     *
     * @return \ArrayIterator
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->elements);
    }

    /**
     * Get the data segment associated with this container, if any.
     *
     * @return \Abivia\NextForm\Data\Segment|null
     */
    public function getSegment()
    {
        return isset($this->configureOptions['parent'])
            ? $this->configureOptions['parent']->getSegment() : null;
    }

    public function offsetExists($offset) {
        return isset($this->elements[$offset]);
    }

    public function offsetGet($offset) {
        return isset($this->elements[$offset]) ? $this->elements[$offset] : null;
    }

    public function offsetSet($offset, $element) {
        if (!$element instanceof Element) {
            throw new DefinitionException(
                "Only Elements can be assigned to Container elements."
            );
        }
        $name = $element->getName();
        if (is_null($offset)) {
            $this->checkInsertable($element);
            $this->elements[] = $element;
            $this->namedElements[$name] = $element;
        } else {
            $this->checkReplaceable($offset, $element);
            $this->elements[$offset] = $element;
        }
    }

    public function offsetUnset($offset) {
        if ($this->elements[$offset]->getName() ?? null !== null) {
            unset($this->namedElements[$this->elements[$offset]->getName()]);
        }
        unset($this->elements[$offset]);
    }

    /**
     * Prepend an element.
     *
     * @param Element|array $elements
     * @param Element|string $before
     * @return $this
     * @throws \RuntimeException
     */
    public function prepend($elements, $before = '')
    {
        $list = is_array($elements) ? $elements : [$elements];
        $this->checkInsertable($list);
        $index = ($before === '') ? 0 : $this->findElementIndex($before);
        array_splice($this->elements, $index, 0, $list);
        $this->addNames($list);

        return $this;
    }

    /**
     * Set the read-only state of this and optionally all contained elements.
     *
     * @param bool|null $readonly
     * @param bool|null $descendants
     *
     * @return $this
     */
    public function readonly(?bool $readonly = true, bool $descendants = true)
    {
        if ($descendants) {
            foreach ($this->elements as $element) {
                $element->readonly($readonly);
            }
        }

        return parent::readonly($readonly);
    }

}