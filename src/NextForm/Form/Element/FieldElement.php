<?php

declare(strict_types=1);

namespace Abivia\NextForm\Form\Element;

use Abivia\Configurable\Configurable;
use Abivia\NextForm\Form\Form;
use Abivia\NextForm\NextForm;
use Abivia\NextForm\Traits\JsonEncoderTrait;
use Abivia\NextForm\Trigger\Trigger;

/**
 * Representation of a form element that accepts user input (including a button).
 */
class FieldElement extends NamedElement
{
    use Configurable;
    use JsonEncoderTrait;

    /**
     * Default value to use
     * @var string
     */
    protected $default;

    /**
     * Rules for the JsonEncoder
     * @var array
     */
    protected static $jsonEncodeMethod = [];

    /**
     * Local rules for the JsonEncoder
     * @var array
     */
    protected static $jsonLocalMethod = [
        'object' => ['method:removeScope', 'order:250'],
        'default' => ['drop:null'],
        'triggers' => ['drop:empty', 'drop:null'],
    ];

    /**
     * The name of an associated schema object
     * @var string
     */
    protected $object;

    /**
     * List of triggers associated with this element.
     * @var \Abivia\Trigger\Trigger[]
     */
    protected $triggers = [];

    /**
     * The current field value.
     * @var string
     */
    protected $value;

    /**
     * Configure the JSON encoder on first instantiation.
     */
    public function __construct()
    {
        parent::__construct();
        if (empty(self::$jsonEncodeMethod)) {
            self::$jsonEncodeMethod = array_merge(
                parent::$jsonEncodeMethod, self::$jsonLocalMethod
            );
        }
        $this->type = 'field';
    }

    /**
     * Add a trigger to the trigger list.
     *
     * @param Trigger $trigger
     * @return $this
     */
    public function addTrigger(Trigger $trigger)
    {
        $this->triggers[] = $trigger;
        return $this;
    }

    protected function configureClassMap($property, $value)
    {
        static $classMap = [
            'triggers' => ['className' => Trigger::class],
        ];
        $result = false;
        if (isset($classMap[$property])) {
            $result = (object) $classMap[$property];
        } else {
            $result = parent::configureClassMap($property, $value);
        }
        return $result;
    }

    /**
     * Pass the completeness check up so we have a label structure.
     * @return bool
     */
    protected function configureComplete()
    {
        // The NamedElement class initializes the labelsMerged property
        return parent::configureComplete();
    }

    /**
     * Extract the form if we have one. Not so DRY because we need local options
     */
    protected function configureInitialize(&$config, ...$context)
    {
        if (\is_string($config)) {
            // Convert to a field/object
            $config = Form::expandString($config);
        }
        $this->registerElement($this->configureOptions);
        return true;
    }

    protected function configurePropertyIgnore($property)
    {
        return parent::configurePropertyIgnore($property);
    }

    protected function configurePropertyMap($property)
    {
        return parent::configurePropertyMap($property);
    }

    /**
     * Make sure the object property has a scope.
     *
     * @param string $property
     * @param mixed $value
     * @return bool
     */
    protected function configureValidate($property, &$value)
    {
        if (in_array($property, array_keys(self::$jsonLocalMethod))) {
            return true;
        }
        return parent::configureValidate($property, $value);
    }

    /**
     * Set the default value for this field.
     *
     * @param scalar|array $value The new default value.
     * @return $this
     */
    public function default($value)
    {
        $this->default = $value;
        return $this;
    }

    /**
     * Get the default value for this field.
     * @return scalar|array
     */
    public function getDefault()
    {
        return $this->default;
    }

    /**
     * Get the name of an associated schema object.
     *
     * @return string
     */
    public function getObject() : ?string
    {
        return $this->object;
    }

    /**
     * Get the element triggers
     *
     * @return Trigger[]
     */
    public function getTriggers()
    {
        return $this->triggers;
    }

    /**
     * If we can represent this field in JSON as a string, return a
     *  string otherwise $this.
     */
    public function jsonCollapse()
    {
        if (!NextForm::$jsonCompact) {
            return $this;
        }
        if ($this->default !== null) {
            return $this;
        }
        if (!empty($this->triggers)) {
            return $this;
        }
        if (!$this->enabled || $this->readonly || !$this->display) {
            return $this;
        }
        if ($this->show !== '') {
            return $this;
        }
        $collapsed = $this->object;
        $this->removeScope($dummy, $collapsed);
        if (!empty($this->groups)) {
            $collapsed .= NextForm::GROUP_DELIM
                . implode(NextForm::GROUP_DELIM, $this->groups);
        }
        return $collapsed;
    }

    /**
     * Get the name of an associated schema object.
     *
     * @return $this
     */
    public function object($objectName) {
        $this->object = $objectName;
        return $this;
    }

    /**
     * Used by the JSON encoder. Remove the scope if the form has a
     *  matching default segment.
     *
     * @param string $property
     * @param string|null $value
     * @return type
     */
    protected function removeScope(?string &$property, ?string &$value)
    {
        return;
        if (!$this->form) {
            return;
        }
        $segment = $this->form->getSegment();
        if ($segment !== '') {
            if (strpos($value, $segment . NextForm::SEGMENT_DELIM) === 0) {
                $value = substr($value, strlen($segment) + 1);
            }
        }
    }

    /**
     * Set the element triggers
     *
     * @param Trigger[]
     * @return $this
     */
    public function triggers($triggers) {
        $this->triggers = $triggers;
        return $this;
    }

}