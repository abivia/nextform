<?php

declare(strict_types=1);

namespace Abivia\NextForm\Form\Element;

use Abivia\Configurable\Configurable;
use Abivia\NextForm\Form\DefinitionException;
use Abivia\NextForm\Traits\JsonEncoderTrait;

/**
 * Representation of a cell, a list of adjacent form elements.
 */
class CellElement Extends ContainerElement
{
    use Configurable;
    use JsonEncoderTrait;

    protected $elements = [];

    /**
     * Rules for the JsonEncoder
     * @var array
     */
    protected static $jsonEncodeMethod = [];

    /**
     * Initialize JSON encoder tables on the first instantiation.
     */
    public function __construct() {
        parent::__construct();
        if (empty(self::$jsonEncodeMethod)) {
            self::$jsonEncodeMethod = parent::$jsonEncodeMethod;
        }
        $this->type = 'cell';
    }

    /**
     * Verify that an element can be added to the form without conflict.
     *
     * @param Element|array $element
     *
     * @throws \RuntimeException
     */
    protected function checkInsertable($element)
    {
        if (is_array($element)) {
            $list = $element;
        } else {
            $list = [$element];
        }
        foreach ($list as $item) {
            if ($item instanceof ContainerElement) {
                throw new DefinitionException(
                    'Cells can\'t contain containers (sections or cells).'
                );
            }
            if ($this->elementExists($item->getName())) {
                throw new DefinitionException(
                    "Can't add element named " . $item->getName()
                    . ". An element with that name already exists"
                    . " on the form {$this->name}."
                );
            }
        }
    }

    /**
     * Verify that an element can be added to the form without conflict.
     *
     * @param Element $element
     *
     * @throws \RuntimeException
     */
    protected function checkReplaceable($offset, Element $element)
    {
        if (is_array($element)) {
            $list = $element;
        } else {
            $list = [$element];
        }
        foreach ($list as $item) {
            if ($item instanceof ContainerElement) {
                throw new DefinitionException(
                    'Cells can\'t contain containers (sections or cells).'
                );
            }
            if (
                $this->elementExists($item->getName())
                && $this->elements[$offset]->getName() !== $item->getName()
            ) {
                throw new DefinitionException(
                    "Can't set element named " . $item->getName()
                    . ". An element with that name already exists"
                    . " in a different position in the cell {$this->name}."
                );
            }
        }
    }

    protected function configureClassMap($property, $value)
    {
        return parent::configureClassMap($property, $value);
    }

    /**
     * Extract the form if we have one. Not so DRY because we need local options
     */
    protected function configureInitialize(&$config, ...$context)
    {
        $this->expandElements($config);
        $this->registerElement($this->configureOptions);
        return true;
    }

    protected function configureComplete()
    {
        return parent::configureComplete();
    }

    protected function configurePropertyIgnore($property)
    {
        return parent::configurePropertyIgnore($property);
    }

    protected function configurePropertyMap($property)
    {
        return parent::configurePropertyMap($property);
    }

    protected function configureValidate($property, &$value)
    {
        return parent::configureValidate($property, $value);
    }

}