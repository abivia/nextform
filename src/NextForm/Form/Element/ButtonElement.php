<?php

declare(strict_types=1);

namespace Abivia\NextForm\Form\Element;

use Abivia\Configurable\Configurable;
use Abivia\NextForm\Form\DefinitionException;
use Abivia\NextForm\Traits\HasRoles;
use Abivia\NextForm\Traits\JsonEncoderTrait;

/**
 * Support for a form button
 */
class ButtonElement Extends NamedElement
{
    use Configurable;
    use HasRoles;
    use JsonEncoderTrait;

    /**
     * The function this button performs on the form.
     * @var string
     */
    protected $function = 'button';

    /**
     * Rules for the JsonEncoder
     * @var array
     */
    protected static $jsonEncodeMethod = [];

    /**
     * Local rules for the JsonEncoder, merged into those of parent classes.
     * @var array
     */
    protected static $jsonLocalMethod = [
        'function' => ['drop:false', 'order:250'],
        'roles' => ['method:jsonCollapseRoles', 'drop:blank', 'drop:empty'],
    ];

    /**
     * A list of valid button functions.
     * @var string[]
     */
    protected static $validFunctions = ['button', 'reset', 'submit'];

    /**
     * Merge JSON encoding rules on first instantiation.
     */
    public function __construct()
    {
        parent::__construct();
        if (empty(self::$jsonEncodeMethod)) {
            self::$jsonEncodeMethod = array_merge(parent::$jsonEncodeMethod, self::$jsonLocalMethod);
        }
        $this->type = 'button';
    }

    protected function configureClassMap($property, $value)
    {
        return parent::configureClassMap($property, $value);
    }

    protected function configureComplete()
    {
        return parent::configureComplete();
    }

    /**
     * Extract the form if we have one. Not so DRY because we need local options
     */
    protected function configureInitialize(&$config, ...$context)
    {
        if (\is_string($config)) {
            // Convert to a field/object
            $config = Form::expandString($config);
        }
        $this->registerElement($this->configureOptions);
        return true;
    }

    protected function configurePropertyIgnore($property)
    {
        return parent::configurePropertyIgnore($property);
    }

    protected function configurePropertyMap($property)
    {
        return parent::configurePropertyMap($property);
    }

    /**
     * Ensure that the button function is valid.
     * @param string $property Name of the property to validate.
     * @param mixed $value Current value of the property.
     * @return bool
     */
    protected function configureValidate($property, &$value)
    {
        if ($property === 'function') {
            if (!in_array($value, self::$validFunctions)) {
                $this->configureLogError(
                    $property . ' must be one of ' . implode(',', self::$validFunctions) . '.'
                );
                return false;
            }
            return true;
        }
        if ($property === 'roles') {
            $value = $this->validateRoles($value);
            if ($value === false) {
                $this->configureLogError(
                    'Access must be one of '
                    . implode(', ', self::$accessLevels) . '.'
                );
                return false;
            }
        }
        return parent::configureValidate($property, $value);
    }

    /**
     * Set the button function.
     *
     * @param string $value The button function.
     * @return $this
     * @throws \RuntimeException If the value is not a valid button function.
     */
    public function function(string $value)
    {
        $this->configureErrors = [];
        if (!$this->configureValidate('function', $value)) {
            throw new DefinitionException(implode("\n", $this->configureErrors));
        }
        $this->function = $value;
        return $this;
    }

    /**
     * Get the function this button has on the form.
     *
     * @return string
     */
    public function getFunction() : string
    {
        return $this->function;
    }

}