<?php

declare(strict_types=1);

namespace Abivia\NextForm\Form;

use Abivia\Configurable\Configurable;
use Abivia\NextForm\Contracts\FormInterface;
use Abivia\NextForm\Form\Element\Element;
use Abivia\NextForm\Form\Element\ContainerElement;
use Abivia\NextForm\Form\Element\FieldElement;
use Abivia\NextForm\NextForm;
use Abivia\NextForm\Traits\JsonEncoderTrait;
use Abivia\NextForm\Traits\ShowableTrait;

/**
 *
 */
class Form
implements FormInterface, \ArrayAccess, \Countable,
    \IteratorAggregate, \JsonSerializable
{
    use Configurable;
    use JsonEncoderTrait;
    use ShowableTrait;

    /**
     * Name of a field to auto-focus on. Does not override field-specified
     * settings.
     *
     * @var string
     */
    protected $autofocus = '';

    /**
     * A list of top level elements on the form.
     * @var Element[]
     */
    protected $elements;

    protected static $jsonEncodeMethod = [
        'name' => ['drop:blank', 'drop:null'],
        'useSegment' => ['drop:blank'],
        'show' => ['drop:blank'],
        'elements' => [],
    ];

    /**
     * The name attribute for the generated form.
     *
     * @var string
     */
    protected $name;

    /**
     * All the top-level elements with names.
     * @var array
     */
    protected $namedElements = [];

    /**
     * Form-specific display settings.
     *
     * @var string
     */
    protected $show = '';

    /**
     * An optional default segment name to use when referencing an object.
     *
     * @var string
     */
    protected $useSegment = '';

    public function __construct()
    {
        $this->initialize();
    }

    /**
     * Append an element or list of Elements to this form.
     *
     * @param Element|array $element
     * @param Element|string $after Optional. Insert after this element name.
     *
     * @return $this
     *
     * @throws \RuntimeException
     */
    public function append($element, $after = '')
    {
        if (is_array($element)) {
            $list = $element;
        } else {
            $list = [$element];
        }
        $this->checkInsertable($list);
        if ($after === '') {
            $index = count($this->elements) - 1;
        } else {
            $index = $this->findElementIndex($after);
        }
        array_splice($this->elements, $index + 1, 0, $list);

        return $this;
    }

    /**
     * Verify that an element can be added to the form without conflict.
     *
     * @param Element|array $element
     *
     * @throws \RuntimeException
     */
    protected function checkInsertable($element)
    {
        if (is_array($element)) {
            $list = $element;
        } else {
            $list = [$element];
        }
        foreach ($list as $item) {
            if ($this->elementExists($item->getName())) {
                throw new DefinitionException(
                    "Can't add element named " . $item->getName()
                    . ". An element with that name already exists"
                    . " on the form {$this->name}."
                );
            }
        }
    }

    /**
     * Verify that an element can be added to the form without conflict.
     *
     * @param int $offset
     * @param Element|array $element
     *
     * @throws \RuntimeException
     */
    protected function checkReplaceable($offset, $element)
    {
        if (is_array($element)) {
            $list = $element;
        } else {
            $list = [$element];
        }
        foreach ($list as $item) {
            if (
                $this->elementExists($item->getName())
                && $this->elements[$offset]->getName() !== $item->getName()
            ) {
                throw new DefinitionException(
                    "Can't set element named " . $item->getName()
                    . ". An element with that name already exists"
                    . " in a different position on the form {$this->name}."
                );
            }
        }
    }

    protected function configureClassMap($property, $value)
    {
        $result = false;
        if ($property === 'elements') {
            $result = new \stdClass();
            $result->key = '';
            $result->className = [Element::class, 'classFromType'];
        }
        return $result;
    }

    /**
     * Sets up options and converts string-valued elements into field objects.
     * @param \stdClass $config
     */
    protected function configureInitialize(&$config, ...$context)
    {
        // Pass an instance of the form down in Configurable's options so we can
        // access the form directly from deep within the data structures.
        $this->configureOptions['_form'] = &$this;

        // Any elements that are simply strings are converted to basic field objects
        if (isset($config->elements) && is_array($config->elements)) {
            foreach ($config->elements as &$value) {
                if (is_string($value)) {
                    $value = self::expandString($value);
                }
            }
        }
    }

    /**
     * Make sure element names are unique.
     *
     * @return boolean
     */
    protected function configureComplete()
    {
        $this->namedElements = [];
        foreach ($this->elements as $element) {
            $name = $element->getName();
            if (isset($this->namedElements[$name])) {
                $this->configureLogError(
                    "Duplicate element name \"{$name}\" in the form "
                    . ($this->name === null
                        ? '(name not set).' : "\"$this->name\"."
                    )
                );
                return false;
            }
            if ($name !== '') {
                $this->namedElements[$name] = $element;
            }
        }
        return true;
    }

    public function count()
    {
        return count($this->elements);
    }

    /**
     * Determine if a named element exists on the top level of the form.
     *
     * @param string $name
     * @return bool|null
     */
    public function elementExists(string $name) {
        if ($name === '') {
            return null;
        }
        return isset($this->namedElements[$name]);
    }

    /**
     * Convert a string of the form property:group:group to a configurable stdClass.
     *
     * @param string $value
     * @return \stdClass
     */
    public static function expandString(string $value)
    {
        $groupParts = explode(NextForm::GROUP_DELIM, $value);
        // Convert to a useful class
        $obj = new \stdClass();
        $obj->type = 'field';
        $obj->object = array_shift($groupParts);
        if (!empty($groupParts)) {
            $obj->memberOf = $groupParts;
        }
        return $obj;
    }

    /**
     * Find a field element by object name.
     *
     * @param string $objectName
     *
     * @return FieldElement|null
     */
    public function findField(string $objectName) : ?FieldElement
    {
        if ($objectName === '') {
            return null;
        }
        foreach ($this->elements as $key => $item) {
            if ($item instanceof FieldElement) {
                if ($item->getObject() === $objectName) {
                    return $this->elements[$key];
                }
            } elseif ($item instanceof ContainerElement) {
                if (($subItem = $item->findField($objectName)) !== null) {
                    return $subItem;
                }
            }
        }

        return null;
    }

    /**
     * Generate a form object from a file
     * @param string $formFile
     * @return \Abivia\NextForm\Form
     */
    public static function fromFile(string $formFile) : ?FormInterface
    {
        $form = new Form();
        return $form->loadFile($formFile);
    }

    /**
     * Generate a form object from a JSON string.
     *
     * @param string $json
     * @return \Abivia\NextForm\Form
     * @throws RuntimeException
     */
    public static function fromJson(string $json)
    {
        $form = new Form();
        return $form->loadJson($json);
    }

    /**
     * Find a top level element by name.
     *
     * @param string $elementName
     *
     * @return Element|null
     */
    public function getElement(string $elementName)
    {
        if ($elementName === '') {
            return null;
        }
        foreach ($this->elements as $key => $item) {
            if ($item->getName() === $elementName) {
                return $this->elements[$key];
            }
        }

        return null;
    }

    /**
     * Find a top level element index by name or matching object.
     *
     * @param string|Element $element
     *
     * @return int|null
     */
    public function findElementIndex($element)
    {
        if (is_string($element)) {
            foreach ($this->elements as $key => $item) {
                if ($item->getName() === $element) {
                    return $key;
                }
            }
        } else {
            foreach ($this->elements as $key => $item) {
                if ($item === $element) {
                    return $key;
                }
            }
        }
        return null;
    }

    /**
     * Get a list of top level elements in the form.
     *
     * @return Element[]
     */
    public function getElements() {
        return $this->elements;
    }

    /**
     * Get an iterator for looping.
     *
     * @return \ArrayIterator
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->elements);
    }

    /**
     * Get the name of the form.
     *
     * @return string|null
     */
    public function getName() : ?string
    {
        return $this->name;
    }

    /**
     * Get the default segment.
     *
     * @return string|null
     */
    public function getSegment() : ?string
    {
        return $this->useSegment;
    }

    /**
     * Initialize the object.
     */
    protected function initialize() {
        $this->elements = null;
        $this->name = null;
        $this->show = '';
        $this->useSegment = '';
    }

    /**
     * Load form definition from file.
     *
     * @param string $formFile Path to the form definition.
     * @return $this
     * @throws \RuntimeException
     */
    public function loadFile(string $formFile)
    {
        $json = \file_get_contents($formFile);
        if ($json === false) {
            throw new DefinitionException(
                'Unable to read ' . $formFile . "\n"
            );
        }
        try {
            $this->loadJson($json);
        } catch (\RuntimeException $err) {
            throw new DefinitionException(
                $err->getMessage() . ' While reading ' . $formFile . "\n"
            );
        }

        return $this;
    }

    /**
     * Load form definition from JSON string.
     *
     * @param string A JSON string.
     * @return $this
     * @throws \RuntimeException
     */
    public function loadJson(string $json)
    {
        $this->initialize();
        if (!$this->configure(\json_decode($json), true)) {
            throw new DefinitionException(
                'Failed to load JSON' . "\n"
                . \implode("\n", $this->configureErrors)
            );
        }
        if (($afElement = $this->findField($this->autofocus))) {
            $afElement->autofocus();
        }

        return $this;
    }

    public function offsetExists($offset) {
        return isset($this->elements[$offset]);
    }

    public function offsetGet($offset) {
        return isset($this->elements[$offset]) ? $this->elements[$offset] : null;
    }

    public function offsetSet($offset, $element) {
        if (!$element instanceof Element) {
            throw new DefinitionException(
                "Only Elements can be assigned to Forms."
            );
        }
        $name = $element->getName();
        if (is_null($offset)) {
            $this->checkInsertable($element);
            $this->elements[] = $element;
            $this->namedElements[$name] = $element;
        } else {
            $this->checkReplaceable($offset, $element);
            $this->elements[$offset] = $element;
        }
    }

    public function offsetUnset($offset) {
        if (isset($this->elements[$offset])) {
            unset($this->namedElements[$this->elements[$offset]->getName()]);
        }
        unset($this->elements[$offset]);
    }

    /**
     * Set form options.
     *
     * @param array $options
     */
    protected function options($options = [])
    {
    }

    /**
     * Prepend an element.
     *
     * @param Element|array $element
     * @param Element|string $before
     * @return $this
     * @throws \RuntimeException
     */
    public function prepend($element, $before = '')
    {
        if (is_array($element)) {
            $list = $element;
        } else {
            $list = [$element];
        }
        $this->checkInsertable($list);
        if ($before === '') {
            $index = 0;
        } else {
            $index = $this->findElementIndex($before);
        }
        array_splice($this->elements, $index, 0, $list);

        return $this;
    }

}
