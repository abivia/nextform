<?php

declare(strict_types=1);

namespace Abivia\NextForm\Form\Binding;

use Abivia\NextForm\Contracts\AccessInterface;
use Abivia\NextForm\Contracts\RenderInterface;
use Abivia\NextForm\Data\Labels;
use Abivia\NextForm\Form\Element\Element;
use Abivia\NextForm\Form\Form;
use Abivia\NextForm\LinkedForm;
use Abivia\NextForm\NextForm;
use Abivia\NextForm\Render\Block;
use Abivia\NextForm\Traits\HasRoles;
use DeepCopy\DeepCopy;
use DeepCopy\Filter\KeepFilter;
use DeepCopy\Filter\SetNullFilter;
use DeepCopy\Matcher\PropertyNameMatcher;
use Illuminate\Contracts\Translation\Translator;

/**
 * Things that get rendered
 */
class Binding
{
    /**
     * System-assigned element ID
     * @var string
     */
    protected $autoId = '';

    /**
     * The source element
     * @var Element
     */
    protected $element;

    /**
     * Mapping of element classes to binding classes when a specialized class
     * is needed.
     *
     * @var type
     */
    protected static $elementMap = [
        'CaptchaElement' => 'SimpleBinding',
        'CellElement' => 'ContainerBinding',
        'FieldElement' => 'FieldBinding',
        'HtmlElement' => 'SimpleBinding',
        'SectionElement' => 'ContainerBinding',
        'StaticElement' => 'SimpleBinding',
    ];

    /**
     * The form the element in this binding is on.
     * @var Form
     */
    protected $form;

    /**
     * Name of this binding on the rendered form.
     * @var string
     */
    protected $nameOnForm;

    /**
     * User-specified element id, overrides auto ID
     * @var string
     */
    protected $id = '';

    /**
     * Text labels for this binding.
     *
     * @var Labels
     */
    protected $labels;

    /**
     * Text labels after translation.
     *
     * @var Labels
     */
    protected $labelsTranslated;

    /**
     * The form this binding appears on.
     *
     * @var LinkedForm
     */
    protected $boundForm;

    /**
     * The current translation object.
     *
     * @var Translator
     */
    protected $translator;

    /**
     * Validation status (tri-state: true, false, or null)
     * @var ?bool
     */
    protected $valid;

    /**
     * The current value for the bound element.
     * @var string
     */
    protected $value;

    /**
     * Connect data elements in the schemas. Only useful for FieldBindings.
     *
     * @param \Abivia\NextForm\Data\SchemaCollection $schemas
     * @return null
     * @codeCoverageIgnore
     */
    public function bindSchema(?\Abivia\NextForm\Data\SchemaCollection $schemas)
    {
        return null;
    }

    /**
     * Check the access level for this binding.
     *
     * @param AccessInterface|string|array|null $access An access control
     * object, role name, or list of role names.
     * @param string $segment The data segment to check, empty for a form name.
     * @param string $objectName The data object name or form name.
     * @param array $options Options, accessOverride is relevant
     * @return array Options with access, accessOverride elements set.
     */
    protected function checkAccess(
        $access,
        string $segment,
        string $objectName,
        $options
    ) {
        // Get the access level for non-matching roles
        if ($access instanceof AccessInterface) {
            $level = 'none';
            if ($access->allows($segment, $objectName, 'write')) {
                $level = 'write';
            } elseif ($access->allows($segment, $objectName, 'view')) {
                $level = 'view';
            } elseif ($access->allows($segment, $objectName, 'hide')) {
                $level = 'hide';
            }
        } else {
            $level = $this->getAccess($access);
        }

        // Limit to the maximum access level we can grant
        $level = HasRoles::accessCap(
            $level, $options['accessOverride'] ?? 'write'
        );

        // Set this as a cap for contained elements.
        $options['accessOverride'] = $level;

        $options['access'] = $level;
        return $options;
    }

    /**
     * Make a copy of this element, cloning/preserving selected properties
     * @return Binding
     */
    public function copy()
    {
        static $cloner = null;

        if ($cloner === null) {
            $cloner = new DeepCopy();
            // Don't copy the form ID
            $cloner->addFilter(
                new SetNullFilter(),
                new PropertyNameMatcher('\Abivia\NextForm\Form\Binding\Binding', 'autoId')
            );
            // Don't clone the linked data
            $cloner->addFilter(
                new KeepFilter(),
                new PropertyNameMatcher('\Abivia\NextForm\Form\Binding\Binding', 'dataProperty')
            );
        }
        return $cloner->copy($this);
    }

    /**
     * Set the element for this binding.
     *
     * @param Element $element
     * @return $this
     */
    public function element(Element $element)
    {
        $this->element = $element;
        $this->form = $element->getForm();
        $this->id = $element->getId();
        $this->labels = $element->getLabels() ?? new Labels();
        $this->labelsTranslated = clone $this->labels;
        return $this;
    }

    /**
     * Create an appropriate bindings for the passed Element and any
     * contained elements.
     *
     * @param Element $element
     * @return Binding
     */
    public static function fromElement(Element $element) : Binding
    {
        $classPath = get_class($element);
        $classParts = explode('\\', $classPath);
        $elementClass = array_pop($classParts);
        if (isset(self::$elementMap[$elementClass])) {
            $bindingClass = __NAMESPACE__ . '\\' . self::$elementMap[$elementClass];
        } else {
            $bindingClass = __NAMESPACE__ . '\\Binding';
        }
        $binding = new $bindingClass();
        $binding->element($element);
        if ($binding instanceof ContainerBinding) {
            foreach ($element->getElements() as $subElement) {
                $binding->addBinding(Binding::fromElement($subElement));
            }
        }

        return $binding;
    }

    /**
     * Use a renderer to turn this element into part of the form.
     *
     * @param RenderInterface $renderer Any Render object.
     * @param AccessInterface|string|array|null $access An access control
     * object, role name, or list of role names.
     * @param array $options Options: accessOverride to override default access.
     * @return Block
     */
    public function generate(
        RenderInterface $renderer,
        $access = null,
        $options = []
    ) : Block {
        $options = $this->checkAccess(
            $access, '', $this->element->getName(), $options
        );

        /**
         * Everything associated with the rendered binding.
         * @var Block
         */
        $pageData = $renderer->render($this, $options);
        return $pageData;
    }

    /**
     * Get the access from the element or default to "write".
     *
     * @param string|array $role
     *
     * @return string
     */
    public function getAccess($role)
    {
        return method_exists($this->element, 'getAccess')
            ? $this->element->getAccess($role) : 'write';
    }

    /**
     * Get the auto-focus flag.
     *
     * @return ?string
     */
    public function getAutofocus() : bool
    {
        return $this->element->getAutofocus();
    }

    /**
     * Get this binding's element.
     * @return Element
     */
    public function getElement()
    {
        return $this->element;
    }

    /**
     * Get this element's form.
     *
     * @return Form|null
     */
    public function getForm() : ?Form
    {
        return $this->form;
    }

    /**
     * Get the form ID for this element.
     * @return string
     */
    public function getId() : string
    {
        if ($this->id !== '') {
            return $this->id;
        }
        if ($this->autoId === '') {
            $this->autoId = NextForm::htmlIdentifier(
                $this->element->getType(), true
            );
        }
        return $this->autoId;
    }

    /**
     * Get native or translated scope-resolved labels for this binding.
     * @param bool $translated
     * @return \Abivia\NextForm\Data\Labels
     */
    public function getLabels(?bool $translated = false) : Labels
    {
        if ($translated) {
            if ($this->labelsTranslated === null) {
                $this->labelsTranslated = new Labels();
            }
            return $this->labelsTranslated;
        } elseif ($this->labels === null) {
            $this->labels = new Labels();
            $this->labelsTranslated = new Labels();
        }
        return $this->labels;
    }

    /**
     * Get the bound form for this binding.
     *
     * @return LinkedForm|null $boundForm
     */
    public function getLinkedForm() : ?LinkedForm
    {
        return $this->boundForm;
    }

    /**
     * Get this element's name on the form. If not assigned, a name is generated.
     *
     * @param bool $baseOnly If set, brackets are omitted. Only useful with
     *             FieldBindings.
     * @return string
     */
    public function getNameOnForm(?bool $baseOnly = false)
    {
        if ($this->nameOnForm === null) {
            $name = $this->element->getName();
            if ($name !== '') {
                $this->nameOnForm = $name;
            } else {
                if ($this->autoId === '') {
                    $this->getId();
                }
                $this->nameOnForm = $this->autoId;
            }
        }
        return $this->nameOnForm;
    }

    /**
     * Get the name of a bound object, null when not a FieldBinding.
     *
     * @return ?string
     */
    public function getObject() : ?string
    {
        return null;
    }

    /**
     * Get the current translation class.
     *
     * @return Translator
     */
    public function getTranslator() : ?Translator
    {
        return $this->translator;
    }

    /**
     * Get the current validation state.
     *
     * @return ?bool
     */
    public function getValid() : ?bool
    {
        return $this->valid;
    }

    /**
     * Get the current value for the bound element.
     *
     * @return int|string|array
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set new labels.
     *
     * @param Labels $labels The new labels.
     * @return $this
     */
    public function labels($labels)
    {
        $this->labels = $labels;

        return $this;
    }

    /**
     * Connect this binding to a linked form.
     *
     * @param LinkedForm $boundForm
     * @return $this
     */
    public function linkedForm(LinkedForm $boundForm)
    {
        $this->boundForm = $boundForm;

        return $this;
    }

    /**
     * Set the form ID for this binding.
     *
     * @param string $id
     * @return $this
     */
    public function id(string $id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Set the value for a label.
     *
     * @param string $labelName Name of the text to be set.
     * @param string|array $text
     * @param array $options {@see Labels::set()}
     * @return $this
     */
    public function label(
        string $labelName,
        $text = null,
        $options = []
    ) {
        if ($this->labels === null) {
            $this->labels = new Labels();
        }
        $this->labels->set($labelName, $text, $options);
        $this->labelsTranslated = $this->labels->translate($this->translator);
        return $this;
    }

    /**
     * Assign or override the current name of this binding on a form.
     * @param string $name
     * @return $this
     */
    public function nameOnForm(string $name)
    {
        $this->nameOnForm = $name;

        return $this;
    }

    /**
     * Set the validation state.
     *
     * @param ?bool $state True, false, or null for indeterminate.
     * @return $this
     */
    public function valid(?bool $state)
    {
        $this->valid = $state;

        return $this;
    }

    /**
     * Set the current value for the bound element.
     *
     * @param int|string|array $value The new value.
     * @return $this
     */
    public function value($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Translate the texts in this element.
     *
     * @param Translator $translator
     * @return $this
     */
    public function translate(?Translator $translator = null) : Binding
    {
        if ($translator !== null) {
            $this->translator = $translator;
        }
        $this->labelsTranslated = $this->labels->translate($this->translator);

        return $this;
    }

}