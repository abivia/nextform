<?php

declare(strict_types=1);

namespace Abivia\NextForm\Form\Binding;

use Illuminate\Contracts\Translation\Translator;

/**
 * Class for binding elements with a preset value
 */
class SimpleBinding Extends Binding
{

    /**
     * Get the current value for the bound element.
     * @return int|string|array
     */
    public function getValue() {
        return $this->element->getValue();
    }

}