<?php

namespace Abivia\NextForm\Form;

use RuntimeException;

class DefinitionException extends RuntimeException
{
    //
}
