<?php

declare(strict_types=1);

namespace Abivia\NextForm\Trigger;

use Abivia\Configurable\Configurable;
use Abivia\NextForm\Traits\JsonEncoderTrait;

/**
 *
 */
class Trigger implements \JsonSerializable
{
    use Configurable;
    use JsonEncoderTrait;

    /**
     * A List of actions to take when this trigger fires.
     *
     * @var Action[]
     */
    protected $actions;

    /**
     * The event that causes this trigger to fire.
     *
     * @var string
     */
    protected $event;

    /**
     * The list of valid events.
     *
     * @var string[]
     */
    protected static $eventValidation = [
        'change', 'invalid', 'valid',
    ];

    /**
     * Rules for converting properties to JSON.
     *
     * @var array
     */
    protected static $jsonEncodeMethod = [
        'event' => ['drop:null'],
        'value' => ['drop:null'],
        'actions' => [],
    ];

    /**
     * The trigger type.
     *
     * @var string
     */
    protected $type;

    /**
     * The value that will cause this trigger to fire.
     *
     * @var scalar
     */
    protected $value;

    /**
     * {@inheritdoc}
     * @codeCoverageIgnore
     */
    protected function configureClassMap($property, $value)
    {
        static $classMap = [
            'actions' => ['className' => '\Abivia\NextForm\Trigger\Action', 'key' => ''],
        ];
        if (isset($classMap[$property])) {
            return (object) $classMap[$property];
        }
        return false;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureComplete()
    {
        // Exactly one of event or value must be set.
        if (isset($this->event) === isset($this->value)) {
            return false;
        }
        if (isset($this->event)) {
            $this->type = 'event';
        } else {
            $this->type = 'value';
        }
        return true;
    }

    /**
     * Initialize this object at the start of configuration.
     */
    protected function configureInitialize()
    {
        $this->event = null;
        $this->type = null;
        $this->value = null;
    }

    /**
     * {@inheritdoc}
     */
    protected function configurePropertyAllow($property)
    {
        return in_array($property, ['actions', 'event', 'value']);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureValidate($property, &$value)
    {
        switch ($property) {
            case 'event':
                $value = strtolower($value);
                $result = in_array($value, self::$eventValidation);
                break;
            default:
                $result = true;
        }
        return $result;
    }

    /**
     * Get the actions to be performed when the trigger fires.
     *
     * @return Action[]
     */
    public function getActions()
    {
        return $this->actions;
    }

    /**
     * Get the trigger category.
     *
     * @return string
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * Get the trigger type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Get the trigger value.
     *
     * @return scalar
     */
    public function getValue()
    {
        return $this->value;
    }

}
