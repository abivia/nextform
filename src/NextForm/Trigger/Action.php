<?php

declare(strict_types=1);

namespace Abivia\NextForm\Trigger;

use Abivia\Configurable\Configurable;
use Abivia\NextForm\NextForm;
use Abivia\NextForm\Traits\JsonEncoderTrait;

/**
 *
 */
class Action implements \JsonSerializable
{
    use Configurable;
    use JsonEncoderTrait;

    protected static $jsonEncodeMethod = [
        'target' => ['drop:empty', 'drop:null', 'scalarize'],
        'subject' => [],
        'value' => [],
    ];
    /**
     * The subject of this action, i.e. what it applies to.
     * @var string
     */
    protected $subject;

    /**
     * Allowed values for the subject.
     *
     * @var string[]
     */
    protected static $subjectValidation = [
        'checked', 'display', 'enable', 'readonly', 'script', 'value', 'visible'
    ];

    /**
     * A list of entities that will receive this action.
     *
     * @var string[]
     */
    protected $target = [];

    /**
     * The value to apply.
     *
     * @var scalar|array|object
     */
    protected $value;

    /**
     * {@inheritdoc}
     */
    protected function configureComplete()
    {
        // if the subject is boolean valued, convert strings to boolean.
        if (!\in_array($this->subject, ['script', 'value'])) {
            if (\is_bool($this->value)) {
                return true;
            }
            if (\strtolower($this->value) === 'true') {
                $this->value = true;
            } elseif (\strtolower($this->value) === 'false') {
                $this->value = false;
            }
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureInitialize(&$config, ...$context)
    {
        if (\is_string($config)) {
            $expanded = new \stdClass();
            $parts = \explode(NextForm::GROUP_DELIM, $config);
            \array_push($parts, null);
            \array_push($parts, null);
            $expanded->target = \explode(',', $parts[0]);
            \array_walk($expanded->target, 'trim');
            $expanded->subject = $parts[1];
            $expanded->value = $parts[2];
            $config = $expanded;
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configureValidate($property, &$value)
    {
        switch ($property) {
            case 'subject':
                if (!($result = \in_array($value, self::$subjectValidation))) {
                    $this->configureLogError(
                        '"' . (\is_scalar($value) ? $value : \gettype($value))
                        . '" is not a valid value for "' . $property . '".'
                    );
                }
                break;
            case 'target':
                if (!\is_array($value)) {
                    $value = [$value];
                }
                $result = true;
                break;
            default:
                $result = true;
        }
        return $result;
    }

    /**
     * Get the current subject.
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Get the target list.
     *
     * @return array
     */
    public function getTargets()
    {
        return $this->target;
    }

    /**
     * Get the action value.
     *
     * @return scalar\array|object
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * {@inheritdoc}
     */
    public function jsonCollapse() {
        if (!NextForm::$jsonCompact) {
            return $this;
        }
        if ($this->subject === 'script') {
            return $this;
        }
        if (\strpos((string) $this->value, NextForm::GROUP_DELIM) !== false) {
            return $this;
        }
        if (\is_bool($this->value)) {
            $strVal = $this->value ? 'true' : 'false';
        } else {
            $strVal = $this->value;
        }
        $result = implode(',', $this->target)
            . NextForm::GROUP_DELIM . $this->subject
            . NextForm::GROUP_DELIM . $strVal;
        return $result;
    }

    /**
     * Set the subject.
     *
     * @param string $value
     * @return $this
     * @throws \UnexpectedValueException
     */
    public function subject(string $value)
    {
        if (!$this->configureValidate('subject', $value)) {
            throw new \UnexpectedValueException(
                'Valid values for subject are: ' . implode('|', self::$subjectValidation)
            );
        }
        $this->subject = $value;
        return $this;
    }

    /**
     * Set the target(s) for this action.
     *
     * @param string|array $value
     * @return $this
     * @throws \UnexpectedValueException
     */
    public function target($value)
    {
        if (!$this->configureValidate('target', $value)) {
            // @codeCoverageIgnoreStart
            throw new \UnexpectedValueException('Invalid value for target.');
            // @codeCoverageIgnoreEnd
        }
        $this->target = $value;
        return $this;
    }

    /**
     * Set the value.
     *
     * @param scalar|array|object $value
     * @return $this
     * @throws \UnexpectedValueException
     */
    public function value($value)
    {
        if (!$this->configureValidate('value', $value)) {
            // @codeCoverageIgnoreStart
            throw new \UnexpectedValueException('Invalid value.');
            // @codeCoverageIgnoreEnd
        }
        $this->value = $value;
        return $this;
    }

}
