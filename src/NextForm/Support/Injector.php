<?php

declare(strict_types=1);

namespace Abivia\Nextform\Support;

/**
 * Lightweight dependency injection manager.
 */
abstract class Injector
{
    /**
     * Service container. Services defined by $wiring.
     *
     * @var array
     */
    protected $services = [];

    /**
     * Class mappings for DI.
     *
     * @var array [function => class]
     */
    private $wiring;

    /**
     * Create a new Injector.
     *
     * @param array|null $map The services supported by this injector.
     */
    public function __construct($map = [])
    {
        $this->wire($map);
    }

    /**
     * Check that a service name is valid.
     *
     * @param string $service A service name.
     *
     * @throws RuntimeException
     */
    protected function checkService(string $service)
    {
        if (!array_key_exists($service, $this->wiring)) {
            throw new \RuntimeException(
                "Service {$service} must be one of "
                . implode(', ', array_keys($this->wiring))
            );
        }
    }

    /**
     * Create an object for a service.
     *
     * @param string $service The name of the service.
     * @param array $args Things to pass to the service factory.
     *
     * @return object|null
     */
    public function make(string $service, ...$args)
    {
        $this->checkService($service);
        $handler = $this->wiring[$service];
        if ($handler === null || $handler === '') {
            $object = null;
        } elseif (is_array($handler)) {
            $object = $handler[1](...$args);
        } else {
            $object = new $handler(...$args);
        }
        return $object;
    }

    /**
     * Get the name of a class provided by a service.
     *
     * @param string $service
     *
     * @return string
     */
    public function serviceClass(string $service)
    {
        $provider = $this->serviceProvider($service);
        return is_array($provider) ? $provider[0] : $provider;
    }

    /**
     * Get the class or [class, factory] for a service provider.
     *
     * @param string $service
     *
     * @return string|array|null
     */
    public function serviceProvider(string $service)
    {
        $this->checkService($service);
        return $this->wiring[$service];
    }

    /**
     * Return a singleton instance of the named service.
     *
     * @param string $service Name of the service to get.
     * @param array $options Things to pass to the service factory.
     *
     * @return object
     */
    public function singleton(string $service, ...$args)
    {
        $this->checkService($service);
        if (!isset($this->services[$service])) {
            $this->services[$service] = $this->make($service, ...$args);
        }
        return $this->services[$service];
    }

    /**
     * Set or replace dependencies. Providers are either a class name or an
     * array of [serviceClass, callable].
     *
     * @param array $services Array of [service => provider].
     *
     * @return $this
     *
     * @throws RuntimeException
     */
    public function wire($services)
    {
        foreach ($services as $service => $provider) {
            if (!is_string($provider)
                && (is_array($provider)
                    && !(is_string($provider[0])
                        && $provider[1] instanceof \Closure
                    )
                )
            ) {
                throw new \RuntimeException(
                    "Provider for {$service} must be className or an"
                    . " array of [className, Closure]."
                );
            }
            $this->wiring[$service] = $provider;
        }
        return $this;
    }

    /**
     * Plug an externally instantiated object into the services table.
     *
     * @param string $service
     * @param object $object
     *
     * @return $this
     */
    protected function wireToInstance(string $service, object $object)
    {
        $this->checkService($service);
        $this->wire([$service => get_class($object)]);
        $this->services[$service] = $object;
        return $this;
    }

}
