<?php

declare(strict_types=1);

namespace Abivia\NextForm;

use Abivia\NextForm\Data\Schema;
use Abivia\NextForm\Data\SchemaCollection;
use Abivia\NextForm\Data\Segment;
use Abivia\NextForm\Form\Binding\Binding;
use Abivia\NextForm\Form\Form;
use Abivia\NextForm\Render\Block;
use Abivia\NextForm\Support\Injector;
use Illuminate\Support\Str;

/**
 * Manager for the form generator.
 *
 * @method void body(?string $formName = null) Output the rendered body of
 *  all forms or a single form.
 * @method void head(?string $formName = null) Output the rendered head
 *  section of all forms or a single form.
 * @method links(?string $formName = null) Output the file links for all
 *  forms or a single form.
 * @method script(?string $formName = null) Output the script for all forms
 *  or a single form.
 * @method scriptFiles(?string $formName = null) Output the script files
 *  for all forms or a single form.
 * @method styles(?string $formName = null) Output the inline styles for
 *  all forms or a single form.
 */
class NextForm extends Injector
{
    public const CONTAINER_LABEL = '_container';
    public const HELP_LABEL = '_formhelp';
    public const GROUP_DELIM = ':';
    public const MASK_TEXT_DEFAULT = '*****';
    public const SEGMENT_DELIM = '.';

    /**
     * The form definitions.
     *
     * @var LinkedForm[]
     */
    protected $boundForms = [];

    /**
     * The suffix to use when generating a confirm field.
     *
     * @var string
     */
    public static $confirmLabel = '_confirm';

    /**
     * External for custom token generation (to return [name, value]),
     *
     * @var callable
     */
    protected static $csrfGenerator;

    /**
     * The current CSRF token [name, value].
     *
     * @var array
     */
    protected static $csrfToken;

    /**
     * The results of generating each form.
     * @var Block[]
     */
    protected $formBlock = [];

    /**
     * The data we will put into the form, indexed by segment ('' for default).
     *
     * @var array
     */
    protected $formData = [];

    /**
     * Error messages to show on the form, indexed by segment ('' for default).
     *
     * @var array
     */
    protected $formErrors;

    /**
     * Counter used to assign HTML identifiers.
     *
     * @var int
     */
    protected static $htmlId = 0;

    /**
     * Default class mappings in our cheap DI implementation.
     *
     * @var array [function => class]
     */
    private static $diWiringStatic = [
        'Access' => Access\NullAccess::class,
        'Captcha' => Captcha\SimpleCaptcha::class,
        'Form' => Form::class,
        'Render' => 'Abivia\\NextForm\\Render\\Html\\Bootstrap\\Bs4\\Render',
        'Schema' => Schema::class,
        'Translate' => null,
    ];

    /**
     * Use the compact form (if available) when encoding JSON via toJson().
     *
     * @var bool
     */
    public static $jsonCompact = false;

    /**
     * Maps form names to form bindings.
     *
     * @var array
     */
    protected $nameMap;

    /**
     * A list of all bindings connected to a property (indexed by segment/name).
     *
     * @var array
     */
    protected $objectMap;

    /**
     * The form and associated data after generation.
     *
     * @var Block
     */
    protected $pageBlock;

    /**
     * Data schemas associated with the form.
     *
     * @var SchemaCollection
     */
    protected $schemas;

    /**
     * The segment name to drop from form names, when segment naming
     *  mode = auto.
     * @var string
     */
    protected $segmentNameDrop;

    /**
     * How segment naming is handled: 'on' = always generate segment name;
     * 'off' = Never generate segment name; 'auto' = suppress the segment listed
     * in $segmentNameDrop.
     *
     * @var string
     */
    protected $segmentNameMode = 'auto';

    public function __call($method, $args)
    {
        $getter = 'get' . Str::ucfirst($method);
        if (method_exists($this, $getter)) {
            echo $this->$getter($args[0] ?? null) ?? '';
        }
    }

    /**
     * Create a new NextForm.
     *
     * @param array|null $options See options()
     */
    public function __construct($options = [])
    {
        parent::__construct(self::$diWiringStatic);
        $this->useOptions($options);
        $this->schemas = new SchemaCollection();
        $this->show = '';
    }

    /**
     * Add form definitions to the form manager.
     *
     * @param Form|string|array $forms The name of a form file, a loaded Form,
     * or a list thereof. Options can be passed in my making the list element
     * an array of [Form|string, options].
     * @param array|null $options Form configuration options (single form case).
     *
     * @return $this
     */
    public function addForm($forms, $options = [])
    {
        if (!is_array($forms)) {
            $forms = [[$forms, $options]];
        }
        foreach ($forms as $form) {
            if (is_array($form)) {
                $form[] = [];
                [$form, $options] = $form;
            } else {
                $options = [];
            }
            if (is_string($form)) {
                //$form = $this->diWiring['Form']::fromFile($form);
                $form = $this->serviceClass('Form')::fromFile($form);
            }
            $formName = $form->getName();
            $this->boundForms[$formName] = LinkedForm::build($form, $options);
        }
        return $this;
    }

    /**
     * Add schema definitions to the form manager.
     *
     * @param Schema|string|array $schemas The name of a schema file, a
     *  loaded Schema, or a list thereof.
     *
     * @return $this
     */
    public function addSchema($schemas)
    {
        if (!is_array($schemas)) {
            $schemas = [$schemas];
        }
        foreach ($schemas as $schema) {
            if (is_string($schema)) {
                //$schema = $this->diWiring['Schema']::fromFile($schema);
                $schema = $this->serviceClass('Schema')::fromFile($schema);
            }
            $this->schemas->addSchema($schema);
            if (
                $this->segmentNameMode === 'auto'
                && $this->segmentNameDrop === null
            ) {
                $segments = $schema->getSegmentNames();
                if (!empty($segments)) {
                    $this->segmentNameDrop = $segments[0];
                }
            }
        }

        return $this;
    }

    /**
     * Create bindings for elements in each form and map to the schemas.
     *
     * @return $this
     */
    public function bind()
    {
        if (empty($this->boundForms)) {
            throw new \RuntimeException('No forms available.');
        }
        $this->objectMap = [];
        foreach ($this->boundForms as $boundForm) {
            $boundForm->bind($this);
        }

        return $this;
    }

    /**
     * Reset the static context.
     */
    public static function boot()
    {
        self::$htmlId = 0;
        self::$csrfToken = null;
    }

    /**
     * Connect a binding to the schema and add to the object map.
     *
     * @param Binding $binding
     *
     * @return $this
     */
    public function connectBinding(Binding $binding)
    {
        $objectRef = $binding->bindSchema($this->schemas);
        if ($objectRef !== null) {
            if (!isset($this->objectMap[$objectRef])) {
                $this->objectMap[$objectRef] = [];
            }
            $this->objectMap[$objectRef][] = $binding;
        }
        return $this;
    }

    /**
     * Set a custom CSRF token generator.
     *
     * @param callable $gen Must return an array of [token name, token value].
     */
    public static function csrfGenerator(callable $gen)
    {
        self::$csrfGenerator = $gen;
        self::$csrfToken = null;
    }

    /**
     * Generate the forms.
     *
     * @param Form|string|null $oneForm A form to generate.
     * @param array|null $options Optional form settings: role
     *
     * @return Block
     */
    public function generate($oneForm = null, $options = []) : Block
    {
        if ($oneForm !== null) {
            $this->addForm($oneForm, $options);
        }
        $this->bind($this);

        $this->populateForms();

        $this->singleton('Render', $options)->show()->set($this->show);

        $this->pageBlock = new Block();
        foreach ($this->boundForms as $boundForm) {
            $boundForm->useOptions($options);
            $formBlock = $boundForm->generate();
            $this->pageBlock->merge($formBlock);
        }
        return $this->pageBlock;
    }

    /**
     * Generate a new CSRF token.
     *
     * @return array [token name, token value]
     */
    public static function generateCsrfToken()
    {
        if (is_callable(self::$csrfGenerator)) {
            self::$csrfToken = call_user_func(self::$csrfGenerator);
        } else {
            self::generateNfToken();
        }
        return self::$csrfToken;
    }

    /**
     * Native random token generator.
     *
     * @return array ['_nf_token', random token value]
     */
    protected static function generateNfToken()
    {
        self::$csrfToken = ['_nf_token', \bin2hex(random_bytes(32))];
        return self::$csrfToken;
    }

    /**
     * Get the current CSRF token.
     *
     * @return array [token name, token value]
     */
    public static function getCsrfToken()
    {
        if (self::$csrfToken === null) {
            self::generateCsrfToken();
        }
        return self::$csrfToken;
    }

    /**
     * Get the page data, optionally for a specific form.
     *
     * @param string|null $formName
     * @return ?Block
     */
    public function getBlock(?string $formName = null) : ?Block
    {
        if ($formName !== null) {
            $form = $this->getLinkedForm($formName);
            $block = $form ? $form->getBlock() : null;
        } else {
            $block = $this->pageBlock;
        }
        return $block;
    }

    /**
     * Get the rendered body of all forms or a single form.
     *
     * @param string|null $formName
     * @return ?string
     */
    public function getBody(?string $formName = null)
    {
        $block = $this->getBlock($formName);
        return $block ? $block->getBody() : null;
    }

    /**
     * Get context data for all forms or a single form.
     *
     * @param string|null $formName
     * @return ?array
     */
    public function getContext(?string $formName = null)
    {
        $block = $this->getBlock($formName);
        return $block ? $block->getContext() : null;
    }

    /**
     * Get all the data objects from the forms.
     * @return Binding[] Data bindings indexed by object name
     */
    public function getData()
    {
        $data = [];
        // The first element should have the value... there should only be one value.
        foreach ($this->objectMap as $objectName => $list) {
            $data[$objectName] = $list[0]->getValue();
        }
        return $data;
    }

    /**
     * Retrieve a form via a linked form.
     *
     * @param string|null $formName
     * @return ?LinkedForm
     */
    public function getForm(?string $formName) : ?Form
    {
        return $this->boundForms[$formName]->getForm() ?? null;
    }

    /**
     * Get the segmented form errors
     *
     * @return array
     */
    public function getFormErrors()
    {
        return $this->formErrors;
    }

    /**
     * Get the rendered head section of all forms or a single form.
     *
     * @param string|null $formName
     * @return ?string
     */
    public function getHead(?string $formName = null)
    {
        $block = $this->getBlock($formName);
        return $block ? $block->getHead() : null;
    }

    /**
     * Retrieve a linked form by name.
     *
     * @param string|null $formName
     * @return ?LinkedForm
     */
    public function getLinkedForm(?string $formName) : ?LinkedForm
    {
        return $this->boundForms[$formName] ?? null;
    }

    /**
     * Get the file links for all forms or a single form.
     *
     * @param string|null $formName
     * @return ?string
     */
    public function getLinks(?string $formName = null)
    {
        $block = $this->getBlock($formName);
        return $block ? \implode("\n", $block->getLinkedFiles()) : null;
    }

    /**
     * Get the script for all forms or a single form.
     *
     * @param string|null $formName
     * @return ?string
     */
    public function getScript(?string $formName = null)
    {
        $block = $this->getBlock($formName);
        return $block ? $block->getScript() : null;
    }

    /**
     * Get the script files for all forms or a single form.
     *
     * @param string|null $formName
     * @return ?string
     */
    public function getScriptFiles(?string $formName = null)
    {
        $block = $this->getBlock($formName);
        return $block ? implode("\n", $block->getScriptFiles()) : null;
    }

    /**
     * Retrieve a segment from the loaded schemas.
     *
     * @param string|null $segmentName
     * @return ?Segment
     */
    public function getSegment(?string $segmentName) : ?Segment
    {
        return $this->schemas->getSegment($segmentName);
    }

    /**
     * Get all the data objects in the specified segment from the form.
     * @param string $segment
     * @return array Data bindings indexed by object name
     */
    public function getSegmentData(string $segment)
    {
        $prefix = $segment . NextForm::SEGMENT_DELIM;
        $prefixLen = \strlen($segment . NextForm::SEGMENT_DELIM);
        $data = [];
        // The first element should have the value... there should only be one value.
        foreach ($this->objectMap as $objectName => $list) {
            if (\substr($objectName, 0, $prefixLen) === $prefix) {
                $data[substr($objectName, $prefixLen)] = $list[0]->getValue();
            }
        }
        return $data;
    }

    /**
     * Get the name of a segment that bound forms can drop when generating
     * form names.
     *
     * @return string|null
     */
    public function getSegmentNameDrop()
    {
        return $this->segmentNameMode !== 'off' ? $this->segmentNameDrop : null;
    }

    /**
     * Get the inline styles for all forms or a single form.
     *
     * @param string|null $formName
     *
     * @return string|null
     */
    public function getStyles(?string $formName = null)
    {
        $block = $this->getBlock($formName);
        return $block ? $block->getStyles() : null;
    }

    /**
     * Turn a string into a valid HTML identifier, or make one up
     *
     * @param string|null $name
     * @param bool|null $appendId If true, append the next from ID.
     *
     * @return string
     */
    public static function htmlIdentifier(
        ?string $name = '',
        ?bool $appendId = false
    ) {
        if ($name === '') {
            $name = 'nf_' . ++self::$htmlId;
        } else {
            if ($appendId) {
                $name .= '_' . ++self::$htmlId;
            }
            // Turn anything risky into a dash
            // @todo escape the set of valid but odd characters.
            $name = \preg_replace('/[^a-z0-9\_]/i', '_', $name);
            // If the first character isn't alpha, prefix with nf-
            $name = \preg_replace('/^[^a-z]/i', 'nf_\1', $name);
        }
        return $name;
    }

    /**
     * Standardize a field name, dropping the default segment.
     *
     * @param string $segment
     * @param string $field
     * @return string
     */
    protected function normalizeField(string $segment, string $field)
    {
        if ($segment === '') {
            if (
                !isset($this->objectMap[$field])
                && $this->segmentNameDrop !== null
            ) {
                $field = $this->segmentNameDrop
                    . NextForm::SEGMENT_DELIM . $field;
            }
        } else {
            $field = $segment . NextForm::SEGMENT_DELIM . $field;
        }
        return $field;
    }

    /**
     * Set values for form data.
     *
     * @param array $data Values indexed by name ([segment/]field).
     * @param string|null $segment Segment name prefix.
     *
     * @return $this
     */
    public function populate($data, ?string $segment = '')
    {
        $this->formData[$segment] = $data;
        return $this;
    }

    /**
     * Set validation errors for form data.
     *
     * @param array|string $bySegment If an array, messages indexed by
     * segment, field.
     * @param string|null $fields Messages indexed by field name
     *
     * @return $this
     */
    public function populateErrors($bySegment, $fields = null)
    {
        if (is_string($bySegment)) {
            $bySegment = [$bySegment => $fields];
        }
        $this->formErrors = array_merge_recursive(
            $this->formErrors ?? [], $bySegment
        );
        return $this;
    }

    /**
     * Populate form bindings.
     *
     * @return $this
     */
    protected function populateForms()
    {
        $this->populateFormData();
        $this->populateFormValidity($this->formErrors, false);
    }

    /**
     * Populate form bindings with data.
     *
     * @return $this
     */
    protected function populateFormData()
    {
        foreach ($this->formData as $segment => $data) {
            foreach ($data as $field => $value) {
                $field = $this->normalizeField($segment, $field);
                if (!isset($this->objectMap[$field])) {
                    continue;
                }
                foreach ($this->objectMap[$field] as $binding) {
                    $binding->value($value);
                }
            }
        }
        return $this;
    }

    /**
     * Populate form bindings with validation results.
     *
     * @return $this
     */
    protected function populateFormValidity($list, $valid)
    {
        if ($list === null) {
            return $this;
        }
        $label = $valid ? 'accept' : 'error';
        foreach ($list as $segment => $data) {
            foreach ($data as $field => $text) {
                $field = $this->normalizeField($segment, $field);
                if (!isset($this->objectMap[$field])) {
                    continue;
                }
                foreach ($this->objectMap[$field] as $binding) {
                    $binding->label($label, $text);
                    $binding->valid($valid);
                }
            }
        }
        return $this;
    }

    /**
     * Map request variables back into segment/property names
     *
     * @param array $requestVars Post variables indexed as [segment_property]
     *
     * @return array Valid Post variables indexed as [segment.property]
     */
    public function request($requestVars, $options = []) {
        $bySegment = $options['segment'] ?? false;
        $mapped = [];
        foreach ($this->schemas as $segName => $schema) {
            foreach($schema->getSegment($segName) as $property) {
                $propName = $property->getName();
                $formName = $segName . '_' . $propName;
                if (isset($requestVars[$formName])) {
                    if ($bySegment) {
                        $mapped[$segName] = $mapped[$segName] ?? [];
                        $mapped[$segName][$propName] = $requestVars[$formName];
                    } else {
                        $mapped[$segName . self::SEGMENT_DELIM . $propName]
                            = $requestVars[$formName];
                    }
                }
            }
        }

        return $mapped;
    }

    /**
     * Reset everything except static bindings.
     *
     * $param array|null $options New settings to apply after the reset
     * @return $this
     */
    public function reset($options = null) : NextForm
    {
        self::$jsonCompact = false;
        self::$htmlId = 0;
        $this->boundForms = [];
        $this->formBlock = [];
        $this->formData = [];
        $this->formErrors = null;
        $this->nameMap = null;
        $this->objectMap = null;
        $this->pageBlock = null;
        $this->schemas = new SchemaCollection();
        $this->segmentNameDrop = null;
        $this->segmentNameMode = 'auto';
        $this->show = '';
        $this->useOptions($options);

        return $this;
    }

    /**
     * JSON encode an object, optionally with compact notation.
     *
     * @param mixed $object An object to be JSON encoded.
     * @param array $options flags: [flags] (int) passed to json_encode;
     * [compact] (bool) use compact notation, default false/off.
     * @return type
     */
    public static function toJson($object, $options = [])
    {
        self::$jsonCompact = $options['compact'] ?? false;
        $json = \json_encode($object, $options['flags'] ?? 0);
        self::$jsonCompact = false;

        return $json;
    }

    /**
     * Specify the options to be used when generating.
     *
     * @param array|null $options
     */
    public function useOptions($options = [])
    {
        if (
            isset($options['segmentNameMode'])
            && in_array($options['segmentNameMode'], ['auto', 'off', 'on'])
        ) {
            $this->segmentNameMode = $options['segmentNameMode'];
        }
        if (isset($options['segmentNameDrop'])) {
            $this->segmentNameDrop = $options['segmentNameDrop'];
        }
        $this->wire($options['wire'] ?? []);
    }

    /**
     * Define the current user for access control.
     *
     * @deprecated since version 1.2.0 Will be removed in 2.0
     *
     * @param string|int $user
     *
     * @return $this
     */
    public function user($user)
    {
        $this->services('Access')->user($user);
        return $this;
    }

    /**
     * Set DI wiring for this instance.
     *
     * @param array $services Array of [service => className].
     *
     * @return $this
     *
     * @throws RuntimeException
     */
    public function wire($services)
    {
        foreach (array_keys($services) as $service) {
            self::wireCheck($service);
        }
        parent::wire($services);
        return $this;
    }

    /**
     * Check a DI wiring service name.
     *
     * @param string $service A service name.
     *
     * @throws RuntimeException
     */
    protected static function wireCheck(string $service)
    {
        if (!array_key_exists($service, self::$diWiringStatic)) {
            throw new \RuntimeException(
                "Service {$service} must be one of "
                . implode(', ', array_keys(self::$diWiringStatic))
            );
        }
    }

    /**
     * Set DI wiring for the static class.
     *
     * @param array $services Array of [service => handler].
     *
     * @throws RuntimeException
     */
    public static function wireStatic($services)
    {
        foreach ($services as $service => $handler) {
            self::wireCheck($service);
            self::$diWiringStatic[$service] = $handler;
        }
    }

}
