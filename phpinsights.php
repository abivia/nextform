<?php

declare(strict_types=1);

return [

    /*
    |--------------------------------------------------------------------------
    | Default Preset
    |--------------------------------------------------------------------------
    |
    | This option controls the default preset that will be used by PHP Insights
    | to make your code reliable, simple, and clean. However, you can always
    | adjust the `Metrics` and `Insights` below in this configuration file.
    |
    | Supported: "default", "laravel", "symfony", "magento2", "drupal"
    |
    */

    'preset' => 'default',

    /*
    |--------------------------------------------------------------------------
    | IDE
    |--------------------------------------------------------------------------
    |
    | This options allow to add hyperlinks in your terminal to quickly open
    | files in your favorite IDE while browsing your PhpInsights report.
    |
    | Supported: "textmate", "macvim", "emacs", "sublime", "phpstorm",
    | "atom", "vscode".
    |
    | If you have another IDE that is not in this list but which provide an
    | url-handler, you could fill this config with a pattern like this:
    |
    | myide://open?url=file://%f&line=%l
    |
    */

    'ide' => null,

    /*
    |--------------------------------------------------------------------------
    | Configuration
    |--------------------------------------------------------------------------
    |
    | Here you may adjust all the various `Insights` that will be used by PHP
    | Insights. You can either add, remove or configure `Insights`. Keep in
    | mind, that all added `Insights` must belong to a specific `Metric`.
    |
    */

    'exclude' => [
        //  'path/to/directory-or-file'
        'doc/', 'example/', 'local/'
    ],

    'add' => [
        //  ExampleMetric::class => [
        //      ExampleInsight::class,
        //  ]
    ],

    'remove' => [
        // "Normal classes are forbidden" WTF? Opinionated is right.
        NunoMaduro\PhpInsights\Domain\Insights\ForbiddenNormalClasses::class,

        // Yeah, well, in the absence of multiple inheritance...
        NunoMaduro\PhpInsights\Domain\Insights\ForbiddenTraits::class,

        // Many classes are POPOs: plain old PHP objects where we just need
        // to set stuff, other times we're setting a behaviour.
        NunoMaduro\PhpInsights\Domain\Sniffs\ForbiddenSetterSniff::class,

        // Yeah unless your code is formatted so it can be read by a human. Duh.
        PHP_CodeSniffer\Standards\Generic\Sniffs\Strings\UnnecessaryStringConcatSniff::class,

        // This thing is generating null patches. Check after updates.
        PhpCsFixer\Fixer\Phpdoc\AlignMultilineCommentFixer::class,

        // Should be checking for the opposite!
        PhpCsFixer\Fixer\Whitespace\SingleBlankLineAtEofFixer::class,

        // Superfluous this.
        SlevomatCodingStandard\Sniffs\Classes\SuperfluousInterfaceNamingSniff::class,
        SlevomatCodingStandard\Sniffs\Classes\SuperfluousAbstractClassNamingSniff::class,

        // Fails to account for explictly enclised assignments:
        //  if (($x = fn()) === 'ok')
        SlevomatCodingStandard\Sniffs\ControlStructures\AssignmentInConditionSniff::class,

        // No thanks, it's great for arrays
        SlevomatCodingStandard\Sniffs\ControlStructures\DisallowEmptySniff::class,

        // Or you could just look it up like any other operator.
        SlevomatCodingStandard\Sniffs\ControlStructures\DisallowShortTernaryOperatorSniff::class,

        // If you don't know how the operator works, look it up.
        SlevomatCodingStandard\Sniffs\Operators\RequireOnlyStandaloneIncrementAndDecrementOperatorsSniff::class,

        // Flags cases where parentheses improve readability
        SlevomatCodingStandard\Sniffs\PHP\UselessParenthesesSniff::class,

        // Utter nonsense!
        SlevomatCodingStandard\Sniffs\TypeHints\DisallowArrayTypeHintSyntaxSniff::class,

        // Disagree.
        SlevomatCodingStandard\Sniffs\TypeHints\ReturnTypeHintSpacingSniff::class,

        // Too many false positives. Besides, sticking stuff into a variable
        // that immediately gets returned eases debugging and isn't that
        // expensive.
        SlevomatCodingStandard\Sniffs\Variables\UselessVariableSniff::class,
    ],

    'config' => [
        //  ExampleInsight::class => [
        //      'key' => 'value',
        //  ],

        // Get rid of this thing where crap is so short for no damn reason that
        // you can't figure out how the hell it works.
        ObjectCalisthenics\Sniffs\Metrics\MaxNestingLevelSniff::class => [
            'maxNestingLevel' => 3
        ],
        ObjectCalisthenics\Sniffs\Files\ClassTraitAndInterfaceLengthSniff::class => [
            'maxLength' => 500
        ],
        ObjectCalisthenics\Sniffs\Files\FunctionLengthSniff::class => [
            'maxLength' => 100
        ],
        ObjectCalisthenics\Sniffs\Metrics\PropertyPerClassLimitSniff::class => [
            'maxCount' => 20
        ],
        ObjectCalisthenics\Sniffs\Metrics\MethodPerClassLimitSniff::class => [
            'maxCount' => 50
        ],

        // Nope, no special case for the last method, sorry. Special cases suck.
        PhpCsFixer\Fixer\Basic\BracesFixer::class => [],



        // Simple alpha sort. Who the hell groups methods by type?
        PhpCsFixer\Fixer\ClassNotation\OrderedClassElementsFixer::class => [
            'order' => [
                'use_trait', 'constant', 'property', 'magic', 'phpunit',
                'method'
            ]
        ],

    ],

    /*
    |--------------------------------------------------------------------------
    | Requirements
    |--------------------------------------------------------------------------
    |
    | Here you may define a level you want to reach per `Insights` category.
    | When a score is lower than the minimum level defined, then an error
    | code will be returned. This is optional and individually defined.
    |
    */

    'requirements' => [
//        'min-quality' => 0,
//        'min-complexity' => 0,
//        'min-architecture' => 0,
//        'min-style' => 0,
//        'disable-security-check' => false,
    ],

];
